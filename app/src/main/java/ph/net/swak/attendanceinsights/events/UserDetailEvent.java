package ph.net.swak.attendanceinsights.events;

/**
 * Created by Knowell on 2/9/2017.
 */

public class UserDetailEvent {

    public final String empId;

    public UserDetailEvent(String empId) {
        this.empId = empId;
    }
}
