package ph.net.swak.attendanceinsights.ui.fragments.lifecycle;


import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.utils.ColorTemplate;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.listeners.ItemFilterListener;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.di.Injectable;
import ph.net.swak.attendanceinsights.repository.Status;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.utils.Helper;
import ph.net.swak.attendanceinsights.utils.PayrollPeriodType;
import ph.net.swak.attendanceinsights.viewmodel.PayrollDetailViewModel;

public class PayrollDetailsManagerFragment extends LifecycleFragment implements Injectable {

    @BindView(R.id.textview_payroll_details_manager_comp)
    protected TextView tvComp;

    @BindView(R.id.textview_payroll_details_manager_count)
    protected TextView tvCount;

    @BindView(R.id.textview_payroll_details_manager_scope)
    protected TextView tvScope;

    @BindView(R.id.textview_payroll_detail_manager_none)
    protected TextView tvNone;

    @BindView(R.id.recyclerview_payroll_detail_manager)
    protected RecyclerView recyclerView;

    @BindView(R.id.swipelayout_payroll_detail_manager)
    protected SwipeRefreshLayout swipeRefreshLayout;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private MainActivity mainActivity;
    private PayrollDetailViewModel viewModel;
    private Unbinder unbinder;
    private FastItemAdapter<PayrollDetailFastAdapter> mFastItemAdapter;

    public PayrollDetailsManagerFragment() {}

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(PayrollDetailViewModel.class);

        Bundle bundle = getArguments();

        if(bundle != null && !bundle.isEmpty()) {
            int payrollPeriod = bundle.getInt("payrollPeriod", 0);
            int payrollYear = bundle.getInt("covYear", 0);
            int payrollMonth = bundle.getInt("covMonth", 0);

            StringBuilder sb = new StringBuilder();

            if(payrollPeriod == PayrollPeriodType.MONTHLY.getOrder()) {
                sb.append(Helper.toStringMonth(payrollMonth-1));
                sb.append(" ");
                sb.append(payrollYear);
            } else if(payrollPeriod == PayrollPeriodType.QUARTERLY.getOrder()) {
                sb.append(Helper.toOrdinal(payrollMonth));
                sb.append(" Quarter ");
                sb.append(payrollYear);
            }

            tvComp.setText(Helper.toTitleCase(bundle.getString("compName", "")));
            tvScope.setText(sb.toString());
            viewModel.fetch(payrollYear, payrollMonth, bundle.getInt("compId"), 3, payrollPeriod);
        }

        viewModel.getPayrollDetail().observe(this, listResource -> {
            if (listResource != null && listResource.data != null) {
                if (listResource.status == Status.SUCCESS) {
                    tvCount.setText(String.format(Locale.ENGLISH, "%d Employees", listResource.data.size()));

                    if (listResource.data.isEmpty())
                        tvNone.setVisibility(View.VISIBLE);
                    else
                        tvNone.setVisibility(View.GONE);

                    mFastItemAdapter.setNewList(listResource.data);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payroll_details_manager, container, false);
        unbinder = ButterKnife.bind(this, view);
        mainActivity = (MainActivity) getActivity();
        setHasOptionsMenu(true);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);

        mFastItemAdapter = new FastItemAdapter<>();
        recyclerView.setAdapter(mFastItemAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(dividerItemDecoration);

        mFastItemAdapter.getItemFilter().withFilterPredicate((item, constraint) -> !item.getfName().toLowerCase().contains(constraint.toString().toLowerCase())
                && !item.getlName().toLowerCase().contains(constraint.toString().toLowerCase()));

        mFastItemAdapter.getItemFilter().withItemFilterListener(new ItemFilterListener<PayrollDetailFastAdapter>() {
            @Override
            public void itemsFiltered(@Nullable CharSequence constraint, @Nullable List<PayrollDetailFastAdapter> list) {
                if (list != null) {
                    if (list.isEmpty()) {
                        tvNone.setVisibility(View.VISIBLE);
                    } else {
                        tvNone.setVisibility(View.GONE);
                    }
                } else {
                    tvNone.setVisibility(View.GONE);
                }
            }

            @Override
            public void onReset() {

            }
        });

        swipeRefreshLayout.setEnabled(false);
        swipeRefreshLayout.setColorSchemeColors(ColorTemplate.MATERIAL_COLORS);
        swipeRefreshLayout.setOnRefreshListener(() -> (new Handler()).postDelayed(() -> {
            swipeRefreshLayout.setRefreshing(false);
        }, 3000));

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);MenuItem searchItem = menu.findItem(R.id.app_bar_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        SearchView.SearchAutoComplete searchText = (SearchView.SearchAutoComplete) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchText.setHint("Search Employee");
        searchText.setHintTextColor(getResources().getColor(R.color.md_grey_300));
        searchText.setTextColor(getResources().getColor(R.color.md_white_1000));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mFastItemAdapter.filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mFastItemAdapter.filter(newText);
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.app_bar_search:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

}
