package ph.net.swak.attendanceinsights.models;

import android.app.Dialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;
import java.util.TreeMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.net.swak.attendanceinsights.R;

public class SimpleList extends AbstractItem<SimpleList, SimpleList.ViewHolder> {

    private String label;
    private String from;
    private String info;
    private String paycode;
    private String empid;
    private Dialog dialog;
    private double gross;

    public TreeMap<String, String> treeMap = new TreeMap<>();

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getPaycode() {
        return paycode;
    }

    public void setPaycode(String paycode) {
        this.paycode = paycode;
    }

    public String getEmpid() {
        return empid;
    }

    public void setEmpid(String empid) {
        this.empid = empid;
    }

    public Dialog getDialog() {
        return dialog;
    }

    public void setDialog(Dialog dialog) {
        this.dialog = dialog;
    }

    public double getGross() {
        return gross;
    }

    public SimpleList withGross(double gross) {
        this.gross = gross;
        return this;
    }

    @Override
    public int getType() {
        return R.id.fastadapter_hris_simple_list_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.hris_simple_list;
    }

    @Override
    public void bindView(final ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);
        holder.label.setText(getLabel());
        holder.info.setText(getInfo());
        /*
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getFrom() == "payrolldetail_employee") {
                    getDialog().dismiss();
                    EventBus.getDefault().post(new UserDetailEvent(getEmpid()));
                }
            }
        });*/
    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);
        holder.label.setText(null);
        holder.info.setText(null);
        //holder.itemView.setOnClickListener(null);
    }

    @Override
    public ViewHolder getViewHolder(View view) {
        return new ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.simple_list_label)
        TextView label;

        @BindView(R.id.simple_list_info)
        TextView info;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
