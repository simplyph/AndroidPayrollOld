package ph.net.swak.attendanceinsights.models;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.utils.Helper;

public class BottomSheetEmp extends AbstractItem<BottomSheetEmp, BottomSheetEmp.ViewHolder> {

    private String name;
    private int empId;
    private String company;
    private String designation;
    private boolean hasAge = false;
    private boolean hasDate = false;
    private int age;
    private String date;
    private String dateLabel;

    public String getName() {
        return name;
    }

    public BottomSheetEmp withName(String name) {
        this.name = name;
        return this;
    }

    public int getEmpId() {
        return empId;
    }

    public BottomSheetEmp withEmpId(int empId) {
        this.empId = empId;
        return this;
    }

    public String getCompany() {
        return company;
    }

    public BottomSheetEmp withCompany(String company) {
        this.company = company;
        return this;
    }

    public String getDesignation() {
        return designation;
    }

    public BottomSheetEmp withDesignation(String designation) {
        this.designation = designation;
        return this;
    }

    public boolean isHasAge() {
        return hasAge;
    }

    public BottomSheetEmp withHasAge(boolean hasAge) {
        this.hasAge = hasAge;
        return this;
    }

    public int getAge() {
        return age;
    }

    public BottomSheetEmp withAge(int age) {
        this.age = age;
        return this;
    }

    public boolean isHasDate() {
        return hasDate;
    }

    public BottomSheetEmp withHasDate(boolean hasDate) {
        this.hasDate = hasDate;
        return this;
    }

    public String getDate() {
        return date;
    }

    public BottomSheetEmp withDate(String date) {
        this.date = date;
        return this;
    }

    public String getDateLabel() {
        return dateLabel;
    }

    public BottomSheetEmp withDateLabel(String dateLabel) {
        this.dateLabel = dateLabel;
        return this;
    }

    @Override
    public int getType() {
        return R.id.fastadapter_bottom_sheet_emp_item_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.list_bottom_sheet_emp;
    }

    @Override
    public void bindView(BottomSheetEmp.ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);
        holder.textName.setText(Helper.toTitleCase(getName()));
        holder.textComp.setText(Helper.toTitleCase(getCompany()));
        holder.textDesig.setText(Helper.toTitleCase(getDesignation()));

        if(isHasAge()) {
            holder.textAge.setVisibility(View.VISIBLE);
            holder.textDate.setVisibility(View.GONE);

            holder.textAge.setText(String.valueOf(getAge()) + " y/o");
        } else if(isHasDate()) {
            holder.textAge.setVisibility(View.VISIBLE);
            holder.textDate.setVisibility(View.VISIBLE);

            holder.textAge.setText(getDate());
            holder.textDate.setText(getDateLabel());
        } else {
            holder.textAge.setVisibility(View.GONE);
            holder.textDate.setVisibility(View.GONE);
        }
    }

    @Override
    public void unbindView(BottomSheetEmp.ViewHolder holder) {
        super.unbindView(holder);
        holder.textName.setText(null);
        holder.textComp.setText(null);
        holder.textDesig.setText(null);
        holder.textAge.setText(null);
        holder.textDate.setText(null);
    }

    @Override
    public ViewHolder getViewHolder(View view) {
        return new ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_name)
        protected TextView textName;

        @BindView(R.id.text_company)
        protected TextView textComp;

        @BindView(R.id.text_designation)
        protected TextView textDesig;

        @BindView(R.id.text_age)
        protected TextView textAge;

        @BindView(R.id.text_date)
        protected TextView textDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
