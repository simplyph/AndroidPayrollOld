package ph.net.swak.attendanceinsights.db.realm;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class RealmHrisQuick implements RealmModel {

    @PrimaryKey
    @SerializedName("comp_id")
    private int compId = -1;
    @SerializedName("comp_name")
    private String compName;
    @SerializedName("employee_qty")
    private int qtyEmployee;
    @SerializedName("manager_qty")
    private int qtyManager;
    @SerializedName("supervisor_qty")
    private int qtySupervisor;
    @SerializedName("staff_qty")
    private int qtyStaff;

    public int getCompId() {
        return compId;
    }

    public RealmHrisQuick withCompId(int compId) {
        this.compId = compId;
        return this;
    }

    public String getCompName() {
        return compName;
    }

    public RealmHrisQuick withCompName(String compName) {
        this.compName = compName;
        return this;
    }

    public int getQtyEmployee() {
        return qtyEmployee;
    }

    public RealmHrisQuick withQtyEmployee(int qtyEmployee) {
        this.qtyEmployee = qtyEmployee;
        return this;
    }

    public int getQtyManager() {
        return qtyManager;
    }

    public RealmHrisQuick withQtyManager(int qtyManager) {
        this.qtyManager = qtyManager;
        return this;
    }

    public int getQtySupervisor() {
        return qtySupervisor;
    }

    public RealmHrisQuick withQtySupervisor(int qtySupervisor) {
        this.qtySupervisor = qtySupervisor;
        return this;
    }

    public int getQtyStaff() {
        return qtyStaff;
    }

    public RealmHrisQuick withQtyStaff(int qtyStaff) {
        this.qtyStaff = qtyStaff;
        return this;
    }

    public RealmHrisQuick() {}
}
