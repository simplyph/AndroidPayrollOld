package ph.net.swak.attendanceinsights.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.HomeFragment;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.HrisDashboardAllFragment;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.HrisDashboardDepartmentFragment;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.HrisDashboardDivisionFragment;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.HrisDashboardFragment;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.PayrollDashboardFragment;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.PayrollDetailsManagerFragment;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.PayrollDetailsStaffFragment;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.PayrollDetailsSupervisorFragment;
import ph.net.swak.attendanceinsights.ui.views.bottomsheet.PayrollEmployeeBottomSheet;
import ph.net.swak.attendanceinsights.ui.views.bottomsheet.PayrollMonthBottomSheet;

@Module
abstract class MainFragmentModule {

    @ContributesAndroidInjector
    abstract HomeFragment contributesHomeFragment();

    @ContributesAndroidInjector
    abstract HrisDashboardAllFragment contributesHrisDashboardAllFragment();

    @ContributesAndroidInjector
    abstract HrisDashboardDepartmentFragment contributesHrisDashboardDepartmentFragment();

    @ContributesAndroidInjector
    abstract HrisDashboardDivisionFragment contributesHrisDashboardDivisionFragment();

    @ContributesAndroidInjector
    abstract HrisDashboardFragment contributesHrisDashboardFragment();

    @ContributesAndroidInjector
    abstract PayrollDashboardFragment contributesPayrollDashboardFragment();

    @ContributesAndroidInjector
    abstract PayrollDetailsManagerFragment contributesPayrollDetailsManagerFragment();

    @ContributesAndroidInjector
    abstract PayrollDetailsStaffFragment contributesPayrollDetailsStaffFragment();

    @ContributesAndroidInjector
    abstract PayrollDetailsSupervisorFragment contributesPayrollDetailsSupervisorFragment();

    @ContributesAndroidInjector
    abstract PayrollMonthBottomSheet contributesPayrollMonthBottomSheet();

    @ContributesAndroidInjector
    abstract PayrollEmployeeBottomSheet contributesPayrollPayrollEmployeeBottomSheet();

}
