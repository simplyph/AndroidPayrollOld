package ph.net.swak.attendanceinsights.di.module;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import ph.net.swak.attendanceinsights.di.annotations.ViewModelKey;
import ph.net.swak.attendanceinsights.viewmodel.HomeViewModel;
import ph.net.swak.attendanceinsights.viewmodel.HrisDashboardCompanyViewModel;
import ph.net.swak.attendanceinsights.viewmodel.HrisDashboardViewModel;
import ph.net.swak.attendanceinsights.viewmodel.HrisEmployeeBottomSheetViewModel;
import ph.net.swak.attendanceinsights.viewmodel.PayrollAnalyticsViewModelFactory;
import ph.net.swak.attendanceinsights.viewmodel.PayrollDashboardViewModel;
import ph.net.swak.attendanceinsights.viewmodel.PayrollDetailViewModel;
import ph.net.swak.attendanceinsights.viewmodel.PayrollEmployeeBottomSheetViewModel;
import ph.net.swak.attendanceinsights.viewmodel.PayrollMonthBottomSheetViewModel;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel.class)
    abstract ViewModel bindHomeViewModel(HomeViewModel homeViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(HrisDashboardCompanyViewModel.class)
    abstract ViewModel bindHrisDashboardCompanyViewModel(HrisDashboardCompanyViewModel hrisDashboardCompanyViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(HrisDashboardViewModel.class)
    abstract ViewModel bindHrisDashboardViewModel(HrisDashboardViewModel hrisDashboardViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(HrisEmployeeBottomSheetViewModel.class)
    abstract ViewModel bindHrisEmployeeBottomSheetViewModel(HrisEmployeeBottomSheetViewModel hrisEmployeeBottomSheetViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PayrollDashboardViewModel.class)
    abstract ViewModel bindPayrollDashboardViewModel(PayrollDashboardViewModel payrollDashboardViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PayrollDetailViewModel.class)
    abstract ViewModel bindPayrollDetailViewModel(PayrollDetailViewModel payrollDetailViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PayrollEmployeeBottomSheetViewModel.class)
    abstract ViewModel bindPayrollEmployeeBottomSheetViewModel(PayrollEmployeeBottomSheetViewModel payrollEmployeeBottomSheetViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PayrollMonthBottomSheetViewModel.class)
    abstract ViewModel bindPayrollMonthBottomSheetViewModel(PayrollMonthBottomSheetViewModel payrollMonthBottomSheetViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(PayrollAnalyticsViewModelFactory factory);

}
