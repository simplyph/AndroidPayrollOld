package ph.net.swak.attendanceinsights.ui.fragments.payroll;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmAsyncTask;
import io.realm.RealmResults;
import io.realm.Sort;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.models.RealmIndividualPayroll;
import ph.net.swak.attendanceinsights.models.SampleSubItem;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.utils.JSONParser;

/**
 * A simple {@link Fragment} subclass.
 */
public class IndividualPayrollFragment extends Fragment {

    @BindView(R.id.recyclerview_individual_payroll)
    protected RecyclerView recyclerView;

    @BindView(R.id.progressbar_individual_payroll)
    protected MaterialProgressBar progressBar;

    @BindView(R.id.progressbar_individual_payroll_text)
    protected TextView progressBarText;

    private Unbinder unbinder;
    private MainActivity mainActivity;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private Realm mRealm;
    private List<RealmAsyncTask> transactionList = new ArrayList<>();
    private FastItemAdapter<RealmIndividualPayroll> fastItemAdapter;
    private final String TAG = "IndividualPayrollFragment";

    public IndividualPayrollFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_individual_payroll, container, false);
        unbinder = ButterKnife.bind(this, view);
        requestQueue = VolleyAPI.getInstance(getActivity()).getRequestQueue();
        volleyRequest = new VolleyRequest();
        mainActivity = (MainActivity) getActivity();
        mRealm = Realm.getDefaultInstance();

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mainActivity, DividerItemDecoration.VERTICAL);

        fastItemAdapter = new FastItemAdapter<>();
        fastItemAdapter.withSelectable(true);
        fastItemAdapter.withPositionBasedStateManagement(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(mainActivity));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(fastItemAdapter);

        Bundle bundle = getArguments();

        if (bundle != null) {
            if (bundle.containsKey("empid")) {
                setupData(bundle.getString("empid"));
            } else {
                progressBar.setVisibility(View.GONE);
                progressBarText.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                progressBarText.setText("No data Found");
            }
        } else {
            progressBar.setVisibility(View.GONE);
            progressBarText.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            progressBarText.setText("No data Found");
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
        closeRealm();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(requestQueue != null)
            requestQueue.cancelAll(TAG);
    }

    public void closeRealm() {
        if (transactionList != null)
            for (RealmAsyncTask transaction : transactionList)
                if (transaction != null && !transaction.isCancelled())
                    transaction.cancel();

        if (!mRealm.isClosed())
            mRealm.close();
    }

    private void setupData(String empid) {
        progressBar.setVisibility(View.VISIBLE);
        progressBarText.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        progressBarText.setText("Loading data");

        JsonArrayRequest jsonArrayRequest = volleyRequest.getAuthJsonArray(mainActivity.getApplicationContext(), TAG, volleyRequest.getIndividualPayroll(empid), response -> {
            final List<RealmIndividualPayroll> realmIndividualPayrolls = JSONParser.parseIndividualPayroll(response);

            mRealm.executeTransaction(realm -> {
                realm.delete(RealmIndividualPayroll.class);

                for (RealmIndividualPayroll realmIndividualPayroll : realmIndividualPayrolls) {
                    realm.copyToRealm(realmIndividualPayroll);
                }
            });

            RealmResults<RealmIndividualPayroll> paycodes = mRealm.where(RealmIndividualPayroll.class).distinct("mPaycode").sort("mPaycode", Sort.DESCENDING);

            List<RealmIndividualPayroll> paycodeList = new ArrayList<>();

            for (RealmIndividualPayroll realmIndividualPayroll : paycodes) {

                List<SampleSubItem> iItems = new LinkedList<>();

                RealmResults<RealmIndividualPayroll> paycode_details =
                        mRealm.where(RealmIndividualPayroll.class)
                                .equalTo("mPaycode", realmIndividualPayroll.getPaycode()).findAllSorted("mIsIncome", Sort.DESCENDING);

                if (paycode_details.size() > 0) {

                    boolean headerSet = false;
                    boolean bodySet = false;
                    double netPay = 0;


                    for (RealmIndividualPayroll details : paycode_details) {
                        netPay = details.getNetpay();

                        if(details.getIsIncome() == 1) {
                            if(!headerSet) {
                                iItems.add(new SampleSubItem().withIsHeading(true)
                                        .withTitle("Gross")
                                        .withContent("Php " + String.valueOf(details.getGross())));
                                headerSet = true;
                            }
                            iItems.add(new SampleSubItem().withIsHeading(false)
                                    .withTitle(details.getAmountName())
                                    .withContent("Php " + String.valueOf(details.getAmount())));
                        } else {
                            if(!bodySet) {
                                iItems.add(new SampleSubItem().withIsHeading(true)
                                        .withTitle("Deduction")
                                        .withContent("Php " + String.valueOf(details.getDeduction())));
                                bodySet = true;
                            }

                            iItems.add(new SampleSubItem().withIsHeading(false)
                                    .withTitle(details.getAmountName())
                                    .withContent("Php " + String.valueOf(details.getAmount())));
                        }
                    }

                    iItems.add(new SampleSubItem().withIsHeading(true)
                            .withTitle("Net Pay")
                            .withContent("Php " + String.valueOf(netPay)));

                    realmIndividualPayroll.withSubItems(iItems);
                }

                paycodeList.add(realmIndividualPayroll);
            }

            if (paycodeList.size() < 1) {
                progressBar.setVisibility(View.GONE);
                progressBarText.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                progressBarText.setText("No data Found");
                return;
            }

            progressBar.setVisibility(View.GONE);
            progressBarText.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            fastItemAdapter.setNewList(paycodeList);
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                progressBarText.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                progressBarText.setText("No data Found");

            }
        });

        requestQueue.add(jsonArrayRequest);
    }
}
