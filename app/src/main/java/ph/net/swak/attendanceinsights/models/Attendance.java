package ph.net.swak.attendanceinsights.models;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.net.swak.attendanceinsights.R;

/**
 * Created by Knowell on 12/1/2016.
 */

public class Attendance extends AbstractItem<Attendance, Attendance.ViewHolder> {

    public String mName;
    public String empId;
    public String details;

    public void setName(String name) {
        this.mName = name;
    }

    public String getName() {
        return this.mName;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public int getType() {
        return R.id.fastadapter_attendance_insight_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.employee_list;
    }

    @Override
    public void bindView(final ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);
        String detail = "Absence: " + details + ((details.equals("0") || details.equals("1") || details.equals("0.5")) ? " day" : " days");
        String uri = "http://payrollstaging.swak.net.ph/Files/" + empId + "/profile/img" + empId + ".jpg";

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.ic_person_black_48dp);

        Glide.with(holder.imageView.getContext()).load(uri)
                .apply(options)
                .into(holder.imageView);

        holder.nameView.setText(mName);
        holder.idView.setText(empId);
        holder.detailsView.setText(detail);

    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);
        holder.nameView.setText(null);
        holder.idView.setText(null);
        holder.detailsView.setText(null);
        holder.imageView.setImageBitmap(null);
    }

    @Override
    public ViewHolder getViewHolder(View view) {
        return new ViewHolder(view);
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        protected TextView nameView;

        @BindView(R.id.tv_empid)
        protected TextView idView;

        @BindView(R.id.tv_details)
        protected TextView detailsView;

        @BindView(R.id.imageView)
        protected ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
