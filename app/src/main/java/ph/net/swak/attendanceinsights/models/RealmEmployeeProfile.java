package ph.net.swak.attendanceinsights.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RealmEmployeeProfile extends RealmObject {

    @PrimaryKey
    protected int emp_id;
    protected String fname;
    protected String lname;
    protected String position;
    protected int status_id;
    protected String status_type;
    protected String status;
    protected String hired;
    protected String terminated;
    protected String regularization;
    protected int comp_id;
    protected String comp_name;
    protected int div_id;
    protected String div_name;
    protected int dept_id;
    protected String dept_name;
    protected int func_id;
    protected String func_name;
    protected int wbase_id;
    protected String wbase_name;
    protected int desig_id;
    protected String desig_name;
    protected int religion_id;
    protected String religion_name;
    protected String birthdate;
    protected String civil_status;
    protected String gender;
    protected String img_path;

    public int getEmpId() {
        return emp_id;
    }

    public RealmEmployeeProfile withEmpId(int emp_id) {
        this.emp_id = emp_id;
        return this;
    }

    public String getFname() {
        return fname;
    }

    public RealmEmployeeProfile withFname(String fname) {
        this.fname = fname;
        return this;
    }

    public String getLname() {
        return lname;
    }

    public RealmEmployeeProfile withLname(String lname) {
        this.lname = lname;
        return this;
    }

    public String getPosition() {
        return position;
    }

    public RealmEmployeeProfile withPosition(String position) {
        this.position = position;
        return this;
    }

    public int getStatusId() {
        return status_id;
    }

    public RealmEmployeeProfile withStatusId(int status_id) {
        this.status_id = status_id;
        return this;
    }

    public String getStatusType() {
        return status_type;
    }

    public RealmEmployeeProfile withStatusType(String status_type) {
        this.status_type = status_type;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public RealmEmployeeProfile withStatus(String status) {
        this.status = status;
        return this;
    }

    public String getHired() {
        return hired;
    }

    public RealmEmployeeProfile withHired(String hired) {
        this.hired = hired;
        return this;
    }

    public String getTerminated() {
        return terminated;
    }

    public RealmEmployeeProfile withTerminated(String terminated) {
        this.terminated = terminated;
        return this;
    }

    public String getRegularization() {
        return regularization;
    }

    public RealmEmployeeProfile withRegularization(String regularization) {
        this.regularization = regularization;
        return this;
    }

    public int getCompId() {
        return comp_id;
    }

    public RealmEmployeeProfile withCompId(int comp_id) {
        this.comp_id = comp_id;
        return this;
    }

    public String getCompName() {
        return comp_name;
    }

    public RealmEmployeeProfile withCompName(String comp_name) {
        this.comp_name = comp_name;
        return this;
    }

    public int getDivId() {
        return div_id;
    }

    public RealmEmployeeProfile withDivId(int div_id) {
        this.div_id = div_id;
        return this;
    }

    public String getDivName() {
        return div_name;
    }

    public RealmEmployeeProfile withDivName(String div_name) {
        this.div_name = div_name;
        return this;
    }

    public int getDeptId() {
        return dept_id;
    }

    public RealmEmployeeProfile withDeptId(int dept_id) {
        this.dept_id = dept_id;
        return this;
    }

    public String getDeptName() {
        return dept_name;
    }

    public RealmEmployeeProfile withDeptName(String dept_name) {
        this.dept_name = dept_name;
        return this;
    }

    public int getFuncId() {
        return func_id;
    }

    public RealmEmployeeProfile withFuncId(int func_id) {
        this.func_id = func_id;
        return this;
    }

    public String getFuncName() {
        return func_name;
    }

    public RealmEmployeeProfile withFuncName(String func_name) {
        this.func_name = func_name;
        return this;
    }

    public int getWbaseId() {
        return wbase_id;
    }

    public RealmEmployeeProfile withWbaseId(int wbase_id) {
        this.wbase_id = wbase_id;
        return this;
    }

    public String getWbaseName() {
        return wbase_name;
    }

    public RealmEmployeeProfile withWbaseName(String wbase_name) {
        this.wbase_name = wbase_name;
        return this;
    }

    public int getDesigId() {
        return desig_id;
    }

    public RealmEmployeeProfile withDesigId(int desig_id) {
        this.desig_id = desig_id;
        return this;
    }

    public String getDesigName() {
        return desig_name;
    }

    public RealmEmployeeProfile withDesigName(String desig_name) {
        this.desig_name = desig_name;
        return this;
    }

    public int getReligion_id() {
        return religion_id;
    }

    public RealmEmployeeProfile withReligionId(int religion_id) {
        this.religion_id = religion_id;
        return this;
    }

    public String getReligionName() {
        return religion_name;
    }

    public RealmEmployeeProfile withReligionName(String religion_name) {
        this.religion_name = religion_name;
        return this;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public RealmEmployeeProfile withBirthdate(String birthdate) {
        this.birthdate = birthdate;
        return this;
    }

    public String getCivilStatus() {
        return civil_status;
    }

    public RealmEmployeeProfile withCivilStatus(String civil_status) {
        this.civil_status = civil_status;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public RealmEmployeeProfile withGender(String gender) {
        this.gender = gender;
        return this;
    }

    public String getImgPath() {
        return img_path;
    }

    public RealmEmployeeProfile withImgPath(String img_path) {
        this.img_path = img_path;
        return this;
    }
}
