package ph.net.swak.attendanceinsights.models;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.IClickable;
import com.mikepenz.fastadapter.IExpandable;
import com.mikepenz.fastadapter.IItem;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import ph.net.swak.attendanceinsights.R;

public class RealmIndividualPayroll extends RealmObject implements IItem<RealmIndividualPayroll, RealmIndividualPayroll.ViewHolder>, IExpandable<RealmIndividualPayroll, SampleSubItem>, IClickable<RealmIndividualPayroll> {


    // Fields
    protected String title;
    protected String empId;
    protected double gross;
    protected double deduction;
    protected double netpay;
    protected int mIsIncome;
    protected int mAmountId;
    protected String mAmountName;
    protected double mAmount;
    @Ignore
    protected boolean mExpanded = false;
    @Ignore
    protected List<SampleSubItem> mSubItems = new ArrayList<>();
    @Ignore
    protected Object mTag;
    @Ignore
    protected boolean mEnabled = true;
    @Ignore
    protected boolean mSelected = false;
    @Ignore
    protected boolean mSelectable = true;
    @PrimaryKey
    protected long mIdentifier = -1;
    @Index
    protected int mPaycode;
    @Ignore
    private FastAdapter.OnClickListener<RealmIndividualPayroll> mOnClickListener;

    public RealmIndividualPayroll withTitle(String title) {
        this.title = title;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public RealmIndividualPayroll withEmpId(String empId) {
        this.empId = empId;
        return this;
    }

    public String getEmpId() {
        return empId;
    }

    public RealmIndividualPayroll withGross(double gross) {
        this.gross = gross;
        return this;
    }

    public double getGross() {
        return gross;
    }

    public double getDeduction() {
        return deduction;
    }

    public RealmIndividualPayroll withDeduction(double deduction) {
        this.deduction = deduction;
        return this;
    }

    public double getNetpay() {
        return netpay;
    }

    public RealmIndividualPayroll withNetpay(double netpay) {
        this.netpay = netpay;
        return this;
    }

    @Override
    public RealmIndividualPayroll withIsExpanded(boolean expanded) {
        this.mExpanded = expanded;
        return this;
    }

    @Override
    public boolean isExpanded() {
        return mExpanded;
    }

    @Override
    public RealmIndividualPayroll withSubItems(List<SampleSubItem> subItems) {
        this.mSubItems = subItems;
        return this;
    }

    @Override
    public List<SampleSubItem> getSubItems() {
        return mSubItems;
    }

    @Override
    public boolean isAutoExpanding() {
        return true;
    }

    @Override
    public Object getTag() {
        return mTag;
    }

    @Override
    public RealmIndividualPayroll withTag(Object tag) {
        this.mTag = tag;
        return this;
    }

    @Override
    public boolean isEnabled() {
        return mEnabled;
    }

    @Override
    public RealmIndividualPayroll withEnabled(boolean enabled) {
        this.mEnabled = enabled;
        return this;
    }

    @Override
    public boolean isSelected() {
        return mSelected;
    }

    @Override
    public RealmIndividualPayroll withSetSelected(boolean selected) {
        this.mSelected = selected;
        return this;
    }

    @Override
    public boolean isSelectable() {
        return getSubItems() != null && getSubItems().size() > 0;
    }

    @Override
    public RealmIndividualPayroll withSelectable(boolean selectable) {
        this.mSelectable = selectable;
        return this;
    }

    @Override
    public RealmIndividualPayroll withIdentifier(long identifier) {
        this.mIdentifier = identifier;
        return this;
    }

    @Override
    public long getIdentifier() {
        return mIdentifier;
    }

    public int getPaycode() {
        return mPaycode;
    }

    public RealmIndividualPayroll withPaycode(int paycode) {
        this.mPaycode = paycode;
        return this;
    }

    public int getIsIncome() {
        return mIsIncome;
    }

    public RealmIndividualPayroll withIsIncome(int isIncome) {
        this.mIsIncome = isIncome;
        return this;
    }

    public int getAmountId() {
        return mAmountId;
    }

    public RealmIndividualPayroll withAmountId(int amountId) {
        this.mAmountId = amountId;
        return this;
    }

    public String getAmountName() {
        return mAmountName;
    }

    public RealmIndividualPayroll withAmountName(String amountName) {
        this.mAmountName = amountName;
        return this;
    }

    public double getAmount() {
        return mAmount;
    }

    public RealmIndividualPayroll withAmount(double amount) {
        this.mAmount = amount;
        return this;
    }

    // OnClickListener

    public FastAdapter.OnClickListener<RealmIndividualPayroll> getOnClickListener() {
        return mOnClickListener;
    }

    public RealmIndividualPayroll withOnClickListener(FastAdapter.OnClickListener<RealmIndividualPayroll> onClickListener) {
        this.mOnClickListener = onClickListener;
        return this;
    }

    @Ignore
    private FastAdapter.OnClickListener<RealmIndividualPayroll> onClickListener = new FastAdapter.OnClickListener<RealmIndividualPayroll>() {
        @Override
        public boolean onClick(View v, IAdapter<RealmIndividualPayroll> adapter, RealmIndividualPayroll item, int position) {

            if(item.getSubItems() != null) {
                if(!item.isExpanded()) {
                    ViewCompat.animate(v.findViewById(R.id.textview_viewgroup_individual_payroll_icon)).rotation(0).start();
                    ViewCompat.animate(v.findViewById(R.id.textview_viewgroup_individual_payroll_content)).alpha(1).start();
                    v.findViewById(R.id.textview_viewgroup_individual_payroll_content).setVisibility(View.VISIBLE);
                    v.setBackgroundColor(ContextCompat.getColor(v.getContext(), android.R.color.transparent));
                } else {
                    ViewCompat.animate(v.findViewById(R.id.textview_viewgroup_individual_payroll_icon)).rotation(180).start();
                    ViewCompat.animate(v.findViewById(R.id.textview_viewgroup_individual_payroll_content)).alpha(0).start();
                    v.findViewById(R.id.textview_viewgroup_individual_payroll_content).setVisibility(View.GONE);
                    v.setBackgroundColor(ContextCompat.getColor(v.getContext(), R.color.md_white_1000));
                }
            }

            if (item.getSubItems() != null) {
                return mOnClickListener == null || mOnClickListener.onClick(v, adapter, item, position);
            }

            return mOnClickListener != null || mOnClickListener.onClick(v, adapter, item, position);
        }
    };

    @Override
    public FastAdapter.OnClickListener<RealmIndividualPayroll> getOnItemClickListener() {
        return onClickListener;
    }

    @Override
    public RealmIndividualPayroll withOnItemPreClickListener(FastAdapter.OnClickListener<RealmIndividualPayroll> onItemPreClickListener) {
        return null;
    }

    @Override
    public FastAdapter.OnClickListener<RealmIndividualPayroll> getOnPreItemClickListener() {
        return null;
    }

    @Override
    public RealmIndividualPayroll withOnItemClickListener(FastAdapter.OnClickListener<RealmIndividualPayroll> onItemClickListener) {
        return null;
    }

    @Override
    public View generateView(Context ctx) {
        ViewHolder viewHolder = getViewHolder(LayoutInflater.from(ctx).inflate(getLayoutRes(), null, false));
        bindView(viewHolder, Collections.EMPTY_LIST);
        return viewHolder.itemView;
    }

    @Override
    public View generateView(Context ctx, ViewGroup parent) {
        ViewHolder viewHolder = getViewHolder(LayoutInflater.from(ctx).inflate(getLayoutRes(), parent, false));
        bindView(viewHolder, Collections.EMPTY_LIST);
        return viewHolder.itemView;
    }

    @Override
    public ViewHolder getViewHolder(ViewGroup parent) {
        return getViewHolder(LayoutInflater.from(parent.getContext()).inflate(getLayoutRes(), parent, false));
    }

    private ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public void bindView(final ViewHolder holder, List<Object> payloads) {
        DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
        holder.name.setText(getTitle());
        holder.content.setText("Gross: Php " + decimalFormat.format(getGross()));
    }

    @Override
    public void unbindView(ViewHolder holder) {
        holder.name.setText(null);
        holder.content.setText(null);
    }

    @Override
    public void attachToWindow(ViewHolder viewHolder) {

    }

    @Override
    public void detachFromWindow(ViewHolder viewHolder) {

    }

    @Override
    public boolean failedToRecycle(ViewHolder viewHolder) {
        return false;
    }

    @Override
    public boolean equals(int id) {
        return false;
    }

    @Override
    public int getType() {
        return R.id.fastadapter_individual_payroll_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.viewgroup_individual_payroll;
    }

    @Override
    public int hashCode() {
        return Long.valueOf(mIdentifier).hashCode();
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textview_viewgroup_individual_payroll_title)
        protected TextView name;

        @BindView(R.id.textview_viewgroup_individual_payroll_content)
        protected TextView content;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
