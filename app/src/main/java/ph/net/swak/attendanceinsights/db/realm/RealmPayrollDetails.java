package ph.net.swak.attendanceinsights.db.realm;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class RealmPayrollDetails implements RealmModel {

    @PrimaryKey
    @SerializedName("autoid")
    private long autoid = -1;
    @SerializedName("paycode_id")
    private int paycodeId;
    @SerializedName("cov_year")
    private int covYear;
    @SerializedName("cov_month")
    private int covMonth;
    @SerializedName("cov_week")
    private String covWeek;
    @SerializedName("fromdate")
    private String fromDate;
    @SerializedName("todate")
    private String toDate;
    @SerializedName("emp_id")
    private long employeeId;
    @SerializedName("is_income")
    private boolean isIncome;
    @SerializedName("hrs_day")
    private double hrsDay;
    @SerializedName("amount_id")
    private int amountId;
    @SerializedName("amount")
    private double amount;
    @SerializedName("account_type")
    private String accountType;
    @SerializedName("account_display")
    private String accountDisplay;
    @SerializedName("img_path")
    private String imgPath;

    public long getAutoid() {
        return autoid;
    }

    public RealmPayrollDetails withAutoid(long autoid) {
        this.autoid = autoid;
        return this;
    }

    public int getPaycodeId() {
        return paycodeId;
    }

    public RealmPayrollDetails withPaycodeId(int paycodeId) {
        this.paycodeId = paycodeId;
        return this;
    }

    public int getCovYear() {
        return covYear;
    }

    public RealmPayrollDetails withCovYear(int covYear) {
        this.covYear = covYear;
        return this;
    }

    public int getCovMonth() {
        return covMonth;
    }

    public RealmPayrollDetails withCovMonth(int covMonth) {
        this.covMonth = covMonth;
        return this;
    }

    public String getCovWeek() {
        return covWeek;
    }

    public RealmPayrollDetails withCovWeek(String covWeek) {
        this.covWeek = covWeek;
        return this;
    }

    public String getFromDate() {
        return fromDate;
    }

    public RealmPayrollDetails withFromDate(String fromDate) {
        this.fromDate = fromDate;
        return this;
    }

    public String getToDate() {
        return toDate;
    }

    public RealmPayrollDetails withToDate(String toDate) {
        this.toDate = toDate;
        return this;
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public RealmPayrollDetails withEmployeeId(long employeeId) {
        this.employeeId = employeeId;
        return this;
    }

    public boolean isIncome() {
        return isIncome;
    }

    public RealmPayrollDetails withIncome(boolean income) {
        isIncome = income;
        return this;
    }

    public double getHrsDay() {
        return hrsDay;
    }

    public RealmPayrollDetails withHrsDay(double hrsDay) {
        this.hrsDay = hrsDay;
        return this;
    }

    public int getAmountId() {
        return amountId;
    }

    public RealmPayrollDetails withAmountId(int amountId) {
        this.amountId = amountId;
        return this;
    }

    public double getAmount() {
        return amount;
    }

    public RealmPayrollDetails withAmount(double amount) {
        this.amount = amount;
        return this;
    }

    public String getAccountType() {
        return accountType;
    }

    public RealmPayrollDetails withAccountType(String accountType) {
        this.accountType = accountType;
        return this;
    }

    public String getAccountDisplay() {
        return accountDisplay;
    }

    public RealmPayrollDetails withAccountDisplay(String accountDisplay) {
        this.accountDisplay = accountDisplay;
        return this;
    }

    public String getImgPath() {
        return imgPath;
    }

    public RealmPayrollDetails withImgPath(String imgPath) {
        this.imgPath = imgPath;
        return this;
    }

    public RealmPayrollDetails() {}

}
