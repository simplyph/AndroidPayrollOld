package ph.net.swak.attendanceinsights.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import ph.net.swak.attendanceinsights.repository.PayrollRepository;
import ph.net.swak.attendanceinsights.repository.Resource;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.PayrollEmployeeFastAdapter;

public class PayrollEmployeeBottomSheetViewModel extends ViewModel {

    private PayrollRepository payrollRepo;
    private MutableLiveData<Resource<List<PayrollEmployeeFastAdapter>>> employeeList = new MutableLiveData<>();

    @Inject
    public PayrollEmployeeBottomSheetViewModel(PayrollRepository payrollRepository) {
        payrollRepo = payrollRepository;
    }

    public void fetch(int year, int compId, int posId, int from, int fromId) {
        employeeList.setValue(null);
        payrollRepo.getPayrollByCompAndPos(year, compId, posId, from, fromId).observeForever(listResource -> employeeList.setValue(listResource));
    }

    public LiveData<Resource<List<PayrollEmployeeFastAdapter>>> getEmployeeList() {
        return employeeList;
    }

}
