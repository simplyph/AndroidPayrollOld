package ph.net.swak.attendanceinsights.models;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Knowell on 3/9/2017.
 */

public class RealmPayroll extends RealmObject {

    @PrimaryKey
    private long mIdentifier = -1;

    private int paycodeId;
    private int covYear;

    @Index
    private int covMonth;

    @Index
    private int empId;

    private String fName;
    private String lName;
    private double totalGross;
    private double totalDeduct;
    private double totalNet;
    private boolean manager;
    private boolean supervisor;
    private boolean staff;

    @Index
    private int compId;
    private String compName;

    @Index
    private int divId;
    private String divName;
    private int desigId;
    private String desigName;
    private String statusType;


    public long getIdentifier() {
        return mIdentifier;
    }

    public RealmPayroll withIdentifier(long identifier) {
        mIdentifier = identifier;
        return this;
    }

    public int getPaycodeId() {
        return paycodeId;
    }

    public RealmPayroll withPaycodeId(int paycodeId) {
        this.paycodeId = paycodeId;
        return this;
    }

    public int getCovYear() {
        return covYear;
    }

    public RealmPayroll withCovYear(int covYear) {
        this.covYear = covYear;
        return this;
    }

    public int getCovMonth() {
        return covMonth;
    }

    public RealmPayroll withCovMonth(int covMonth) {
        this.covMonth = covMonth;
        return this;
    }

    public int getEmpId() {
        return empId;
    }

    public RealmPayroll withEmpId(int empId) {
        this.empId = empId;
        return this;
    }

    public String getfName() {
        return fName;
    }

    public RealmPayroll withfName(String fName) {
        this.fName = fName;
        return this;
    }

    public String getlName() {
        return lName;
    }

    public RealmPayroll withlName(String lName) {
        this.lName = lName;
        return this;
    }

    public double getTotalGross() {
        return totalGross;
    }

    public RealmPayroll withTotalGross(double totalGross) {
        this.totalGross = totalGross;
        return this;
    }

    public double getTotalDeduct() {
        return totalDeduct;
    }

    public RealmPayroll withTotalDeduct(double totalDeduct) {
        this.totalDeduct = totalDeduct;
        return this;
    }

    public double getTotalNet() {
        return totalNet;
    }

    public RealmPayroll withTotalNet(double totalNet) {
        this.totalNet = totalNet;
        return this;
    }

    public boolean isManager() {
        return manager;
    }

    public RealmPayroll withManager(boolean manager) {
        this.manager = manager;
        return this;
    }

    public boolean isSupervisor() {
        return supervisor;
    }

    public RealmPayroll withSupervisor(boolean supervisor) {
        this.supervisor = supervisor;
        return this;
    }

    public boolean isStaff() {
        return staff;
    }

    public RealmPayroll withStaff(boolean staff) {
        this.staff = staff;
        return this;
    }

    public int getCompId() {
        return compId;
    }

    public RealmPayroll withCompId(int compId) {
        this.compId = compId;
        return this;
    }

    public String getCompName() {
        return compName;
    }

    public RealmPayroll withCompName(String compName) {
        this.compName = compName;
        return this;
    }

    public int getDivId() {
        return divId;
    }

    public RealmPayroll withDivId(int divId) {
        this.divId = divId;
        return this;
    }

    public String getDivName() {
        return divName;
    }

    public RealmPayroll withDivName(String divName) {
        this.divName = divName;
        return this;
    }

    public int getDesigId() {
        return desigId;
    }

    public RealmPayroll withDesigId(int desigId) {
        this.desigId = desigId;
        return this;
    }

    public String getDesigName() {
        return desigName;
    }

    public RealmPayroll withDesigName(String desigName) {
        this.desigName = desigName;
        return this;
    }

    public String getStatusType() {
        return statusType;
    }

    public RealmPayroll withStatusType(String statusType) {
        this.statusType = statusType;
        return this;
    }
}
