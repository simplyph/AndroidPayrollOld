package ph.net.swak.attendanceinsights.utils

enum class PayrollPeriodType(val order : Int = 0) {
    MONTHLY(1),
    QUARTERLY(2)
}
