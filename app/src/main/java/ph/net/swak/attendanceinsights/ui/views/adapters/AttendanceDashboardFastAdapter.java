package ph.net.swak.attendanceinsights.ui.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.net.swak.attendanceinsights.R;

public class AttendanceDashboardFastAdapter extends AbstractItem<AttendanceDashboardFastAdapter, AttendanceDashboardFastAdapter.ViewHolder> {

    private int empId;
    private String firstName;
    private String lastName;
    private String compName;
    private String desigName;

    private boolean hasSched;
    private String schedName;

    private boolean hasAbsent;

    private boolean hasLateToday;
    private double lateToday;

    private boolean hasLateMonthly;
    private String lateMonthName;
    private double lateMonthValue;

    public int getEmpId() {
        return empId;
    }

    public AttendanceDashboardFastAdapter withEmpId(int empId) {
        this.empId = empId;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public AttendanceDashboardFastAdapter withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public AttendanceDashboardFastAdapter withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getCompName() {
        return compName;
    }

    public AttendanceDashboardFastAdapter withCompName(String compName) {
        this.compName = compName;
        return this;
    }

    public String getDesigName() {
        return desigName;
    }

    public AttendanceDashboardFastAdapter withDesigName(String desigName) {
        this.desigName = desigName;
        return this;
    }

    public boolean isHasSched() {
        return hasSched;
    }

    public AttendanceDashboardFastAdapter withHasSched(boolean hasSched) {
        this.hasSched = hasSched;
        return this;
    }

    public String getSchedName() {
        return schedName;
    }

    public AttendanceDashboardFastAdapter withSchedName(String schedName) {
        this.schedName = schedName;
        return this;
    }

    public boolean isHasAbsent() {
        return hasAbsent;
    }

    public AttendanceDashboardFastAdapter withHasAbsent(boolean hasAbsent) {
        this.hasAbsent = hasAbsent;
        return this;
    }

    public boolean isHasLateToday() {
        return hasLateToday;
    }

    public AttendanceDashboardFastAdapter withHasLateToday(boolean hasLateToday) {
        this.hasLateToday = hasLateToday;
        return this;
    }

    public double getLateToday() {
        return lateToday;
    }

    public AttendanceDashboardFastAdapter withLateToday(double lateToday) {
        this.lateToday = lateToday;
        return this;
    }

    public boolean isHasLateMonthly() {
        return hasLateMonthly;
    }

    public AttendanceDashboardFastAdapter withHasLateMonthly(boolean hasLateMonthly) {
        this.hasLateMonthly = hasLateMonthly;
        return this;
    }

    public String getLateMonthName() {
        return lateMonthName;
    }

    public AttendanceDashboardFastAdapter withLateMonthName(String lateMonthName) {
        this.lateMonthName = lateMonthName;
        return this;
    }

    public double getLateMonthValue() {
        return lateMonthValue;
    }

    public AttendanceDashboardFastAdapter withLateMonthValue(double lateMonthValue) {
        this.lateMonthValue = lateMonthValue;
        return this;
    }

    @Override
    public int getType() {
        return R.id.fastadapter_bottom_sheet_att_item_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.list_bottom_sheet_att;
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);

        DecimalFormat dF = new DecimalFormat("###,##0");

        holder.textName.setText(getLastName() + ", " + getFirstName());
        holder.textComp.setText(getCompName());
        holder.textDesig.setText(getDesigName());



        if(isHasSched()) {
            holder.containerSchedule.setVisibility(View.VISIBLE);
            holder.textSched.setText(getSchedName());
        } else {
            holder.containerSchedule.setVisibility(View.GONE);
        }

        if(isHasAbsent()) {
            holder.textAbsent.setVisibility(View.VISIBLE);
        } else {
            holder.textAbsent.setVisibility(View.GONE);
        }

        if(isHasLateToday()) {
            holder.containerToday.setVisibility(View.VISIBLE);
            holder.textLateToday.setText(dF.format(getLateToday()) + (getLateToday() < 2d ? " Min" : " Mins"));
        } else {
            holder.containerToday.setVisibility(View.GONE);
        }

        if(isHasLateMonthly()) {
            holder.containerMonthly.setVisibility(View.VISIBLE);
            holder.textLateMonthlyLabel.setText("Total Late for " + getLateMonthName());
            holder.textLateMonthlyValue.setText(dF.format(getLateMonthValue()) + (getLateMonthValue() < 2d ? " Min" : " Mins"));
        } else {
            holder.containerMonthly.setVisibility(View.GONE);
        }
    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);

        holder.textName.setText(null);
        holder.textComp.setText(null);
        holder.textDesig.setText(null);
        holder.textSched.setText(null);
        holder.textLateToday.setText(null);
        holder.textLateMonthlyLabel.setText(null);
        holder.textLateMonthlyValue.setText(null);
    }

    @Override
    public ViewHolder getViewHolder(View view) {
        return null;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_name)
        protected TextView textName;

        @BindView(R.id.text_company)
        protected TextView textComp;

        @BindView(R.id.text_designation)
        protected TextView textDesig;

        @BindView(R.id.container_schedule)
        protected RelativeLayout containerSchedule;

        @BindView(R.id.text_sched)
        protected TextView textSched;

        @BindView(R.id.text_absent)
        protected TextView textAbsent;

        @BindView(R.id.container_late_today)
        protected RelativeLayout containerToday;

        @BindView(R.id.text_late_today)
        protected TextView textLateToday;

        @BindView(R.id.container_late_monthly)
        protected RelativeLayout containerMonthly;

        @BindView(R.id.text_late_monthly_label)
        protected TextView textLateMonthlyLabel;

        @BindView(R.id.text_late_monthly_value)
        protected TextView textLateMonthlyValue;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
