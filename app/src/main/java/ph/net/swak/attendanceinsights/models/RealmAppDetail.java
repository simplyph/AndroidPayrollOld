package ph.net.swak.attendanceinsights.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RealmAppDetail extends RealmObject {

    @PrimaryKey
    private int rate;

    private String rate_name;

    private int lastCount;

    private String lastEmpid;

    private int mIdentifier = -1;

    public int getRate() {
        return rate;
    }

    public RealmAppDetail withRate(int rate) {
        this.rate = rate;
        return this;
    }

    public String getRateName() {
        return rate_name;
    }

    public RealmAppDetail withRateName(String rate_name) {
        this.rate_name = rate_name;
        return this;
    }

    public int getLastCount() {
        return lastCount;
    }

    public RealmAppDetail withLastCount(int lastCount) {
        this.lastCount = lastCount;
        return this;
    }

    public String getLastEmpid() {
        return lastEmpid;
    }

    public RealmAppDetail withLastEmpid(String lastEmpid) {
        this.lastEmpid = lastEmpid;
        return this;
    }

    public int getmIdentifier() {
        return mIdentifier;
    }

    public RealmAppDetail withIdentifier(int mIdentifier) {
        this.mIdentifier = mIdentifier;
        return this;
    }
}
