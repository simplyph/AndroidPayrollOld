package ph.net.swak.attendanceinsights.db.realm;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class RealmPayrollHead implements RealmModel {

    @PrimaryKey
    @SerializedName("autoid")
    private long autoid = -1;
    @SerializedName("paycode_id")
    private int paycodeId;
    @SerializedName("cov_year")
    private int covYear;
    @SerializedName("cov_month")
    private int covMonth;
    @SerializedName("cov_week")
    private String covWeek;
    @SerializedName("emp_id")
    private long employeeId;
    @SerializedName("firstname")
    private String fName;
    @SerializedName("middlename")
    private String mName;
    @SerializedName("lastname")
    private String lName;
    @SerializedName("total_gross")
    private double totalGross;
    @SerializedName("total_deduct")
    private double totalDeduction;
    @SerializedName("total_net")
    private double totalNet;
    @SerializedName("is_manager")
    private boolean isManager;
    @SerializedName("is_supervisor")
    private boolean isSupervisor;
    @SerializedName("is_staff")
    private boolean isStaff;
    @SerializedName("comp_id")
    private int compId;
    @SerializedName("comp_name")
    private String compName;
    @SerializedName("div_id")
    private int divId;
    @SerializedName("div_name")
    private String divName;
    @SerializedName("dept_id")
    private int deptId;
    @SerializedName("dept_name")
    private String deptName;
    @SerializedName("desig_id")
    private int desigId;
    @SerializedName("desig_name")
    private String desigName;
    @SerializedName("status_type")
    private String statusType;
    @SerializedName("img_path")
    private String imgPath;


    public long getAutoid() {
        return autoid;
    }

    public RealmPayrollHead withAutoid(long autoid) {
        this.autoid = autoid;
        return this;
    }

    public int getPaycodeId() {
        return paycodeId;
    }

    public RealmPayrollHead withPaycodeId(int paycodeId) {
        this.paycodeId = paycodeId;
        return this;
    }

    public int getCovYear() {
        return covYear;
    }

    public RealmPayrollHead withCovYear(int covYear) {
        this.covYear = covYear;
        return this;
    }

    public int getCovMonth() {
        return covMonth;
    }

    public RealmPayrollHead withCovMonth(int covMonth) {
        this.covMonth = covMonth;
        return this;
    }

    public String getCovWeek() {
        return covWeek;
    }

    public RealmPayrollHead withCovWeek(String covWeek) {
        this.covWeek = covWeek;
        return this;
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public RealmPayrollHead withEmployeeId(long employeeId) {
        this.employeeId = employeeId;
        return this;
    }

    public String getfName() {
        return fName;
    }

    public RealmPayrollHead withfName(String fName) {
        this.fName = fName;
        return this;
    }

    public String getmName() {
        return mName;
    }

    public RealmPayrollHead withmName(String mName) {
        this.mName = mName;
        return this;
    }

    public String getlName() {
        return lName;
    }

    public RealmPayrollHead withlName(String lName) {
        this.lName = lName;
        return this;
    }

    public double getTotalGross() {
        return totalGross;
    }

    public RealmPayrollHead withTotalGross(double totalGross) {
        this.totalGross = totalGross;
        return this;
    }

    public double getTotalDeduction() {
        return totalDeduction;
    }

    public RealmPayrollHead withTotalDeduction(double totalDeduction) {
        this.totalDeduction = totalDeduction;
        return this;
    }

    public double getTotalNet() {
        return totalNet;
    }

    public RealmPayrollHead withTotalNet(double totalNet) {
        this.totalNet = totalNet;
        return this;
    }

    public boolean isManager() {
        return isManager;
    }

    public RealmPayrollHead withManager(boolean manager) {
        isManager = manager;
        return this;
    }

    public boolean isSupervisor() {
        return isSupervisor;
    }

    public RealmPayrollHead withSupervisor(boolean supervisor) {
        isSupervisor = supervisor;
        return this;
    }

    public boolean isStaff() {
        return isStaff;
    }

    public RealmPayrollHead withStaff(boolean staff) {
        isStaff = staff;
        return this;
    }

    public int getCompId() {
        return compId;
    }

    public RealmPayrollHead withCompId(int compId) {
        this.compId = compId;
        return this;
    }

    public String getCompName() {
        return compName;
    }

    public RealmPayrollHead withCompName(String compName) {
        this.compName = compName;
        return this;
    }

    public int getDivId() {
        return divId;
    }

    public RealmPayrollHead withDivId(int divId) {
        this.divId = divId;
        return this;
    }

    public String getDivName() {
        return divName;
    }

    public RealmPayrollHead withDivName(String divName) {
        this.divName = divName;
        return this;
    }

    public int getDeptId() {
        return deptId;
    }

    public RealmPayrollHead withDeptId(int deptId) {
        this.deptId = deptId;
        return this;
    }

    public String getDeptName() {
        return deptName;
    }

    public RealmPayrollHead withDeptName(String deptName) {
        this.deptName = deptName;
        return this;
    }

    public int getDesigId() {
        return desigId;
    }

    public RealmPayrollHead withDesigId(int desigId) {
        this.desigId = desigId;
        return this;
    }

    public String getDesigName() {
        return desigName;
    }

    public RealmPayrollHead withDesigName(String desigName) {
        this.desigName = desigName;
        return this;
    }

    public String getStatusType() {
        return statusType;
    }

    public RealmPayrollHead withStatusType(String statusType) {
        this.statusType = statusType;
        return this;
    }

    public String getImgPath() {
        return imgPath;
    }

    public RealmPayrollHead withImgPath(String imgPath) {
        this.imgPath = imgPath;
        return this;
    }

    public RealmPayrollHead() {

    }

}
