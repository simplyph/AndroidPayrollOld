package ph.net.swak.attendanceinsights.ui.views.fastadapter;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.ui.views.bottomsheet.PayrollEmployeeBottomSheet;
import ph.net.swak.attendanceinsights.ui.views.bottomsheet.PayrollMonthBottomSheet;
import ph.net.swak.attendanceinsights.utils.Helper;
import ph.net.swak.attendanceinsights.utils.PayrollPeriodType;

public class PayrollDashboardFastAdapter extends AbstractItem<PayrollDashboardFastAdapter, PayrollDashboardFastAdapter.ViewHolder> {

    private String compName;
    private int compId;
    private double valueEmployee;
    private double valueManager;
    private double valueSupervisor;
    private double valueStaff;
    private int countEmployee;
    private int countManager;
    private int countSupervisor;
    private int countStaff;
    private int from;
    private int fromId;
    private int year;

    public String getCompName() {
        return compName;
    }

    public PayrollDashboardFastAdapter withCompName(String compName) {
        this.compName = compName;
        return this;
    }

    public int getCompId() {
        return compId;
    }

    public PayrollDashboardFastAdapter withCompId(int compId) {
        this.compId = compId;
        return this;
    }

    public double getValueEmployee() {
        return valueEmployee;
    }

    public PayrollDashboardFastAdapter withValueEmployee(double valueEmployee) {
        this.valueEmployee = valueEmployee;
        return this;
    }

    public double getValueManager() {
        return valueManager;
    }

    public PayrollDashboardFastAdapter withValueManager(double valueManager) {
        this.valueManager = valueManager;
        return this;
    }

    public double getValueSupervisor() {
        return valueSupervisor;
    }

    public PayrollDashboardFastAdapter withValueSupervisor(double valueSupervisor) {
        this.valueSupervisor = valueSupervisor;
        return this;
    }

    public double getValueStaff() {
        return valueStaff;
    }

    public PayrollDashboardFastAdapter withValueStaff(double valueStaff) {
        this.valueStaff = valueStaff;
        return this;
    }

    public int getCountEmployee() {
        return countEmployee;
    }

    public PayrollDashboardFastAdapter withCountEmployee(int countEmployee) {
        this.countEmployee = countEmployee;
        return this;
    }

    public int getCountManager() {
        return countManager;
    }

    public PayrollDashboardFastAdapter withCountManager(int countManager) {
        this.countManager = countManager;
        return this;
    }

    public int getCountSupervisor() {
        return countSupervisor;
    }

    public PayrollDashboardFastAdapter withCountSupervisor(int countSupervisor) {
        this.countSupervisor = countSupervisor;
        return this;
    }

    public int getCountStaff() {
        return countStaff;
    }

    public PayrollDashboardFastAdapter withCountStaff(int countStaff) {
        this.countStaff = countStaff;
        return this;
    }

    public int getFrom() {
        return from;
    }

    public PayrollDashboardFastAdapter withFrom(int from) {
        this.from = from;
        return this;
    }

    public int getFromId() {
        return fromId;
    }

    public PayrollDashboardFastAdapter withFromId(int fromId) {
        this.fromId = fromId;
        return this;
    }

    public int getYear() {
        return year;
    }

    public PayrollDashboardFastAdapter withYear(int year) {
        this.year = year;
        return this;
    }

    @Override
    public ViewHolder getViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public int getType() {
        return R.id.fastadapter_payroll_dashboard_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.item_payroll_dashboard;
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);

        holder.tvTitle.setText(Helper.toTitleCase(getCompName()));
        holder.tvSubtitle.setText(String.format(Locale.ENGLISH, "Php %s", Helper.toDecimal(getValueEmployee())));
        holder.tvCaption.setText(String.format(Locale.ENGLISH, "%d Employees", getCountEmployee()));
        holder.tvManagerValue.setText(String.format(Locale.ENGLISH, "Php %s", Helper.toDecimal(getValueManager())));
        holder.tvManagerCount.setText(String.format(Locale.ENGLISH,"%d Employees", getCountManager()));
        holder.tvSupervisorValue.setText(String.format(Locale.ENGLISH, "Php %s", Helper.toDecimal(getValueSupervisor())));
        holder.tvSupervisorCount.setText(String.format(Locale.ENGLISH, "%d Employees", getCountSupervisor()));
        holder.tvStaffValue.setText(String.format(Locale.ENGLISH, "Php %s", Helper.toDecimal(getValueStaff())));
        holder.tvStaffCount.setText(String.format(Locale.ENGLISH, "%d Employees", getCountStaff()));

        holder.linearLayoutManager.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putInt("type", 3);
            bundle.putString("typeName", "Manager");
            bundle.putInt("compId", getCompId());
            bundle.putString("compName", getCompName());
            bundle.putInt("from", getFrom());
            bundle.putInt("fromId", getFromId());
            bundle.putInt("year", getYear());

            PayrollEmployeeBottomSheet payrollEmployeeBottomSheet = new PayrollEmployeeBottomSheet();
            payrollEmployeeBottomSheet.setArguments(bundle);
            payrollEmployeeBottomSheet.show(((MainActivity) holder.itemView.getContext()).getSupportFragmentManager(), null);
        });

        holder.linearLayoutSupervisor.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putInt("type", 2);
            bundle.putString("typeName", "Supervisor");
            bundle.putInt("compId", getCompId());
            bundle.putString("compName", getCompName());
            bundle.putInt("from", getFrom());
            bundle.putInt("fromId", getFromId());
            bundle.putInt("year", getYear());

            PayrollEmployeeBottomSheet payrollEmployeeBottomSheet = new PayrollEmployeeBottomSheet();
            payrollEmployeeBottomSheet.setArguments(bundle);
            payrollEmployeeBottomSheet.show(((MainActivity) holder.itemView.getContext()).getSupportFragmentManager(), null);
        });

        holder.linearLayoutStaff.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putInt("type", 1);
            bundle.putString("typeName", "Staff");
            bundle.putInt("compId", getCompId());
            bundle.putString("compName", getCompName());
            bundle.putInt("from", getFrom());
            bundle.putInt("fromId", getFromId());
            bundle.putInt("year", getYear());

            PayrollEmployeeBottomSheet payrollEmployeeBottomSheet = new PayrollEmployeeBottomSheet();
            payrollEmployeeBottomSheet.setArguments(bundle);
            payrollEmployeeBottomSheet.show(((MainActivity) holder.itemView.getContext()).getSupportFragmentManager(), null);
        });

        holder.bMonthly.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("typeName", "Staff");
            bundle.putInt("compId", getCompId());
            bundle.putString("compName", getCompName());
            bundle.putInt("from", getFrom());
            bundle.putInt("fromId", getFromId());
            bundle.putInt("year", getYear());
            bundle.putInt("periodType", PayrollPeriodType.MONTHLY.getOrder());


            PayrollMonthBottomSheet payrollMonthBottomSheet = new PayrollMonthBottomSheet();
            payrollMonthBottomSheet.setArguments(bundle);
            payrollMonthBottomSheet.show(((MainActivity) holder.itemView.getContext()).getSupportFragmentManager(), null);
        });



        holder.bQuarterly.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("typeName", "Staff");
            bundle.putInt("compId", getCompId());
            bundle.putString("compName", getCompName());
            bundle.putInt("from", getFrom());
            bundle.putInt("fromId", getFromId());
            bundle.putInt("year", getYear());
            bundle.putInt("periodType", PayrollPeriodType.QUARTERLY.getOrder());


            PayrollMonthBottomSheet payrollMonthBottomSheet = new PayrollMonthBottomSheet();
            payrollMonthBottomSheet.setArguments(bundle);
            payrollMonthBottomSheet.show(((MainActivity) holder.itemView.getContext()).getSupportFragmentManager(), null);
        });
    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);

        holder.tvTitle.setText(null);
        holder.tvSubtitle.setText(null);
        holder.tvCaption.setText(null);
        holder.tvManagerValue.setText(null);
        holder.tvManagerCount.setText(null);
        holder.tvSupervisorValue.setText(null);
        holder.tvSupervisorCount.setText(null);
        holder.tvStaffValue.setText(null);
        holder.tvStaffCount.setText(null);

        holder.linearLayoutManager.setOnClickListener(null);
        holder.linearLayoutSupervisor.setOnClickListener(null);
        holder.linearLayoutStaff.setOnClickListener(null);
        holder.bMonthly.setOnClickListener(null);
        holder.bQuarterly.setOnClickListener(null);
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textview_item_payroll_dashboard_title)
        protected TextView tvTitle;

        @BindView(R.id.textview_item_payroll_dashboard_subtitle)
        protected TextView tvSubtitle;

        @BindView(R.id.textview_item_payroll_dashboard_caption)
        protected TextView tvCaption;

        @BindView(R.id.linearlayout_item_payroll_dashboard_manager)
        protected LinearLayout linearLayoutManager;

        @BindView(R.id.linearlayout_item_payroll_dashboard_supervisor)
        protected LinearLayout linearLayoutSupervisor;

        @BindView(R.id.linearlayout_item_payroll_dashboard_staff)
        protected LinearLayout linearLayoutStaff;

        @BindView(R.id.textview_item_payroll_dashboard_manager_value)
        protected TextView tvManagerValue;

        @BindView(R.id.textview_item_payroll_dashboard_manager_count)
        protected TextView tvManagerCount;

        @BindView(R.id.textview_item_payroll_dashboard_supervisor_value)
        protected TextView tvSupervisorValue;

        @BindView(R.id.textview_item_payroll_dashboard_supervisor_count)
        protected TextView tvSupervisorCount;

        @BindView(R.id.textview_item_payroll_dashboard_staff_value)
        protected TextView tvStaffValue;

        @BindView(R.id.textview_item_payroll_dashboard_staff_count)
        protected TextView tvStaffCount;

        @BindView(R.id.button_item_payroll_dashboard_monthly)
        protected Button bMonthly;

        @BindView(R.id.button_item_payroll_dashboard_quarterly)
        protected Button bQuarterly;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
