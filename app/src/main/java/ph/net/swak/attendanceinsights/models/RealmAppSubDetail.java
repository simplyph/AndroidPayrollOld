package ph.net.swak.attendanceinsights.models;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mikepenz.fastadapter.IItem;
import com.mikepenz.fastadapter.items.AbstractItem;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.events.UserDetailEvent;


public class RealmAppSubDetail extends RealmObject implements IItem<RealmAppSubDetail, RealmAppSubDetail.ViewHolder> {

    private String compName;
    private String divName;
    private String desigName;

    @Index
    private int compId;
    private int divId;
    @Index
    private int desigId;

    public String getCompName() {
        return compName;
    }

    public RealmAppSubDetail withCompName(String compName) {
        this.compName = compName;
        return this;
    }

    public String getDivName() {
        return divName;
    }

    public RealmAppSubDetail withDivName(String divName) {
        this.divName = divName;
        return this;
    }

    public String getDesigName() {
        return desigName;
    }

    public RealmAppSubDetail withDesigName(String desigName) {
        this.desigName = desigName;
        return this;
    }

    public int getCompId() {
        return compId;
    }

    public RealmAppSubDetail withCompId(int compId) {
        this.compId = compId;
        return this;
    }

    public int getDivId() {
        return divId;
    }

    public RealmAppSubDetail withDivId(int divId) {
        this.divId = divId;
        return this;
    }

    public int getDesigId() {
        return desigId;
    }

    public RealmAppSubDetail withDesigId(int desigId) {
        this.desigId = desigId;
        return this;
    }

    @Ignore
    private Dialog dialog;

    public Dialog getDialog() {
        return dialog;
    }

    public RealmAppSubDetail withDialog(Dialog dialog) {
        this.dialog = dialog;
        return this;
    }

    private double lastScore;

    public double getLastScore() {
        return lastScore;
    }

    public RealmAppSubDetail withLastScore(double lastScore) {
        this.lastScore = lastScore;
        return this;
    }

    private String fname;

    public String getFname() {
        return fname;
    }

    public RealmAppSubDetail withFname(String fname) {
        this.fname = fname;
        return this;
    }

    private String lname;

    public String getLname() {
        return lname;
    }

    public RealmAppSubDetail withLname(String lname) {
        this.lname = lname;
        return this;
    }

    private String empId;

    public String getEmpId() {
        return empId;
    }

    public RealmAppSubDetail withEmpId(String empId) {
        this.empId = empId;
        return this;
    }

    @PrimaryKey
    protected long mIdentifier = -1;

    public RealmAppSubDetail withIdentifier(long identifier) {
        this.mIdentifier = identifier;
        return this;
    }

    @Override
    public long getIdentifier() {
        return mIdentifier;
    }

    @Ignore
    protected Object mTag;

    public RealmAppSubDetail withTag(Object object) {
        this.mTag = object;
        return this;
    }

    @Override
    public Object getTag() {
        return mTag;
    }

    @Ignore
    protected boolean mEnabled = true;

    @Override
    public RealmAppSubDetail withEnabled(boolean enabled) {
        this.mEnabled = enabled;
        return this;
    }

    @Override
    public boolean isEnabled() {
        return mEnabled;
    }

    @Ignore
    protected boolean mSelected = false;

    @Override
    public RealmAppSubDetail withSetSelected(boolean selected) {
        this.mSelected = selected;
        return this;
    }

    @Override
    public boolean isSelected() {
        return mSelected;
    }

    @Ignore
    protected boolean mSelectable = true;

    @Override
    public RealmAppSubDetail withSelectable(boolean selectable) {
        this.mSelectable = selectable;
        return this;
    }

    @Override
    public boolean isSelectable() {
        return mSelectable;
    }

    @Override
    public int getType() {
        return R.id.fastadapter_app_sub_detail_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.list_attendance_dashboard;
    }

    @Override
    public View generateView(Context ctx) {
        ViewHolder viewHolder = getViewHolder(LayoutInflater.from(ctx).inflate(getLayoutRes(), null, false));

        bindView(viewHolder, Collections.EMPTY_LIST);

        return viewHolder.itemView;
    }

    @Override
    public View generateView(Context ctx, ViewGroup parent) {
        ViewHolder viewHolder = getViewHolder(LayoutInflater.from(ctx).inflate(getLayoutRes(), parent, false));

        bindView(viewHolder, Collections.EMPTY_LIST);

        return viewHolder.itemView;
    }

    @Override
    public ViewHolder getViewHolder(ViewGroup parent) {
        return getViewHolder(LayoutInflater.from(parent.getContext()).inflate(getLayoutRes(), parent, false));
    }

    private ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public void bindView(final ViewHolder holder, List<Object> payloads) {
        DecimalFormat format = new DecimalFormat("###,###.00");

        holder.textName.setText(getLname() + ", " + getFname());
        holder.textComp.setText(getCompName());
        holder.textDesig.setText(getDesigName());

        holder.textRank.setVisibility(View.GONE);
        holder.textName.setText(getLname() + ", " + getFname());
        holder.textComp.setText(getCompName());
        holder.textDesig.setText(getDesigName());
        holder.textDetails.setText("Score: " + String.valueOf(format.format(getLastScore())) + "%");

        //holder.textViewScore.setText("Score: " + String.valueOf(format.format(getLastScore())) + "%");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
                EventBus.getDefault().post(new UserDetailEvent(getEmpId()));
            }
        });
    }

    @Override
    public void unbindView(ViewHolder holder) {
        holder.textRank.setVisibility(View.GONE);
        holder.textName.setText(null);
        holder.textComp.setText(null);
        holder.textDesig.setText(null);
        holder.textDetails.setText(null);
        holder.itemView.setOnClickListener(null);
    }

    @Override
    public void attachToWindow(ViewHolder viewHolder) {

    }

    @Override
    public void detachFromWindow(ViewHolder viewHolder) {

    }

    @Override
    public boolean failedToRecycle(ViewHolder viewHolder) {
        return false;
    }

    @Override
    public boolean equals(int id) {
        return id == mIdentifier;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        AbstractItem<?, ?> that = (AbstractItem<?, ?>) obj;
        return mIdentifier == that.getIdentifier();
    }

    @Override
    public int hashCode() {
        return Long.valueOf(mIdentifier).hashCode();
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_rank)
        protected TextView textRank;

        @BindView(R.id.text_name)
        protected TextView textName;

        @BindView(R.id.text_comp)
        protected TextView textComp;

        @BindView(R.id.text_desig)
        protected TextView textDesig;

        @BindView(R.id.text_details)
        protected TextView textDetails;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
