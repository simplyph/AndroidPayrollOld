package ph.net.swak.attendanceinsights.events;

/**
 * Created by Knowell on 2/13/2017.
 */

public class FilterClickEvent {

    public final String type;

    public FilterClickEvent(String type) {
        this.type = type;
    }

}
