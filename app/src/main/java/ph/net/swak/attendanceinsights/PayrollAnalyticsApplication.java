package ph.net.swak.attendanceinsights;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.stetho.Stetho;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerUIUtils;
import com.squareup.leakcanary.AndroidExcludedRefs;
import com.squareup.leakcanary.ExcludedRefs;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import ph.net.swak.attendanceinsights.di.AppInjector;
import ph.net.swak.attendanceinsights.di.DaggerPayrollAnalyticsComponent;
import ph.net.swak.attendanceinsights.utils.LocaleHelper;
import timber.log.Timber;

public class PayrollAnalyticsApplication extends Application implements HasActivityInjector {

    /**
     * Leak Canary refwatcher
     */
    private RefWatcher refWatcher;

    /**
     * Get application current refwatcher
     *
     * @param context {@link Context}
     * @return {@link RefWatcher}
     */
    public static RefWatcher getRefWatcher(Context context) {
        PayrollAnalyticsApplication application = (PayrollAnalyticsApplication) context.getApplicationContext();
        return application.refWatcher;
    }

    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;

    @Override
    public void onCreate() {
        super.onCreate();

        // LeakCanary
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.xLIO
            // You should not init your app in this process.
            return;
        }
        refWatcher = initializeLeakCanary(true);

        // Timber
        // TODO: Create implementation of writing logs into external file or viewable source
        if (BuildConfig.DEBUG)
            Timber.plant(new Timber.DebugTree());

        // Realm
        initializeRealm();

        AppInjector.init(this);

        // DrawerImageLoader
        configureDrawerImageLoader();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "en-US"));
    }

    /**
     * Initialize Leak Canary
     *
     * @param enabled Set to false if for release
     * @return {@link RefWatcher}
     */
    private RefWatcher initializeLeakCanary(boolean enabled) {
        if (enabled) {
            // list of excluded reference
            ExcludedRefs excludedRefs = AndroidExcludedRefs.createAppDefaults()
                    .instanceField("com.android.volley.NetworkDispatcher", "mListener")
                    .build();

            return LeakCanary.refWatcher(this)
                    .watchDelay(10, TimeUnit.SECONDS)
                    .excludedRefs(excludedRefs)
                    .buildAndInstall();
        } else {
            return RefWatcher.DISABLED;
        }
    }

    /**
     * Initialize Realm
     */
    private void initializeRealm() {
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                        .build()
        );
    }

    /**
     * Configure the default {@link DrawerImageLoader} of Material Drawer library
     */
    private void configureDrawerImageLoader() {
        DrawerImageLoader.init(new AbstractDrawerImageLoader() {
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder) {
                RequestOptions options = new RequestOptions().placeholder(placeholder);

                Glide.with(imageView.getContext()).load(uri).apply(options).into(imageView);
            }

            @Override
            public void cancel(ImageView imageView) {
                Glide.with(imageView.getContext()).clear(imageView);
            }

            @Override
            public Drawable placeholder(Context ctx, String tag) {
                if (DrawerImageLoader.Tags.PROFILE.name().equals(tag)) {
                    return DrawerUIUtils.getPlaceHolder(ctx);
                } else if (DrawerImageLoader.Tags.ACCOUNT_HEADER.name().equals(tag)) {
                    return new IconicsDrawable(ctx).iconText(" ").backgroundColorRes(com.mikepenz.materialdrawer.R.color.primary).sizeDp(56);
                } else if ("customUrlItem".equals(tag)) {
                    return new IconicsDrawable(ctx).iconText(" ").backgroundColorRes(R.color.md_red_500).sizeDp(56);
                }

                return super.placeholder(ctx, tag);
            }
        });
    }

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }
}
