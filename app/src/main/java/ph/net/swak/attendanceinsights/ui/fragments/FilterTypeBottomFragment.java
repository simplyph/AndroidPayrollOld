package ph.net.swak.attendanceinsights.ui.fragments;

import android.app.Dialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.RequestQueue;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;

/**
 * Created by Knowell on 2/13/2017.
 */

public class FilterTypeBottomFragment extends BottomSheetDialogFragment {

    @BindView(R.id.radiogroup_bottom_sheet_filter_type)
    protected ListView listView;

    private Unbinder unbinder;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private MainActivity mainActivity;
    private Realm mRealm;

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        mRealm = Realm.getDefaultInstance();
        mainActivity = (MainActivity) getContext();
        View view = View.inflate(getContext(), R.layout.fragment_bottom_sheet_filter_type, null);
        unbinder = ButterKnife.bind(this, view);
        requestQueue = VolleyAPI.getInstance(getContext()).getRequestQueue();
        volleyRequest = new VolleyRequest();

        final String[] values = new String[]{
                "Absent", "Late", "Undertime", "Overbreak",
                "Leave", "Overtime", "Night Differential",
                "Itinerary", "Perfect Attendance"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<>(mainActivity, android.R.layout.simple_list_item_1, android.R.id.text1, values);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mainActivity.mFilterTypeContent.setText(values[i]);
                dismiss();
            }
        });

        dialog.setContentView(view);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @OnClick(R.id.button_bottom_sheet_filter_type_close)
    protected void onClose() {
        dismiss();
    }
}
