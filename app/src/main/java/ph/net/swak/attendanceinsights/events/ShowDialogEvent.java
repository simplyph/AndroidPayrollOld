package ph.net.swak.attendanceinsights.events;

import android.support.design.widget.BottomSheetDialogFragment;

public class ShowDialogEvent {

    public BottomSheetDialogFragment bottomSheet;

    public ShowDialogEvent(BottomSheetDialogFragment bottomSheet) {
        this.bottomSheet = bottomSheet;
    }

}
