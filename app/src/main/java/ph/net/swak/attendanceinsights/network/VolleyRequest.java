package ph.net.swak.attendanceinsights.network;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ph.net.swak.attendanceinsights.R;

import static android.content.Context.MODE_PRIVATE;

public class VolleyRequest {

    // Variables
    public static final RetryPolicy CUSTOM_RETRY_POLICY =
            new DefaultRetryPolicy(20000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

    public static String ACCESS_TOKEN = "";

    public static final int OK = 200;
    public static final int CREATED = 201;
    public static final int UNAUTHORIZED = 401;
    public static final int FORBIDDEN = 403;
    public static final int NOT_FOUND = 404;
    public static final int BAD_REQUEST = 400;

    // Authentication
    private final String CLIENT_ID = "Android";
    private final String CLIENT_SECRET = "swakAndroid_webApi_2017";
    private final String CLIENT_TYPE = "1";

    // URLS
    public static final String TOKEN_ENDPOINT = "token";
    public static final String URL_BASE = "http://mdrpayroll.swak.net.ph/";
    private static final String URL_ATTENDANCE_INSIGHTS = "api/attendance/particulars";
    private static final String URL_LOGIN = "api/employee/login";
    private static final String URL_ATTENDANCE_DAILY = "api/attendance/summary";
    private static final String URL_PAYROLL_DETAIL = "api/payroll/";
    private static final String URL_PAYROLL_FILTER = "api/misc/filter/payroll";
    private static final String URL_INDIVIDUAL_PAYROLL = "api/payroll/id/";
    private static final String URL_EMPLOYEE_PROFILE = "api/employee/profiles";
    private static final String URL_APP_DETAIL = "api/appraisal/summary";
    private static final String URL_APP_SUB_DETAIL = "api/appraisal/sub/";
    private static final String URL_INDIVIDUAL_APPRAISAL = "api/appraisal/";
    private static final String URL_HRIS_DASHBOARD = "api/misc/dashboard/hris/";
    private static final String URL_BULLETIN = "api/misc/bulletin";
    private static final String URL_EMPLOYEE_ITINERARY = "webservice/GetEmpItinerary";
    //private static final String URL_ATTENDANCE_INSIGHTS = "webservice/GetAttendanceInsight";
    //private static final String URL_LOGIN = "webservice/GetLogin";
    //private static final String URL_ATTENDANCE_DAILY = "webservice/GetAttendanceStatsDaily";
    //private static final String URL_PAYROLL_DETAIL = "webservice/GetPayrollDetail";
    //private static final String URL_PAYROLL_FILTER = "webservice/GetPayrollFilter";
    //private static final String URL_INDIVIDUAL_PAYROLL = "webservice/GetIndividualPayroll";
    //private static final String URL_EMPLOYEE_PROFILE = "webservice/GetEmployeeProfile";
    //private static final String URL_APP_DETAIL = "webservice/GetAppraisalSummary";
    //private static final String URL_APP_SUB_DETAIL = "webservice/GetEmployeeByList";
    //private static final String URL_INDIVIDUAL_APPRAISAL = "webservice/GetIndividualAppraisal";
    //private static final String URL_HRIS_DASHBOARD = "api/misc/dashboard/hris/";
    //private static final String URL_BULLETIN = "webservice/GetBulletin";
    //private static final String URL_EMPLOYEE_ITINERARY = "webservice/GetEmpItinerary";

    //<editor-fold description="Request Methods">
    public StringRequest getString(String tag, int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        StringRequest stringRequest = new StringRequest(method, url, listener, errorListener);
        stringRequest.setRetryPolicy(CUSTOM_RETRY_POLICY);
        stringRequest.setShouldCache(false);
        stringRequest.setTag(tag);
        return stringRequest;
    }

    public StringRequest getString(String tag, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        return getString(tag, Request.Method.GET, url, listener, errorListener);
    }

    public JsonArrayRequest getJsonArray(String tag, int method, String url, JSONArray jsonRequest, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(method, url, jsonRequest, listener, errorListener);
        jsonArrayRequest.setRetryPolicy(CUSTOM_RETRY_POLICY);
        jsonArrayRequest.setShouldCache(false);
        jsonArrayRequest.setTag(tag);
        return jsonArrayRequest;
    }

    public JsonArrayRequest getJsonArray(String tag, String url, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener) {
        return getJsonArray(tag, Request.Method.GET, url, null, listener, errorListener);
    }

    public JsonObjectRequest getJsonObject(String tag, int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(method, url, jsonRequest, listener, errorListener);
        jsonObjectRequest.setRetryPolicy(CUSTOM_RETRY_POLICY);
        jsonObjectRequest.setShouldCache(false);
        jsonObjectRequest.setTag(tag);
        return jsonObjectRequest;
    }

    public JsonObjectRequest getJsonObject(String tag, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        return getJsonObject(tag, jsonRequest == null ? Request.Method.GET : Request.Method.POST, url, jsonRequest, listener, errorListener);
    }

    public JsonArrayRequest getAuthJsonArray(Context appContext, String tag, int method, String url, JSONArray jsonRequest, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(method, url, jsonRequest, listener, errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> auth = new HashMap<>();
                auth.put("Authorization", "Bearer " + ACCESS_TOKEN);
                return auth;
            }
        };
        jsonArrayRequest.setRetryPolicy(new TokenRetryPolicy(appContext, TokenRetryPolicy.DEFAULT_TIMEOUT_MS, TokenRetryPolicy.DEFAULT_MAX_RETRIES, TokenRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonArrayRequest.setShouldCache(false);
        jsonArrayRequest.setTag(tag);
        return jsonArrayRequest;
    }

    public JsonArrayRequest getAuthJsonArray(Context appContext, String tag, String url, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener) {
        return getAuthJsonArray(appContext, tag, Request.Method.GET, url, null, listener, errorListener);
    }

    public StringRequest getOAuthToken(final Context appContext) {
        return new StringRequest(Request.Method.POST, URL_BASE + TOKEN_ENDPOINT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    ACCESS_TOKEN = jsonObject.getString("access_token");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO
            }
        }) {

            @Override
            public Map<String, String> getParams() {
                SharedPreferences sharedPreferences = appContext.getSharedPreferences(appContext.getString(R.string.preference_file_key), MODE_PRIVATE);

                Map<String, String> params = new HashMap<>();
                params.put("username", sharedPreferences.getString("username", ""));
                params.put("password", sharedPreferences.getString("password", ""));
                params.put("grant_type", "password");
                params.put("client_id", CLIENT_ID);
                params.put("client_secret", CLIENT_SECRET);
                params.put("client_type", CLIENT_TYPE);
                return params;
            }

            @Override
            public Priority getPriority() {
                return Priority.IMMEDIATE;
            }
        };
    }

    //</editor-fold>

    // Done
    public String getInsights(String fromdate, String todate, String company, String type) {
        return URL_BASE + URL_ATTENDANCE_INSIGHTS + "?datefrom=" + fromdate + "&dateto=" + todate +
                "&type=" + type + "&company=" + company;
    }

    // Done
    public String getLogin(String user, String pass) {
        StringBuilder sb = new StringBuilder();
        sb.append(URL_BASE);
        sb.append(URL_LOGIN);
        sb.append("/");
        sb.append(user);
        sb.append("/");
        sb.append(pass);

        return sb.toString();
    }

    // Done
    public String getAttendanceDaily(String dateNow, String dateFrom, String dateTo) {
        StringBuilder sb = new StringBuilder();
        sb.append(URL_BASE);
        sb.append(URL_ATTENDANCE_DAILY);
        sb.append("?dateNow=");
        sb.append(dateNow);
        sb.append("&dateFrom=");
        sb.append(dateFrom);
        sb.append("&dateTo=");
        sb.append(dateTo);

        return sb.toString();
    }

    // Done
    public String getPayrollOne(String year) {
        StringBuilder sb = new StringBuilder();
        sb.append(URL_BASE);
        sb.append(URL_PAYROLL_DETAIL);
        sb.append(year);

        return sb.toString();
    }

    // Done
    public String getPayrollFilterYear() {
        return URL_BASE + URL_PAYROLL_FILTER + "/year";
    }

    // Done
    public String getPayrollFilterCompany() {
        return URL_BASE + URL_PAYROLL_FILTER + "/company";
    }

    // Done
    public String getIndividualPayroll(String empid) {
        return URL_BASE + URL_INDIVIDUAL_PAYROLL + empid;
    }

    // Done
    public String getEmployeeProfile(String empid) {
        return URL_BASE + URL_EMPLOYEE_PROFILE + "/" + empid;
    }

    // Done
    public String getEmployeeList() {
        return URL_BASE + URL_EMPLOYEE_PROFILE;
    }


    // Done
    public String getAppDetail() {
        return URL_BASE + URL_APP_DETAIL;
    }

    // Done
    public String getEmployeeByList(String empid) {
        return URL_BASE + URL_APP_SUB_DETAIL + empid;
    }

    // Done
    public String getIndividualAppraisal(String empid) {
        return URL_BASE + URL_INDIVIDUAL_APPRAISAL + empid;
    }

    // Done
    public String getHrisDashboard(String compid) {
        return URL_BASE + URL_HRIS_DASHBOARD + compid;
    }

    // Done
    public String getBulletin() {
        return URL_BASE + URL_BULLETIN;
    }

    // Done
    public String getEmpItinerary(String emp_id, String startDate, String endDate) {
        return URL_BASE + URL_EMPLOYEE_ITINERARY + "?emp_id=" + emp_id + "&startDate=" + startDate + "&endDate=" + endDate;
    }
}
