package ph.net.swak.attendanceinsights.network;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.annotation.Nullable;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import ph.net.swak.attendanceinsights.R;

import static android.content.Context.MODE_PRIVATE;

public class TokenAuthenticator implements Authenticator {

    private AccessTokenResult accessTokenResult;
    private Context mContext;

    public TokenAuthenticator(Context context) {
        this.mContext = context;
    }

    @Nullable
    @Override
    public Request authenticate(Route route, Response response) throws IOException {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(mContext.getString(R.string.preference_file_key), MODE_PRIVATE);
        String username = sharedPreferences.getString("username", "");
        String password = sharedPreferences.getString("password", "");
        String endpoint = "http://mdrpayroll.swak.net.ph/token";

        boolean tokenResult = accessToken(endpoint, username, password);

        if (tokenResult) {
            String newAccess = accessTokenResult.accessToken;

            return response.request().newBuilder()
                    .header("Authorization", "Bearer " + newAccess)
                    .build();

        } else {
            return null;
        }
    }

    public boolean accessToken(String endpoint, String username, String password) throws IOException {
        URL refreshUrl = new URL(endpoint);
        HttpURLConnection urlConnection = (HttpURLConnection) refreshUrl.openConnection();
        urlConnection.setDoInput(true);
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        urlConnection.setUseCaches(false);

        String urlParam = "grant_type=password&client_type=1&client_id=Android&client_secret=swakAndroid_webApi_2017&username=" + username + "&password=" + password;

        urlConnection.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
        wr.writeBytes(urlParam);
        wr.flush();
        wr.close();

        int responseCode = urlConnection.getResponseCode();

        if (responseCode == 200) {
            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

            Gson gson = new Gson();
            accessTokenResult = gson.fromJson(response.toString(), AccessTokenResult.class);

            return true;
        } else {
            return false;
        }
    }
}
