package ph.net.swak.attendanceinsights.ui.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mikepenz.fastadapter.items.AbstractItem;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.events.UserDetailEvent;
import ph.net.swak.attendanceinsights.network.VolleyRequest;

public class EmployeeFastAdapter extends AbstractItem<EmployeeFastAdapter, EmployeeFastAdapter.ViewHolder> {

    private String fname;
    private String lname;
    private String imgPath;
    private int empId;

    public String getFname() {
        return fname;
    }

    public EmployeeFastAdapter withFname(String fname) {
        this.fname = fname;
        return this;
    }

    public String getLname() {
        return lname;
    }

    public EmployeeFastAdapter withLname(String lname) {
        this.lname = lname;
        return this;
    }

    public String getImgPath() {
        return imgPath;
    }

    public EmployeeFastAdapter withImgPath(String imgPath) {
        this.imgPath = imgPath;
        return this;
    }

    public int getEmpId() {
        return empId;
    }

    public EmployeeFastAdapter withEmpId(int empId) {
        this.empId = empId;
        return this;
    }

    @Override
    public int getType() {
        return R.id.fastadapter_employee_list_item_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.viewgroup_employee_listing;
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);
        holder.textViewEmpId.setText(String.valueOf(getEmpId()));
        holder.textViewName.setText(getLname() + ", " + getFname());
        RequestOptions options = new RequestOptions().centerCrop();

        Glide.with(holder.itemView.getContext()).load(VolleyRequest.URL_BASE + getImgPath()).apply(options)
                .apply(RequestOptions.bitmapTransform(new CropCircleTransformation(holder.itemView.getContext())))
                .into(holder.imageView);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new UserDetailEvent(String.valueOf(getEmpId())));
            }
        });

    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);

        holder.textViewEmpId.setText(null);
        holder.textViewName.setText(null);
        holder.imageView.setImageDrawable(null);
    }

    @Override
    public ViewHolder getViewHolder(View view) {
        return new ViewHolder(view);
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageview_employee_listing)
        protected ImageView imageView;

        @BindView(R.id.textview_employee_listing_empid)
        protected TextView textViewEmpId;

        @BindView(R.id.textview_employee_listing_name)
        protected TextView textViewName;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
