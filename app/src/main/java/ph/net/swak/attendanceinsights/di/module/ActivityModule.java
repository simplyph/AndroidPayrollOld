package ph.net.swak.attendanceinsights.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;

@Module
public abstract class ActivityModule {

    @ContributesAndroidInjector(modules = MainFragmentModule.class)
    abstract MainActivity contributeMainActivity();

}
