package ph.net.swak.attendanceinsights.ui.fragments.lifecycle;


import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.actionitembadge.library.ActionItemBadge;
import com.mikepenz.actionitembadge.library.ActionItemBadgeAdder;
import com.mikepenz.actionitembadge.library.utils.BadgeStyle;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.db.realm.RealmHrisQuick;
import ph.net.swak.attendanceinsights.db.realm.RealmPayrollQuick;
import ph.net.swak.attendanceinsights.di.Injectable;
import ph.net.swak.attendanceinsights.di.PayrollAnalyticsComponent;
import ph.net.swak.attendanceinsights.models.PayrollQuickList;
import ph.net.swak.attendanceinsights.repository.Resource;
import ph.net.swak.attendanceinsights.repository.Status;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.HrisQuickFastAdapter;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.PayrollQuickFastAdapter;
import ph.net.swak.attendanceinsights.viewmodel.HomeViewModel;

public class HomeFragment extends LifecycleFragment implements Injectable {

    public static final String TAG = "HomeViewModel";

    private HomeViewModel viewModel;

    @BindView(R.id.textview_subtitle_payroll)
    protected TextView textViewSub;

    @BindView(R.id.recyclerview_home_attendance_quick)
    protected RecyclerView rvAttendance;

    @BindView(R.id.recyclerview_home_payroll_quick)
    protected RecyclerView rvPayroll;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private FastItemAdapter<HrisQuickFastAdapter> mFastItemAdapterAttendance;
    private FastItemAdapter<PayrollQuickFastAdapter> mFastItemAdapterPayroll;

    private Unbinder unbinder;
    private MainActivity mainActivity;

    public HomeFragment() { }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel.class);

        int prevMonth = 5;
        int prevYear = 2017;
        int presentMonth = 6;
        int presentYear = 2017;

        viewModel.updateLiveData(prevMonth, prevYear, presentMonth, presentYear);
        viewModel.getPayrollQuick().observe(this, listResource -> {
            if(listResource != null && listResource.status == Status.SUCCESS) {
                List<PayrollQuickFastAdapter> payrollQuickFastAdapter = new ArrayList<>();
                List<PayrollQuickList> presentList = new ArrayList<>();
                List<PayrollQuickList> prevList = new ArrayList<>();

                if (listResource.data != null) {
                    double totalPrevPayroll = 0;
                    double totalPresentPayroll = 0;

                    for(RealmPayrollQuick realmPayrollQuick : listResource.data) {
                        if(realmPayrollQuick.getCoveringMonth() == prevMonth) {
                            totalPrevPayroll += realmPayrollQuick.getTotalEmployee();
                            prevList.add(new PayrollQuickList()
                                    .withCompName(realmPayrollQuick.getCompName())
                                    .withTotalEmployee(realmPayrollQuick.getTotalEmployee())
                                    .withTotalManager(realmPayrollQuick.getTotalManager())
                                    .withTotalStaff(realmPayrollQuick.getTotalStaff())
                                    .withTotalSupervisor(realmPayrollQuick.getTotalSupervisor())
                                    .withCountEmployee(realmPayrollQuick.getCountEmployee())
                                    .withCountManager(realmPayrollQuick.getCountManager())
                                    .withCountSupervisor(realmPayrollQuick.getCountSupervsior())
                                    .withCountStaff(realmPayrollQuick.getCountStaff())
                            );
                        }
                        if(realmPayrollQuick.getCoveringMonth() == presentMonth) {
                            totalPresentPayroll += realmPayrollQuick.getTotalEmployee();
                            presentList.add(new PayrollQuickList()
                                    .withCompName(realmPayrollQuick.getCompName())
                                    .withTotalEmployee(realmPayrollQuick.getTotalEmployee())
                                    .withTotalManager(realmPayrollQuick.getTotalManager())
                                    .withTotalStaff(realmPayrollQuick.getTotalStaff())
                                    .withTotalSupervisor(realmPayrollQuick.getTotalSupervisor())
                                    .withCountEmployee(realmPayrollQuick.getCountEmployee())
                                    .withCountManager(realmPayrollQuick.getCountManager())
                                    .withCountSupervisor(realmPayrollQuick.getCountSupervsior())
                                    .withCountStaff(realmPayrollQuick.getCountStaff())
                            );
                        }
                    };

                    payrollQuickFastAdapter.add(new PayrollQuickFastAdapter()
                            .withCovMonth(presentMonth)
                            .withCovYear(presentYear)
                            .withTotalPayroll(totalPresentPayroll)
                            .withCompListData(presentList)
                    );

                    payrollQuickFastAdapter.add(new PayrollQuickFastAdapter()
                            .withCovMonth(prevMonth)
                            .withCovYear(prevMonth)
                            .withTotalPayroll(totalPrevPayroll)
                            .withCompListData(prevList)
                    );
                }

                mFastItemAdapterPayroll.setNewList(payrollQuickFastAdapter);
            }
        });

        viewModel.getHrisQuick().observe(this, listResource -> {
            if(listResource != null && listResource.status == Status.SUCCESS) {
                List<HrisQuickFastAdapter> hrisQuickFastAdapters = new ArrayList<>();
                int totalEmployee = 0;

                if (listResource.data != null) {
                    for(RealmHrisQuick realmHrisQuick : listResource.data) {
                        totalEmployee += realmHrisQuick.getQtyEmployee();

                        hrisQuickFastAdapters.add(new HrisQuickFastAdapter()
                                .withCompId(realmHrisQuick.getCompId())
                                .withCompName(realmHrisQuick.getCompName())
                                .withTotalEmployee(realmHrisQuick.getQtyEmployee())
                                .withTotalManager(realmHrisQuick.getQtyManager())
                                .withTotalSupervisor(realmHrisQuick.getQtySupervisor())
                                .withTotalStaff(realmHrisQuick.getQtyStaff())
                        );
                    }
                }

                if(totalEmployee == 0 || totalEmployee == 1 || totalEmployee < 0) {
                    textViewSub.setText(String.format(Locale.ENGLISH, "Total of %d Employee", totalEmployee));
                } else {
                    textViewSub.setText(String.format(Locale.ENGLISH, "Total of %d Employees", totalEmployee));
                }
                mFastItemAdapterAttendance.setNewList(hrisQuickFastAdapters);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home2, container, false);
        unbinder = ButterKnife.bind(this, view);
        mainActivity = (MainActivity) getActivity();
        mainActivity.setupUI("Payroll Analytics", false, false, false);
        //setHasOptionsMenu(true);

        mFastItemAdapterAttendance = new FastItemAdapter<>();
        rvAttendance.setAdapter(mFastItemAdapterAttendance);
        rvAttendance.setItemAnimator(new DefaultItemAnimator());
        rvAttendance.setLayoutManager(new LinearLayoutManager(getContext()));
        mFastItemAdapterPayroll = new FastItemAdapter<>();
        rvPayroll.setAdapter(mFastItemAdapterPayroll);
        rvPayroll.setItemAnimator(new DefaultItemAnimator());
        rvPayroll.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_notif, menu);

        int badgeCount = 12;

        if(badgeCount > 0) {
            ActionItemBadge.update(mainActivity, menu.findItem(R.id.app_bar_notif),
                    GoogleMaterial.Icon.gmd_notifications,
                    new BadgeStyle(BadgeStyle.Style.DEFAULT, R.layout.menu_action_item_badge_smaller, Color.parseColor("#ff0303"), Color.parseColor("#c7c7c7"), Color.WHITE), badgeCount);
        } else {
            ActionItemBadge.hide(menu.findItem(R.id.app_bar_notif));
        }
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
