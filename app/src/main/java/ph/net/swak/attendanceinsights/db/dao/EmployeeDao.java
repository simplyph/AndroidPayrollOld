package ph.net.swak.attendanceinsights.db.dao;

import android.arch.lifecycle.LiveData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.Provides;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import ph.net.swak.attendanceinsights.db.realm.LiveRealmData;
import ph.net.swak.attendanceinsights.db.realm.RealmEmployee;
import ph.net.swak.attendanceinsights.db.realm.RealmHrisQuick;
import ph.net.swak.attendanceinsights.db.realm.RealmPayrollQuick;
import ph.net.swak.attendanceinsights.di.scope.ApplicationScope;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.HrisCompanyDetailFastAdapter;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.HrisCompanyFastAdapter;

import static ph.net.swak.attendanceinsights.utils.RealmUtils.asLiveData;

/**
 * All about realm, persisting and collecting data from local storage
 */
public class EmployeeDao {

    private Realm mRealm;

    public EmployeeDao() {
        mRealm = Realm.getDefaultInstance();
    }

    public LiveRealmData<RealmEmployee> getAllEmployees() {
        return asLiveData(mRealm.where(RealmEmployee.class)
                .findAllAsync());
    }

    public LiveData<List<HrisCompanyFastAdapter>> getEmployeesByCompany() {
        RealmResults<RealmEmployee> results = mRealm.where(RealmEmployee.class).equalTo("compStatus", 1).distinct("compId").sort("compName", Sort.ASCENDING);

        List<HrisCompanyFastAdapter> hrisCompanies = new ArrayList<>();

        hrisCompanies.add(new HrisCompanyFastAdapter()
                .withCompName("Total Employee")
                .withCompId(0)
                .withTotalEmployee((int) mRealm.where(RealmEmployee.class).equalTo("compStatus", 1).equalTo("statusType", "active", Case.INSENSITIVE).count())
                .withTotalManager((int) mRealm.where(RealmEmployee.class).equalTo("compStatus", 1).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("positionName", "manager", Case.INSENSITIVE).count())
                .withTotalSupervisor((int) mRealm.where(RealmEmployee.class).equalTo("compStatus", 1).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("positionName", "supervisor", Case.INSENSITIVE).count())
                .withTotalStaff((int) mRealm.where(RealmEmployee.class).equalTo("compStatus", 1).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("positionName", "regular staff", Case.INSENSITIVE).count())
                .withFrom(0)
                .withFromId(0)
        );

        for (RealmEmployee realmEmployee : results) {
            hrisCompanies.add(new HrisCompanyFastAdapter()
                    .withCompName(realmEmployee.getCompName())
                    .withCompId(realmEmployee.getCompId())
                    .withTotalEmployee((int) mRealm.where(RealmEmployee.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("compId", realmEmployee.getCompId()).count())
                    .withTotalManager((int) mRealm.where(RealmEmployee.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("compId", realmEmployee.getCompId()).equalTo("positionName", "manager", Case.INSENSITIVE).count())
                    .withTotalSupervisor((int) mRealm.where(RealmEmployee.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("compId", realmEmployee.getCompId()).equalTo("positionName", "supervisor", Case.INSENSITIVE).count())
                    .withTotalStaff((int) mRealm.where(RealmEmployee.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("compId", realmEmployee.getCompId()).equalTo("positionName", "regular staff", Case.INSENSITIVE).count())
                    .withFrom(0)
                    .withFromId(0)
            );
        }

        return new LiveData<List<HrisCompanyFastAdapter>>() {
            @Override
            protected void onActive() {
                super.onActive();
                postValue(hrisCompanies);
            }
        };
    }

    public LiveRealmData<RealmEmployee> getEmployeesByCompAndPos(int compId, int posId, int from, int fromId) {
        RealmQuery<RealmEmployee> query = mRealm.where(RealmEmployee.class)
                .equalTo("compStatus", 1)
                .equalTo("statusType", "active", Case.INSENSITIVE)
                .equalTo("positionId", posId);

        if (compId != 0)
            query.equalTo("compId", compId);

        if (from == 2) {
            query.equalTo("deptId", fromId);
        } else if (from == 1) {
            query.equalTo("divId", fromId);
        }

        return asLiveData(query.findAllSortedAsync("lName", Sort.ASCENDING, "fName", Sort.ASCENDING));
    }

    public LiveData<List<HrisCompanyDetailFastAdapter>> getEmployeesByCompAndType(int compId, int type) {
        RealmQuery<RealmEmployee> query = mRealm.where(RealmEmployee.class);
        RealmResults<RealmEmployee> results;

        if (compId != 0) {
            query.equalTo("compId", compId);
        } else {
            query.equalTo("compStatus", 1);
        }

        switch (type) {
            case 2:
                results = query.distinct("deptId").sort("deptName", Sort.ASCENDING);
                break;
            case 1:
                results = query.distinct("divId").sort("divName", Sort.ASCENDING);
                break;
            case 0:
            default:
                results = query.distinct("compId");
                break;
        }

        List<HrisCompanyDetailFastAdapter> hrisCompanies = new ArrayList<>();

        if (type == 0) {
            if (compId == 0) {
                hrisCompanies.add(new HrisCompanyDetailFastAdapter()
                        .withCompName("Total Employee")
                        .withCompId(0)
                        .withTotalEmployee((int) mRealm.where(RealmEmployee.class).equalTo("compStatus", 1).equalTo("statusType", "active", Case.INSENSITIVE).count())
                        .withTotalManager((int) mRealm.where(RealmEmployee.class).equalTo("compStatus", 1).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("positionName", "manager", Case.INSENSITIVE).count())
                        .withTotalSupervisor((int) mRealm.where(RealmEmployee.class).equalTo("compStatus", 1).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("positionName", "supervisor", Case.INSENSITIVE).count())
                        .withTotalStaff((int) mRealm.where(RealmEmployee.class).equalTo("compStatus", 1).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("positionName", "regular staff", Case.INSENSITIVE).count())
                        .withFrom(0)
                        .withFromId(0)
                );
            } else {
                hrisCompanies.add(new HrisCompanyDetailFastAdapter()
                        .withCompName(results.get(0).getCompName())
                        .withCompId(results.get(0).getCompId())
                        .withTotalEmployee((int) mRealm.where(RealmEmployee.class).equalTo("compId", compId).equalTo("compStatus", 1).equalTo("statusType", "active", Case.INSENSITIVE).count())
                        .withTotalManager((int) mRealm.where(RealmEmployee.class).equalTo("compId", compId).equalTo("compStatus", 1).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("positionName", "manager", Case.INSENSITIVE).count())
                        .withTotalSupervisor((int) mRealm.where(RealmEmployee.class).equalTo("compId", compId).equalTo("compStatus", 1).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("positionName", "supervisor", Case.INSENSITIVE).count())
                        .withTotalStaff((int) mRealm.where(RealmEmployee.class).equalTo("compId", compId).equalTo("compStatus", 1).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("positionName", "regular staff", Case.INSENSITIVE).count())
                        .withFrom(0)
                        .withFromId(0)
                );
            }

        } else {
            for (RealmEmployee realmEmployee : results) {
                if(type == 2) {
                    hrisCompanies.add(new HrisCompanyDetailFastAdapter()
                            .withCompName(realmEmployee.getDeptName())
                            .withCompId(realmEmployee.getCompId())
                            .withTotalEmployee((int) mRealm.where(RealmEmployee.class).equalTo("deptId", realmEmployee.getDeptId()).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("compId", realmEmployee.getCompId()).count())
                            .withTotalManager((int) mRealm.where(RealmEmployee.class).equalTo("deptId", realmEmployee.getDeptId()).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("compId", realmEmployee.getCompId()).equalTo("positionName", "manager", Case.INSENSITIVE).count())
                            .withTotalSupervisor((int) mRealm.where(RealmEmployee.class).equalTo("deptId", realmEmployee.getDeptId()).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("compId", realmEmployee.getCompId()).equalTo("positionName", "supervisor", Case.INSENSITIVE).count())
                            .withTotalStaff((int) mRealm.where(RealmEmployee.class).equalTo("deptId", realmEmployee.getDeptId()).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("compId", realmEmployee.getCompId()).equalTo("positionName", "regular staff", Case.INSENSITIVE).count())
                            .withFrom(type)
                            .withFromId(realmEmployee.getDeptId())
                    );
                } else if(type == 1) {
                    hrisCompanies.add(new HrisCompanyDetailFastAdapter()
                            .withCompName(realmEmployee.getDivName())
                            .withCompId(realmEmployee.getCompId())
                            .withTotalEmployee((int) mRealm.where(RealmEmployee.class).equalTo("divId", realmEmployee.getDivId()).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("compId", realmEmployee.getCompId()).count())
                            .withTotalManager((int) mRealm.where(RealmEmployee.class).equalTo("divId", realmEmployee.getDivId()).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("compId", realmEmployee.getCompId()).equalTo("positionName", "manager", Case.INSENSITIVE).count())
                            .withTotalSupervisor((int) mRealm.where(RealmEmployee.class).equalTo("divId", realmEmployee.getDivId()).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("compId", realmEmployee.getCompId()).equalTo("positionName", "supervisor", Case.INSENSITIVE).count())
                            .withTotalStaff((int) mRealm.where(RealmEmployee.class).equalTo("divId", realmEmployee.getDivId()).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("compId", realmEmployee.getCompId()).equalTo("positionName", "regular staff", Case.INSENSITIVE).count())
                            .withFrom(type)
                            .withFromId(realmEmployee.getDivId())
                    );
                }
            }
        }


        return new LiveData<List<HrisCompanyDetailFastAdapter>>() {
            @Override
            protected void onActive() {
                super.onActive();
                postValue(hrisCompanies);
            }
        };
    }

    public LiveRealmData<RealmEmployee> getEmployeeById(int employeeId) {
        return asLiveData(mRealm.where(RealmEmployee.class)
                .equalTo("employeeId", employeeId)
                .findAllAsync());
    }

    public LiveRealmData<RealmHrisQuick> getHrisQuick() {
        return asLiveData(mRealm.where(RealmHrisQuick.class)
                .findAllAsync());
    }

    public void saveEmployees(List<RealmEmployee> realmEmployees) {
        mRealm.executeTransaction(realm -> {
            for (RealmEmployee realmEmployee : realmEmployees) {
                if (realmEmployee.getEmployeeId() != -1) {
                    realm.copyToRealmOrUpdate(realmEmployee);
                }
            }
        });
    }

    public void saveHrisQuick(List<RealmHrisQuick> realmHrisQuicks) {
        mRealm.executeTransaction(realm -> {
            for (RealmHrisQuick realmHrisQuick : realmHrisQuicks) {
                if(realmHrisQuick.getCompId() != -1) {
                    realm.copyToRealmOrUpdate(realmHrisQuick);
                }
            }
        });
    }
}
