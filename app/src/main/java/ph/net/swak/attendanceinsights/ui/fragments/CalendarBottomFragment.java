package ph.net.swak.attendanceinsights.ui.fragments;

import android.app.Dialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.models.RealmFilterParticulars;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.utils.DateUtil;

/**
 * Created by Knowell on 2/13/2017.
 */

public class CalendarBottomFragment extends BottomSheetDialogFragment {

    @BindView(R.id.calendarview_bottom_sheet_filter_range)
    protected MaterialCalendarView calendarPickerView;

    @BindView(R.id.progress_bar_bottom_sheet_filter_range)
    protected MaterialProgressBar materialProgressBar;

    @BindView(R.id.textview_bottom_sheet_filter_range_info)
    protected TextView textViewNoData;

    private Unbinder unbinder;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private MainActivity mainActivity;
    private Realm mRealm;

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        mRealm = Realm.getDefaultInstance();
        mainActivity = (MainActivity) getContext();
        View view = View.inflate(getContext(), R.layout.fragment_bottom_sheet_calendar, null);
        unbinder = ButterKnife.bind(this, view);
        requestQueue = VolleyAPI.getInstance(getContext()).getRequestQueue();
        volleyRequest = new VolleyRequest();

        materialProgressBar.setVisibility(View.GONE);
        textViewNoData.setVisibility(View.GONE);
        calendarPickerView.setVisibility(View.VISIBLE);

        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);

        Date today = new Date();

        calendarPickerView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_RANGE);

        dialog.setContentView(view);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @OnClick(R.id.button_bottom_sheet_filter_range_close)
    protected void onCloseClick() {
        dismiss();
    }

    @OnClick(R.id.button_bottom_sheet_filter_range_select)
    protected void onDisplayClick() {
        StringBuilder firstDate = new StringBuilder();
        StringBuilder secondDate = new StringBuilder();

        List<CalendarDay> calendarDays = calendarPickerView.getSelectedDates();

        if(calendarDays.size() == 0) {
            Toast.makeText(getContext(), "Please select a Date Range", Toast.LENGTH_SHORT).show();
            return;
        }

        if (calendarDays.size() == 1) {
            firstDate.append(calendarDays.get(0).getMonth()+1);
            firstDate.append(" ");
            firstDate.append(calendarDays.get(0).getDay());
            firstDate.append(" ");
            firstDate.append(calendarDays.get(0).getYear());

            secondDate.append(calendarDays.get(0).getMonth()+1);
            secondDate.append(" ");
            secondDate.append(calendarDays.get(0).getDay());
            secondDate.append(" ");
            secondDate.append(calendarDays.get(0).getYear());
        } else if (calendarDays.size() > 1) {
            if(calendarDays.get(0).isBefore(calendarDays.get(1))) {
                firstDate.append(calendarDays.get(0).getMonth()+1);
                firstDate.append(" ");
                firstDate.append(calendarDays.get(0).getDay());
                firstDate.append(" ");
                firstDate.append(calendarDays.get(0).getYear());

                secondDate.append(calendarDays.get(1).getMonth()+1);
                secondDate.append(" ");
                secondDate.append(calendarDays.get(1).getDay());
                secondDate.append(" ");
                secondDate.append(calendarDays.get(1).getYear());
            } else {
                firstDate.append(calendarDays.get(1).getMonth()+1);
                firstDate.append(" ");
                firstDate.append(calendarDays.get(1).getDay());
                firstDate.append(" ");
                firstDate.append(calendarDays.get(1).getYear());

                secondDate.append(calendarDays.get(0).getMonth()+1);
                secondDate.append(" ");
                secondDate.append(calendarDays.get(0).getDay());
                secondDate.append(" ");
                secondDate.append(calendarDays.get(0).getYear());
            }
        }
        DateUtil dateUtil = new DateUtil();

        final String dateString1st = dateUtil.formatDate(firstDate.toString(), "M d y", "MMM d, yyyy");
        final String dateString2nd = dateUtil.formatDate(secondDate.toString(), "M d y", "MMM d, yyyy");
        final String dateStringHead = dateUtil.formatDate(firstDate.toString(), "M d y", "MMM d") + " - " + dateUtil.formatDate(secondDate.toString(), "M d y", "MMM d");

        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(RealmFilterParticulars.class);

                RealmFilterParticulars realmFilterParticulars = new RealmFilterParticulars();
                realmFilterParticulars
                        .withFirstDate(dateString1st)
                        .withSecondDate(dateString2nd);

                realm.copyToRealm(realmFilterParticulars);
            }
        });

        mainActivity.mFilterRangeContent.setText(dateString1st + " - " + dateString2nd);
        mainActivity.mFilterHeadYear.setText(dateStringHead);

        dismiss();
    }
}
