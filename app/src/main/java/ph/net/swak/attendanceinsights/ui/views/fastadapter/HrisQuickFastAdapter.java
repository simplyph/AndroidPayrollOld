package ph.net.swak.attendanceinsights.ui.views.fastadapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.utils.Helper;
import timber.log.Timber;

public class HrisQuickFastAdapter extends AbstractItem<HrisQuickFastAdapter, HrisQuickFastAdapter.ViewHolder> {

    private int compId;
    private String compName;
    private int totalEmployee;
    private int totalManager;
    private int totalSupervisor;
    private int totalStaff;

    public int getCompId() {
        return compId;
    }

    public HrisQuickFastAdapter withCompId(int compId) {
        this.compId = compId;
        return this;
    }

    public String getCompName() {
        return compName;
    }

    public HrisQuickFastAdapter withCompName(String compName) {
        this.compName = compName;
        return this;
    }

    public int getTotalEmployee() {
        return totalEmployee;
    }

    public HrisQuickFastAdapter withTotalEmployee(int totalEmployee) {
        this.totalEmployee = totalEmployee;
        return this;
    }

    public int getTotalManager() {
        return totalManager;
    }

    public HrisQuickFastAdapter withTotalManager(int totalManager) {
        this.totalManager = totalManager;
        return this;
    }

    public int getTotalSupervisor() {
        return totalSupervisor;
    }

    public HrisQuickFastAdapter withTotalSupervisor(int totalSupervisor) {
        this.totalSupervisor = totalSupervisor;
        return this;
    }

    public int getTotalStaff() {
        return totalStaff;
    }

    public HrisQuickFastAdapter withTotalStaff(int totalStaff) {
        this.totalStaff = totalStaff;
        return this;
    }

    @Override
    public HrisQuickFastAdapter.ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return 0;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.item_home_simple_list;
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);

        holder.tvName.setText(Helper.toTitleCase(getCompName()));
        holder.tvValue.setText(String.valueOf(getTotalEmployee()));
        holder.itemView.setOnClickListener(v -> new MaterialDialog.Builder(holder.itemView.getContext())
                .title(getCompName())
                .content(String.format(Locale.ENGLISH, "Total of %d Employees\nTotal of %d Manager\nTotal of %d Supervisor\nTotal of %d Staff\n", getTotalEmployee(), getTotalManager(), getTotalSupervisor(), getTotalStaff()))
                .positiveText(R.string.dialog_prompt_ok)
                .build().show());
    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);

        holder.tvName.setText(null);
        holder.tvValue.setText(null);
        holder.itemView.setOnClickListener(null);
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textview_home_simple_list_name)
        protected TextView tvName;

        @BindView(R.id.textview_home_simple_list_value)
        protected TextView tvValue;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
