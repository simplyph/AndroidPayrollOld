package ph.net.swak.attendanceinsights.utils;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

/**
 * Created by Knowell on 1/19/2017.
 */

public class MonthValueFormatter implements IAxisValueFormatter {

    private String[] month = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return month[(int)value - 1];
    }
}
