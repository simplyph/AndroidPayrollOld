package ph.net.swak.attendanceinsights.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ph.net.swak.attendanceinsights.models.FileItinerary;
import ph.net.swak.attendanceinsights.models.RealmAppDetail;
import ph.net.swak.attendanceinsights.models.RealmAppSubDetail;
import ph.net.swak.attendanceinsights.models.RealmAttParticular;
import ph.net.swak.attendanceinsights.models.RealmAttendanceNow;
import ph.net.swak.attendanceinsights.models.RealmBulletinInfo;
import ph.net.swak.attendanceinsights.models.RealmEmployeeList;
import ph.net.swak.attendanceinsights.models.RealmEmployeeProfile;
import ph.net.swak.attendanceinsights.models.RealmFilterComp;
import ph.net.swak.attendanceinsights.models.RealmFilterYear;
import ph.net.swak.attendanceinsights.models.RealmHrisDashboard;
import ph.net.swak.attendanceinsights.models.RealmIndividualAppraisal;
import ph.net.swak.attendanceinsights.models.RealmIndividualPayroll;
import ph.net.swak.attendanceinsights.models.RealmPayroll;
import ph.net.swak.attendanceinsights.models.User;
import ph.net.swak.attendanceinsights.network.VolleyRequest;

public class JSONParser {

    //region Description
    /*

    public static List<Attendance> parseAttendance(JSONArray jsonArray, String filter) {
        try {
            List<Attendance> attendanceList = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);
                Attendance attendance = new Attendance();

                if (jsonObjects.getString("fullname").toLowerCase().trim().contains(filter.toLowerCase().trim())) {
                    attendance.setName(jsonObjects.getString("fullname"));
                    attendance.setEmpId(jsonObjects.getString("emp_id"));
                    attendance.setDetails(jsonObjects.getString("absent"));
                    attendanceList.add(attendance);
                }
            }

            return attendanceList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<Filter> parseFilter(JSONArray jsonArray, @Nullable Filter.FilterTypes filterTypes) {
        try {
            List<Filter> filterList = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);
                Filter filter = new Filter();

                filter.setId(jsonObjects.getInt("id"));
                filter.setName(jsonObjects.getString("name"));

                filterList.add(filter);
            }

            return filterList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<Chart> parseChartData(JSONArray jsonArray) {
        try {
            List<Chart> chartList = new ArrayList<>();

            for (int i = 1; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);
                Chart chart = new Chart();

                chart.setEmpid(jsonObjects.getString("emp_id"));
                chart.setDateHired(jsonObjects.getString("hired_date"));
                chart.setDateTerminated(jsonObjects.getString("date_terminated"));
                chart.setAge(jsonObjects.getInt("age"));
                chart.setEmploymentStatus(jsonObjects.getString("status_desc"));
                chart.setSalaryRate(jsonObjects.getDouble("salary_rate"));
                chart.setfName(jsonObjects.getString("firstname"));
                chart.setlName(jsonObjects.getString("lastname"));
                chart.setDesignation(jsonObjects.getString("designation"));
                chart.setDepartment(jsonObjects.getString("department"));
                chart.setCompany(jsonObjects.getString("comp_name"));
                chart.setGender(jsonObjects.getString("gender"));
                chart.setActive(jsonObjects.getString("status_type"));

                chartList.add(chart);
            }

            return chartList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<Payroll> parseGrossPayroll(JSONArray jsonArray) {
        try {
            List<Payroll> chartList = new ArrayList<>();

            for (int i = 1; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);
                Payroll chart = new Payroll();

                chart.setCoveringYear(jsonObjects.getInt("coveringyear"));
                chart.setCoveringMonth(jsonObjects.getInt("coveringmonth2"));
                chart.setTotalEmployee(jsonObjects.getInt("total_employee"));
                chart.setTotalCompany(jsonObjects.getInt("total_company"));
                chart.setGross(jsonObjects.getDouble("total_income"));
                chart.setNetpay(jsonObjects.getDouble("total_netpay"));
                chart.setDeduction(jsonObjects.getDouble("total_deduction"));

                chartList.add(chart);
            }

            return chartList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<Payroll> parseLastPayroll(JSONArray jsonArray) {
        try {
            List<Payroll> chartList = new ArrayList<>();

            for (int i = 1; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);
                Payroll chart = new Payroll();

                chart.setCoveringYear(jsonObjects.getInt("coveringyear"));
                chart.setGross(jsonObjects.getDouble("gross"));
                chart.setDeduction(jsonObjects.getDouble("deduction"));
                chart.setNetpay(jsonObjects.getDouble("netpay"));

                chartList.add(chart);
            }

            return chartList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<AttendanceDashboard> parseAttendanceMonthly(JSONArray jsonArray) {
        try {
            List<AttendanceDashboard> chartList = new ArrayList<>();

            for (int i = 1; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);
                AttendanceDashboard chart = new AttendanceDashboard();

                chart.setEmpId(jsonObjects.getString("emp_id"));
                chart.setFullname(jsonObjects.getString("fullname"));
                chart.setLate(jsonObjects.getDouble("late"));
                chart.setEarly(jsonObjects.getDouble("early"));
                chart.setAbsent(jsonObjects.getDouble("absent"));
                chart.setCoveringDay(jsonObjects.getString("covering_day"));
                chart.setLeave(jsonObjects.getDouble("leave"));
                chart.setTardiness(jsonObjects.getDouble("tardiness"));

                chartList.add(chart);
            }

            return chartList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<PayrollDetail> parsePayrollDetail(JSONArray jsonArray) {
        try {
            List<PayrollDetail> payrollDetailList = new ArrayList<>();

            for (int i = 1; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);
                PayrollDetail payrollDetail = new PayrollDetail();

                JSONGetter jsonGetter = new JSONGetter(jsonObjects);

                payrollDetail.setPaycode(jsonGetter.getInt("autoid"));
                payrollDetail.setPaytype(jsonGetter.getString("paytype"));
                payrollDetail.setFromdate(jsonGetter.getString("fromdate"));
                payrollDetail.setTodate(jsonGetter.getString("todate"));
                payrollDetail.setWeek(jsonGetter.getString("week"));
                payrollDetail.setClosed(jsonGetter.getString("status"));
                payrollDetail.setCompName(jsonGetter.getString("comp_name"));
                payrollDetail.setCompId(jsonGetter.getString("comp_id"));
                payrollDetail.setFirstName(jsonGetter.getString("firstname"));
                payrollDetail.setLastName(jsonGetter.getString("lastname"));
                payrollDetail.setEmpId(jsonGetter.getString("emp_id"));


                payrollDetail.setGross(jsonGetter.getDouble("total_income"));
                payrollDetail.setDeduction(jsonGetter.getDouble("total_deduction"));
                payrollDetail.setNet(jsonGetter.getDouble("total_netpay"));
                payrollDetail.setTotalCompany(jsonGetter.getInt("total_company"));
                payrollDetail.setTotalEmployee(jsonGetter.getInt("total_employee"));
                payrollDetail.setTotalManager(jsonGetter.getInt("total_manager"));
                payrollDetail.setTotalSupervisor(jsonGetter.getInt("total_supervisor"));
                payrollDetail.setTotalStaff(jsonGetter.getInt("total_staff"));
                payrollDetail.setTotalGrossManager(jsonGetter.getDouble("total_manager_gross"));
                payrollDetail.setTotalGrossSupervisor(jsonGetter.getDouble("total_supervisor_gross"));
                payrollDetail.setTotalGrossStaff(jsonGetter.getDouble("total_staff_gross"));

                payrollDetailList.add(payrollDetail);
            }

            return payrollDetailList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    */
    //endregion

    public static User parseLogin(JSONArray jsonArray) {
        try {
            JSONObject jsonObjects = jsonArray.getJSONObject(0);
            JSONGetter jsonGetter = new JSONGetter(jsonObjects);

            User user = new User();
            user.setSuccess(true);
            user.setFullname(jsonObjects.getString("fullname"));
            user.setActive(jsonObjects.getString("isactive").equalsIgnoreCase("1"));
            user.setUsername(jsonObjects.getString("username"));
            user.setRole(jsonObjects.getString("role"));
            user.setImg(jsonObjects.getString("img_path"));

            return user;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<RealmAttendanceNow> parseAttendanceDaily(JSONArray jsonArray) {
        try {
            List<RealmAttendanceNow> attendanceNows = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);
                JSONGetter jsonGetter = new JSONGetter(jsonObjects);


                attendanceNows.add(new RealmAttendanceNow()
                        .withEmpId(jsonGetter.getInt("emp_id"))
                        .withCompName(jsonGetter.getString("comp_name"))
                        .withDesigName(jsonGetter.getString("designation"))
                        .withFirstName(jsonGetter.getString("firstname"))
                        .withLastName(jsonGetter.getString("lastname"))
                        .withStatusType(jsonGetter.getString("status_type"))
                        .withSchedDesc(jsonGetter.getString("sched_desc"))
                        .withPresent(!jsonGetter.getString("isPresent").equalsIgnoreCase("absent"))
                        .withLateToday(jsonGetter.getDouble("late_min"))
                        .withTotalAbsent(jsonGetter.getDouble("total_absent"))
                        .withTotalLate(jsonGetter.getDouble("total_late"))
                        .withIdentifier(i)
                );
            }
            return attendanceNows;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<RealmFilterYear> parsePayrollFilterYear(JSONArray jsonArray) {
        try {
            List<RealmFilterYear> realmFilterYears = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);
                RealmFilterYear realmFilterYear = new RealmFilterYear();

                JSONGetter jsonGetter = new JSONGetter(jsonObjects);

                realmFilterYear.withTitle(jsonGetter.getString("coveringyear"));
                realmFilterYear.withIdentifier(jsonGetter.getInt("coveringyear"));

                realmFilterYears.add(realmFilterYear);
            }

            return realmFilterYears;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<RealmFilterComp> parsePayrollFilterComp(JSONArray jsonArray) {
        try {
            List<RealmFilterComp> realmFilterComps = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);
                RealmFilterComp realmFilterComp = new RealmFilterComp();

                JSONGetter jsonGetter = new JSONGetter(jsonObjects);

                realmFilterComp.withTitle(jsonGetter.getString("comp_name"));
                realmFilterComp.withIdentifier(jsonGetter.getInt("comp_id"));

                realmFilterComps.add(realmFilterComp);
            }

            return realmFilterComps;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<RealmPayroll> parsePayrollOne(JSONArray jsonArray) {
        try {
            List<RealmPayroll> realmPayrolls = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                JSONGetter jsonGetter = new JSONGetter(jsonObject);

                realmPayrolls.add(new RealmPayroll()
                        .withPaycodeId(jsonGetter.getInt("paycode_id"))
                        .withCovYear(jsonGetter.getInt("cov_year"))
                        .withCovMonth(jsonGetter.getInt("cov_month"))
                        .withEmpId(jsonGetter.getInt("emp_id"))
                        .withfName(jsonGetter.getString("firstname"))
                        .withlName(jsonGetter.getString("lastname"))
                        .withTotalGross(jsonGetter.getDouble("total_gross"))
                        .withTotalDeduct(jsonGetter.getDouble("total_deduct"))
                        .withTotalNet(jsonGetter.getDouble("total_net"))
                        .withManager(jsonGetter.getInt("is_manager") == 1)
                        .withSupervisor(jsonGetter.getInt("is_supervisor") == 1)
                        .withStaff(jsonGetter.getInt("is_staff") == 1)
                        .withCompId(jsonGetter.getInt("comp_id"))
                        .withCompName(jsonGetter.getString("comp_name"))
                        .withDivId(jsonGetter.getInt("div_id"))
                        .withDivName(jsonGetter.getString("div_name"))
                        .withDesigId(jsonGetter.getInt("desig_id"))
                        .withDesigName(jsonGetter.getString("desig_name"))
                        .withStatusType(jsonGetter.getString("status_type").toLowerCase())
                        .withIdentifier(i)
                );
            }

            return realmPayrolls;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<RealmIndividualPayroll> parseIndividualPayroll(JSONArray jsonArray) {
        try {
            List<RealmIndividualPayroll> realmIndividualPayrolls = new ArrayList<>();

            DateUtil dateUtil = new DateUtil();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);
                RealmIndividualPayroll realmIndividualPayroll = new RealmIndividualPayroll();

                JSONGetter jsonGetter = new JSONGetter(jsonObjects);
                StringBuilder title = new StringBuilder();

                title.append(dateUtil.getMonth(jsonGetter.getString("fromdate"), "M/d/y h:mm:ss a"));
                title.append(" ");
                title.append(dateUtil.getDay(jsonGetter.getString("fromdate"), "M/d/y h:mm:ss a"));
                title.append(" to ");
                title.append(dateUtil.getDay(jsonGetter.getString("todate"), "M/d/y h:mm:ss a"));
                title.append(", ");
                title.append(dateUtil.getYear(jsonGetter.getString("todate"), "M/d/y h:mm:ss a"));

                realmIndividualPayroll.withTitle(title.toString());
                realmIndividualPayroll.withIsIncome(jsonGetter.getInt("is_income"));
                realmIndividualPayroll.withGross(jsonGetter.getDouble("total_income"));
                realmIndividualPayroll.withDeduction(jsonGetter.getDouble("total_deduction"));
                realmIndividualPayroll.withNetpay(jsonGetter.getDouble("total_netpay"));
                realmIndividualPayroll.withEmpId(jsonGetter.getString("emp_id"));
                realmIndividualPayroll.withPaycode(jsonGetter.getInt("paycode_id"));
                realmIndividualPayroll.withAmountId(jsonGetter.getInt("amount_id"));
                realmIndividualPayroll.withAmountName(jsonGetter.getString("displayname"));
                realmIndividualPayroll.withAmount(jsonGetter.getDouble("amount"));
                realmIndividualPayroll.withIdentifier(i);

                realmIndividualPayrolls.add(realmIndividualPayroll);
            }

            return realmIndividualPayrolls;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<RealmEmployeeProfile> parseEmployeeProfile(JSONArray jsonArray) {
        try {
            List<RealmEmployeeProfile> realmEmployeeProfiles = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);
                JSONGetter jsonGetter = new JSONGetter(jsonObjects);

                realmEmployeeProfiles.add(new RealmEmployeeProfile()
                        .withEmpId(jsonGetter.getInt("emp_id"))
                        .withFname(jsonGetter.getString("fname"))
                        .withLname(jsonGetter.getString("lname"))
                        .withPosition(jsonGetter.getString("position"))
                        .withStatusId(jsonGetter.getInt("status_id"))
                        .withStatusType(jsonGetter.getString("status_type"))
                        .withStatus(jsonGetter.getString("status"))
                        .withHired(jsonGetter.getString("hired"))
                        .withTerminated(jsonGetter.getString("terminated"))
                        .withRegularization(jsonGetter.getString("regularization"))
                        .withCompId(jsonGetter.getInt("comp_id"))
                        .withCompName(jsonGetter.getString("comp_name"))
                        .withDivId(jsonGetter.getInt("div_id"))
                        .withDivName(jsonGetter.getString("div_name"))
                        .withDeptId(jsonGetter.getInt("dept_id"))
                        .withDeptName(jsonGetter.getString("dept_name"))
                        .withFuncId(jsonGetter.getInt("func_id"))
                        .withFuncName(jsonGetter.getString("func_name"))
                        .withWbaseId(jsonGetter.getInt("wbase_id"))
                        .withWbaseName(jsonGetter.getString("wbase_name"))
                        .withDesigId(jsonGetter.getInt("desig_id"))
                        .withDesigName(jsonGetter.getString("desig_name"))
                        .withReligionId(jsonGetter.getInt("religion_id"))
                        .withReligionName(jsonGetter.getString("religion_name"))
                        .withBirthdate(jsonGetter.getString("birthdate"))
                        .withCivilStatus(jsonGetter.getString("civil_status"))
                        .withGender(jsonGetter.getString("gender"))
                        .withImgPath(jsonGetter.getString("img_path"))
                );
            }

            return realmEmployeeProfiles;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<RealmEmployeeList> parseEmployeeList(JSONArray jsonArray) {
        try {
            List<RealmEmployeeList> realmEmployeeLists = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);

                JSONGetter jsonGetter = new JSONGetter(jsonObjects);

                realmEmployeeLists.add(new RealmEmployeeList()
                        .withEmpId(jsonGetter.getInt("emp_id"))
                        .withFname(jsonGetter.getString("fname"))
                        .withLname(jsonGetter.getString("lname"))
                        .withStatusType(jsonGetter.getString("status_type"))
                        .withImgPath(jsonGetter.getString("img_path"))
                        .withIdentifier(i)
                );
            }

            return realmEmployeeLists;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<RealmAttParticular> parseAttParticular(JSONArray jsonArray) {
        try {
            List<RealmAttParticular> realmAttParticulars = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);

                JSONGetter jsonGetter = new JSONGetter(jsonObjects);

                realmAttParticulars.add(new RealmAttParticular()
                        .withEmpId(jsonGetter.getString("emp_id"))
                        .withFname(jsonGetter.getString("firstname"))
                        .withLname(jsonGetter.getString("lastname"))
                        .withResult(jsonGetter.getDouble("result"))
                        .withImgPath(jsonGetter.getString("img_path"))
                        .withIdentifier(i)
                );
            }

            return realmAttParticulars;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<RealmAppDetail> parseAppDetail(JSONArray jsonArray) {
        try {
            List<RealmAppDetail> realmAppDetails = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);

                JSONGetter jsonGetter = new JSONGetter(jsonObjects);

                realmAppDetails.add(new RealmAppDetail()
                        .withRate(jsonGetter.getInt("rate"))
                        .withRateName(jsonGetter.getString("rate_name"))
                        .withLastCount(jsonGetter.getInt("last_appraisal_count"))
                        .withLastEmpid(jsonGetter.getString("last_appraisal_emp_id"))
                        .withIdentifier(jsonGetter.getInt("rate"))
                );
            }

            return realmAppDetails;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<RealmAppSubDetail> parseAppSubDetail(JSONArray jsonArray) {
        try {
            List<RealmAppSubDetail> realmAppSubDetails = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);

                JSONGetter jsonGetter = new JSONGetter(jsonObjects);

                realmAppSubDetails.add(new RealmAppSubDetail()
                        .withFname(jsonGetter.getString("firstname"))
                        .withLname(jsonGetter.getString("lastname"))
                        .withEmpId(jsonGetter.getString("emp_id"))
                        .withLastScore(jsonGetter.getDouble("last_score"))
                        .withCompName(jsonGetter.getString("comp_name"))
                        .withDivName(jsonGetter.getString("div_name"))
                        .withDesigName(jsonGetter.getString("designation"))
                        .withCompId(jsonGetter.getInt("comp_id"))
                        .withDivId(jsonGetter.getInt("div_id"))
                        .withDesigId(jsonGetter.getInt("designation_id"))
                        .withIdentifier(i)
                );
            }

            return realmAppSubDetails;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<RealmIndividualAppraisal> parseIndividualAppraisal(JSONArray jsonArray) {
        try {
            List<RealmIndividualAppraisal> realmIndividualAppraisals = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);

                JSONGetter jsonGetter = new JSONGetter(jsonObjects);

                realmIndividualAppraisals.add(new RealmIndividualAppraisal()
                        .withEmpId(jsonGetter.getInt("emp_id"))
                        .withAppId(jsonGetter.getInt("app_id"))
                        .withDueDate(jsonGetter.getString("due_date"))
                        .withMainScore(jsonGetter.getDouble("main_score"))
                        .withMainRating(jsonGetter.getDouble("main_rating"))
                        .withMainStatus(jsonGetter.getInt("main_status"))
                        .withMainRatingId(jsonGetter.getInt("main_rating_id"))
                        .withMainRatingName(jsonGetter.getString("main_rating_name"))
                        .withSubRatingId(jsonGetter.getInt("sub_rating_id"))
                        .withSubRatingName(jsonGetter.getString("sub_rating_name"))
                        .withAppraiseByFname(jsonGetter.getString("appraise_by_fname"))
                        .withAppraiseByLname(jsonGetter.getString("appraise_by_lname"))
                        .withCatMainId(jsonGetter.getInt("cat_main_id"))
                        .withCatMainDesc(jsonGetter.getString("cat_main_desc"))
                        .withCatSubId(jsonGetter.getInt("cat_sub_id"))
                        .withCatSubDesc(jsonGetter.getString("cat_sub_desc"))
                        .withCatMainRate(jsonGetter.getDouble("cat_main_rate"))
                        .withCatMainWeight(jsonGetter.getDouble("cat_main_weight"))
                        .withCatMainScore(jsonGetter.getDouble("cat_main_score"))
                        .withCatSubRate(jsonGetter.getDouble("cat_sub_rate"))
                        .withCatSubWeight(jsonGetter.getDouble("cat_sub_weight"))
                        .withCatSubScore(jsonGetter.getDouble("cat_sub_score"))
                        .withIdentifier(i)
                );
            }

            return realmIndividualAppraisals;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<RealmHrisDashboard> parseHrisDashboard(JSONArray jsonArray) {
        try {
            List<RealmHrisDashboard> realmHrisDashboards = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);

                SimpleDateFormat sdf1 = new SimpleDateFormat("M/d/y h:mm:ss a");
                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
                JSONGetter jsonGetter = new JSONGetter(jsonObjects);

                boolean hireDateIsNull = jsonGetter.getString("hired_date").equals("") || jsonGetter.getString("hired_date").equals("0");
                boolean renounceDateIsNull = jsonGetter.getString("date_terminated").equals("") || jsonGetter.getString("date_terminated").equals("0");
                int hireYear, renounceYear;
                Calendar calendar = Calendar.getInstance();

                if (hireDateIsNull) {
                    hireYear = 0;
                } else {
                    calendar.setTime(sdf1.parse(jsonGetter.getString("hired_date")));
                    hireYear = calendar.get(Calendar.YEAR);
                }

                if (renounceDateIsNull) {
                    renounceYear = 0;
                } else {
                    calendar.setTime(sdf2.parse(jsonGetter.getString("date_terminated")));
                    renounceYear = calendar.get(Calendar.YEAR);
                }

                realmHrisDashboards.add(new RealmHrisDashboard()
                        .withEmpId(jsonGetter.getInt("emp_id"))
                        .withNameLast(jsonGetter.getString("lastname"))
                        .withNameFirst(jsonGetter.getString("firstname"))
                        .withAge(jsonGetter.getInt("age"))
                        .withDateHired(jsonGetter.getString("hired_date"))
                        .withDateTerminated(jsonGetter.getString("date_terminated"))
                        .withStatusDesc(jsonGetter.getString("status_desc"))
                        .withStatusType(jsonGetter.getString("status_type"))
                        .withSalaryRate(jsonGetter.getDouble("salary_rate"))
                        .withCompId(jsonGetter.getInt("company_id"))
                        .withComp(jsonGetter.getString("comp_name"))
                        .withDeptId(jsonGetter.getInt("dept_id"))
                        .withDept(jsonGetter.getString("department"))
                        .withDesigId(jsonGetter.getInt("designation_id"))
                        .withDesig(jsonGetter.getString("designation"))
                        .withGender(jsonGetter.getString("gender"))
                        .withHiredDate(jsonGetter.getString("hired_date").equals("") ? null : sdf1.parse(jsonGetter.getString("hired_date")))
                        .withRenounceDate(jsonGetter.getString("date_terminated").equals("") ? null : sdf2.parse(jsonGetter.getString("date_terminated")))
                        .withHireYear(hireYear)
                        .withRenounceYear(renounceYear)
                        .withIdentifier(i)
                );
            }

            return realmHrisDashboards;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<RealmBulletinInfo> parseBulletinInfo(JSONArray jsonArray) {
        try {
            List<RealmBulletinInfo> realmBulletinInfos = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);

                JSONGetter jsonGetter = new JSONGetter(jsonObjects);

                realmBulletinInfos.add(new RealmBulletinInfo()
                        .withFileName(jsonGetter.getString("filename"))
                        .withFilePath(VolleyRequest.URL_BASE + "Files/Bulletin/" + jsonGetter.getString("autoid") + "/" + jsonGetter.getString("filename"))
                        .withTitle(jsonGetter.getString("title"))
                        .withContent(jsonGetter.getString("content"))
                        .withIsPost(jsonGetter.getInt("ispost"))
                        .withIsFront(jsonGetter.getInt("isfront"))
                        .withCreatedDate(jsonGetter.getString("date_created"))
                        .withCreatedBy(jsonGetter.getString("createdby"))
                        .withModifiedDate(jsonGetter.getString("date_modified"))
                        .withModifiedBy(jsonGetter.getString("modifiedby"))
                        .withIdentifier(i));
            }

            return realmBulletinInfos;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<FileItinerary> parseEmployeeItinerary(JSONArray jsonArray) {
        try {
            List<FileItinerary> FileItineraries = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjects = jsonArray.getJSONObject(i);

                JSONGetter jsonGetter = new JSONGetter(jsonObjects);

                FileItinerary fileItinerary = new FileItinerary();
                fileItinerary.setReason(jsonGetter.getString("reason"));
                fileItinerary.setDatefiled(jsonGetter.getString("datefiled"));
                fileItinerary.setDateitinerary(jsonGetter.getString("dateitinerary"));
                fileItinerary.setStatus(jsonGetter.getString("status"));
                fileItinerary.setApprover(jsonGetter.getString("approver"));
                fileItinerary.setReason_disapprove(jsonGetter.getString("reason_disapprove"));

                FileItineraries.add(fileItinerary);
            }

            return FileItineraries;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static class JSONGetter {

        private JSONObject jsonObject;

        public JSONGetter(JSONObject jsonObject) {
            this.jsonObject = jsonObject;
        }

        private String getString(String paramName) {
            if (jsonObject.isNull(paramName)) {
                return "";
            } else {
                try {
                    return jsonObject.getString(paramName);
                } catch (JSONException e) {
                    e.printStackTrace();
                    return "";
                }
            }
        }

        private int getInt(String paramName) {
            if (jsonObject.isNull(paramName)) {
                return 0;
            } else {
                try {
                    return jsonObject.getInt(paramName);
                } catch (JSONException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }

        private double getDouble(String paramName) {
            if (jsonObject.isNull(paramName)) {
                return 0;
            } else {
                try {
                    return jsonObject.getDouble(paramName);
                } catch (JSONException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }

        private boolean getBoolean(String paramName) {
            if (jsonObject.isNull(paramName)) {
                return false;
            } else {
                try {
                    return jsonObject.getBoolean(paramName);
                } catch (JSONException e) {
                    e.printStackTrace();
                    return false;
                }
            }
        }

    }

}
