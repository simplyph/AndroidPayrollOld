package ph.net.swak.attendanceinsights.models;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.IClickable;
import com.mikepenz.fastadapter.IExpandable;
import com.mikepenz.fastadapter.IItem;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.utils.DateUtil;

public class RealmIndividualAppraisal extends RealmObject implements IItem<RealmIndividualAppraisal, RealmIndividualAppraisal.ViewHolder>, IExpandable<RealmIndividualAppraisal, AppraisalDetail>, IClickable<RealmIndividualAppraisal> {

    //region Primary Key
    @PrimaryKey
    protected long mIdentifier = -1;

    @Override
    public RealmIndividualAppraisal withIdentifier(long identifier) {
        this.mIdentifier = identifier;
        return this;
    }
    @Override
    public long getIdentifier() {
        return mIdentifier;
    }
    //endregion

    //region Model fields
    private int empId;
    @Index
    private int appId;
    private String dueDate;
    private double mainScore;
    private double mainRating;
    private int mainStatus;
    private int mainRatingId;
    private String mainRatingName;
    private int subRatingId;
    private String subRatingName;
    private String appraiseByFname;
    private String appraiseByLname;
    private int catMainId;
    private String catMainDesc;
    private int catSubId;
    private String catSubDesc;
    private double catMainRate;
    private double catMainWeight;
    private double catMainScore;
    private double catSubWeight;
    private double catSubRate;
    private double catSubScore;
    //endregion


    //region Model Getter/Setter
    public int getEmpId() {
        return empId;
    }

    public RealmIndividualAppraisal withEmpId(int empId) {
        this.empId = empId;
        return this;
    }

    public int getAppId() {
        return appId;
    }

    public RealmIndividualAppraisal withAppId(int appId) {
        this.appId = appId;
        return this;
    }

    public String getDueDate() {
        return dueDate;
    }

    public RealmIndividualAppraisal withDueDate(String dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public double getMainScore() {
        return mainScore;
    }

    public RealmIndividualAppraisal withMainScore(double mainScore) {
        this.mainScore = mainScore;
        return this;
    }

    public double getMainRating() {
        return mainRating;
    }

    public RealmIndividualAppraisal withMainRating(double mainRating) {
        this.mainRating = mainRating;
        return this;
    }

    public int getMainStatus() {
        return mainStatus;
    }

    public RealmIndividualAppraisal withMainStatus(int mainStatus) {
        this.mainStatus = mainStatus;
        return this;
    }

    public int getMainRatingId() {
        return mainRatingId;
    }

    public RealmIndividualAppraisal withMainRatingId(int mainRatingId) {
        this.mainRatingId = mainRatingId;
        return this;
    }

    public String getMainRatingName() {
        return mainRatingName;
    }

    public RealmIndividualAppraisal withMainRatingName(String mainRatingName) {
        this.mainRatingName = mainRatingName;
        return this;
    }

    public int getSubRatingId() {
        return subRatingId;
    }

    public RealmIndividualAppraisal withSubRatingId(int subRatingId) {
        this.subRatingId = subRatingId;
        return this;
    }

    public String getSubRatingName() {
        return subRatingName;
    }

    public RealmIndividualAppraisal withSubRatingName(String subRatingName) {
        this.subRatingName = subRatingName;
        return this;
    }

    public String getAppraiseByFname() {
        return appraiseByFname;
    }

    public RealmIndividualAppraisal withAppraiseByFname(String appraiseByFname) {
        this.appraiseByFname = appraiseByFname;
        return this;
    }

    public String getAppraiseByLname() {
        return appraiseByLname;
    }

    public RealmIndividualAppraisal withAppraiseByLname(String appraiseByLname) {
        this.appraiseByLname = appraiseByLname;
        return this;
    }

    public int getCatMainId() {
        return catMainId;
    }

    public RealmIndividualAppraisal withCatMainId(int catMainId) {
        this.catMainId = catMainId;
        return this;
    }

    public String getCatMainDesc() {
        return catMainDesc;
    }

    public RealmIndividualAppraisal withCatMainDesc(String catMainDesc) {
        this.catMainDesc = catMainDesc;
        return this;
    }

    public int getCatSubId() {
        return catSubId;
    }

    public RealmIndividualAppraisal withCatSubId(int catSubId) {
        this.catSubId = catSubId;
        return this;
    }

    public String getCatSubDesc() {
        return catSubDesc;
    }

    public RealmIndividualAppraisal withCatSubDesc(String catSubDesc) {
        this.catSubDesc = catSubDesc;
        return this;
    }

    public double getCatMainRate() {
        return catMainRate;
    }

    public RealmIndividualAppraisal withCatMainRate(double catMainRate) {
        this.catMainRate = catMainRate;
        return this;
    }

    public double getCatMainWeight() {
        return catMainWeight;
    }

    public RealmIndividualAppraisal withCatMainWeight(double catMainWeight) {
        this.catMainWeight = catMainWeight;
        return this;
    }

    public double getCatMainScore() {
        return catMainScore;
    }

    public RealmIndividualAppraisal withCatMainScore(double catMainScore) {
        this.catMainScore = catMainScore;
        return this;
    }

    public double getCatSubWeight() {
        return catSubWeight;
    }

    public RealmIndividualAppraisal withCatSubWeight(double catSubWeight) {
        this.catSubWeight = catSubWeight;
        return this;
    }

    public double getCatSubRate() {
        return catSubRate;
    }

    public RealmIndividualAppraisal withCatSubRate(double catSubRate) {
        this.catSubRate = catSubRate;
        return this;
    }

    public double getCatSubScore() {
        return catSubScore;
    }

    public RealmIndividualAppraisal withCatSubScore(double catSubScore) {
        this.catSubScore = catSubScore;
        return this;
    }
    //endregion

    //region Ignore fields
    @Ignore
    private List<AppraisalDetail> mSubItems = new ArrayList<>();
    @Ignore
    private boolean mExpanded = false;
    @Ignore
    private Object mTag;
    @Ignore
    private boolean mEnabled = true;
    @Ignore
    protected boolean mSelected = false;
    @Ignore
    private boolean mSelectable = true;
    //endregion

    //region Ignore Getter/Setter
    @Override
    public List<AppraisalDetail> getSubItems() {
        return mSubItems;
    }
    @Override
    public RealmIndividualAppraisal withSubItems(List<AppraisalDetail> subItems) {
        this.mSubItems = subItems;
        return this;
    }

    @Override
    public boolean isExpanded() {
        return mExpanded;
    }
    @Override
    public RealmIndividualAppraisal withIsExpanded(boolean expanded) {
        this.mExpanded = expanded;
        return this;
    }

    @Override
    public Object getTag() {
        return mTag;
    }
    @Override
    public RealmIndividualAppraisal withTag(Object tag) {
        this.mTag = tag;
        return this;
    }

    @Override
    public boolean isEnabled() {
        return mEnabled;
    }
    @Override
    public RealmIndividualAppraisal withEnabled(boolean enabled) {
        this.mEnabled = enabled;
        return this;
    }

    @Override
    public boolean isSelected() {
        return mSelected;
    }
    @Override
    public RealmIndividualAppraisal withSetSelected(boolean selected) {
        this.mSelected = selected;
        return this;
    }

    @Override
    public boolean isAutoExpanding() {
        return true;
    }

    @Override
    public boolean isSelectable() {
        return getSubItems() != null && getSubItems().size() > 0;
    }
    @Override
    public RealmIndividualAppraisal withSelectable(boolean selectable) {
        this.mSelectable = selectable;
        return this;
    }
    //endregion

    //region IClickable Methods
    @Ignore
    private FastAdapter.OnClickListener<RealmIndividualAppraisal> mOnClickListener;

    public FastAdapter.OnClickListener<RealmIndividualAppraisal> getOnClickListener() {
        return mOnClickListener;
    }

    public RealmIndividualAppraisal withOnClickListener(FastAdapter.OnClickListener<RealmIndividualAppraisal> onClickListener) {
        this.mOnClickListener = onClickListener;
        return this;
    }

    @Ignore
    private FastAdapter.OnClickListener<RealmIndividualAppraisal> onClickListener = new FastAdapter.OnClickListener<RealmIndividualAppraisal>() {
        @Override
        public boolean onClick(View v, IAdapter<RealmIndividualAppraisal> adapter, RealmIndividualAppraisal item, int position) {

            if (item.getSubItems() != null) {
                return mOnClickListener == null || mOnClickListener.onClick(v, adapter, item, position);
            }

            return mOnClickListener != null || mOnClickListener.onClick(v, adapter, item, position);
        }
    };


    @Override
    public RealmIndividualAppraisal withOnItemClickListener(FastAdapter.OnClickListener<RealmIndividualAppraisal> onItemClickListener) {
        return null;
    }
    @Override
    public FastAdapter.OnClickListener<RealmIndividualAppraisal> getOnItemClickListener() {
        return onClickListener;
    }

    @Override
    public RealmIndividualAppraisal withOnItemPreClickListener(FastAdapter.OnClickListener<RealmIndividualAppraisal> onItemPreClickListener) {
        return null;
    }
    @Override
    public FastAdapter.OnClickListener<RealmIndividualAppraisal> getOnPreItemClickListener() {
        return null;
    }
    //endregion

    //region FastAdapter Boilerplate Code
    @Override
    public int getType() {
        return R.id.fastadapter_individual_appraisal_id;
    }
    @Override
    public int getLayoutRes() {
        return R.layout.viewgroup_individual_appraisal;
    }
    @Override
    public View generateView(Context ctx) {
        ViewHolder viewHolder = getViewHolder(LayoutInflater.from(ctx).inflate(getLayoutRes(), null, false));
        bindView(viewHolder, Collections.EMPTY_LIST);
        return viewHolder.itemView;
    }
    @Override
    public View generateView(Context ctx, ViewGroup parent) {
        ViewHolder viewHolder = getViewHolder(LayoutInflater.from(ctx).inflate(getLayoutRes(), parent, false));
        bindView(viewHolder, Collections.EMPTY_LIST);
        return viewHolder.itemView;
    }
    @Override
    public ViewHolder getViewHolder(ViewGroup parent) {
        return getViewHolder(LayoutInflater.from(parent.getContext()).inflate(getLayoutRes(), parent, false));
    }
    private ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }
    //endregion

    @Override
    public boolean equals(int id) {
        return false;
    }

    @Override
    public int hashCode() {
        return Long.valueOf(mIdentifier).hashCode();
    }

    @Override
    public void bindView(final ViewHolder holder, List<Object> payloads) {
        DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
        DateUtil dateUtil = new DateUtil();

        holder.textViewTitle.setText(dateUtil.formatDate(getDueDate(), "M/d/y h:mm:ss a", "MMM d, yyyy"));
        holder.textViewSubtitle.setText("Due Date");
        holder.textViewInfo.setText("Score: " + decimalFormat.format(getMainScore()) + "%");
        holder.textViewSubinfo.setText(getMainRatingName());

    }
    @Override
    public void unbindView(ViewHolder holder) {
        holder.textViewTitle.setText(null);
        holder.textViewSubtitle.setText(null);
        holder.textViewInfo.setText(null);
        holder.textViewSubinfo.setText(null);
    }

    @Override
    public void attachToWindow(ViewHolder viewHolder) {

    }

    @Override
    public void detachFromWindow(ViewHolder viewHolder) {

    }

    @Override
    public boolean failedToRecycle(ViewHolder viewHolder) {
        return false;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textview_viewgroup_individual_appraisal_title)
        protected TextView textViewTitle;

        @BindView(R.id.textview_viewgroup_individual_appraisal_subtitle)
        protected TextView textViewSubtitle;

        @BindView(R.id.textview_viewgroup_individual_appraisal_info)
        protected TextView textViewInfo;

        @BindView(R.id.textview_viewgroup_individual_appraisal_subinfo)
        protected TextView textViewSubinfo;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
