package ph.net.swak.attendanceinsights.ui.views.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import ph.net.swak.attendanceinsights.ui.fragments.payroll.PayrollDetail2Fragment;
import ph.net.swak.attendanceinsights.ui.fragments.payroll.PayrollDetailFragment;

public class PayrollDetailAdapter extends FragmentStatePagerAdapter {

    private ArrayList<String> tabTitles = new ArrayList<>();
    private Bundle savedBundle;

    public PayrollDetailAdapter(FragmentManager fm, Bundle bundle) {
        super(fm);
        tabTitles = bundle.getStringArrayList("divs");
        savedBundle = bundle;
    }

    @Override
    public Fragment getItem(int position) {
        if(position%2 == 0) {
            return PayrollDetailFragment.newInstance(savedBundle, getPageTitle(position).toString());
        } else {
            return PayrollDetail2Fragment.newInstance(savedBundle, getPageTitle(position).toString());
        }
    }

    @Override
    public int getCount() {
        return tabTitles.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles.get(position);
    }
}
