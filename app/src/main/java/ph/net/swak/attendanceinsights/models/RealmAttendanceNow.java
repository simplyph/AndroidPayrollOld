package ph.net.swak.attendanceinsights.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Knowell on 3/4/2017.
 */

public class RealmAttendanceNow extends RealmObject {

    @PrimaryKey
    private int mIdentifier = -1;

    private int empId;
    private String compName;
    private String desigName;
    private String firstName;
    private String lastName;
    private String statusType;
    private String schedDesc;
    private boolean isPresent;
    private double lateToday;
    private double totalAbsent;
    private double totalLate;

    public int getIdentifier() {
        return mIdentifier;
    }

    public RealmAttendanceNow withIdentifier(int identifier) {
        mIdentifier = identifier;
        return this;
    }

    public int getEmpId() {
        return empId;
    }

    public RealmAttendanceNow withEmpId(int empId) {
        this.empId = empId;
        return this;
    }

    public String getCompName() {
        return compName;
    }

    public RealmAttendanceNow withCompName(String compName) {
        this.compName = compName;
        return this;
    }

    public String getDesigName() {
        return desigName;
    }

    public RealmAttendanceNow withDesigName(String desigName) {
        this.desigName = desigName;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public RealmAttendanceNow withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public RealmAttendanceNow withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getStatusType() {
        return statusType;
    }

    public RealmAttendanceNow withStatusType(String statusType) {
        this.statusType = statusType;
        return this;
    }

    public String getSchedDesc() {
        return schedDesc;
    }

    public RealmAttendanceNow withSchedDesc(String schedDesc) {
        this.schedDesc = schedDesc;
        return this;
    }

    public boolean isPresent() {
        return isPresent;
    }

    public RealmAttendanceNow withPresent(boolean present) {
        isPresent = present;
        return this;
    }

    public double getLateToday() {
        return lateToday;
    }

    public RealmAttendanceNow withLateToday(double lateToday) {
        this.lateToday = lateToday;
        return this;
    }

    public double getTotalAbsent() {
        return totalAbsent;
    }

    public RealmAttendanceNow withTotalAbsent(double totalAbsent) {
        this.totalAbsent = totalAbsent;
        return this;
    }

    public double getTotalLate() {
        return totalLate;
    }

    public RealmAttendanceNow withTotalLate(double totalLate) {
        this.totalLate = totalLate;
        return this;
    }
}
