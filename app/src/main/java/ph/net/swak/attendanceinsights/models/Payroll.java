package ph.net.swak.attendanceinsights.models;

/**
 * Created by Knowell on 1/13/2017.
 */

public class Payroll {

    private int coveringYear;
    private int coveringMonth;
    private int totalEmployee;
    private int totalCompany;
    private double gross;
    private double deduction;
    private double netpay;

    public int getCoveringYear() {
        return coveringYear;
    }

    public void setCoveringYear(int coveringYear) {
        this.coveringYear = coveringYear;
    }

    public int getCoveringMonth() {
        return coveringMonth;
    }

    public void setCoveringMonth(int coveringMonth) {
        this.coveringMonth = coveringMonth;
    }

    public double getGross() {
        return gross;
    }

    public void setGross(double gross) {
        this.gross = gross;
    }

    public double getDeduction() {
        return deduction;
    }

    public void setDeduction(double deduction) {
        this.deduction = deduction;
    }

    public double getNetpay() {
        return netpay;
    }

    public void setNetpay(double netpay) {
        this.netpay = netpay;
    }

    public int getTotalEmployee() {
        return totalEmployee;
    }

    public void setTotalEmployee(int totalEmployee) {
        this.totalEmployee = totalEmployee;
    }

    public int getTotalCompany() {
        return totalCompany;
    }

    public void setTotalCompany(int totalCompany) {
        this.totalCompany = totalCompany;
    }
}
