package ph.net.swak.attendanceinsights.ui.fragments.lifecycle;


import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.di.Injectable;
import ph.net.swak.attendanceinsights.repository.Status;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.HrisCompanyDetailFastAdapter;
import ph.net.swak.attendanceinsights.viewmodel.HrisDashboardCompanyViewModel;

public class HrisDashboardDepartmentFragment extends LifecycleFragment implements Injectable {

    @BindView(R.id.recyclerview_company_department_details)
    protected RecyclerView recyclerView;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private MainActivity mainActivity;
    private Unbinder unbinder;
    private HrisDashboardCompanyViewModel viewModel;
    private FastItemAdapter<HrisCompanyDetailFastAdapter> mFastItemAdapter;

    public HrisDashboardDepartmentFragment() {}

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(HrisDashboardCompanyViewModel.class);

        Bundle bundle = getArguments();
        if (bundle != null) {
            viewModel.getEmployeeListDiv(bundle.getInt("compId", 0), bundle.getInt("type", 0)).observe(this, listResource -> {
                if (listResource != null && listResource.status == Status.SUCCESS) {
                    mFastItemAdapter.setNewList(listResource.data);
                }
            });
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hris_dashboard_dept, container, false);
        unbinder = ButterKnife.bind(this, view);
        mainActivity = (MainActivity) getActivity();

        mFastItemAdapter = new FastItemAdapter<>();
        recyclerView.setAdapter(mFastItemAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
