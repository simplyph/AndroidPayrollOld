package ph.net.swak.attendanceinsights.ui.fragments;

import android.app.Dialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.android.volley.RequestQueue;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;

/**
 * Created by Knowell on 2/20/2017.
 */

public class GeneralCompBottomFragment extends BottomSheetDialogFragment {

    @BindView(R.id.progress_bottom_sheet_general_comp)
    protected MaterialProgressBar progressBar;

    @BindView(R.id.progress_bottom_sheet_general_comp_text)
    protected TextView progressBarText;

    @BindView(R.id.recyclerview_bottom_sheet_general_comp)
    protected RecyclerView recyclerView;

    private Unbinder unbinder;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View view = View.inflate(getContext(), R.layout.fragment_bottom_sheet, null);
        unbinder = ButterKnife.bind(this, view);
        requestQueue = VolleyAPI.getInstance(getContext()).getRequestQueue();
        volleyRequest = new VolleyRequest();
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @OnClick(R.id.textview_bottom_sheet_general_comp_close)
    protected void onCloseClick() {
        dismiss();
    }
}
