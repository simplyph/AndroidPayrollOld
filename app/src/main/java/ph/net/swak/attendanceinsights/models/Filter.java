package ph.net.swak.attendanceinsights.models;

/**
 * Created by Knowell on 12/13/2016.
 */

public class Filter {

    private int id;
    private String name;
    private String type;

    public enum FilterTypes {
        COMPANY
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
