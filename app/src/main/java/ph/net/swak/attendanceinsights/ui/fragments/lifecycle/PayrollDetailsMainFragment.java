package ph.net.swak.attendanceinsights.ui.fragments.lifecycle;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.ui.views.pageradapter.PayrollDetailsPagerAdapter;

public class PayrollDetailsMainFragment extends Fragment {

    @BindView(R.id.viewpager_payroll_details_main)
    protected ViewPager viewPager;

    private Unbinder unbinder;
    private MainActivity mainActivity;


    public PayrollDetailsMainFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payroll_details_main, container, false);
        unbinder = ButterKnife.bind(this, view);
        mainActivity = (MainActivity) getActivity();
        mainActivity.setupUI("Payroll Details", false, false, true);

        Bundle bundle = getArguments();
        if (bundle != null) {
            Bundle passBundle = new Bundle();
            passBundle.putInt("compId", bundle.getInt("compId", 0));
            passBundle.putInt("covYear", bundle.getInt("covYear", 0));
            passBundle.putInt("covMonth", bundle.getInt("covMonth", 0));
            passBundle.putString("compName", bundle.getString("compName", "None"));
            passBundle.putInt("payrollPeriod", bundle.getInt("payrollPeriod", 0));

            viewPager.setAdapter(new PayrollDetailsPagerAdapter(getChildFragmentManager(), passBundle));
            mainActivity.mTabLayout.setupWithViewPager(viewPager);

        }

        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
