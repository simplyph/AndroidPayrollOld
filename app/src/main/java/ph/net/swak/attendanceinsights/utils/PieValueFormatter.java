package ph.net.swak.attendanceinsights.utils;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;

/**
 * Created by Knowell on 1/11/2017.
 */

public class PieValueFormatter implements IValueFormatter {

    private boolean isValueWhole = false;

    public PieValueFormatter(boolean isValueWhole) {
        this.isValueWhole = isValueWhole;
    }

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        StringBuilder sb = new StringBuilder();
        sb.append(((PieEntry)entry).getLabel());
        sb.append("(");

        if(isValueWhole)
            sb.append(new DecimalFormat("###,###,##0").format(value));
        else
            sb.append(value);

        sb.append(")");

        return sb.toString();
    }
}
