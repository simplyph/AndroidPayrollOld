package ph.net.swak.attendanceinsights.ui.fragments.hris;


import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.transition.TransitionManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.listeners.ClickEventHook;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.Unbinder;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.models.RealmHrisDashboard;
import ph.net.swak.attendanceinsights.models.SimpleList;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.ui.views.widgets.bottomsheet.HrisEmployeeBottomSheet;
import ph.net.swak.attendanceinsights.utils.DateUtil;
import ph.net.swak.attendanceinsights.utils.JSONParser;

public class HrisDashboardFragment extends Fragment {

    //@BindView(R.id.dashboard_gender)
    //protected PieChart chartGender;
//
    //@BindView(R.id.dashboard_age)
    //protected PieChart chartAge;
//
    //@BindView(R.id.dashboard_department)
    //protected BarChart chartDepartment;
//
    //@BindView(R.id.dashboard_status)
    //protected PieChart chartStatus;
//
    //@BindView(R.id.dashboard_hired)
    //protected LineChart chartHired;
//
    //@BindView(R.id.dashboard_resignation)
    //protected LineChart chartResign;
//
    @BindView(R.id.hris_dashboard_population_total)
    protected TextView populationTotal;

    @BindView(R.id.progress_dashboard_textual)
    protected MaterialProgressBar progressBar;

    @BindView(R.id.progress_dashboard_textual_text)
    protected TextView progressBarText;

    @BindView(R.id.dashboard_hris_nested)
    protected NestedScrollView nestedScrollView;

    @BindView(R.id.spinner_hris_year)
    protected Spinner spinnerYear;

    // Gender
    @BindView(R.id.hris_dashboard_gender_container)
    protected RelativeLayout genderContainer;

    @BindView(R.id.hris_dashboard_gender_toggle)
    protected ImageView genderToggle;

    @BindView(R.id.hris_dashboard_gender_recycler)
    protected RecyclerView genderRecycle;

    // Age
    @BindView(R.id.hris_dashboard_age_container)
    protected RelativeLayout ageContainer;

    @BindView(R.id.hris_dashboard_age_toggle)
    protected ImageView ageToggle;

    @BindView(R.id.hris_dashboard_age_recycler)
    protected RecyclerView ageRecycle;

    // Department
    @BindView(R.id.hris_dashboard_dept_container)
    protected RelativeLayout deptContainer;

    @BindView(R.id.hris_dashboard_dept_toggle)
    protected ImageView deptToggle;

    @BindView(R.id.hris_dashboard_dept_recycler)
    protected RecyclerView deptRecycle;

    // Status
    @BindView(R.id.hris_dashboard_stat_container)
    protected RelativeLayout statusContainer;

    @BindView(R.id.hris_dashboard_stat_toggle)
    protected ImageView statusToggle;

    @BindView(R.id.hris_dashboard_stat_recycler)
    protected RecyclerView statusRecycle;

    // Hiring
    @BindView(R.id.hris_dashboard_hiring_container)
    protected RelativeLayout hiringContainer;

    @BindView(R.id.hris_dashboard_hiring_toggle)
    protected ImageView hiringToggle;

    @BindView(R.id.hris_dashboard_hiring_recycler)
    protected RecyclerView hiringRecycle;

    // Renounciation
    @BindView(R.id.hris_dashboard_renounce_container)
    protected RelativeLayout renounceContainer;

    @BindView(R.id.hris_dashboard_renounce_toggle)
    protected ImageView renounceToggle;

    @BindView(R.id.hris_dashboard_renounce_recycler)
    protected RecyclerView renounceRecycle;

    private Unbinder unbinder;
    private MainActivity mainActivity;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private Realm mRealm;
    private final String TAG = "HrisDashboardFragment";

    private FastItemAdapter<SimpleList> fastAdapterGender;
    private FastItemAdapter<SimpleList> fastAdapterAge;
    private FastItemAdapter<SimpleList> fastAdapterDept;
    private FastItemAdapter<SimpleList> fastAdapterStatus;
    private FastItemAdapter<SimpleList> fastAdapterHiring;
    private FastItemAdapter<SimpleList> fastAdapterRenounce;

    public HrisDashboardFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hris_textual, container, false);
        mRealm = Realm.getDefaultInstance();
        unbinder = ButterKnife.bind(this, view);
        requestQueue = VolleyAPI.getInstance(getActivity()).getRequestQueue();
        volleyRequest = new VolleyRequest();
        mainActivity = (MainActivity) getActivity();

        List<Integer> filterOption = new ArrayList<>();
        mainActivity.setupUI("HRIS Dashboard", false, false, false);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mainActivity, DividerItemDecoration.VERTICAL);

        fastAdapterGender = new FastItemAdapter<>();
        fastAdapterGender.withSelectable(true);
        genderRecycle.addItemDecoration(dividerItemDecoration);
        genderRecycle.setLayoutManager(new LinearLayoutManager(mainActivity));
        genderRecycle.setAdapter(fastAdapterGender);

        fastAdapterAge = new FastItemAdapter<>();
        fastAdapterAge.withSelectable(true);
        ageRecycle.addItemDecoration(dividerItemDecoration);
        ageRecycle.setLayoutManager(new LinearLayoutManager(mainActivity));
        ageRecycle.setAdapter(fastAdapterAge);

        fastAdapterDept = new FastItemAdapter<>();
        fastAdapterDept.withSelectable(true);
        deptRecycle.addItemDecoration(dividerItemDecoration);
        deptRecycle.setLayoutManager(new LinearLayoutManager(mainActivity));
        deptRecycle.setAdapter(fastAdapterDept);

        fastAdapterStatus = new FastItemAdapter<>();
        fastAdapterStatus.withSelectable(true);
        statusRecycle.addItemDecoration(dividerItemDecoration);
        statusRecycle.setLayoutManager(new LinearLayoutManager(mainActivity));
        statusRecycle.setAdapter(fastAdapterStatus);

        fastAdapterHiring = new FastItemAdapter<>();
        fastAdapterHiring.withSelectable(true);
        hiringRecycle.addItemDecoration(dividerItemDecoration);
        hiringRecycle.setLayoutManager(new LinearLayoutManager(mainActivity));
        hiringRecycle.setAdapter(fastAdapterHiring);

        fastAdapterRenounce = new FastItemAdapter<>();
        fastAdapterRenounce.withSelectable(true);
        renounceRecycle.addItemDecoration(dividerItemDecoration);
        renounceRecycle.setLayoutManager(new LinearLayoutManager(mainActivity));
        renounceRecycle.setAdapter(fastAdapterRenounce);

        GetChartData("0");

        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(requestQueue != null) {
            requestQueue.cancelAll(TAG);
        }
    }

    private int convertPx(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    @OnClick({R.id.hris_dashboard_gender_toggle, R.id.hris_dashboard_gender_title})
    protected void onGenderClick() {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) genderContainer.getLayoutParams();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionManager.beginDelayedTransition(genderContainer);
            TransitionManager.beginDelayedTransition(genderRecycle);
        }

        if (params.bottomMargin != convertPx(8)) {
            params.setMargins(0, 0, 0, convertPx(8));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                params.setMarginStart(0);
                params.setMarginEnd(0);
            }

            genderContainer.setLayoutParams(params);
            genderRecycle.setVisibility(View.VISIBLE);
            genderToggle.setImageResource(R.drawable.ic_close_black_48dp);
        } else {
            params.setMargins(convertPx(8), 0, convertPx(8), 0);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                params.setMarginStart(convertPx(8));
                params.setMarginEnd(convertPx(8));
            }

            genderContainer.setLayoutParams(params);
            genderRecycle.setVisibility(View.GONE);
            genderToggle.setImageResource(R.drawable.ic_keyboard_arrow_down_black_48dp);
        }
    }

    @OnClick({R.id.hris_dashboard_age_toggle, R.id.hris_dashboard_age_title})
    protected void onAgeClick() {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) ageContainer.getLayoutParams();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionManager.beginDelayedTransition(ageContainer);
            TransitionManager.beginDelayedTransition(ageRecycle);
        }

        if (params.bottomMargin != convertPx(8)) {
            params.setMargins(0, 0, 0, convertPx(8));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                params.setMarginStart(0);
                params.setMarginEnd(0);
            }

            ageContainer.setLayoutParams(params);
            ageRecycle.setVisibility(View.VISIBLE);
            ageToggle.setImageResource(R.drawable.ic_close_black_48dp);
        } else {
            params.setMargins(convertPx(8), 0, convertPx(8), 0);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                params.setMarginStart(convertPx(8));
                params.setMarginEnd(convertPx(8));
            }

            ageContainer.setLayoutParams(params);
            ageRecycle.setVisibility(View.GONE);
            ageToggle.setImageResource(R.drawable.ic_keyboard_arrow_down_black_48dp);
        }
    }

    @OnClick({R.id.hris_dashboard_dept_toggle, R.id.hris_dashboard_dept_title})
    protected void onDeptClick() {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) deptContainer.getLayoutParams();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionManager.beginDelayedTransition(deptContainer);
            TransitionManager.beginDelayedTransition(deptRecycle);
        }

        if (params.bottomMargin != convertPx(8)) {
            params.setMargins(0, 0, 0, convertPx(8));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                params.setMarginStart(0);
                params.setMarginEnd(0);
            }

            deptContainer.setLayoutParams(params);
            deptRecycle.setVisibility(View.VISIBLE);
            deptToggle.setImageResource(R.drawable.ic_close_black_48dp);
        } else {
            params.setMargins(convertPx(8), 0, convertPx(8), 0);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                params.setMarginStart(convertPx(8));
                params.setMarginEnd(convertPx(8));
            }

            deptContainer.setLayoutParams(params);
            deptRecycle.setVisibility(View.GONE);
            deptToggle.setImageResource(R.drawable.ic_keyboard_arrow_down_black_48dp);
        }
    }

    @OnClick({R.id.hris_dashboard_stat_toggle, R.id.hris_dashboard_stat_title})
    protected void onStatusClick() {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) statusContainer.getLayoutParams();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionManager.beginDelayedTransition(statusContainer);
            TransitionManager.beginDelayedTransition(statusRecycle);
        }

        if (params.bottomMargin != convertPx(8)) {
            params.setMargins(0, 0, 0, convertPx(8));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                params.setMarginStart(0);
                params.setMarginEnd(0);
            }

            statusContainer.setLayoutParams(params);
            statusRecycle.setVisibility(View.VISIBLE);
            statusToggle.setImageResource(R.drawable.ic_close_black_48dp);
        } else {
            params.setMargins(convertPx(8), 0, convertPx(8), 0);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                params.setMarginStart(convertPx(8));
                params.setMarginEnd(convertPx(8));
            }

            statusContainer.setLayoutParams(params);
            statusRecycle.setVisibility(View.GONE);
            statusToggle.setImageResource(R.drawable.ic_keyboard_arrow_down_black_48dp);
        }
    }

    @OnClick({R.id.hris_dashboard_hiring_toggle, R.id.hris_dashboard_hiring_title})
    protected void onHiringClick() {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) hiringContainer.getLayoutParams();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionManager.beginDelayedTransition(hiringContainer);
            TransitionManager.beginDelayedTransition(hiringRecycle);
        }

        if (params.bottomMargin != convertPx(8)) {
            params.setMargins(0, 0, 0, convertPx(8));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                params.setMarginStart(0);
                params.setMarginEnd(0);
            }

            hiringContainer.setLayoutParams(params);
            hiringRecycle.setVisibility(View.VISIBLE);
            hiringToggle.setImageResource(R.drawable.ic_close_black_48dp);
        } else {
            params.setMargins(convertPx(8), 0, convertPx(8), 0);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                params.setMarginStart(convertPx(8));
                params.setMarginEnd(convertPx(8));
            }

            hiringContainer.setLayoutParams(params);
            hiringRecycle.setVisibility(View.GONE);
            hiringToggle.setImageResource(R.drawable.ic_keyboard_arrow_down_black_48dp);
        }
    }

    @OnClick({R.id.hris_dashboard_renounce_toggle, R.id.hris_dashboard_renounce_title})
    protected void onRenounceClick() {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) renounceContainer.getLayoutParams();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionManager.beginDelayedTransition(renounceContainer);
            TransitionManager.beginDelayedTransition(renounceRecycle);
        }

        if (params.bottomMargin != convertPx(8)) {
            params.setMargins(0, 0, 0, convertPx(8));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                params.setMarginStart(0);
                params.setMarginEnd(0);
            }

            renounceContainer.setLayoutParams(params);
            renounceRecycle.setVisibility(View.VISIBLE);
            renounceToggle.setImageResource(R.drawable.ic_close_black_48dp);
        } else {
            params.setMargins(convertPx(8), 0, convertPx(8), 0);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                params.setMarginStart(convertPx(8));
                params.setMarginEnd(convertPx(8));
            }

            renounceContainer.setLayoutParams(params);
            renounceRecycle.setVisibility(View.GONE);
            renounceToggle.setImageResource(R.drawable.ic_keyboard_arrow_down_black_48dp);
        }
    }

    @OnItemSelected(R.id.spinner_hris_year)
    protected void onSpinnerItemSelected(Spinner spinner) {
        DateUtil dateUtil = new DateUtil();
        TreeMap<Integer, Integer> hiredMap = new TreeMap<>();
        TreeMap<Integer, Integer> resignMap = new TreeMap<>();
        int tempVar = 0;

        RealmResults<RealmHrisDashboard> realmRecruitment = mRealm.where(RealmHrisDashboard.class).findAll();

        for(RealmHrisDashboard realmHrisDashboard : realmRecruitment) {

            if (dateUtil.getYear(realmHrisDashboard.getDateHired(), "M/d/y h:mm:ss a").equalsIgnoreCase(spinner.getSelectedItem().toString())) {
                if (!hiredMap.containsKey(dateUtil.getMonthInt(realmHrisDashboard.getDateHired(), "M/d/y h:mm:ss a"))) {
                    hiredMap.put(dateUtil.getMonthInt(realmHrisDashboard.getDateHired(), "M/d/y h:mm:ss a"), 1);
                } else {
                    tempVar = hiredMap.get(dateUtil.getMonthInt(realmHrisDashboard.getDateHired(), "M/d/y h:mm:ss a"));
                    hiredMap.remove(dateUtil.getMonthInt(realmHrisDashboard.getDateHired(), "M/d/y h:mm:ss a"));
                    hiredMap.put(dateUtil.getMonthInt(realmHrisDashboard.getDateHired(), "M/d/y h:mm:ss a"), tempVar + 1);

                }
            }

            if (realmHrisDashboard.getDateTerminated().trim() != "" && !realmHrisDashboard.getStatusType().equalsIgnoreCase("active")) {
                if (dateUtil.getYear(realmHrisDashboard.getDateTerminated(), "yyyy-MM-dd H:mm:ss").equalsIgnoreCase(spinner.getSelectedItem().toString())) {
                    if (!resignMap.containsKey(dateUtil.getMonthInt(realmHrisDashboard.getDateTerminated(), "yyyy-MM-dd H:mm:ss"))) {
                        resignMap.put(dateUtil.getMonthInt(realmHrisDashboard.getDateTerminated(), "yyyy-MM-dd H:mm:ss"), 1);
                    } else {
                        tempVar = resignMap.get(dateUtil.getMonthInt(realmHrisDashboard.getDateTerminated(), "yyyy-MM-dd H:mm:ss"));
                        resignMap.remove(dateUtil.getMonthInt(realmHrisDashboard.getDateTerminated(), "yyyy-MM-dd H:mm:ss"));

                        resignMap.put(dateUtil.getMonthInt(realmHrisDashboard.getDateTerminated(), "yyyy-MM-dd H:mm:ss"), tempVar + 1);

                    }
                }
            }

        }

        String[] monthList = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

        List<SimpleList> hiringList = new ArrayList<>();
        List<SimpleList> renounceList = new ArrayList<>();

        for (Map.Entry<Integer, Integer> entry : hiredMap.entrySet()) {
            SimpleList hiringSimpleList = new SimpleList();
            hiringSimpleList.setLabel(monthList[entry.getKey() - 1]);
            hiringSimpleList.setInfo(String.valueOf(entry.getValue()) + (entry.getValue() < 2 ? " employee was hired" : " employees were hired"));
            hiringSimpleList.setFrom("hris");

            hiringList.add(hiringSimpleList);
        }

        for (Map.Entry<Integer, Integer> entry : resignMap.entrySet()) {
            SimpleList renounceSimpleList = new SimpleList();
            renounceSimpleList.setLabel(monthList[entry.getKey() - 1]);
            renounceSimpleList.setInfo(String.valueOf(entry.getValue()) + (entry.getValue() < 2 ? " employee was renounced" : " employees were renounced"));
            renounceSimpleList.setFrom("hris");

            renounceList.add(renounceSimpleList);
        }

        fastAdapterHiring.setNewList(hiringList);
        fastAdapterRenounce.setNewList(renounceList);
    }

    private void GetChartData(String compid) {
        progressBar.setVisibility(View.VISIBLE);
        progressBarText.setVisibility(View.VISIBLE);
        nestedScrollView.setVisibility(View.GONE);
        progressBarText.setText("Loading data");

        JsonArrayRequest jsonArrayRequest = volleyRequest.getAuthJsonArray(getContext().getApplicationContext(), TAG, volleyRequest.getHrisDashboard(compid), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                final List<RealmHrisDashboard> realmList = JSONParser.parseHrisDashboard(response);

                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.delete(RealmHrisDashboard.class);

                        for(RealmHrisDashboard hrisDashboard : realmList) {
                            realm.copyToRealm(hrisDashboard);
                        }
                    }
                });

                TreeMap<String, Integer> departmentMap = new TreeMap<>();
                TreeMap<String, Integer> statusMap = new TreeMap<>();
                TreeMap<Integer, Integer> hiredMap = new TreeMap<>();
                TreeMap<Integer, Integer> resignMap = new TreeMap<>();
                int tempVar = 0;

                List<String> yearList = new ArrayList<>();
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(), R.layout.custom_spinner, yearList);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerYear.setAdapter(arrayAdapter);
                Calendar calendar = Calendar.getInstance();

                int minYear = mRealm.where(RealmHrisDashboard.class).min("hireYear") == null || mRealm.where(RealmHrisDashboard.class).min("hireYear").intValue() == 0 ? calendar.get(Calendar.YEAR) : mRealm.where(RealmHrisDashboard.class).min("hireYear").intValue();
                int maxYear1 = mRealm.where(RealmHrisDashboard.class).max("renounceYear").intValue();
                int maxYear2 = mRealm.where(RealmHrisDashboard.class).max("hireYear").intValue();


                for(int year = minYear; maxYear1 >= year || maxYear2 >= year || calendar.get(Calendar.YEAR) >= year; year++)
                    yearList.add(String.valueOf(year));

                arrayAdapter.notifyDataSetChanged();
                spinnerYear.setSelection(yearList.size()-1);

                long total = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).count();
                long male = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("gender", "male", Case.INSENSITIVE).count();
                long female = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("gender", "female", Case.INSENSITIVE).count();
                long ageBelow17 = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).lessThanOrEqualTo("age", 17).count();
                long age18to25 = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).between("age", 18, 25).count();
                long age26to39 = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).between("age", 26, 39).count();
                long age40to59 = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).between("age", 40, 59).count();
                long ageAbove60 = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).greaterThanOrEqualTo("age", 60).count();
                RealmResults<RealmHrisDashboard> realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).findAll();
                RealmResults<RealmHrisDashboard> realmRecruitment = mRealm.where(RealmHrisDashboard.class).findAll();




                for(RealmHrisDashboard realmHrisDashboard : realmResults) {

                    if (!departmentMap.containsKey(realmHrisDashboard.getDept().toLowerCase())) {
                        departmentMap.put(realmHrisDashboard.getDept().toLowerCase(), 1);
                    } else {
                        tempVar = departmentMap.get(realmHrisDashboard.getDept().toLowerCase());
                        departmentMap.remove(realmHrisDashboard.getDept().toLowerCase());

                        departmentMap.put(realmHrisDashboard.getDept().toLowerCase(), tempVar + 1);
                    }

                    if (!statusMap.containsKey(realmHrisDashboard.getStatusDesc().toLowerCase())) {
                        statusMap.put(realmHrisDashboard.getStatusDesc().toLowerCase(), 1);
                    } else {
                        tempVar = statusMap.get(realmHrisDashboard.getStatusDesc().toLowerCase());
                        statusMap.remove(realmHrisDashboard.getStatusDesc().toLowerCase());

                        statusMap.put(realmHrisDashboard.getStatusDesc().toLowerCase(), tempVar + 1);
                    }

                }

                DateUtil dateUtil = new DateUtil();

                for(RealmHrisDashboard realmHrisDashboard : realmRecruitment) {

                    if (dateUtil.getYear(realmHrisDashboard.getDateHired(), "M/d/y h:mm:ss a").equalsIgnoreCase("2016")) {
                        if (!hiredMap.containsKey(dateUtil.getMonthInt(realmHrisDashboard.getDateHired(), "M/d/y h:mm:ss a"))) {
                            hiredMap.put(dateUtil.getMonthInt(realmHrisDashboard.getDateHired(), "M/d/y h:mm:ss a"), 1);
                        } else {
                            tempVar = hiredMap.get(dateUtil.getMonthInt(realmHrisDashboard.getDateHired(), "M/d/y h:mm:ss a"));
                            hiredMap.remove(dateUtil.getMonthInt(realmHrisDashboard.getDateHired(), "M/d/y h:mm:ss a"));
                            hiredMap.put(dateUtil.getMonthInt(realmHrisDashboard.getDateHired(), "M/d/y h:mm:ss a"), tempVar + 1);

                        }
                    }

                    if (realmHrisDashboard.getDateTerminated().trim() != "" && !realmHrisDashboard.getStatusType().equalsIgnoreCase("active")) {
                        if (dateUtil.getYear(realmHrisDashboard.getDateTerminated(), "yyyy-MM-dd H:mm:ss").equalsIgnoreCase("2016")) {
                            if (!resignMap.containsKey(dateUtil.getMonthInt(realmHrisDashboard.getDateTerminated(), "yyyy-MM-dd H:mm:ss"))) {
                                resignMap.put(dateUtil.getMonthInt(realmHrisDashboard.getDateTerminated(), "yyyy-MM-dd H:mm:ss"), 1);
                            } else {
                                tempVar = resignMap.get(dateUtil.getMonthInt(realmHrisDashboard.getDateTerminated(), "yyyy-MM-dd H:mm:ss"));
                                resignMap.remove(dateUtil.getMonthInt(realmHrisDashboard.getDateTerminated(), "yyyy-MM-dd H:mm:ss"));

                                resignMap.put(dateUtil.getMonthInt(realmHrisDashboard.getDateTerminated(), "yyyy-MM-dd H:mm:ss"), tempVar + 1);

                            }
                        }
                    }

                }

                populationTotal.setText("Total of " + String.valueOf(total) + " Employees");

                List<SimpleList> genderList = new ArrayList<>();

                SimpleList simpleList101 = new SimpleList();
                simpleList101.setLabel("Male");
                simpleList101.setInfo(male + (male < 2 ? " Employee" : " Employees") + " (" + Math.round((((float) male / (float) total) * 100)) + "%)");
                simpleList101.setFrom("hris");
                genderList.add(simpleList101);

                SimpleList simpleList102 = new SimpleList();
                simpleList102.setLabel("Female");
                simpleList102.setInfo(female + (female < 2 ? " Employee" : " Employees") + " (" + Math.round((((float) female / (float) total) * 100)) + "%)");
                simpleList102.setFrom("hris");
                genderList.add(simpleList102);


                List<SimpleList> ageList = new ArrayList<>();

                SimpleList simpleList201 = new SimpleList();
                simpleList201.setLabel("Age below 17");
                simpleList201.setInfo(String.valueOf(ageBelow17) + (ageBelow17 < 2 ? " Employee" : " Employees") + " (" + String.valueOf(Math.round((((float) ageBelow17 / (float) total) * 100))) + "%)");
                simpleList201.setFrom("hris");
                ageList.add(simpleList201);

                SimpleList simpleList202 = new SimpleList();
                simpleList202.setLabel("Age of 18 to 25");
                simpleList202.setInfo(String.valueOf(age18to25) + (age18to25 < 2 ? " Employee" : " Employees") + " (" + String.valueOf(Math.round((((float) age18to25 / (float) total) * 100))) + "%)");
                simpleList202.setFrom("hris");
                ageList.add(simpleList202);

                SimpleList simpleList203 = new SimpleList();
                simpleList203.setLabel("Age of 26 to 39");
                simpleList203.setInfo(String.valueOf(age26to39) + (age26to39 < 2 ? " Employee" : " Employees") + " (" + String.valueOf(Math.round((((float) age26to39 / (float) total) * 100))) + "%)");
                simpleList203.setFrom("hris");
                ageList.add(simpleList203);

                SimpleList simpleList204 = new SimpleList();
                simpleList204.setLabel("Age of 40 to 59");
                simpleList204.setInfo(String.valueOf(age40to59) + (age40to59 < 2 ? " Employee" : " Employees") + " (" + String.valueOf(Math.round((((float) age40to59 / (float) total) * 100))) + "%)");
                simpleList204.setFrom("hris");
                ageList.add(simpleList204);

                SimpleList simpleList205 = new SimpleList();
                simpleList205.setLabel("Age above 60");
                simpleList205.setInfo(String.valueOf(ageAbove60) + (ageAbove60 < 2 ? " Employee" : " Employees") + " (" + String.valueOf(Math.round((((float) ageAbove60 / (float) total) * 100))) + "%)");
                simpleList205.setFrom("hris");
                ageList.add(simpleList205);

                List<SimpleList> deptList = new ArrayList<>();

                for (Map.Entry<String, Integer> entry : departmentMap.entrySet()) {
                    SimpleList deptSimpleList = new SimpleList();
                    deptSimpleList.setLabel(entry.getKey().substring(0, 1).toUpperCase() + entry.getKey().substring(1));
                    deptSimpleList.setInfo(entry.getValue() + (entry.getValue() < 2 ? " employee" : " employees") + " (" + Math.round((((float) entry.getValue() / (float) total) * 100)) + "%)");
                    deptSimpleList.setFrom("hris");

                    deptList.add(deptSimpleList);
                }

                List<SimpleList> statList = new ArrayList<>();

                for (Map.Entry<String, Integer> entry : statusMap.entrySet()) {
                    SimpleList statSimpleList = new SimpleList();
                    statSimpleList.setLabel(entry.getKey().substring(0, 1).toUpperCase() + entry.getKey().substring(1));
                    statSimpleList.setInfo(entry.getValue() + (entry.getValue() < 2 ? " employee" : " employees") + " (" + Math.round((((float) entry.getValue() / (float) total) * 100)) + "%)");
                    statSimpleList.setFrom("hris");

                    statList.add(statSimpleList);
                }

                String[] monthList = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

                List<SimpleList> hiringList = new ArrayList<>();

                for (Map.Entry<Integer, Integer> entry : hiredMap.entrySet()) {
                    SimpleList hiringSimpleList = new SimpleList();
                    hiringSimpleList.setLabel(monthList[entry.getKey() - 1]);
                    hiringSimpleList.setInfo(String.valueOf(entry.getValue()) + (entry.getValue() < 2 ? " employee was hired" : " employees were hired"));
                    hiringSimpleList.setFrom("hris");

                    hiringList.add(hiringSimpleList);
                }

                List<SimpleList> renounceList = new ArrayList<>();

                for (Map.Entry<Integer, Integer> entry : resignMap.entrySet()) {
                    SimpleList renounceSimpleList = new SimpleList();
                    renounceSimpleList.setLabel(monthList[entry.getKey() - 1]);
                    renounceSimpleList.setInfo(String.valueOf(entry.getValue()) + (entry.getValue() < 2 ? " employee was renounced" : " employees were renounced"));
                    renounceSimpleList.setFrom("hris");

                    renounceList.add(renounceSimpleList);
                }

                fastAdapterGender.withItemEvent(new ClickEventHook<SimpleList>() {
                    @Nullable
                    @Override
                    public View onBind(@NonNull RecyclerView.ViewHolder viewHolder) {
                        if (viewHolder instanceof SimpleList.ViewHolder) {
                            return ((SimpleList.ViewHolder) viewHolder).itemView;
                        }
                        return null;
                    }

                    @Override
                    public void onClick(View v, int position, FastAdapter<SimpleList> fastAdapter, SimpleList item) {
                        Bundle bundle = new Bundle();
                        bundle.putString("category", "gender");
                        bundle.putString("type", item.getLabel());
                        HrisEmployeeBottomSheet fragment = new HrisEmployeeBottomSheet();
                        fragment.setArguments(bundle);
                        fragment.show(mainActivity.getSupportFragmentManager(), null);
                    }
                });
                fastAdapterAge.withItemEvent(new ClickEventHook<SimpleList>() {
                    @Nullable
                    @Override
                    public View onBind(@NonNull RecyclerView.ViewHolder viewHolder) {
                        if (viewHolder instanceof SimpleList.ViewHolder) {
                            return ((SimpleList.ViewHolder) viewHolder).itemView;
                        }
                        return null;
                    }

                    @Override
                    public void onClick(View v, int position, FastAdapter<SimpleList> fastAdapter, SimpleList item) {
                        Bundle bundle = new Bundle();
                        bundle.putString("category", "age");
                        bundle.putString("type", item.getLabel());
                        HrisEmployeeBottomSheet fragment = new HrisEmployeeBottomSheet();
                        fragment.setArguments(bundle);
                        fragment.show(mainActivity.getSupportFragmentManager(), null);
                    }
                });
                fastAdapterDept.withItemEvent(new ClickEventHook<SimpleList>() {
                    @Nullable
                    @Override
                    public View onBind(@NonNull RecyclerView.ViewHolder viewHolder) {
                        if (viewHolder instanceof SimpleList.ViewHolder) {
                            return ((SimpleList.ViewHolder) viewHolder).itemView;
                        }
                        return null;
                    }

                    @Override
                    public void onClick(View v, int position, FastAdapter<SimpleList> fastAdapter, SimpleList item) {
                        Bundle bundle = new Bundle();
                        bundle.putString("category", "dept");
                        bundle.putString("type", item.getLabel());
                        HrisEmployeeBottomSheet fragment = new HrisEmployeeBottomSheet();
                        fragment.setArguments(bundle);
                        fragment.show(mainActivity.getSupportFragmentManager(), null);
                    }
                });
                fastAdapterStatus.withItemEvent(new ClickEventHook<SimpleList>() {
                    @Nullable
                    @Override
                    public View onBind(@NonNull RecyclerView.ViewHolder viewHolder) {
                        if (viewHolder instanceof SimpleList.ViewHolder) {
                            return ((SimpleList.ViewHolder) viewHolder).itemView;
                        }
                        return null;
                    }

                    @Override
                    public void onClick(View v, int position, FastAdapter<SimpleList> fastAdapter, SimpleList item) {
                        Bundle bundle = new Bundle();
                        bundle.putString("category", "stat");
                        bundle.putString("type", item.getLabel());
                        HrisEmployeeBottomSheet fragment = new HrisEmployeeBottomSheet();
                        fragment.setArguments(bundle);
                        fragment.show(mainActivity.getSupportFragmentManager(), null);
                    }
                });
                fastAdapterHiring.withItemEvent(new ClickEventHook<SimpleList>() {
                    @Nullable
                    @Override
                    public View onBind(@NonNull RecyclerView.ViewHolder viewHolder) {
                        if (viewHolder instanceof SimpleList.ViewHolder) {
                            return ((SimpleList.ViewHolder) viewHolder).itemView;
                        }
                        return null;
                    }

                    @Override
                    public void onClick(View v, int position, FastAdapter<SimpleList> fastAdapter, SimpleList item) {
                        Bundle bundle = new Bundle();
                        bundle.putString("category", "hiring");
                        bundle.putString("type", item.getLabel());
                        bundle.putString("year", spinnerYear.getSelectedItem().toString());
                        HrisEmployeeBottomSheet fragment = new HrisEmployeeBottomSheet();
                        fragment.setArguments(bundle);
                        fragment.show(mainActivity.getSupportFragmentManager(), null);
                    }
                });
                fastAdapterRenounce.withItemEvent(new ClickEventHook<SimpleList>() {
                    @Nullable
                    @Override
                    public View onBind(@NonNull RecyclerView.ViewHolder viewHolder) {
                        if (viewHolder instanceof SimpleList.ViewHolder) {
                            return ((SimpleList.ViewHolder) viewHolder).itemView;
                        }
                        return null;
                    }

                    @Override
                    public void onClick(View v, int position, FastAdapter<SimpleList> fastAdapter, SimpleList item) {
                        Bundle bundle = new Bundle();
                        bundle.putString("category", "renounce");
                        bundle.putString("type", item.getLabel());
                        bundle.putString("year", spinnerYear.getSelectedItem().toString());
                        HrisEmployeeBottomSheet fragment = new HrisEmployeeBottomSheet();
                        fragment.setArguments(bundle);
                        fragment.show(mainActivity.getSupportFragmentManager(), null);
                    }
                });

                fastAdapterGender.setNewList(genderList);
                fastAdapterAge.setNewList(ageList);
                fastAdapterDept.setNewList(deptList);
                fastAdapterStatus.setNewList(statList);
                fastAdapterHiring.setNewList(hiringList);
                fastAdapterRenounce.setNewList(renounceList);

                progressBar.setVisibility(View.GONE);
                progressBarText.setVisibility(View.GONE);
                nestedScrollView.setVisibility(View.VISIBLE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                progressBarText.setVisibility(View.VISIBLE);
                nestedScrollView.setVisibility(View.GONE);
                progressBarText.setText("No data found");
                try {
                    String responseBody = new String( error.networkResponse.data, "utf-8" );
                    JSONObject jsonObject = new JSONObject( responseBody );

                    Log.e("AuthSettings", "Status Code: " + error.networkResponse.statusCode);
                    Log.e("AuthSettings", "Status Error: " + jsonObject.toString());
                } catch ( JSONException e ) {
                    //Handle a malformed json response
                } catch (UnsupportedEncodingException error1){

                }
            }
        });

        requestQueue.add(jsonArrayRequest);
    }

}
