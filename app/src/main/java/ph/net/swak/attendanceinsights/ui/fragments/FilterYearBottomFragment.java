package ph.net.swak.attendanceinsights.ui.fragments;

import android.app.Dialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmAsyncTask;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.models.RealmFilterYear;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;

/**
 * Created by Knowell on 2/6/2017.
 */

public class FilterYearBottomFragment extends BottomSheetDialogFragment {

    @BindView(R.id.recyclerview_bottom_sheet_filter_year)
    protected RecyclerView recyclerView;

    @BindView(R.id.progress_bar_bottom_sheet_filter_year)
    protected MaterialProgressBar progressBar;

    @BindView(R.id.textview_bottom_sheet_filter_year_info)
    protected TextView textViewInfo;

    private FastItemAdapter<RealmFilterYear> mFastItemAdapter;

    private Realm mRealm;
    private Unbinder unbinder;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private MainActivity mainActivity;
    private List<RealmAsyncTask> transactionList = new ArrayList<>();

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        mainActivity = (MainActivity) getContext();
        View view = View.inflate(mainActivity, R.layout.fragment_bottom_sheet_filter_year, null);
        unbinder = ButterKnife.bind(this, view);
        requestQueue = VolleyAPI.getInstance(mainActivity).getRequestQueue();
        volleyRequest = new VolleyRequest();

        mFastItemAdapter = new FastItemAdapter<>();
        mFastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<RealmFilterYear>() {
            @Override
            public boolean onClick(View v, IAdapter<RealmFilterYear> adapter, final RealmFilterYear item, int position) {
                mainActivity.mFilterYearContent.setText(item.getTitle());
                mainActivity.mFilterHeadYear.setText(item.getTitle());
                getDialog().dismiss();
                return false;
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mFastItemAdapter);

        textViewInfo.setText("Loading data");

        // Realm
        mRealm = Realm.getDefaultInstance();

        mRealm.where(RealmFilterYear.class).findAllAsync().addChangeListener(new RealmChangeListener<RealmResults<RealmFilterYear>>() {
            @Override
            public void onChange(RealmResults<RealmFilterYear> element) {
                if (element.size() > 0) {
                    progressBar.setVisibility(View.GONE);
                    textViewInfo.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                } else {
                    textViewInfo.setText("No data found.");
                    progressBar.setVisibility(View.VISIBLE);
                    textViewInfo.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }

                mFastItemAdapter.setNewList(element);
            }
        });

        dialog.setContentView(view);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
        if(transactionList != null)
            for(RealmAsyncTask transaction : transactionList)
                if(transaction != null && !transaction.isCancelled())
                    transaction.cancel();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        closeRealm();
    }

    private void closeRealm() {
        if (!mRealm.isClosed())
            mRealm.close();
    }

    @OnClick(R.id.button_bottom_sheet_filter_year_close)
    protected void onClose() {
        dismiss();
    }
}
