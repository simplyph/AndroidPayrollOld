package ph.net.swak.attendanceinsights.ui.views.pageradapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.Locale;

import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.PayrollDetailsAllFragment;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.PayrollDetailsStaffFragment;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.PayrollDetailsManagerFragment;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.PayrollDetailsSupervisorFragment;
import timber.log.Timber;

public class PayrollDetailsPagerAdapter extends FragmentStatePagerAdapter {

    private int PAGE_COUNT = 3;
    private String tabTitles[] = new String[]{"Manager", "Supervisor", "Staff"};
    private int compId = 0;
    private int covYear = 0;
    private int covMonth = 0;
    private String compName = "";
    private int payrollPeriod = 0;

    public PayrollDetailsPagerAdapter(FragmentManager fm, Bundle bundle) {
        super(fm);
        this.compId = bundle.getInt("compId", 0);
        this.covYear = bundle.getInt("covYear", 0);
        this.covMonth = bundle.getInt("covMonth", 0);
        this.compName = bundle.getString("compName", "None");
        this.payrollPeriod = bundle.getInt("payrollPeriod", 0);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new Fragment();
        Bundle bundle = new Bundle();
        bundle.putInt("compId", compId);
        bundle.putInt("covYear", covYear);
        bundle.putInt("covMonth", covMonth);
        bundle.putString("compName", compName);
        bundle.putInt("payrollPeriod", payrollPeriod);

        if (position == 0) {
            fragment = new PayrollDetailsManagerFragment();
            bundle.putInt("type", 0);
        } else if (position == 1) {
            fragment = new PayrollDetailsSupervisorFragment();
            bundle.putInt("type", 1);
        } else if (position == 2) {
            fragment = new PayrollDetailsStaffFragment();
            bundle.putInt("type", 2);
        }


        Timber.e(String.format(Locale.ENGLISH, "Position val: %d", position));
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}