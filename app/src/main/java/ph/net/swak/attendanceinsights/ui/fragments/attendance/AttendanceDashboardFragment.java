package ph.net.swak.attendanceinsights.ui.fragments.attendance;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Case;
import io.realm.Realm;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.models.RealmAttendanceNow;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.ui.views.widgets.bottomsheet.AttDashboardBottomSheet;
import ph.net.swak.attendanceinsights.utils.DateUtil;
import ph.net.swak.attendanceinsights.utils.JSONParser;

import static android.view.View.GONE;

public class AttendanceDashboardFragment extends Fragment {

    @BindView(R.id.dashboard_attendance_spinner)
    protected MaterialProgressBar spinner;

    @BindView(R.id.dashboard_attendance_nested)
    protected NestedScrollView nestedScrollView;

    @BindView(R.id.dashboard_attendance_active)
    protected TextView tvActive;

    @BindView(R.id.dashboard_attendance_present)
    protected TextView tvPresent;

    @BindView(R.id.dashboard_attendance_absent)
    protected TextView tvAbsent;

    @BindView(R.id.text_date_today)
    protected TextView tvDateToday;

    @BindView(R.id.text_top_late)
    protected TextView textTopLate;

    @BindView(R.id.text_top_absent)
    protected TextView textTopAbsent;

    @BindView(R.id.text_top_perfect)
    protected TextView textTopPerfect;

    private Unbinder unbinder;
    private MainActivity mainActivity;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private final String TAG = "AttendanceDashboardFragment";
    private Realm mRealm;

    private boolean isLoaded1 = false;
    private boolean isLoaded2 = false;

    public AttendanceDashboardFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_attendance_dashboard, container, false);
        unbinder = ButterKnife.bind(this, view);
        mRealm = Realm.getDefaultInstance();
        requestQueue = VolleyAPI.getInstance(getActivity()).getRequestQueue();
        volleyRequest = new VolleyRequest();
        mainActivity = (MainActivity) getActivity();
        mainActivity.setupUI("Attendance Dashboard");

        GetChartData();

        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(requestQueue != null)
            requestQueue.cancelAll(TAG);
    }

    private void GetChartData() {
        isLoaded1 = isLoaded2 = false;

        final DateUtil dateUtil = new DateUtil();

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat s1 = new SimpleDateFormat("MMM d, yyyy");
        SimpleDateFormat s2 = new SimpleDateFormat("yyyy-MM-dd");

        tvDateToday.setText(s1.format(calendar.getTime()));
        textTopLate.setText("Top 20 Tardiness for " + calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()));
        textTopAbsent.setText("Top 20 Absenteeism for " + calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()));
        textTopPerfect.setText("Perfect Attendance for " + calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()));

        Calendar firstDay = Calendar.getInstance();
        Calendar secondDay = Calendar.getInstance();

        firstDay.set(Calendar.DATE, 1);
        secondDay.set(Calendar.DATE, firstDay.getActualMaximum(Calendar.DAY_OF_MONTH));

        Log.e("FIRST DAY", s2.format(firstDay.getTime()));
        Log.e("SECOND DAY", s2.format(secondDay.getTime()));


        /// JsonArrayRequest jsonArrayRequest1 = volleyRequest.getJsonArray(TAG, volleyRequest.getAttendanceDaily(s2.format(calendar.getTime()), "2017-03-01", "2017-03-03"), new Response.Listener<JSONArray>() {
        JsonArrayRequest jsonArrayRequest1 = volleyRequest.getAuthJsonArray(mainActivity.getApplicationContext(), TAG, volleyRequest.getAttendanceDaily(s2.format(calendar.getTime()), s2.format(firstDay.getTime()), s2.format(calendar.getTime())), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                final List<RealmAttendanceNow> realmAttendanceNows = JSONParser.parseAttendanceDaily(response);

                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.delete(RealmAttendanceNow.class);

                        for(RealmAttendanceNow realmAttendanceNow : realmAttendanceNows)
                            realm.copyToRealm(realmAttendanceNow);
                    }
                });

                isLoaded2 = true;

                long total = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).count();
                long absentToday = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("isPresent", false).count();
                long lateToday = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).greaterThan("lateToday", 0d).count();
                double lateTodayTotal = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).greaterThan("lateToday", 0d).sum("lateToday").doubleValue();

                DecimalFormat dF = new DecimalFormat("###,##0.00");

                tvActive.setText(String.valueOf(total));
                tvAbsent.setText(String.valueOf(absentToday));
                tvPresent.setText(String.valueOf(lateToday) + " ( " + dF.format(lateTodayTotal) + " Mins.)");

                refreshChart();
                spinner.setVisibility(GONE);
                nestedScrollView.setVisibility(View.VISIBLE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        requestQueue.add(jsonArrayRequest1);
    }

    private void refreshChart() {
        if (isLoaded2) {
            spinner.setVisibility(GONE);
            nestedScrollView.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.container_total)
    protected void onTotalClick() {
        Bundle bundle = new Bundle();
        bundle.putString("category", "today");
        bundle.putString("type", "today_total");
        AttDashboardBottomSheet fragment = new AttDashboardBottomSheet();
        fragment.setArguments(bundle);
        fragment.show(mainActivity.getSupportFragmentManager(), null);
    }

    @OnClick(R.id.container_late)
    protected void onLateClick() {
        Bundle bundle = new Bundle();
        bundle.putString("category", "today");
        bundle.putString("type", "today_late");
        AttDashboardBottomSheet fragment = new AttDashboardBottomSheet();
        fragment.setArguments(bundle);
        fragment.show(mainActivity.getSupportFragmentManager(), null);
    }

    @OnClick(R.id.container_absent)
    protected void onAbsentClick() {
        Bundle bundle = new Bundle();
        bundle.putString("category", "today");
        bundle.putString("type", "today_absent");
        AttDashboardBottomSheet fragment = new AttDashboardBottomSheet();
        fragment.setArguments(bundle);
        fragment.show(mainActivity.getSupportFragmentManager(), null);
    }

    @OnClick(R.id.text_top_late)
    protected void onTopLateClick() {
        Bundle bundle = new Bundle();
        bundle.putString("category", "monthly");
        bundle.putString("type", "monthly_late");
        AttDashboardBottomSheet fragment = new AttDashboardBottomSheet();
        fragment.setArguments(bundle);
        fragment.show(mainActivity.getSupportFragmentManager(), null);
    }

    @OnClick(R.id.text_top_absent)
    protected void onTopAbsentClick() {
        Bundle bundle = new Bundle();
        bundle.putString("category", "monthly");
        bundle.putString("type", "monthly_absent");
        AttDashboardBottomSheet fragment = new AttDashboardBottomSheet();
        fragment.setArguments(bundle);
        fragment.show(mainActivity.getSupportFragmentManager(), null);
    }

    @OnClick(R.id.text_top_perfect)
    protected void onPerfectClick() {
        Bundle bundle = new Bundle();
        bundle.putString("category", "monthly");
        bundle.putString("type", "monthly_perfect");
        AttDashboardBottomSheet fragment = new AttDashboardBottomSheet();
        fragment.setArguments(bundle);
        fragment.show(mainActivity.getSupportFragmentManager(), null);
    }

}
