package ph.net.swak.attendanceinsights.models;

import android.app.Dialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.events.PayrollDetailEvent;

public class BottomSheetCompany extends AbstractItem<BottomSheetCompany, BottomSheetCompany.ViewHolder> {

    private String title;
    private String subtitle;
    private String info1;
    private String info2;
    private String info3;
    private String info4;
    private Dialog dialog;

    private String year;
    private String month;
    private int compid;
    private String compName;

    private int totalManager;
    private int totalSupervisor;
    private int totalStaff;

    private double grossManager;
    private double grossSupervisor;
    private double grossStaff;

    private String payrollType;

    private ArrayList<String> divList = new ArrayList<>();

    public ArrayList<String> getDivList() {
        return divList;
    }

    public BottomSheetCompany withDivList(ArrayList<String> divList) {
        this.divList = divList;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getInfo1() {
        return info1;
    }

    public void setInfo1(String info1) {
        this.info1 = info1;
    }

    public String getInfo2() {
        return info2;
    }

    public void setInfo2(String info2) {
        this.info2 = info2;
    }

    public String getInfo3() {
        return info3;
    }

    public void setInfo3(String info3) {
        this.info3 = info3;
    }

    public String getInfo4() {
        return info4;
    }

    public void setInfo4(String info4) {
        this.info4 = info4;
    }

    public Dialog getDialog() {
        return dialog;
    }

    public void setDialog(Dialog dialog) {
        this.dialog = dialog;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getCompid() {
        return compid;
    }

    public void setCompid(int compid) {
        this.compid = compid;
    }

    public int getTotalManager() {
        return totalManager;
    }

    public void setTotalManager(int totalManager) {
        this.totalManager = totalManager;
    }

    public int getTotalSupervisor() {
        return totalSupervisor;
    }

    public void setTotalSupervisor(int totalSupervisor) {
        this.totalSupervisor = totalSupervisor;
    }

    public int getTotalStaff() {
        return totalStaff;
    }

    public void setTotalStaff(int totalStaff) {
        this.totalStaff = totalStaff;
    }

    public String getCompName() {
        return compName;
    }

    public double getGrossManager() {
        return grossManager;
    }

    public double getGrossSupervisor() {
        return grossSupervisor;
    }

    public double getGrossStaff() {
        return grossStaff;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public void setGrossManager(double grossManager) {
        this.grossManager = grossManager;
    }

    public void setGrossSupervisor(double grossSupervisor) {
        this.grossSupervisor = grossSupervisor;
    }

    public void setGrossStaff(double grossStaff) {
        this.grossStaff = grossStaff;
    }

    public String getPayrollType() {
        return payrollType;
    }

    public BottomSheetCompany withPayrollType(String payrollType) {
        this.payrollType = payrollType;
        return this;
    }

    @Override
    public int getType() {
        return R.id.fastadapter_bottom_sheet_company_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.viewgroup_bottom_sheet;
    }

    @Override
    public void bindView(final BottomSheetCompany.ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);
        holder.title.setText(getTitle());
        holder.subtitle.setText(getSubtitle());
        holder.info1.setText(getInfo1());
        holder.info2.setText(getInfo2());
        holder.info3.setText(getInfo3());
        holder.info4.setText(getInfo4());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
                EventBus.getDefault().post(new PayrollDetailEvent(getYear(), getMonth(), getCompid(), getCompName(), getTotalManager(), getTotalSupervisor(), getTotalStaff(), getGrossManager(), getGrossSupervisor(), getGrossStaff(), getDivList(), getPayrollType()));
            }
        });
    }

    @Override
    public void unbindView(BottomSheetCompany.ViewHolder holder) {
        super.unbindView(holder);
        holder.title.setText(null);
        holder.subtitle.setText(null);
        holder.info1.setText(null);
        holder.info2.setText(null);
        holder.info3.setText(null);
        holder.info4.setText(null);
        holder.itemView.setOnClickListener(null);
    }

    @Override
    public ViewHolder getViewHolder(View view) {
        return new ViewHolder(view);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textview_bottom_sheet_title)
        TextView title;

        @BindView(R.id.textview_bottom_sheet_subtitle)
        TextView subtitle;

        @BindView(R.id.textview_bottom_sheet_info1)
        TextView info1;

        @BindView(R.id.textview_bottom_sheet_info2)
        TextView info2;

        @BindView(R.id.textview_bottom_sheet_info3)
        TextView info3;

        @BindView(R.id.textview_bottom_sheet_info4)
        TextView info4;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
