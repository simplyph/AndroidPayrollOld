package ph.net.swak.attendanceinsights.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import ph.net.swak.attendanceinsights.db.realm.LiveRealmData;
import ph.net.swak.attendanceinsights.db.realm.RealmPayrollHead;
import ph.net.swak.attendanceinsights.db.realm.RealmPayrollQuick;
import ph.net.swak.attendanceinsights.di.scope.ApplicationScope;
import ph.net.swak.attendanceinsights.network.ApiResponse;
import ph.net.swak.attendanceinsights.network.PAService;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.PayrollDetailFastAdapter;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.PayrollDashboardFastAdapter;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.PayrollEmployeeFastAdapter;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.PayrollMonthFastAdapter;
import ph.net.swak.attendanceinsights.utils.AppExecutors;

import static ph.net.swak.attendanceinsights.utils.RealmUtils.payrollModel;

@ApplicationScope
public class PayrollRepository {

    private final AppExecutors appExecutors;
    private final Realm mRealm;
    private final PAService paService;

    @Inject
    public PayrollRepository(AppExecutors appExecutors, Realm realm, PAService paService) {
        this.appExecutors = appExecutors;
        this.mRealm = realm;
        this.paService = paService;
    }

    public LiveData<Resource<List<RealmPayrollHead>>> getPayrollHead(int year) {
        return new NetworkBoundResource<List<RealmPayrollHead>, List<RealmPayrollHead>>(appExecutors) {

            @Override
            protected boolean shouldFetch(@Nullable List<RealmPayrollHead> data) {
                return data == null || data.isEmpty() || true;
            }

            @Override
            protected void saveCallResult(@NonNull List<RealmPayrollHead> item) {
                payrollModel(mRealm).savePayrollHead(item);
            }

            @NonNull
            @Override
            protected LiveData<List<RealmPayrollHead>> loadFromDb() {
                LiveRealmData<RealmPayrollHead> payrollHeadLiveRealmData = payrollModel(mRealm).getPayrollHead(year);

                return Transformations.map(payrollHeadLiveRealmData, realmPayrollHeads -> {
                    List<RealmPayrollHead> payrollHeads = new ArrayList<>();
                    payrollHeads.addAll(realmPayrollHeads);
                    return payrollHeads;
                });
            }

            @Override
            protected LiveData<ApiResponse<List<RealmPayrollHead>>> createCall() {
                return paService.loadPayrollHead(year);
            }

            @Override
            protected void cancelTransaction() {

            }
        }.asLiveData();
    }

    public LiveData<Resource<List<RealmPayrollQuick>>> getPayrollQuick(int prevMonth, int prevYear, int presentMonth, int presentYear) {
        return new NetworkBoundResource<List<RealmPayrollQuick>, List<RealmPayrollQuick>>(appExecutors) {

            @Override
            protected boolean shouldFetch(@Nullable List<RealmPayrollQuick> data) {
                return true;
            }

            @Override
            protected void saveCallResult(@NonNull List<RealmPayrollQuick> item) {
                payrollModel(mRealm).savePayrollQuick(item);
            }

            @NonNull
            @Override
            protected LiveData<List<RealmPayrollQuick>> loadFromDb() {
                LiveRealmData<RealmPayrollQuick> payrollHeadLiveRealmData = payrollModel(mRealm).getPayrollQuick();

                return Transformations.map(payrollHeadLiveRealmData, realmPayrollHeads -> {
                    List<RealmPayrollQuick> payrollHeads = new ArrayList<>();
                    payrollHeads.addAll(realmPayrollHeads);
                    return payrollHeads;
                });
            }

            @Override
            protected LiveData<ApiResponse<List<RealmPayrollQuick>>> createCall() {
                return paService.loadPayrollQuick(prevMonth, prevYear, presentMonth, presentYear);
            }

            @Override
            protected void cancelTransaction() {

            }
        }.asLiveData();
    }

    public LiveData<Resource<List<PayrollDashboardFastAdapter>>> getPayrollMonthly(int year, Long[] companyId) {
        return new NetworkBoundResource<List<PayrollDashboardFastAdapter>, List<PayrollDashboardFastAdapter>>(appExecutors) {

            @Override
            protected boolean shouldFetch(@Nullable List<PayrollDashboardFastAdapter> data) {
                return false;
            }

            @Override
            protected void saveCallResult(@NonNull List<PayrollDashboardFastAdapter> item) {

            }

            @NonNull
            @Override
            protected LiveData<List<PayrollDashboardFastAdapter>> loadFromDb() {
                return payrollModel(mRealm).getPayrollMonthly(year, companyId);
            }

            @Override
            protected LiveData<ApiResponse<List<PayrollDashboardFastAdapter>>> createCall() {
                return null;
            }

            @Override
            protected void cancelTransaction() {

            }

        }.asLiveData();
    }

    public LiveData<Resource<List<PayrollEmployeeFastAdapter>>> getPayrollByCompAndPos(int year, int compId, int posId, int from, int fromId) {
        return new NetworkBoundResource<List<PayrollEmployeeFastAdapter>, List<PayrollEmployeeFastAdapter>>(appExecutors) {

            @Override
            protected boolean shouldFetch(@Nullable List<PayrollEmployeeFastAdapter> data) {
                return false;
            }

            @Override
            protected void saveCallResult(@NonNull List<PayrollEmployeeFastAdapter> item) {

            }

            @NonNull
            @Override
            protected LiveData<List<PayrollEmployeeFastAdapter>> loadFromDb() {
                return payrollModel(mRealm).getPayrollByCompAndPos(year, compId, posId, from, fromId);
            }

            @Override
            protected LiveData<ApiResponse<List<PayrollEmployeeFastAdapter>>> createCall() {
                return null;
            }

            @Override
            protected void cancelTransaction() {

            }
        }.asLiveData();
    }

    public LiveData<Resource<List<PayrollMonthFastAdapter>>> getPayrollByMonth(int year, int compId, int from, int fromId) {
        return new NetworkBoundResource<List<PayrollMonthFastAdapter>, List<PayrollMonthFastAdapter>>(appExecutors) {

            @Override
            protected boolean shouldFetch(@Nullable List<PayrollMonthFastAdapter> data) {
                return false;
            }

            @Override
            protected void saveCallResult(@NonNull List<PayrollMonthFastAdapter> item) {

            }

            @NonNull
            @Override
            protected LiveData<List<PayrollMonthFastAdapter>> loadFromDb() {
                return payrollModel(mRealm).getPayrollByMonth(year, compId, from, fromId);
            }

            @Override
            protected LiveData<ApiResponse<List<PayrollMonthFastAdapter>>> createCall() {
                return null;
            }

            @Override
            protected void cancelTransaction() {

            }
        }.asLiveData();
    }

    public LiveData<Resource<List<PayrollMonthFastAdapter>>> getPayrollByQuarter(int year, int compId, int from, int fromId) {
        return new NetworkBoundResource<List<PayrollMonthFastAdapter>, List<PayrollMonthFastAdapter>>(appExecutors) {

            @Override
            protected boolean shouldFetch(@Nullable List<PayrollMonthFastAdapter> data) {
                return false;
            }

            @Override
            protected void saveCallResult(@NonNull List<PayrollMonthFastAdapter> item) {

            }

            @NonNull
            @Override
            protected LiveData<List<PayrollMonthFastAdapter>> loadFromDb() {
                return payrollModel(mRealm).getPayrollByQuarter(year, compId, from, fromId);
            }

            @Override
            protected LiveData<ApiResponse<List<PayrollMonthFastAdapter>>> createCall() {
                return null;
            }

            @Override
            protected void cancelTransaction() {

            }
        }.asLiveData();
    }

    public LiveData<Resource<List<PayrollDetailFastAdapter>>> getPayrollByCompAndType(int covYear, int covMonth, int compId, int posId, int payrollPeriod) {
        return new NetworkBoundResource<List<PayrollDetailFastAdapter>, List<PayrollDetailFastAdapter>>(appExecutors) {

            @Override
            protected boolean shouldFetch(@Nullable List<PayrollDetailFastAdapter> data) {
                return false;
            }

            @Override
            protected void saveCallResult(@NonNull List<PayrollDetailFastAdapter> item) {

            }

            @NonNull
            @Override
            protected LiveData<List<PayrollDetailFastAdapter>> loadFromDb() {
                return payrollModel(mRealm).getPayrollByCompAndType(covYear, covMonth, compId, posId, payrollPeriod);
            }

            @Override
            protected LiveData<ApiResponse<List<PayrollDetailFastAdapter>>> createCall() {
                return null;
            }

            @Override
            protected void cancelTransaction() {

            }
        }.asLiveData();
    }
}
