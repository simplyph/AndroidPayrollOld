package ph.net.swak.attendanceinsights.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Knowell on 1/12/2017.
 */

public class DateUtil {

    public String formatDate(String dateString, String passFormat, String returnFormat) {
        DateFormat dateFormat = new SimpleDateFormat(returnFormat);
        Date date;
        String formattedDate;

        try {
            date = new SimpleDateFormat(passFormat).parse(dateString);
            formattedDate = dateFormat.format(date);
        } catch (ParseException e) {
            formattedDate = dateString;
            e.printStackTrace();
        }

        return formattedDate;
    }

    public String getYear(String dateString, String passFormat) {

        Date date = null;

        String formattedDate = "";

        try {
            date = new SimpleDateFormat(passFormat).parse(dateString);
            formattedDate = new SimpleDateFormat("yyyy").format(date);
        } catch (ParseException e) {
            formattedDate = dateString;
            e.printStackTrace();
        }

        return formattedDate;
    }

    public String getMonth(String dateString, String passFormat) {

        Date date = null;

        String formattedDate = "";

        try {
            date = new SimpleDateFormat(passFormat).parse(dateString);
            formattedDate = new SimpleDateFormat("MMM").format(date);
        } catch (ParseException e) {
            formattedDate = dateString;
            e.printStackTrace();
        }

        return formattedDate;
    }

    public String getMonthWithZero(String dateString, String passFormat) {

        Date date = null;

        String formattedDate = "";

        try {
            date = new SimpleDateFormat(passFormat).parse(dateString);
            formattedDate = new SimpleDateFormat("MM").format(date);
        } catch (ParseException e) {
            formattedDate = dateString;
            e.printStackTrace();
        }

        return formattedDate;
    }

    public int getDay(String dateString, String passFormat) {

        Date date;

        int formattedDate;

        try {
            date = new SimpleDateFormat(passFormat).parse(dateString);
            formattedDate = Integer.parseInt(new SimpleDateFormat("d").format(date));
        } catch (ParseException e) {
            formattedDate = 1;
            e.printStackTrace();
        }

        return formattedDate;
    }

    public String getDate(String dateString, String passFormat) {

        Date date = null;

        String formattedDate = "";

        try {
            date = new SimpleDateFormat(passFormat).parse(dateString);
            formattedDate = new SimpleDateFormat("MM-dd-yyyy").format(date);
        } catch (ParseException e) {
            formattedDate = dateString;
            e.printStackTrace();
        }

        return formattedDate;
    }

    public int getMonthInt(String dateString, String passFormat) {

        Date date = null;

        int formattedDate;

        try {
            date = new SimpleDateFormat(passFormat).parse(dateString);
            formattedDate = Integer.parseInt(new SimpleDateFormat("M").format(date));
        } catch (ParseException e) {
            formattedDate = 1;
            e.printStackTrace();
        }

        return formattedDate;
    }

}
