package ph.net.swak.attendanceinsights.ui.fragments.appraisal;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.models.RealmAppDetail;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.ui.fragments.AppraisalDetailBottomFragment;
import ph.net.swak.attendanceinsights.utils.JSONParser;

public class AppraisalDashboardFragment extends Fragment {

    @BindView(R.id.textview_app_dashboard_detail_a)
    protected TextView detailA;

    @BindView(R.id.textview_app_dashboard_detail_b)
    protected TextView detailB;

    @BindView(R.id.textview_app_dashboard_detail_c)
    protected TextView detailC;

    @BindView(R.id.textview_app_dashboard_detail_d)
    protected TextView detailD;

    @BindView(R.id.textview_app_dashboard_detail_e)
    protected TextView detailE;

    @BindView(R.id.progressbar_app_dashboard)
    protected MaterialProgressBar progressBar;

    @BindView(R.id.textview_app_dashboard)
    protected TextView progressBarText;

    @BindView(R.id.linearlayout_app_dashboard)
    protected LinearLayout linearLayout;

    // Variables
    private Unbinder unbinder;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private Realm mRealm;
    private final String TAG = "AppraisalDashboardFragment";

    private MainActivity mainActivity;

    public AppraisalDashboardFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_appraisal_dashboard, container, false);
        unbinder = ButterKnife.bind(this, view);
        requestQueue = VolleyAPI.getInstance(getActivity()).getRequestQueue();
        volleyRequest = new VolleyRequest();
        mainActivity = (MainActivity) getActivity();
        mRealm = Realm.getDefaultInstance();
        mainActivity.setupUI("Appraisal");

        getData();

        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(requestQueue != null)
            requestQueue.cancelAll(TAG);
    }

    @OnClick(R.id.relativelayout_app_dashboard_a)
    protected void onAClick() {
        AppraisalDetailBottomFragment app = new AppraisalDetailBottomFragment();
        RealmResults<RealmAppDetail> realmAppDetails = mRealm.where(RealmAppDetail.class).equalTo("rate", 5).findAll();
        Bundle bundle = new Bundle();
        bundle.putString("empid", realmAppDetails.get(0).getLastEmpid());
        bundle.putString("title", realmAppDetails.get(0).getRateName());
        app.setArguments(bundle);
        app.show(mainActivity.getSupportFragmentManager(), null);
    }

    @OnClick(R.id.relativelayout_app_dashboard_b)
    protected void onBClick() {
        AppraisalDetailBottomFragment app = new AppraisalDetailBottomFragment();
        RealmResults<RealmAppDetail> realmAppDetails = mRealm.where(RealmAppDetail.class).equalTo("rate", 4).findAll();
        Bundle bundle = new Bundle();
        bundle.putString("empid", realmAppDetails.get(0).getLastEmpid());
        bundle.putString("title", realmAppDetails.get(0).getRateName());
        app.setArguments(bundle);
        app.show(mainActivity.getSupportFragmentManager(), null);
    }

    @OnClick(R.id.relativelayout_app_dashboard_c)
    protected void onCClick() {
        AppraisalDetailBottomFragment app = new AppraisalDetailBottomFragment();
        RealmResults<RealmAppDetail> realmAppDetails = mRealm.where(RealmAppDetail.class).equalTo("rate", 3).findAll();
        Bundle bundle = new Bundle();
        bundle.putString("empid", realmAppDetails.get(0).getLastEmpid());
        bundle.putString("title", realmAppDetails.get(0).getRateName());
        app.setArguments(bundle);
        app.show(mainActivity.getSupportFragmentManager(), null);
    }

    @OnClick(R.id.relativelayout_app_dashboard_d)
    protected void onDClick() {
        AppraisalDetailBottomFragment app = new AppraisalDetailBottomFragment();
        RealmResults<RealmAppDetail> realmAppDetails = mRealm.where(RealmAppDetail.class).equalTo("rate", 2).findAll();
        Bundle bundle = new Bundle();
        bundle.putString("empid", realmAppDetails.get(0).getLastEmpid());
        bundle.putString("title", realmAppDetails.get(0).getRateName());
        app.setArguments(bundle);
        app.show(mainActivity.getSupportFragmentManager(), null);
    }

    @OnClick(R.id.relativelayout_app_dashboard_e)
    protected void onEClick() {
        AppraisalDetailBottomFragment app = new AppraisalDetailBottomFragment();
        RealmResults<RealmAppDetail> realmAppDetails = mRealm.where(RealmAppDetail.class).equalTo("rate", 1).findAll();
        Bundle bundle = new Bundle();
        bundle.putString("empid", realmAppDetails.get(0).getLastEmpid());
        bundle.putString("title", realmAppDetails.get(0).getRateName());
        app.setArguments(bundle);
        app.show(mainActivity.getSupportFragmentManager(), null);
    }

    private void getData() {
        progressBar.setVisibility(View.VISIBLE);
        progressBarText.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.GONE);
        progressBarText.setText("Loading Data");

        JsonArrayRequest request = volleyRequest.getAuthJsonArray(mainActivity.getApplicationContext(), TAG, volleyRequest.getAppDetail(), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                final List<RealmAppDetail> realmAppDetails = JSONParser.parseAppDetail(response);

                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.delete(RealmAppDetail.class);

                        for(RealmAppDetail realmAppDetail : realmAppDetails) {
                            realm.copyToRealm(realmAppDetail);
                        }
                    }
                });

                RealmResults<RealmAppDetail> query1 = mRealm.where(RealmAppDetail.class).findAll();

                for(RealmAppDetail realmAppDetail : query1) {
                    if(realmAppDetail.getRate() == 5) {
                        detailA.setText(String.valueOf(realmAppDetail.getLastCount()));
                    } else if(realmAppDetail.getRate() == 4) {
                        detailB.setText(String.valueOf(realmAppDetail.getLastCount()));
                    } else if(realmAppDetail.getRate() == 3) {
                        detailC.setText(String.valueOf(realmAppDetail.getLastCount()));
                    } else if(realmAppDetail.getRate() == 2) {
                        detailD.setText(String.valueOf(realmAppDetail.getLastCount()));
                    } else if(realmAppDetail.getRate() == 1) {
                        detailE.setText(String.valueOf(realmAppDetail.getLastCount()));
                    }
                }

                if(query1.size() > 0) {
                    progressBar.setVisibility(View.GONE);
                    progressBarText.setVisibility(View.GONE);
                    linearLayout.setVisibility(View.VISIBLE);

                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBarText.setVisibility(View.VISIBLE);
                    linearLayout.setVisibility(View.GONE);
                    progressBarText.setText("No Data Found");
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(request);
    }

}
