@file:JvmName("RealmUtils")

package ph.net.swak.attendanceinsights.utils

import io.realm.Realm
import io.realm.RealmModel
import io.realm.RealmResults
import ph.net.swak.attendanceinsights.db.dao.EmployeeDao
import ph.net.swak.attendanceinsights.db.dao.PayrollDao
import ph.net.swak.attendanceinsights.db.realm.LiveRealmData

fun Realm.employeeModel(): EmployeeDao = EmployeeDao()
fun Realm.payrollModel(): PayrollDao = PayrollDao()


fun <T : RealmModel> RealmResults<T>.asLiveData() = LiveRealmData<T>(this)