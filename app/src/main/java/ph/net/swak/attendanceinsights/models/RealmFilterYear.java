package ph.net.swak.attendanceinsights.models;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mikepenz.fastadapter.IItem;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import ph.net.swak.attendanceinsights.R;


public class RealmFilterYear extends RealmObject implements IItem<RealmFilterYear, RealmFilterYear.ViewHolder> {

    private String title;

    public String getTitle() {
        return title;
    }

    public RealmFilterYear withTitle(String title) {
        this.title = title;
        return this;
    }

    @PrimaryKey
    protected long mIdentifier = -1;

    public RealmFilterYear withIdentifier(long identifier) {
        this.mIdentifier = identifier;
        return this;
    }

    @Override
    public long getIdentifier() {
        return mIdentifier;
    }

    @Ignore
    protected Object mTag;

    public RealmFilterYear withTag(Object object) {
        this.mTag = object;
        return this;
    }

    @Override
    public Object getTag() {
        return mTag;
    }

    @Ignore
    protected boolean mEnabled = true;

    @Override
    public RealmFilterYear withEnabled(boolean enabled) {
        this.mEnabled = enabled;
        return this;
    }

    @Override
    public boolean isEnabled() {
        return mEnabled;
    }


    protected boolean mSelected = false;

    @Override
    public RealmFilterYear withSetSelected(boolean selected) {
        this.mSelected = selected;
        return this;
    }

    @Override
    public boolean isSelected() {
        return mSelected;
    }

    @Ignore
    protected boolean mSelectable = true;

    @Override
    public RealmFilterYear withSelectable(boolean selectable) {
        this.mSelectable = selectable;
        return this;
    }

    @Override
    public boolean isSelectable() {
        return mSelectable;
    }

    @Override
    public int getType() {
        return R.id.fastadapter_bottom_sheet_filter_year_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.viewgroup_filter_year;
    }

    @Override
    public View generateView(Context ctx) {
        ViewHolder viewHolder = getViewHolder(LayoutInflater.from(ctx).inflate(getLayoutRes(), null, false));

        bindView(viewHolder, Collections.EMPTY_LIST);

        return viewHolder.itemView;
    }

    @Override
    public View generateView(Context ctx, ViewGroup parent) {
        ViewHolder viewHolder = getViewHolder(LayoutInflater.from(ctx).inflate(getLayoutRes(), parent, false));

        bindView(viewHolder, Collections.EMPTY_LIST);

        return viewHolder.itemView;
    }

    @Override
    public ViewHolder getViewHolder(ViewGroup parent) {
        return getViewHolder(LayoutInflater.from(parent.getContext()).inflate(getLayoutRes(), parent, false));
    }

    private ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public void bindView(final ViewHolder holder, List<Object> payloads) {
        holder.name.setText(getTitle());
    }

    @Override
    public void unbindView(ViewHolder holder) {
        holder.name.setText(null);
    }

    @Override
    public void attachToWindow(ViewHolder viewHolder) {

    }

    @Override
    public void detachFromWindow(ViewHolder viewHolder) {

    }

    @Override
    public boolean failedToRecycle(ViewHolder viewHolder) {
        return false;
    }

    @Override
    public boolean equals(int id) {
        return id == mIdentifier;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        AbstractItem<?, ?> that = (AbstractItem<?, ?>) obj;
        return mIdentifier == that.getIdentifier();
    }

    @Override
    public int hashCode() {
        return Long.valueOf(mIdentifier).hashCode();
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textview_viewgroup_filter_year_title)
        protected TextView name;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
