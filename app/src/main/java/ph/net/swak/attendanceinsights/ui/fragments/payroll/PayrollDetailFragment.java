package ph.net.swak.attendanceinsights.ui.fragments.payroll;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Case;
import io.realm.Realm;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.models.RealmPayroll;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.ui.fragments.EmployeeBottomFragment;
import ph.net.swak.attendanceinsights.utils.DateUtil;
import ph.net.swak.attendanceinsights.utils.Helper;

/**
 * A simple {@link Fragment} subclass.
 */
public class PayrollDetailFragment extends Fragment {

    @BindView(R.id.cardview_payroll_detail)
    protected NestedScrollView container;

    @BindView(R.id.textview_payroll_detail_title)
    protected TextView companyTitle;
    @BindView(R.id.textview_payroll_detail_div_name)
    protected TextView textDivName;

    @BindView(R.id.textview_payroll_detail_manager_total_employee)
    protected TextView managerTotal;
    @BindView(R.id.textview_payroll_detail_supervisor_total_employee)
    protected TextView supervisorTotal;
    @BindView(R.id.textview_payroll_detail_staff_total_employee)
    protected TextView staffTotal;

    @BindView(R.id.textview_payroll_detail_manager_total_gross)
    protected TextView managerGross;
    @BindView(R.id.textview_payroll_detail_supervisor_total_gross)
    protected TextView supervisorGross;
    @BindView(R.id.textview_payroll_detail_staff_total_gross)
    protected TextView staffGross;

    @BindView(R.id.textview_payroll_detail_date_scope)
    protected TextView dateScope;

    private Unbinder unbinder;
    private MainActivity mainActivity;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private Bundle bundlePayrollDetail;
    private final String TAG = "PayrollDetailFragment";
    private DecimalFormat dF = new DecimalFormat("#,##0.00");
    private Realm mRealm;

    public PayrollDetailFragment() {
    }

    public static PayrollDetailFragment newInstance(Bundle bundle, String title) {
        PayrollDetailFragment fragment = new PayrollDetailFragment();
        Bundle args = new Bundle();
        args.putAll(bundle);
        args.putString("divName", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payroll_detail, container, false);
        mRealm = Realm.getDefaultInstance();
        unbinder = ButterKnife.bind(this, view);
        requestQueue = VolleyAPI.getInstance(getActivity()).getRequestQueue();
        volleyRequest = new VolleyRequest();
        mainActivity = (MainActivity) getActivity();

        loadData();

        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (requestQueue != null)
            requestQueue.cancelAll(TAG);
    }

    @OnClick(R.id.textview_payroll_detail_manager)
    public void onManagerClick() {
        EmployeeBottomFragment employeeBottomFragment = new EmployeeBottomFragment();

        Bundle bundle = new Bundle();

        if (bundlePayrollDetail != null && !bundlePayrollDetail.isEmpty()) {

            if(bundlePayrollDetail.getString("payrolltype").equalsIgnoreCase("monthly")) {
                bundle.putString("month", bundlePayrollDetail.getString("month"));
            } else if(bundlePayrollDetail.getString("payrolltype").equalsIgnoreCase("quarterly")) {
                if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 1)
                    bundle.putString("month", "1");
                else if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 2)
                    bundle.putString("month", "2");
                else if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 3)
                    bundle.putString("month", "3");
                else if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 4)
                    bundle.putString("month", "4");
            }

            bundle.putInt("compid", bundlePayrollDetail.getInt("compid"));
            bundle.putString("divname", bundlePayrollDetail.getString("divName"));
            bundle.putString("payrolltype", bundlePayrollDetail.getString("payrolltype"));
        }
        bundle.putString("position", "manager");
        employeeBottomFragment.setArguments(bundle);
        employeeBottomFragment.show(mainActivity.getSupportFragmentManager(), null);
    }

    @OnClick(R.id.textview_payroll_detail_supervisor)
    public void onSupervisorClick() {
        EmployeeBottomFragment employeeBottomFragment = new EmployeeBottomFragment();
        Bundle bundle = new Bundle();

        if (bundlePayrollDetail != null && !bundlePayrollDetail.isEmpty()) {

            if(bundlePayrollDetail.getString("payrolltype").equalsIgnoreCase("monthly")) {
                bundle.putString("month", bundlePayrollDetail.getString("month"));
            } else if(bundlePayrollDetail.getString("payrolltype").equalsIgnoreCase("quarterly")) {
                if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 1)
                    bundle.putString("month", "1");
                else if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 2)
                    bundle.putString("month", "2");
                else if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 3)
                    bundle.putString("month", "3");
                else if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 4)
                    bundle.putString("month", "4");
            }

            bundle.putInt("compid", bundlePayrollDetail.getInt("compid"));
            bundle.putString("divname", bundlePayrollDetail.getString("divName"));
            bundle.putString("payrolltype", bundlePayrollDetail.getString("payrolltype"));
        }

        bundle.putString("position", "supervisor");
        employeeBottomFragment.setArguments(bundle);
        employeeBottomFragment.show(mainActivity.getSupportFragmentManager(), null);
    }

    @OnClick(R.id.textview_payroll_detail_staff)
    public void onStaffClick() {
        EmployeeBottomFragment employeeBottomFragment = new EmployeeBottomFragment();
        Bundle bundle = new Bundle();

        if (bundlePayrollDetail != null && !bundlePayrollDetail.isEmpty()) {

            if(bundlePayrollDetail.getString("payrolltype").equalsIgnoreCase("monthly")) {
                bundle.putString("month", bundlePayrollDetail.getString("month"));
            } else if(bundlePayrollDetail.getString("payrolltype").equalsIgnoreCase("quarterly")) {
                if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 1)
                    bundle.putString("month", "1");
                else if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 2)
                    bundle.putString("month", "2");
                else if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 3)
                    bundle.putString("month", "3");
                else if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 4)
                    bundle.putString("month", "4");
            }

            bundle.putInt("compid", bundlePayrollDetail.getInt("compid"));
            bundle.putString("divname", bundlePayrollDetail.getString("divName"));
            bundle.putString("payrolltype", bundlePayrollDetail.getString("payrolltype"));
        }

        bundle.putString("position", "staff");
        employeeBottomFragment.setArguments(bundle);
        employeeBottomFragment.show(mainActivity.getSupportFragmentManager(), null);
    }

    private void loadData() {
        bundlePayrollDetail = getArguments();
        if (bundlePayrollDetail != null && !bundlePayrollDetail.isEmpty()) {
            companyTitle.setText(bundlePayrollDetail.getString("compName"));
            textDivName.setText(bundlePayrollDetail.getString("divName").equalsIgnoreCase("all") ? "All Division" : Helper.toTitleCase(bundlePayrollDetail.getString("divName")));

            int totalManager = 0, totalSupervisor = 0, totalStaff = 0;
            double grossManager = 0, grossSupervisor = 0, grossStaff = 0;

            if(bundlePayrollDetail.getString("divName", "all").equalsIgnoreCase("all")) {
                if(bundlePayrollDetail.getString("payrolltype").equalsIgnoreCase("monthly")) {

                    totalManager = mRealm.where(RealmPayroll.class).equalTo("covMonth", Integer.parseInt(bundlePayrollDetail.getString("month"))).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("manager", true).distinct("empId").size();
                    totalSupervisor = mRealm.where(RealmPayroll.class).equalTo("covMonth", Integer.parseInt(bundlePayrollDetail.getString("month"))).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("supervisor", true).distinct("empId").size();
                    totalStaff = mRealm.where(RealmPayroll.class).equalTo("covMonth", Integer.parseInt(bundlePayrollDetail.getString("month"))).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("staff", true).distinct("empId").size();

                    grossManager = mRealm.where(RealmPayroll.class).equalTo("covMonth", Integer.parseInt(bundlePayrollDetail.getString("month"))).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("manager", true).sum("totalGross").doubleValue();
                    grossSupervisor = mRealm.where(RealmPayroll.class).equalTo("covMonth", Integer.parseInt(bundlePayrollDetail.getString("month"))).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("supervisor", true).sum("totalGross").doubleValue();
                    grossStaff = mRealm.where(RealmPayroll.class).equalTo("covMonth", Integer.parseInt(bundlePayrollDetail.getString("month"))).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("staff", true).sum("totalGross").doubleValue();

                } else if(bundlePayrollDetail.getString("payrolltype").equalsIgnoreCase("quarterly")) {
                    Integer[] monthArray = new Integer[]{1,2,3};

                    if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 1)
                        monthArray = new Integer[]{1,2,3};
                    else if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 2)
                        monthArray = new Integer[]{4,5,6};
                    else if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 3)
                        monthArray = new Integer[]{7,8,9};
                    else if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 4)
                        monthArray = new Integer[]{10,11,12};

                    totalManager = mRealm.where(RealmPayroll.class).in("covMonth", monthArray).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("manager", true).distinct("empId").size();
                    totalSupervisor = mRealm.where(RealmPayroll.class).in("covMonth", monthArray).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("supervisor", true).distinct("empId").size();
                    totalStaff = mRealm.where(RealmPayroll.class).in("covMonth", monthArray).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("staff", true).distinct("empId").size();

                    grossManager = mRealm.where(RealmPayroll.class).in("covMonth", monthArray).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("manager", true).sum("totalGross").doubleValue();
                    grossSupervisor = mRealm.where(RealmPayroll.class).in("covMonth", monthArray).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("supervisor", true).sum("totalGross").doubleValue();
                    grossStaff = mRealm.where(RealmPayroll.class).in("covMonth", monthArray).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("staff", true).sum("totalGross").doubleValue();

                }
            } else {

                if(bundlePayrollDetail.getString("payrolltype").equalsIgnoreCase("monthly")) {

                    totalManager = mRealm.where(RealmPayroll.class).equalTo("divName", bundlePayrollDetail.getString("divName"), Case.INSENSITIVE).equalTo("covMonth", Integer.parseInt(bundlePayrollDetail.getString("month"))).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("manager", true).distinct("empId").size();
                    totalSupervisor = mRealm.where(RealmPayroll.class).equalTo("divName", bundlePayrollDetail.getString("divName"), Case.INSENSITIVE).equalTo("covMonth", Integer.parseInt(bundlePayrollDetail.getString("month"))).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("supervisor", true).distinct("empId").size();
                    totalStaff = mRealm.where(RealmPayroll.class).equalTo("divName", bundlePayrollDetail.getString("divName"), Case.INSENSITIVE).equalTo("covMonth", Integer.parseInt(bundlePayrollDetail.getString("month"))).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("staff", true).distinct("empId").size();

                    grossManager = mRealm.where(RealmPayroll.class).equalTo("divName", bundlePayrollDetail.getString("divName"), Case.INSENSITIVE).equalTo("covMonth", Integer.parseInt(bundlePayrollDetail.getString("month"))).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("manager", true).sum("totalGross").doubleValue();
                    grossSupervisor = mRealm.where(RealmPayroll.class).equalTo("divName", bundlePayrollDetail.getString("divName"), Case.INSENSITIVE).equalTo("covMonth", Integer.parseInt(bundlePayrollDetail.getString("month"))).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("supervisor", true).sum("totalGross").doubleValue();
                    grossStaff = mRealm.where(RealmPayroll.class).equalTo("divName", bundlePayrollDetail.getString("divName"), Case.INSENSITIVE).equalTo("covMonth", Integer.parseInt(bundlePayrollDetail.getString("month"))).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("staff", true).sum("totalGross").doubleValue();

                } else if(bundlePayrollDetail.getString("payrolltype").equalsIgnoreCase("quarterly")) {
                    Integer[] monthArray = new Integer[]{1,2,3};

                    if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 1)
                        monthArray = new Integer[]{1,2,3};
                    else if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 2)
                        monthArray = new Integer[]{4,5,6};
                    else if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 3)
                        monthArray = new Integer[]{7,8,9};
                    else if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 4)
                        monthArray = new Integer[]{10,11,12};

                    totalManager = mRealm.where(RealmPayroll.class).equalTo("divName", bundlePayrollDetail.getString("divName"), Case.INSENSITIVE).in("covMonth", monthArray).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("manager", true).distinct("empId").size();
                    totalSupervisor = mRealm.where(RealmPayroll.class).equalTo("divName", bundlePayrollDetail.getString("divName"), Case.INSENSITIVE).in("covMonth", monthArray).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("supervisor", true).distinct("empId").size();
                    totalStaff = mRealm.where(RealmPayroll.class).equalTo("divName", bundlePayrollDetail.getString("divName"), Case.INSENSITIVE).in("covMonth", monthArray).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("staff", true).distinct("empId").size();

                    grossManager = mRealm.where(RealmPayroll.class).equalTo("divName", bundlePayrollDetail.getString("divName"), Case.INSENSITIVE).in("covMonth", monthArray).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("manager", true).sum("totalGross").doubleValue();
                    grossSupervisor = mRealm.where(RealmPayroll.class).equalTo("divName", bundlePayrollDetail.getString("divName"), Case.INSENSITIVE).in("covMonth", monthArray).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("supervisor", true).sum("totalGross").doubleValue();
                    grossStaff = mRealm.where(RealmPayroll.class).equalTo("divName", bundlePayrollDetail.getString("divName"), Case.INSENSITIVE).in("covMonth", monthArray).equalTo("compId", bundlePayrollDetail.getInt("compid")).equalTo("staff", true).sum("totalGross").doubleValue();

                }
            }

            managerTotal.setText(String.valueOf(totalManager) + " Employees");
            supervisorTotal.setText(String.valueOf(totalSupervisor) + " Employees");
            staffTotal.setText(String.valueOf(totalStaff) + " Employees");

            managerGross.setText("Gross: Php " + dF.format(grossManager));
            supervisorGross.setText("Gross: Php " + dF.format(grossSupervisor));
            staffGross.setText("Gross: Php " + dF.format(grossStaff));

            DateUtil dateUtil = new DateUtil();
            if(bundlePayrollDetail.getString("payrolltype").equalsIgnoreCase("monthly")) {
                dateScope.setText("Date Scope: " + dateUtil.getMonth(bundlePayrollDetail.getString("month"), "MM") + " " + bundlePayrollDetail.getString("year"));
            } else if(bundlePayrollDetail.getString("payrolltype").equalsIgnoreCase("quarterly")) {
                StringBuilder sb = new StringBuilder();
                sb.append("Date Scope: ");

                if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 1)
                    sb.append("1st Quarter");
                else if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 2)
                    sb.append("2nd Quarter");
                else if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 3)
                    sb.append("3rd Quarter");
                else if(Integer.parseInt(bundlePayrollDetail.getString("month")) == 4)
                    sb.append("4th Quarter");

                sb.append(bundlePayrollDetail.getString(" year", ""));
                dateScope.setText(sb.toString());
            }

        }
    }
}
