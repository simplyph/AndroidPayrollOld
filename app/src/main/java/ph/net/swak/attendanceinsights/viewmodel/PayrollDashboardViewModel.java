package ph.net.swak.attendanceinsights.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import ph.net.swak.attendanceinsights.db.AbsentLiveData;
import ph.net.swak.attendanceinsights.db.realm.RealmPayrollHead;
import ph.net.swak.attendanceinsights.repository.PayrollRepository;
import ph.net.swak.attendanceinsights.repository.Resource;
import ph.net.swak.attendanceinsights.repository.Status;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.PayrollDashboardFastAdapter;
import ph.net.swak.attendanceinsights.utils.Helper;

public class PayrollDashboardViewModel extends ViewModel {

    private PayrollRepository payrollRepo;
    private MutableLiveData<Resource<List<RealmPayrollHead>>> payrollHead = new MutableLiveData<>();
    private LiveData<Resource<List<PayrollDashboardFastAdapter>>> payrollMonthly;
    private int year = Helper.getCurrentYear();
    private Long[] companyId;

    @Inject
    public PayrollDashboardViewModel(PayrollRepository payrollRepository) {
        payrollRepo = payrollRepository;
        // Initialize payroll monthly
        payrollMonthly = Transformations.switchMap(payrollHead, payrollHead -> {
            if(payrollHead != null) {
                return payrollRepo.getPayrollMonthly(year, companyId);
            } else {
                return AbsentLiveData.create();
            }
        });
    }

    public void updateLiveData(int year, Long[] companyId) {
        payrollHead.setValue(null);
        this.year = year;
        this.companyId = companyId;
        payrollRepo.getPayrollHead(year).observeForever(listResource -> {
            if (listResource != null && listResource.status == Status.SUCCESS) {
                payrollHead.setValue(listResource);
            }
        });
    }

    public LiveData<Resource<List<PayrollDashboardFastAdapter>>> getPayrollMonthly() {
        return payrollMonthly;
    }
}
