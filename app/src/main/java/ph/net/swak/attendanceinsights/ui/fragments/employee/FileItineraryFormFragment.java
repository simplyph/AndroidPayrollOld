package ph.net.swak.attendanceinsights.ui.fragments.employee;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;

public class FileItineraryFormFragment extends Fragment {

    @BindView(R.id.sampledate1)
    protected EditText fromDateET;

    @BindView(R.id.seltime1)
    protected EditText editTimeFrom;

    private MainActivity mainActivity;
    private Unbinder unbinder;

    private Calendar calendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener fromDate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            calendar.set(Calendar.YEAR, i);
            calendar.set(Calendar.MONTH, i1);
            calendar.set(Calendar.DAY_OF_MONTH, i2);

            updateFromDate();

        }
    };
    private Calendar calendarTime = Calendar.getInstance();
    TimePickerDialog.OnTimeSetListener fromTime = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hour, int min) {
            calendarTime.set(Calendar.HOUR, hour);
            calendarTime.set(Calendar.MINUTE, min);

            updateFromTime();

        }
    };


    public FileItineraryFormFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_file_itinerary_form, container, false);
        unbinder = ButterKnife.bind(this, view);
        mainActivity = (MainActivity) getActivity();

        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @OnClick(R.id.sampledate1)
    protected void onFromDateClick() {
        new DatePickerDialog(mainActivity, fromDate, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();

    }

    @OnClick(R.id.seltime1)
    protected void onFromTimeClick() {
        new TimePickerDialog(mainActivity, fromTime, calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), false).show();

    }

    protected void updateFromTime() {

        String format = "hh:mm aaa";
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);

        editTimeFrom.setText(sdf.format(calendarTime.getTime()));

    }

    protected void updateFromDate() {

        String format = "MMM dd, yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);

        fromDateET.setText(sdf.format(calendar.getTime()));

    }


}
