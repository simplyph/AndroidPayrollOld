package ph.net.swak.attendanceinsights.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.android.volley.RequestQueue;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.utils.LocaleHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {

    @BindView(R.id.setting_select_language)
    protected Spinner spinner;

    private Unbinder unbinder;
    private MainActivity mainActivity;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;

    public SettingsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        unbinder = ButterKnife.bind(this, view);
        requestQueue = VolleyAPI.getInstance(getActivity()).getRequestQueue();
        volleyRequest = new VolleyRequest();
        mainActivity = (MainActivity) getActivity();

        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(mainActivity, R.array.language_selection, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);

        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @OnClick(R.id.setting_save_language)
    public void saveLanguage() {

        String selected = spinner.getSelectedItem().toString();

        String languageCode;

        if(selected.equalsIgnoreCase("filipino")) {
            languageCode = "fil";
        } else {
            languageCode = "en";
        }

        LocaleHelper.setLocale(mainActivity, languageCode);

        mainActivity.recreate();
    }
}
