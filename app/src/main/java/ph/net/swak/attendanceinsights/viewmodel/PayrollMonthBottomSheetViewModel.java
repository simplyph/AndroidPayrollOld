package ph.net.swak.attendanceinsights.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import ph.net.swak.attendanceinsights.repository.PayrollRepository;
import ph.net.swak.attendanceinsights.repository.Resource;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.PayrollMonthFastAdapter;

public class PayrollMonthBottomSheetViewModel extends ViewModel {

    private PayrollRepository payrollRepo;
    private MutableLiveData<Resource<List<PayrollMonthFastAdapter>>> monthList = new MutableLiveData<>();
    private MutableLiveData<Resource<List<PayrollMonthFastAdapter>>> quarterList = new MutableLiveData<>();

    @Inject
    public PayrollMonthBottomSheetViewModel(PayrollRepository payrollRepository) {
        payrollRepo = payrollRepository;
    }

    public void fetchData(int year, int compId, int from, int fromId) {
        payrollRepo.getPayrollByMonth(year, compId, from, fromId).observeForever(listResource -> {
            if (listResource != null && listResource.data != null) {
                monthList.setValue(listResource);
            }
        });

        payrollRepo.getPayrollByQuarter(year, compId, from, fromId).observeForever(listResource -> {
            if (listResource != null && listResource.data != null) {
                quarterList.setValue(listResource);
            }
        });
    }

    public LiveData<Resource<List<PayrollMonthFastAdapter>>> getMonthList() {
        return monthList;
    }

    public LiveData<Resource<List<PayrollMonthFastAdapter>>> getQuarterList() {
        return quarterList;
    }
}
