package ph.net.swak.attendanceinsights.models;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RealmHrisDashboard extends RealmObject {

    @PrimaryKey
    private long mIdentifier = -1;
    private int empId;
    private String nameLast;
    private String nameFirst;
    private int age;
    private String dateHired;
    private String dateTerminated;
    private String statusDesc;
    private String statusType;
    private double salaryRate;
    private int compId;
    private String comp;
    private int deptId;
    private String dept;
    private int desigId;
    private String desig;
    private String gender;
    private Date hiredDate;
    private Date renounceDate;
    private int hireYear;
    private int renounceYear;

    public long getIdentifier() {
        return mIdentifier;
    }

    public RealmHrisDashboard withIdentifier(long mIdentifier) {
        this.mIdentifier = mIdentifier;
        return this;
    }

    public int getEmpId() {
        return empId;
    }

    public RealmHrisDashboard withEmpId(int empId) {
        this.empId = empId;
        return this;
    }

    public String getNameLast() {
        return nameLast;
    }

    public RealmHrisDashboard withNameLast(String nameLast) {
        this.nameLast = nameLast;
        return this;
    }

    public String getNameFirst() {
        return nameFirst;
    }

    public RealmHrisDashboard withNameFirst(String nameFirst) {
        this.nameFirst = nameFirst;
        return this;
    }

    public int getAge() {
        return age;
    }

    public RealmHrisDashboard withAge(int age) {
        this.age = age;
        return this;
    }

    public String getDateHired() {
        return dateHired;
    }

    public RealmHrisDashboard withDateHired(String dateHired) {
        this.dateHired = dateHired;
        return this;
    }

    public String getDateTerminated() {
        return dateTerminated;
    }

    public RealmHrisDashboard withDateTerminated(String dateTerminated) {
        this.dateTerminated = dateTerminated;
        return this;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public RealmHrisDashboard withStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
        return this;
    }

    public String getStatusType() {
        return statusType;
    }

    public RealmHrisDashboard withStatusType(String statusType) {
        this.statusType = statusType;
        return this;
    }

    public double getSalaryRate() {
        return salaryRate;
    }

    public RealmHrisDashboard withSalaryRate(double salaryRate) {
        this.salaryRate = salaryRate;
        return this;
    }

    public int getCompId() {
        return compId;
    }

    public RealmHrisDashboard withCompId(int compId) {
        this.compId = compId;
        return this;
    }

    public String getComp() {
        return comp;
    }

    public RealmHrisDashboard withComp(String comp) {
        this.comp = comp;
        return this;
    }

    public int getDeptId() {
        return deptId;
    }

    public RealmHrisDashboard withDeptId(int deptId) {
        this.deptId = deptId;
        return this;
    }

    public String getDept() {
        return dept;
    }

    public RealmHrisDashboard withDept(String dept) {
        this.dept = dept;
        return this;
    }

    public int getDesigId() {
        return desigId;
    }

    public RealmHrisDashboard withDesigId(int desigId) {
        this.desigId = desigId;
        return this;
    }

    public String getDesig() {
        return desig;
    }

    public RealmHrisDashboard withDesig(String desig) {
        this.desig = desig;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public RealmHrisDashboard withGender(String gender) {
        this.gender = gender;
        return this;
    }

    public Date getHiredDate() {
        return hiredDate;
    }

    public RealmHrisDashboard withHiredDate(Date hiredDate) {
        this.hiredDate = hiredDate;
        return this;
    }

    public Date getRenounceDate() {
        return renounceDate;
    }

    public RealmHrisDashboard withRenounceDate(Date renounceDate) {
        this.renounceDate = renounceDate;
        return this;
    }

    public int getHireYear() {
        return hireYear;
    }

    public RealmHrisDashboard withHireYear(int hireYear) {
        this.hireYear = hireYear;
        return this;
    }

    public int getRenounceYear() {
        return renounceYear;
    }

    public RealmHrisDashboard withRenounceYear(int renounceYear) {
        this.renounceYear = renounceYear;
        return this;
    }
}
