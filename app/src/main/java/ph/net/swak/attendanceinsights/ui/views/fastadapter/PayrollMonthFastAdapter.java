package ph.net.swak.attendanceinsights.ui.views.fastadapter;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.PayrollDetailsMainFragment;
import ph.net.swak.attendanceinsights.ui.views.pageradapter.PayrollDetailsPagerAdapter;
import ph.net.swak.attendanceinsights.utils.Helper;
import ph.net.swak.attendanceinsights.utils.PayrollPeriodType;

public class PayrollMonthFastAdapter extends AbstractItem<PayrollMonthFastAdapter, PayrollMonthFastAdapter.ViewHolder> {

    private int compId;
    private String compName;
    private int month;
    private int year;
    private double value;
    private int count;
    private int payrollPeriod;
    private Dialog dialog;

    public int getCompId() {
        return compId;
    }

    public PayrollMonthFastAdapter withCompId(int compId) {
        this.compId = compId;
        return this;
    }

    public String getCompName() {
        return compName;
    }

    public PayrollMonthFastAdapter withCompName(String compName) {
        this.compName = compName;
        return this;
    }

    public int getMonth() {
        return month;
    }

    public PayrollMonthFastAdapter withMonth(int month) {
        this.month = month;
        return this;
    }

    public int getYear() {
        return year;
    }

    public PayrollMonthFastAdapter withYear(int year) {
        this.year = year;
        return this;
    }

    public double getValue() {
        return value;
    }

    public PayrollMonthFastAdapter withValue(double value) {
        this.value = value;
        return this;
    }

    public int getCount() {
        return count;
    }

    public PayrollMonthFastAdapter withCount(int count) {
        this.count = count;
        return this;
    }

    public int getPayrollPeriod() {
        return payrollPeriod;
    }

    public PayrollMonthFastAdapter withPayrollPeriod(int payrollPeriod) {
        this.payrollPeriod = payrollPeriod;
        return this;
    }

    public Dialog getDialog() {
        return dialog;
    }

    public PayrollMonthFastAdapter withDialog(Dialog dialog) {
        this.dialog = dialog;
        return this;
    }

    @Override
    public ViewHolder getViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public int getType() {
        return R.id.fastadapter_payroll_month_bottom_sheet_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.item_payroll_month_bottom_sheet;
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);
        if(payrollPeriod == PayrollPeriodType.MONTHLY.getOrder())
            holder.tvMonth.setText( Helper.toStringMonth(getMonth()-1));
        else
            holder.tvMonth.setText(String.format(Locale.ENGLISH, "Quarter %d", getMonth()));

        holder.tvValue.setText(String.format(Locale.ENGLISH, "Php %s", Helper.toDecimal(getValue())));
        holder.tvCount.setText(String.format(Locale.ENGLISH, "%d Employees", getCount()));
        holder.itemView.setOnClickListener(v -> {
            getDialog().dismiss();
            Bundle bundle = new Bundle();
            bundle.putInt("compId", getCompId());
            bundle.putInt("covMonth", getMonth());
            bundle.putInt("covYear", getYear());
            bundle.putString("compName", getCompName());
            bundle.putInt("payrollPeriod", payrollPeriod);

            PayrollDetailsMainFragment mainFragment = new PayrollDetailsMainFragment();
            mainFragment.setArguments(bundle);
            ((MainActivity)holder.itemView.getContext()).getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, mainFragment, null).addToBackStack("payroll").commit();
        });
    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);

        holder.tvMonth.setText(null);
        holder.tvValue.setText(null);
        holder.tvCount.setText(null);
        holder.itemView.setOnClickListener(null);
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textview_payroll_month_bottom_sheet_month)
        protected TextView tvMonth;

        @BindView(R.id.textview_payroll_month_bottom_sheet_value)
        protected TextView tvValue;

        @BindView(R.id.textview_payroll_month_bottom_sheet_count)
        protected TextView tvCount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
