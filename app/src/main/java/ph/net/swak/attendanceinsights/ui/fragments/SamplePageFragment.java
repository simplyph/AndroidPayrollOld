package ph.net.swak.attendanceinsights.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ph.net.swak.attendanceinsights.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SamplePageFragment extends Fragment {

    @BindView(R.id.textview_sample_page)
    protected TextView textView;

    public static final String ARG_PAGE = "ARG_PAGE";

    private int mPage;
    private Unbinder unbinder;

    public static SamplePageFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        SamplePageFragment fragment = new SamplePageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public SamplePageFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sample_page, container, false);
        unbinder = ButterKnife.bind(this, view);
        textView.setText("Fragment #" + mPage);
        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
