package ph.net.swak.attendanceinsights.db.realm;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class RealmPayrollQuick implements RealmModel {

    @PrimaryKey
    private int autoid = -1;
    @SerializedName("comp_id")
    private int compId;
    @SerializedName("comp_name")
    private String compName;
    @SerializedName("employee_total")
    private double totalEmployee;
    @SerializedName("employee_count")
    private int countEmployee;
    @SerializedName("manager_total")
    private double totalManager;
    @SerializedName("manager_count")
    private int countManager;
    @SerializedName("supervisor__total")
    private double totalSupervisor;
    @SerializedName("supervisor_count")
    private int countSupervsior;
    @SerializedName("staff_total")
    private double totalStaff;
    @SerializedName("staff_count")
    private int countStaff;
    @SerializedName("coveringyear")
    private int coveringYear;
    @SerializedName("coveringmonth2")
    private int coveringMonth;

    public int getAutoid() {
        return autoid;
    }

    public RealmPayrollQuick withAutoid(int autoid) {
        this.autoid = autoid;
        return this;
    }

    public int getCompId() {
        return compId;
    }

    public RealmPayrollQuick withCompId(int compId) {
        this.compId = compId;
        return this;
    }

    public String getCompName() {
        return compName;
    }

    public RealmPayrollQuick withCompName(String compName) {
        this.compName = compName;
        return this;
    }

    public double getTotalEmployee() {
        return totalEmployee;
    }

    public RealmPayrollQuick withTotalEmployee(double totalEmployee) {
        this.totalEmployee = totalEmployee;
        return this;
    }

    public int getCountEmployee() {
        return countEmployee;
    }

    public RealmPayrollQuick withCountEmployee(int countEmployee) {
        this.countEmployee = countEmployee;
        return this;
    }

    public double getTotalManager() {
        return totalManager;
    }

    public RealmPayrollQuick withTotalManager(double totalManager) {
        this.totalManager = totalManager;
        return this;
    }

    public int getCountManager() {
        return countManager;
    }

    public RealmPayrollQuick withCountManager(int countManager) {
        this.countManager = countManager;
        return this;
    }

    public double getTotalSupervisor() {
        return totalSupervisor;
    }

    public RealmPayrollQuick withTotalSupervisor(double totalSupervisor) {
        this.totalSupervisor = totalSupervisor;
        return this;
    }

    public int getCountSupervsior() {
        return countSupervsior;
    }

    public RealmPayrollQuick withCountSupervsior(int countSupervsior) {
        this.countSupervsior = countSupervsior;
        return this;
    }

    public double getTotalStaff() {
        return totalStaff;
    }

    public RealmPayrollQuick withTotalStaff(double totalStaff) {
        this.totalStaff = totalStaff;
        return this;
    }

    public int getCountStaff() {
        return countStaff;
    }

    public RealmPayrollQuick withCountStaff(int countStaff) {
        this.countStaff = countStaff;
        return this;
    }

    public int getCoveringYear() {
        return coveringYear;
    }

    public RealmPayrollQuick withCoveringYear(int coveringYear) {
        this.coveringYear = coveringYear;
        return this;
    }

    public int getCoveringMonth() {
        return coveringMonth;
    }

    public RealmPayrollQuick withCoveringMonth(int coveringMonth) {
        this.coveringMonth = coveringMonth;
        return this;
    }

    public RealmPayrollQuick() {}
}
