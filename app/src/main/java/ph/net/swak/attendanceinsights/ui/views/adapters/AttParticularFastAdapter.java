package ph.net.swak.attendanceinsights.ui.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikepenz.fastadapter.items.AbstractItem;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.events.UserDetailEvent;
import ph.net.swak.attendanceinsights.network.VolleyRequest;

public class AttParticularFastAdapter extends AbstractItem<AttParticularFastAdapter, AttParticularFastAdapter.ViewHolder> {

    private int empId;

    private String name;

    private String detail;

    private String imgPath;

    public int getEmpId() {
        return empId;
    }

    public AttParticularFastAdapter withEmpId(int empId) {
        this.empId = empId;
        return this;
    }

    public String getName() {
        return name;
    }

    public AttParticularFastAdapter withName(String name) {
        this.name = name;
        return this;
    }

    public String getDetail() {
        return detail;
    }

    public AttParticularFastAdapter withDetail(String detail) {
        this.detail = detail;
        return this;
    }

    public String getImgPath() {
        return imgPath;
    }

    public AttParticularFastAdapter withImgPath(String imgPath) {
        this.imgPath = imgPath;
        return this;
    }

    @Override
    public int getType() {
        return R.id.fastadapter_attendance_particular_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.viewgroup_att_particular;
    }

    @Override
    public void bindView(final ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);
        Log.e("ASASASA", VolleyRequest.URL_BASE + getImgPath());
        Glide.with(holder.itemView.getContext()).load(VolleyRequest.URL_BASE + getImgPath()).into(holder.imageView);
        holder.textViewEmpId.setText(String.valueOf(getEmpId()));
        holder.textViewName.setText(getName());
        holder.textViewDetail.setText(getDetail());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new UserDetailEvent(String.valueOf(getEmpId())));
            }
        });
    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);

        holder.imageView.setImageDrawable(null);
        holder.textViewEmpId.setText(null);
        holder.textViewName.setText(null);
        holder.textViewDetail.setText(null);
        holder.itemView.setOnClickListener(null);
    }

    @Override
    public ViewHolder getViewHolder(View view) {
        return new ViewHolder(view);
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageview_viewgroup_att_particular)
        protected ImageView imageView;

        @BindView(R.id.textview_viewgroup_att_particular_empid)
        protected TextView textViewEmpId;

        @BindView(R.id.textview_viewgroup_att_particular_name)
        protected TextView textViewName;

        @BindView(R.id.textview_viewgroup_att_particular_detail)
        protected TextView textViewDetail;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
