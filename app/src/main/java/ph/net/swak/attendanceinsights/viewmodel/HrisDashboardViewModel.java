package ph.net.swak.attendanceinsights.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import ph.net.swak.attendanceinsights.db.realm.RealmEmployee;
import ph.net.swak.attendanceinsights.repository.HrisRepository;
import ph.net.swak.attendanceinsights.repository.Resource;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.HrisCompanyFastAdapter;

public class HrisDashboardViewModel extends ViewModel {

    private HrisRepository hrisRepo;
    private LiveData<Resource<List<RealmEmployee>>> employeeList;
    private LiveData<Resource<List<HrisCompanyFastAdapter>>> employeeByCompany;

    @Inject
    public HrisDashboardViewModel(HrisRepository hrisRepository) {
        hrisRepo = hrisRepository;
        subscribeToEmployees();
    }

    public LiveData<Resource<List<RealmEmployee>>> getEmployees() {
        return employeeList;
    }

    public LiveData<Resource<List<HrisCompanyFastAdapter>>> getEmployeeByCompany() {
        return employeeByCompany;
    }

    private void subscribeToEmployees() {
        employeeList = hrisRepo.getEmployees();
        employeeByCompany = hrisRepo.getEmployeesByCompany();
    }

    public void getAllData() {
        employeeByCompany = hrisRepo.getEmployeesByCompany();
    }
}
