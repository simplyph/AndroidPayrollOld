package ph.net.swak.attendanceinsights.models;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.mikepenz.fastadapter.IItem;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import ph.net.swak.attendanceinsights.R;


public class RealmBulletinInfo extends RealmObject implements IItem<RealmBulletinInfo, RealmBulletinInfo.ViewHolder> {

    @PrimaryKey
    protected long mIdentifier = -1;
    @Ignore
    protected Object mTag;
    @Ignore
    protected boolean mEnabled = true;
    protected boolean mSelected = false;
    @Ignore
    protected boolean mSelectable = true;
    private String fileName;
    private String filePath;
    private String title;
    private String content;
    private int isPost;
    private int isFront;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    public String getFileName() {
        return fileName;
    }

    public RealmBulletinInfo withFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public String getFilePath() {
        return filePath;
    }

    public RealmBulletinInfo withFilePath(String filePath) {
        this.filePath = filePath;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public RealmBulletinInfo withTitle(String title) {
        this.title = title;
        return this;
    }

    public String getContent() {
        return content;
    }

    public RealmBulletinInfo withContent(String content) {
        this.content = content;
        return this;
    }

    public int getIsPost() {
        return isPost;
    }

    public RealmBulletinInfo withIsPost(int isPost) {
        this.isPost = isPost;
        return this;
    }

    public int getIsFront() {
        return isFront;
    }

    public RealmBulletinInfo withIsFront(int isFront) {
        this.isFront = isFront;
        return this;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public RealmBulletinInfo withCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public RealmBulletinInfo withCreatedDate(String createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public RealmBulletinInfo withModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
        return this;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public RealmBulletinInfo withModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
        return this;
    }

    public RealmBulletinInfo withIdentifier(long identifier) {
        this.mIdentifier = identifier;
        return this;
    }

    @Override
    public long getIdentifier() {
        return mIdentifier;
    }

    public RealmBulletinInfo withTag(Object object) {
        this.mTag = object;
        return this;
    }

    @Override
    public Object getTag() {
        return mTag;
    }

    @Override
    public RealmBulletinInfo withEnabled(boolean enabled) {
        this.mEnabled = enabled;
        return this;
    }

    @Override
    public boolean isEnabled() {
        return mEnabled;
    }

    @Override
    public RealmBulletinInfo withSetSelected(boolean selected) {
        this.mSelected = selected;
        return this;
    }

    @Override
    public boolean isSelected() {
        return mSelected;
    }

    @Override
    public RealmBulletinInfo withSelectable(boolean selectable) {
        this.mSelectable = selectable;
        return this;
    }

    @Override
    public boolean isSelectable() {
        return mSelectable;
    }

    @Override
    public int getType() {
        return R.id.fastadapter_bulletin_item_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.viewgroup_bulletin_info;
    }

    @Override
    public View generateView(Context ctx) {
        ViewHolder viewHolder = getViewHolder(LayoutInflater.from(ctx).inflate(getLayoutRes(), null, false));

        bindView(viewHolder, Collections.EMPTY_LIST);

        return viewHolder.itemView;
    }

    @Override
    public View generateView(Context ctx, ViewGroup parent) {
        ViewHolder viewHolder = getViewHolder(LayoutInflater.from(ctx).inflate(getLayoutRes(), parent, false));
        bindView(viewHolder, Collections.EMPTY_LIST);
        return viewHolder.itemView;
    }

    @Override
    public ViewHolder getViewHolder(ViewGroup parent) {
        return getViewHolder(LayoutInflater.from(parent.getContext()).inflate(getLayoutRes(), parent, false));
    }

    private ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public void bindView(final ViewHolder holder, List<Object> payloads) {
        holder.itemView.setTag(this);

        final Context context = holder.itemView.getContext();

        holder.imageview.setImageBitmap(null);

        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop();

        Glide.with(context).load(getFilePath())
                .thumbnail(0.5f)
                .apply(options)
                .into(holder.imageview);

        holder.textview.setText(getTitle());
    }

    @Override
    public void unbindView(ViewHolder holder) {
        Glide.with(holder.itemView.getContext()).clear(holder.imageview);
        holder.imageview.setImageDrawable(null);
        holder.textview.setText(null);
    }

    @Override
    public void attachToWindow(ViewHolder viewHolder) {

    }

    @Override
    public void detachFromWindow(ViewHolder viewHolder) {

    }

    @Override
    public boolean failedToRecycle(ViewHolder viewHolder) {
        return false;
    }

    @Override
    public boolean equals(int id) {
        return id == mIdentifier;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        AbstractItem<?, ?> that = (AbstractItem<?, ?>) obj;
        return mIdentifier == that.getIdentifier();
    }

    @Override
    public int hashCode() {
        return Long.valueOf(mIdentifier).hashCode();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageview)
        protected ImageView imageview;

        @BindView(R.id.textview)
        protected TextView textview;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
