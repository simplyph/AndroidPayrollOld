package ph.net.swak.attendanceinsights.ui.views.widgets.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.ui.views.adapters.SlideshowPagerAdapter;

public class SlideshowDialogFragment extends DialogFragment {

    @BindView(R.id.viewpager_slideshow)
    protected ViewPager viewPager;

    @BindView(R.id.textview_page)
    protected TextView textViewPage;

    @BindView(R.id.textview_title)
    protected TextView textViewTitle;

    @BindView(R.id.textview_info)
    protected TextView textViewInfo;

    private ArrayList<String> imagePaths = new ArrayList<>();
    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            displayInfo(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
    private int selectedPosition = 0;
    private Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_slideshow, container, false);
        unbinder = ButterKnife.bind(this, view);

        Bundle bundle = getArguments();

        imagePaths = bundle.getStringArrayList("paths");
        selectedPosition = bundle.getInt("position");

        SlideshowPagerAdapter pagerAdapter = new SlideshowPagerAdapter(imagePaths);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(onPageChangeListener);

        setCurrentItem(selectedPosition);

        return view;
    }

    private void setCurrentItem(int position) {
        viewPager.setCurrentItem(position, false);
        displayInfo(selectedPosition);
    }

    private void displayInfo(int position) {
        textViewPage.setText(String.valueOf(position + 1) + " of " + String.valueOf(imagePaths.size()));
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
