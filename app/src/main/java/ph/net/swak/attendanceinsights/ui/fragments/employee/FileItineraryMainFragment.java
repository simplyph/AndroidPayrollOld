package ph.net.swak.attendanceinsights.ui.fragments.employee;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.models.FileItinerary;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.utils.JSONParser;

/**
 * A simple {@link Fragment} subclass.
 */
public class FileItineraryMainFragment extends Fragment {

    private final String TAG = "FileItineraryMainFragment";
    // list view organizer
    @BindView(R.id.fileItiListView)
    protected ListView listView;
    @BindView(R.id.progressbar_employee_list_text)
    protected TextView progressBarText;
    @BindView(R.id.progressbar_employee_list)
    protected MaterialProgressBar progressBar;
    @BindView(R.id.container_employee_list)
    protected CardView container;
    @BindView(R.id.btn_fileItineraryFilter)
    protected Button filterFileItinerary;
    ArrayList<String> reason_resource = new ArrayList<>();
    ArrayList<String> date_iti_resource = new ArrayList<>();
    ArrayList<String> date_filed_resource = new ArrayList<>();
    ArrayList<String> status_resource = new ArrayList<>();
    ArrayList<String> approvers_name_resource = new ArrayList<>();
    ArrayList<String> approvers_reason_resource = new ArrayList<>();
    // get activities of MainActivity
    private MainActivity mainActivity;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    // Maintain data collection of the Main and this fragment
    private Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_file_itinerary_main, container, false);
        unbinder = ButterKnife.bind(this, view);

        requestQueue = VolleyAPI.getInstance(getActivity()).getRequestQueue();
        volleyRequest = new VolleyRequest();

        mainActivity = (MainActivity) getActivity();
        mainActivity.mTabLayout.setVisibility(View.GONE);

        mainActivity.fab.setVisibility(View.VISIBLE);
        mainActivity.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //HomeFragment fragment = new HomeFragment();
                //Bundle b = new Bundle();
                //b.putString("key", "ulul");
                //fragment.setArguments(b);
                mainActivity.getSupportFragmentManager().beginTransaction().addToBackStack("ParentItinerary").replace(R.id.main_frame, new FileItineraryFormFragment()).commit();
                mainActivity.fab.setVisibility(View.GONE);
                //Snackbar.make(v, "SAMPLE", Snackbar.LENGTH_SHORT).show();
            }
        });

        setupData();

        // Inflate the layout for this fragment
        return view;
    }


    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (requestQueue != null)
            requestQueue.cancelAll(TAG);
    }

    private void setupData() {

        progressBar.setVisibility(View.VISIBLE);
        progressBarText.setVisibility(View.VISIBLE);
        container.setVisibility(View.GONE);
        progressBarText.setText("Loading Data");

        JsonArrayRequest jsonArrayRequest = volleyRequest.getAuthJsonArray(mainActivity.getApplicationContext(), TAG, volleyRequest.getEmpItinerary("17000877", "", ""), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                List<FileItinerary> fileItineraryList = JSONParser.parseEmployeeItinerary(response);

                for (FileItinerary fileItinerary : fileItineraryList) {
                    reason_resource.add(fileItinerary.getReason());
                    date_iti_resource.add(fileItinerary.getDateitinerary());
                    date_filed_resource.add(fileItinerary.getDatefiled());
                    status_resource.add(fileItinerary.getStatus());
                    approvers_name_resource.add(fileItinerary.getApprover());
                    approvers_reason_resource.add(fileItinerary.getReason_disapprove());
                }

                listView.setAdapter(new ItineraryAdapter(mainActivity, reason_resource, date_iti_resource, date_filed_resource, status_resource, approvers_name_resource, approvers_reason_resource));

                progressBar.setVisibility(View.GONE);
                progressBarText.setVisibility(View.GONE);
                container.setVisibility(View.VISIBLE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressBar.setVisibility(View.GONE);
                progressBarText.setVisibility(View.VISIBLE);
                container.setVisibility(View.GONE);
                progressBarText.setText("No Data Found");
            }
        });
        requestQueue.add(jsonArrayRequest);
    }


    private class ItineraryAdapter extends ArrayAdapter<String> {

        private ArrayList<String> reason_resource;
        private ArrayList<String> date_iti_resource;
        private ArrayList<String> date_filed_resource;
        private ArrayList<String> status_resource;
        private ArrayList<String> approvers_name_resource;
        private ArrayList<String> approvers_reason_resource;

        public ItineraryAdapter(Context context, ArrayList<String> reason_resource, ArrayList<String> date_iti_resource, ArrayList<String> date_filed_resource, ArrayList<String> status_resource, ArrayList<String> approvers_name_resource, ArrayList<String> approvers_reason_resource) {
            super(context, 0, reason_resource);
            this.reason_resource = reason_resource;
            this.date_iti_resource = date_iti_resource;
            this.date_filed_resource = date_filed_resource;
            this.status_resource = status_resource;
            this.approvers_name_resource = approvers_name_resource;
            this.approvers_reason_resource = approvers_reason_resource;
        }

        @NonNull
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.viewgroup_file_itinerary, parent, false);
            }

            TextView value1 = (TextView) convertView.findViewById(R.id.labelReasonItinerary);
            value1.setText("Filed reason: " + reason_resource.get(position));

            TextView value2 = (TextView) convertView.findViewById(R.id.LabelDateIti);
            value2.setText("Date itinerary: " + date_iti_resource.get(position));

            TextView value3 = (TextView) convertView.findViewById(R.id.labelDateFiled);
            value3.setText("Date filed: " + date_filed_resource.get(position));

            TextView value4 = (TextView) convertView.findViewById(R.id.labelStatusIfApprove);
            value4.setText(status_resource.get(position));

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new MaterialDialog.Builder(mainActivity)
                            .title("Itinerary Details")
                            .content("Approver:\n" + approvers_name_resource.get(position) + "\n\nApprover's reason:\n" + approvers_reason_resource.get(position))
                            .positiveText("OK")
                            .show();

                }
            });

            if (status_resource.get(position).equalsIgnoreCase("disapproved"))
                value4.setTextColor(ContextCompat.getColor(mainActivity, R.color.md_red_400));
            else
                value4.setTextColor(ContextCompat.getColor(mainActivity, R.color.md_green_600));

            return convertView;
        }

    }


}
