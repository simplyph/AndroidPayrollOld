package ph.net.swak.attendanceinsights.ui.views.bottomsheet;

import android.app.Dialog;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.listeners.ItemFilterListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.db.realm.RealmEmployee;
import ph.net.swak.attendanceinsights.repository.Status;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.HrisEmployeeFastAdapter;
import ph.net.swak.attendanceinsights.viewmodel.HrisEmployeeBottomSheetViewModel;

public class HrisEmployeeBottomSheet extends BottomSheetDialogFragment implements LifecycleRegistryOwner {

    @BindView(R.id.textview_bottomsheet_hris_employee_title)
    protected TextView tvTitle;

    @BindView(R.id.textview_bottomsheet_hris_employee_subtitle)
    protected TextView tvSubtitle;

    @BindView(R.id.textview_bottomsheet_hris_employee_none)
    protected TextView tvNoData;

    @BindView(R.id.recyclerview_bottom_sheet_hris_employee)
    protected RecyclerView recyclerView;

    private HrisEmployeeBottomSheetViewModel viewModel;
    private LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    private Unbinder unbinder;
    private FastItemAdapter<HrisEmployeeFastAdapter> mFastItemAdapter;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(HrisEmployeeBottomSheetViewModel.class);

        Bundle bundle = getArguments();
        if (bundle != null && !bundle.isEmpty()) {
            viewModel.getEmployeeList(bundle.getInt("compId", 0), bundle.getInt("type", 0), bundle.getInt("from", 0), bundle.getInt("fromId", 0)).observe(this, listResource -> {
                tvTitle.setText(bundle.getString("compName", "None"));
                tvSubtitle.setText(bundle.getString("typeName", "None"));

                if (listResource != null && listResource.status == Status.SUCCESS) {
                    List<HrisEmployeeFastAdapter> list = new ArrayList<>();

                    for (RealmEmployee employee : listResource.data) {
                        list.add(new HrisEmployeeFastAdapter()
                                .withEmpId((int) employee.getEmployeeId())
                                .withDesignation(employee.getDesigName())
                                .withfName(employee.getfName())
                                .withmName(employee.getmName())
                                .withlName(employee.getlName())
                                .withDialog(getDialog())
                        );
                    }

                    if (list.isEmpty())
                        tvNoData.setVisibility(View.VISIBLE);
                    else
                        tvNoData.setVisibility(View.GONE);

                    mFastItemAdapter.setNewList(list);
                }
            });
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this.getContext(), this.getTheme());
        View view = View.inflate(getContext(), R.layout.bottomsheet_hris_employee, null);
        unbinder = ButterKnife.bind(this, view);
        bottomSheetDialog.setContentView(view);

        mFastItemAdapter = new FastItemAdapter<>();
        mFastItemAdapter.withSelectable(true);
        recyclerView.setAdapter(mFastItemAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        // ItemFilter
        mFastItemAdapter.getItemFilter().withFilterPredicate((item, charSequence) -> !(item.getlName().toLowerCase() + ", " + item.getfName().toLowerCase() + " " + item.getmName().toLowerCase().substring(0, 1)).contains(String.valueOf(charSequence).trim().toLowerCase()) &&
                !item.getDesignation().toLowerCase().contains(String.valueOf(charSequence).trim().toLowerCase()));

        mFastItemAdapter.getItemFilter().withItemFilterListener(new ItemFilterListener<HrisEmployeeFastAdapter>() {
            @Override
            public void itemsFiltered(@Nullable CharSequence charSequence, @Nullable List<HrisEmployeeFastAdapter> list) {
                if (list != null) {
                    if (list.isEmpty()) {
                        tvNoData.setVisibility(View.VISIBLE);
                    } else {
                        tvNoData.setVisibility(View.GONE);
                    }
                } else {
                    tvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onReset() {

            }
        });

        return bottomSheetDialog;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }

    @OnTextChanged(R.id.textinputlayout_bottomsheet_hris_employee_query)
    protected void onQuerySearch(CharSequence sequence) {
        mFastItemAdapter.filter(sequence);
    }
}
