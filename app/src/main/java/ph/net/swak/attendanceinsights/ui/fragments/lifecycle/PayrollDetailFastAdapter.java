package ph.net.swak.attendanceinsights.ui.fragments.lifecycle;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.events.UserDetailEvent;
import ph.net.swak.attendanceinsights.utils.Helper;

public class PayrollDetailFastAdapter extends AbstractItem<PayrollDetailFastAdapter, PayrollDetailFastAdapter.ViewHolder> {

    private long empId;
    private double value;
    private String desig;
    private String fName;
    private String lName;

    public long getEmpId() {
        return empId;
    }

    public PayrollDetailFastAdapter withEmpId(long empId) {
        this.empId = empId;
        return this;
    }

    public double getValue() {
        return value;
    }

    public PayrollDetailFastAdapter withValue(double value) {
        this.value = value;
        return this;
    }

    public String getDesig() {
        return desig;
    }

    public PayrollDetailFastAdapter withDesig(String desig) {
        this.desig = desig;
        return this;
    }

    public String getfName() {
        return fName;
    }

    public PayrollDetailFastAdapter withfName(String fName) {
        this.fName = fName;
        return this;
    }

    public String getlName() {
        return lName;
    }

    public PayrollDetailFastAdapter withlName(String lName) {
        this.lName = lName;
        return this;
    }

    @Override
    public ViewHolder getViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public int getType() {
        return R.id.fastadapter_payroll_detail_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.item_payroll_detail;
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);

        holder.tvName.setText(Helper.toTitleCase(String.format("%s, %s", getlName(), getfName())));
        holder.tvValue.setText(String.format("Php %s", Helper.toDecimal(getValue())));
        holder.tvCaption.setText(Helper.toTitleCase(getDesig()));
        holder.itemView.setOnClickListener(v -> {
            EventBus.getDefault().post(new UserDetailEvent(String.valueOf(getEmpId())));
        });
    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);

        holder.tvName.setText(null);
        holder.tvValue.setText(null);
        holder.tvCaption.setText(null);
        holder.itemView.setOnClickListener(null);
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textview_item_payroll_detail_name)
        protected TextView tvName;

        @BindView(R.id.textview_item_payroll_detail_value)
        protected TextView tvValue;

        @BindView(R.id.textview_item_payroll_detail_desig)
        protected TextView tvCaption;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
