package ph.net.swak.attendanceinsights.ui.views.pageradapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.HrisDashboardAllFragment;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.HrisDashboardDepartmentFragment;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.HrisDashboardDivisionFragment;

public class HrisDashboardPagerAdapter extends FragmentStatePagerAdapter {

    private int PAGE_COUNT = 3;
    private String tabTitles[] = new String[]{"All", "Division", "Department"};
    private int compId = 0;
    private String compName = "";

    public HrisDashboardPagerAdapter(FragmentManager fm, Bundle bundle) {
        super(fm);
        this.compId = bundle.getInt("compId", 0 );
        this.compName = bundle.getString("compName", "");
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new Fragment();
        Bundle bundle = new Bundle();

        if (position == 0) {
            fragment = new HrisDashboardAllFragment();
            bundle.putInt("type", 0);
        } else if (position == 1) {
            fragment = new HrisDashboardDivisionFragment();
            bundle.putInt("type", 1);
        } else if (position == 2) {
            fragment = new HrisDashboardDepartmentFragment();
            bundle.putInt("type", 2);
        }

        bundle.putInt("compId", compId);
        bundle.putString("compName", compName);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
