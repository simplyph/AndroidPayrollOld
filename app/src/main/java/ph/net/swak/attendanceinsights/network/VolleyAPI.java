package ph.net.swak.attendanceinsights.network;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

public class VolleyAPI {

    // Variables
    private Context ctx;
    private volatile static VolleyAPI mInstance;

    private ImageLoader imageLoader;
    private RequestQueue requestQueue;

    // Instantation
    private VolleyAPI(Context context) {
        ctx = context;
        requestQueue = getRequestQueue();
        imageLoader = new ImageLoader(getRequestQueue(), new LruBitmapCache(LruBitmapCache.getCacheSize(ctx.getApplicationContext())));
    }

    // Get instance
    public static synchronized VolleyAPI getInstance(Context context) {
        if (mInstance == null)
            mInstance = new VolleyAPI(context.getApplicationContext());

        return mInstance;
    }

    // RequestQueue
    public RequestQueue getRequestQueue() {
        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(ctx.getApplicationContext());

        return requestQueue;
    }

    // ImageLoader
    public ImageLoader getImageLoader() {
        if (imageLoader == null)
            imageLoader = new ImageLoader(getRequestQueue(), new LruBitmapCache(LruBitmapCache.getCacheSize(ctx.getApplicationContext())));

        return imageLoader;
    }

    // RequestQueue Stack
    public <T> void addToRequestQueue(Request<T> request) {
        getRequestQueue().add(request);
    }


}
