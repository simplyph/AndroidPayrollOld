package ph.net.swak.attendanceinsights.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RealmEmployeeList extends RealmObject {

    @PrimaryKey
    private long mIdentifier = 1;
    private String imgPath;
    private String fname;
    private String lname;
    private String status_type;
    private int empId;

    public long getIdentifier() {
        return this.mIdentifier;
    }

    public RealmEmployeeList withIdentifier(long identifier) {
        this.mIdentifier = identifier;
        return this;
    }

    public String getImgPath() {
        return imgPath;
    }

    public RealmEmployeeList withImgPath(String imgPath) {
        this.imgPath = imgPath;
        return this;
    }

    public String getFname() {
        return fname;
    }

    public RealmEmployeeList withFname(String fname) {
        this.fname = fname;
        return this;
    }

    public String getLname() {
        return lname;
    }

    public RealmEmployeeList withLname(String lname) {
        this.lname = lname;
        return this;
    }

    public String getStatusType() {
        return status_type;
    }

    public RealmEmployeeList withStatusType(String status_type) {
        this.status_type = status_type;
        return this;
    }

    public int getEmpId() {
        return empId;
    }

    public RealmEmployeeList withEmpId(int empId) {
        this.empId = empId;
        return this;
    }


}
