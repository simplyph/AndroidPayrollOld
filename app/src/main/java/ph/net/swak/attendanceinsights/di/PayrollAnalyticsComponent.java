package ph.net.swak.attendanceinsights.di;

import android.app.Application;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import ph.net.swak.attendanceinsights.PayrollAnalyticsApplication;
import ph.net.swak.attendanceinsights.di.module.ActivityModule;
import ph.net.swak.attendanceinsights.di.module.AppModule;
import ph.net.swak.attendanceinsights.di.scope.ApplicationScope;

@ApplicationScope
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
        ActivityModule.class
})
public interface PayrollAnalyticsComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance Builder application(Application application);
        PayrollAnalyticsComponent build();
    }

    void inject(PayrollAnalyticsApplication app);

}
