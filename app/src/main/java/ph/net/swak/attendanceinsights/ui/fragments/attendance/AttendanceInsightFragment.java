package ph.net.swak.attendanceinsights.ui.fragments.attendance;


import android.os.Bundle;
import android.support.transition.TransitionManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.mikepenz.fastadapter.IItemAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.events.FilterClickEvent;
import ph.net.swak.attendanceinsights.models.RealmAttParticular;
import ph.net.swak.attendanceinsights.models.RealmFilterComp;
import ph.net.swak.attendanceinsights.models.RealmFilterParticulars;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.ui.views.adapters.AttParticularFastAdapter;
import ph.net.swak.attendanceinsights.utils.DateUtil;
import ph.net.swak.attendanceinsights.utils.JSONParser;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * A simple {@link Fragment} subclass.
 */
public class AttendanceInsightFragment extends Fragment {

    // Variables
    private Unbinder unbinder;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private Realm mRealm;

    private MainActivity mainActivity;
    private FastItemAdapter<AttParticularFastAdapter> fastItemAdapter;

    // ButterKnife
    @BindView(R.id.recyclerview_attendance_insight_text)
    public RecyclerView recyclerView;

    @BindView(R.id.progressbar_attendance_insight)
    public MaterialProgressBar progressBar;

    @BindView(R.id.progressbar_attendance_insight_text)
    public TextView progressBarText;

    @BindView(R.id.button_attendance_insight_sort_highest)
    public Button sortHighest;

    @BindView(R.id.sort_label)
    public TextView sortLabel;

    private boolean listIsLoaded = false;
    private static final String TAG = "AttendanceInsightFragment";
    private DateUtil dateUtil = new DateUtil();
    private Calendar mCalendar = Calendar.getInstance();


    public AttendanceInsightFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Initialization Methods
        View view = inflater.inflate(R.layout.fragment_attendance_insight, container, false);
        unbinder = ButterKnife.bind(this, view);
        requestQueue = VolleyAPI.getInstance(getActivity()).getRequestQueue();
        volleyRequest = new VolleyRequest();
        mainActivity = (MainActivity) getActivity();
        setHasOptionsMenu(true);
        mRealm = Realm.getDefaultInstance();

        mainActivity.fab.setVisibility(View.GONE);
        mainActivity.mTabLayout.setVisibility(View.GONE);
        mainActivity.mFilter.setVisibility(View.VISIBLE);
        mainActivity.mFilterYear.setVisibility(GONE);
        mainActivity.mFilterRange.setVisibility(VISIBLE);
        mainActivity.mFilterComp.setVisibility(VISIBLE);

        List<Integer> filterOption = new ArrayList<>();
        filterOption.add(MainActivity.FILTER_RANGE);
        filterOption.add(MainActivity.FILTER_TYPE);
        filterOption.add(MainActivity.FILTER_COMP);

        mainActivity.setupUI("Attendance Particular", false, true, filterOption, false, null);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mainActivity, DividerItemDecoration.VERTICAL);
        fastItemAdapter = new FastItemAdapter<>();
        fastItemAdapter.withSelectable(true);
        fastItemAdapter.getItemFilter().withFilterPredicate(new IItemAdapter.Predicate<AttParticularFastAdapter>() {
            @Override
            public boolean filter(AttParticularFastAdapter item, CharSequence constraint) {
                return !item.getName().toLowerCase().contains(constraint.toString().toLowerCase());
            }
        });

        // RecyclerView Setup
        recyclerView.setLayoutManager(new LinearLayoutManager(mainActivity));
        recyclerView.setAdapter(fastItemAdapter);
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setItemAnimator(new DefaultItemAnimator()); // TODO: Create ItemAnimator

        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(RealmFilterParticulars.class);
                SimpleDateFormat dF1 = new SimpleDateFormat("MMM d, yyyy");
                SimpleDateFormat dF2 = new SimpleDateFormat("MMM d");

                RealmFilterParticulars realmFilterParticulars = new RealmFilterParticulars();
                realmFilterParticulars
                        .withFirstDate(dF1.format(mCalendar.getTime()))
                        .withSecondDate(dF1.format(mCalendar.getTime()));

                realm.copyToRealm(realmFilterParticulars);



                RealmResults<RealmFilterComp> query1 = mRealm.where(RealmFilterComp.class).findAll();
                RealmResults<RealmFilterComp> query2 = mRealm.where(RealmFilterComp.class).equalTo("mSelected", true).findAll();

                String compString = "All Companies";

                if(query2.where().count() == 0) {
                    compString = "No Company is Selected";
                } else if(query2.where().count() == 1) {
                    if(query1.where().count() > 1)
                        compString = query2.where().findFirst().getTitle();
                } else if(query2.where().count() > 1) {
                    if(query1.where().count() == query2.where().count())
                        compString = "All Companies";
                    else if(query1.where().count() > query2.where().count())
                        compString = String.valueOf(query2.where().count()) + " Companies";
                }

                mainActivity.mFilterRangeContent.setText(dF1.format(mCalendar.getTime()) + " - " + dF1.format(mCalendar.getTime()));
                mainActivity.mFilterHeadYear.setText(dF2.format(mCalendar.getTime()) + " - " + dF2.format(mCalendar.getTime()));
                mainActivity.mFilterHeadComp.setText(compString);
                mainActivity.mFilterCompContent.setText(compString);
            }
        });

        mainActivity.mFilterHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onFilterClick();
            }
        });

        mainActivity.mFilterComp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new FilterClickEvent("comp"));
            }
        });

        mainActivity.mFilterRange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new FilterClickEvent("range"));
            }
        });

        mainActivity.mFilterType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new FilterClickEvent("type"));
            }
        });

        mainActivity.mFilterDisplayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RealmResults<RealmFilterParticulars> query1 =  mRealm.where(RealmFilterParticulars.class).findAll();
                RealmResults<RealmFilterComp> query2 =  mRealm.where(RealmFilterComp.class).equalTo("mSelected", true).findAll();

                StringBuilder compId = new StringBuilder();

                boolean firstComp = false;

                for(RealmFilterComp realmFilterComp : query2) {
                    if(!firstComp) {
                        compId.append(realmFilterComp.getIdentifier());
                        firstComp = true;
                    } else {
                        compId.append(",");
                        compId.append(realmFilterComp.getIdentifier());
                    }
                }

                if(!firstComp)
                    compId.append("0");

                String filterType = "absent";

                if(mainActivity.mFilterTypeContent.getText().toString().equalsIgnoreCase("absent"))
                    filterType = "absent";
                else if(mainActivity.mFilterTypeContent.getText().toString().equalsIgnoreCase("late"))
                    filterType = "late";
                else if(mainActivity.mFilterTypeContent.getText().toString().equalsIgnoreCase("undertime"))
                    filterType = "ut";
                else if(mainActivity.mFilterTypeContent.getText().toString().equalsIgnoreCase("overbreak"))
                    filterType = "ob";
                else if(mainActivity.mFilterTypeContent.getText().toString().equalsIgnoreCase("leave"))
                    filterType = "leave";
                else if(mainActivity.mFilterTypeContent.getText().toString().equalsIgnoreCase("night differential"))
                    filterType = "nd";
                else if(mainActivity.mFilterTypeContent.getText().toString().equalsIgnoreCase("itinerary"))
                    filterType = "itinerary";
                else if(mainActivity.mFilterTypeContent.getText().toString().equalsIgnoreCase("perfect attendance"))
                    filterType = "perfect";

                onFilterClick();
                displayInsights(
                        dateUtil.formatDate(query1.get(0).getFirstDate(), "MMM d, yyyy", "yyyy/MM/dd"),
                        dateUtil.formatDate(query1.get(0).getSecondDate(), "MMM d, yyyy", "yyyy/MM/dd"),
                        compId.toString(), filterType);
            }
        });

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        sortLabel.setText("Alphabetical Order");

        displayInsights(sdf.format(mCalendar.getTime()), sdf.format(mCalendar.getTime()), "0", "late");


        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(requestQueue != null)
            requestQueue.cancelAll(TAG);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem searchItem = menu.findItem(R.id.app_bar_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        SearchView.SearchAutoComplete searchText = (SearchView.SearchAutoComplete) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchText.setHint("Search Employee");
        searchText.setHintTextColor(getResources().getColor(R.color.md_grey_300));
        searchText.setTextColor(getResources().getColor(R.color.md_white_1000));
        //searchView.setBackgroundColor(getResources().getColor(R.color.md_white_1000));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                fastItemAdapter.filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                fastItemAdapter.filter(newText);
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.app_bar_search:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    protected void displayInsights(String fromdate, String todate, final String comp, final String filter) {
        progressBar.setVisibility(View.VISIBLE);
        progressBarText.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        progressBarText.setText("Loading Data");

        listIsLoaded = false;

        if (fromdate == "") fromdate = "2016/08/12";
        if (todate == "") todate = "2016/08/12";

        JsonArrayRequest jsonArrayRequest = volleyRequest.getAuthJsonArray(mainActivity.getApplicationContext(), TAG, volleyRequest.getInsights(fromdate, todate, comp, filter), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                final List<RealmAttParticular> attendanceList = JSONParser.parseAttParticular(response);

                List<AttParticularFastAdapter> newList = new ArrayList<>();


                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.delete(RealmAttParticular.class);

                        for (RealmAttParticular particular : attendanceList) {
                            RealmAttParticular realmAttParticular = realm.copyToRealm(particular);
                            if (filter.equalsIgnoreCase("absent"))
                                realmAttParticular.withDetail("Absence: " + String.valueOf(particular.getResult()) + (particular.getResult() > 1 ? " days" : " day"));
                            else if (filter.equalsIgnoreCase("late"))
                                realmAttParticular.withDetail("Late: " + String.valueOf(particular.getResult()) + (particular.getResult() > 1 ? " minutes" : " minute"));
                            else if (filter.equalsIgnoreCase("ut"))
                                realmAttParticular.withDetail("Undertime: " + String.valueOf(particular.getResult()) + (particular.getResult() > 1 ? " minutes" : " minute"));
                            else if (filter.equalsIgnoreCase("ob"))
                                realmAttParticular.withDetail("Overbreak: " + String.valueOf(particular.getResult()) + (particular.getResult() > 1 ? " minutes" : " minute"));
                            else if (filter.equalsIgnoreCase("leave"))
                                realmAttParticular.withDetail("Leave: " + String.valueOf(particular.getResult()) + (particular.getResult() > 1 ? " days" : " day"));
                            else if (filter.equalsIgnoreCase("ot"))
                                realmAttParticular.withDetail("Overtime: " + String.valueOf(particular.getResult()) + (particular.getResult() > 1 ? " minutes" : " minute"));
                            else if (filter.equalsIgnoreCase("nd"))
                                realmAttParticular.withDetail("Night Differential: " + String.valueOf(particular.getResult()) + (particular.getResult() > 1 ? " minutes" : " minute"));
                            else if (filter.equalsIgnoreCase("itinerary"))
                                realmAttParticular.withDetail("Itinerary: " + String.valueOf(particular.getResult()) + (particular.getResult() > 1 ? " days" : " day"));
                            else if (filter.equalsIgnoreCase("perfect"))
                                realmAttParticular.withDetail("Perfect Attendance: " + String.valueOf(particular.getResult()));
                            else
                                realmAttParticular.withDetail(String.valueOf(particular.getResult()) + (particular.getResult() > 1 ? " days" : " day"));
                        }
                    }
                });

                RealmResults<RealmAttParticular> query1 = mRealm.where(RealmAttParticular.class).findAllSorted("lname", Sort.ASCENDING);
                sortLabel.setText("Alphabetical Order");
                for (RealmAttParticular realmAttParticular : query1) {
                    Log.e("IMGPATH", realmAttParticular.getImgPath());
                    newList.add(new AttParticularFastAdapter()
                            .withEmpId(Integer.parseInt(realmAttParticular.getEmpId()))
                            .withName(realmAttParticular.getLname() + ", " + realmAttParticular.getFname())
                            .withDetail(realmAttParticular.getDetail())
                            .withImgPath(realmAttParticular.getImgPath())
                    );
                }

                if (newList.size() > 0) {
                    fastItemAdapter.setNewList(newList);
                    progressBar.setVisibility(View.GONE);
                    progressBarText.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.GONE);
                    progressBarText.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    progressBarText.setText("No Data Found");
                }

                listIsLoaded = true;


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                progressBarText.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                progressBarText.setText("No Data Found");
            }
        });

        requestQueue.add(jsonArrayRequest);
    }

    private int convertPx(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    @OnClick(R.id.button_attendance_insight_sort_highest)
    protected void onSortHighest(Button button) {
        if(listIsLoaded) {
            List<AttParticularFastAdapter> newList = new ArrayList<>();
            Sort sortStyle = Sort.DESCENDING;

            if(button.getText().toString().equalsIgnoreCase("highest")) {
                sortStyle = Sort.DESCENDING;
                button.setText("LOWEST");
                sortLabel.setText("Descending Order");
            } else {
                sortStyle = Sort.ASCENDING;
                button.setText("HIGHEST");
                sortLabel.setText("Ascending Order");
            }

            RealmResults<RealmAttParticular> query1 = mRealm.where(RealmAttParticular.class).findAllSorted("result", sortStyle, "lname", Sort.ASCENDING);

            for (RealmAttParticular realmAttParticular : query1) {
                newList.add(new AttParticularFastAdapter()
                        .withEmpId(Integer.parseInt(realmAttParticular.getEmpId()))
                        .withName(realmAttParticular.getLname() + ", " + realmAttParticular.getFname())
                        .withDetail(realmAttParticular.getDetail())
                        .withImgPath(realmAttParticular.getImgPath())
                );
            }


            fastItemAdapter.setNewList(newList);


        }
    }

    @OnClick(R.id.button_attendance_insight_sort_alpha)
    protected void onSortAlpha() {
        if(listIsLoaded) {
            List<AttParticularFastAdapter> newList = new ArrayList<>();
            RealmResults<RealmAttParticular> query1 = mRealm.where(RealmAttParticular.class).findAllSorted("lname", Sort.ASCENDING);

            for (RealmAttParticular realmAttParticular : query1) {
                newList.add(new AttParticularFastAdapter()
                        .withEmpId(Integer.parseInt(realmAttParticular.getEmpId()))
                        .withName(realmAttParticular.getLname() + ", " + realmAttParticular.getFname())
                        .withDetail(realmAttParticular.getDetail())
                        .withImgPath(realmAttParticular.getImgPath())
                );
            }

            sortLabel.setText("Alphabetical Order");
            fastItemAdapter.setNewList(newList);
        }
    }

    protected void onFilterClick() {
        TransitionManager.beginDelayedTransition(mainActivity.appBarLayout);
        ViewGroup.LayoutParams params = mainActivity.mFilter.getLayoutParams();
        if (params.height == convertPx(48)) {
            mainActivity.mFilterHeadYear.setVisibility(GONE);
            mainActivity.mFilterHeadComp.setVisibility(GONE);
            mainActivity.mFilterHeadArrow.setImageDrawable(ContextCompat.getDrawable(mainActivity, R.drawable.ic_close_black_48dp));
            mainActivity.mFilterYear.setVisibility(GONE);
            mainActivity.mFilterRange.setVisibility(VISIBLE);
            mainActivity.mFilterComp.setVisibility(VISIBLE);
            mainActivity.mFilterDisplay.setVisibility(VISIBLE);
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        } else {
            mainActivity.mFilterHeadYear.setVisibility(VISIBLE);
            mainActivity.mFilterHeadComp.setVisibility(VISIBLE);
            mainActivity.mFilterHeadArrow.setImageDrawable(ContextCompat.getDrawable(mainActivity, R.drawable.ic_keyboard_arrow_down_black_48dp));
            mainActivity.mFilterYear.setVisibility(GONE);
            mainActivity.mFilterRange.setVisibility(GONE);
            mainActivity.mFilterComp.setVisibility(GONE);
            mainActivity.mFilterDisplay.setVisibility(GONE);
            params.height = convertPx(48);
        }

        mainActivity.mFilter.setLayoutParams(params);
    }
}
