package ph.net.swak.attendanceinsights.network;

import android.arch.lifecycle.LiveData;

import java.util.List;

import ph.net.swak.attendanceinsights.db.realm.RealmEmployee;
import ph.net.swak.attendanceinsights.db.realm.RealmHrisQuick;
import ph.net.swak.attendanceinsights.db.realm.RealmPayrollDetails;
import ph.net.swak.attendanceinsights.db.realm.RealmPayrollHead;
import ph.net.swak.attendanceinsights.db.realm.RealmPayrollQuick;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PAService {

    @GET("api/employee/profiles")
    LiveData<ApiResponse<List<RealmEmployee>>> loadEmployees();

    @GET("api/payroll/v2/{year}")
    LiveData<ApiResponse<List<RealmPayrollHead>>> loadPayrollHead(@Path("year") int year);

    @GET("api/payroll/v2/id/{id}")
    LiveData<ApiResponse<List<RealmPayrollDetails>>> loadPayrollDetails(@Path("id") int id);

    @GET("api/payroll/quick")
    LiveData<ApiResponse<List<RealmPayrollQuick>>> loadPayrollQuick(
            @Query("prevMonth") int prevMonth,
            @Query("prevYear") int prevYear,
            @Query("presentMonth") int presentMonth,
            @Query("presentYear") int presentYear
    );

    @GET("api/attendance/quick")
    LiveData<ApiResponse<List<RealmHrisQuick>>> loadHrisQuick();

    @FormUrlEncoded
    @POST("token?grant_type=password")
    LiveData<ApiResponse<String>> getAccessToken(
            @Field("client_type") String clientType,
            @Field("client_id") String clientId,
            @Field("client_secret") String clientSecret,
            @Field("username") String username,
            @Field("password") String password
    );

}
