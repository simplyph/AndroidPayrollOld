package ph.net.swak.attendanceinsights.models;

import io.realm.RealmObject;

public class RealmFilterParticulars extends RealmObject {

    protected String firstDate = "";
    protected String secondDate = "";
    protected int year;

    public String getFirstDate() {
        return firstDate;
    }

    public RealmFilterParticulars withFirstDate(String firstDate) {
        this.firstDate = firstDate;
        return this;
    }

    public String getSecondDate() {
        return secondDate;
    }

    public RealmFilterParticulars withSecondDate(String secondDate) {
        this.secondDate = secondDate;
        return this;
    }

    public int getYear() {
        return year;
    }

    public RealmFilterParticulars withYear(int year) {
        this.year = year;
        return this;
    }
}
