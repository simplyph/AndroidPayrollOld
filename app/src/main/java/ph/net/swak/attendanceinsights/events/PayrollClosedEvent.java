package ph.net.swak.attendanceinsights.events;

/**
 * Created by Knowell on 1/31/2017.
 */

public class PayrollClosedEvent {

    public final String paycode;
    public final double gross;

    public PayrollClosedEvent(String paycode, double gross) {
        this.paycode = paycode;
        this.gross = gross;
    }

}
