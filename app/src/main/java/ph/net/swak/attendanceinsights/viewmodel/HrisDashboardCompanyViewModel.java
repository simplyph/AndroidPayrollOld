package ph.net.swak.attendanceinsights.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import ph.net.swak.attendanceinsights.repository.HrisRepository;
import ph.net.swak.attendanceinsights.repository.Resource;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.HrisCompanyDetailFastAdapter;

public class HrisDashboardCompanyViewModel extends ViewModel {

    private HrisRepository hrisRepo;
    private LiveData<Resource<List<HrisCompanyDetailFastAdapter>>> employeeList;
    private LiveData<Resource<List<HrisCompanyDetailFastAdapter>>> employeeListDiv;
    private LiveData<Resource<List<HrisCompanyDetailFastAdapter>>> employeeListDept;

    @Inject
    public HrisDashboardCompanyViewModel(HrisRepository hrisRepository) {
        hrisRepo = hrisRepository;
    }

    public LiveData<Resource<List<HrisCompanyDetailFastAdapter>>> getEmployeeList(int compId, int type) {
        employeeList = hrisRepo.getEmployeesByCompAndType(compId, type);

        return employeeList;
    }

    public LiveData<Resource<List<HrisCompanyDetailFastAdapter>>> getEmployeeListDiv(int compId, int type) {
        employeeListDiv = hrisRepo.getEmployeesByCompAndType(compId, type);

        return employeeListDiv;
    }

    public LiveData<Resource<List<HrisCompanyDetailFastAdapter>>> getEmployeeListDept(int compId, int type) {
        employeeListDept = hrisRepo.getEmployeesByCompAndType(compId, type);

        return employeeListDept;
    }
}
