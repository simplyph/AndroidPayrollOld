package ph.net.swak.attendanceinsights.models;

public class FileItinerary {

    private int autoid;
    private int empid;
    private String firstname;
    private String lastname;
    private String reason;
    private String datefiled;
    private String dateitinerary;
    private String status;
    private String statusby;
    private String statusreason;
    private String statusdate;
    private String reason_disapprove;

    private String approver;

    public int getAutoid() {
        return autoid;
    }

    public void setAutoid(int autoid) {
        this.autoid = autoid;
    }

    public int getEmpid() {
        return empid;
    }

    public void setEmpid(int empid) {
        this.empid = empid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDatefiled() {
        return datefiled;
    }

    public void setDatefiled(String datefiled) {
        this.datefiled = datefiled;
    }

    public String getDateitinerary() {
        return dateitinerary;
    }

    public void setDateitinerary(String dateitinerary) {
        this.dateitinerary = dateitinerary;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusby() {
        return statusby;
    }

    public void setStatusby(String statusby) {
        this.statusby = statusby;
    }

    public String getStatusreason() {
        return statusreason;
    }

    public void setStatusreason(String statusreason) {
        this.statusreason = statusreason;
    }

    public String getStatusdate() {
        return statusdate;
    }

    public void setStatusdate(String statusdate) {
        this.statusdate = statusdate;
    }

    public String getReason_disapprove() {
        return reason_disapprove;
    }

    public void setReason_disapprove(String reason_disapprove) {
        this.reason_disapprove = reason_disapprove;
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }
}
