package ph.net.swak.attendanceinsights.models;

import io.realm.RealmObject;

/**
 * Created by Knowell on 2/15/2017.
 */

public class RealmString extends RealmObject {

    private String string;

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }
}
