package ph.net.swak.attendanceinsights.utils.firebase;

import android.app.NotificationManager;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.events.PayrollClosedEvent;
import timber.log.Timber;

public class FirebaseService extends FirebaseMessagingService {

    public FirebaseService() { }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // Data origination
        if(remoteMessage.getFrom() != null) {
            Timber.i("Message From Notification: " + remoteMessage.getFrom());
        }

        // Data payload
        if(remoteMessage.getData().size() > 0) {
            Timber.i("Message Data Body: " + remoteMessage.getData());
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            if (remoteMessage.getData().containsKey("payroll")) {
                int index = 1;
                for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {
                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.notif_dark)
                            .setContentTitle("Payroll Process")
                            .setContentText(entry.getValue())
                            .setSubText("Tap for more details.");

                    notificationManager.notify(index++, mBuilder.build());
                }
            } else if (remoteMessage.getData().containsKey("promotion")) {
                int index = 1;
                for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {
                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.notif_dark)
                            .setContentTitle("Employee Promotion")
                            .setContentText(entry.getValue())
                            .setSubText("Tap for more details.");

                    notificationManager.notify(index++, mBuilder.build());
                }
            } else if (remoteMessage.getData().containsKey("announcement")) {
                int index = 1;
                for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {
                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.notif_dark)
                            .setContentTitle("Announcement")
                            .setContentText(entry.getValue())
                            .setSubText("Tap for more details.");

                    notificationManager.notify(index++, mBuilder.build());
                }
            }
        }

        // Notification payload
        if(remoteMessage.getNotification() != null) {
            Timber.i("Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }
}
