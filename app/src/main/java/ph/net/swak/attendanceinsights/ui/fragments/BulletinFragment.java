package ph.net.swak.attendanceinsights.ui.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.listeners.ClickEventHook;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.models.RealmBulletinInfo;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.ui.views.widgets.dialog.SlideshowDialogFragment;
import ph.net.swak.attendanceinsights.utils.JSONParser;

public class BulletinFragment extends Fragment {

    private final String TAG = "BulletinFragment";
    @BindView(R.id.recyclerview)
    protected RecyclerView recyclerView;
    private Unbinder unbinder;
    private MainActivity mainActivity;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private Realm mRealm;
    private FastItemAdapter<RealmBulletinInfo> fastItemAdapter;

    public BulletinFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bulletin, container, false);
        mRealm = Realm.getDefaultInstance();
        unbinder = ButterKnife.bind(this, view);
        requestQueue = VolleyAPI.getInstance(getActivity()).getRequestQueue();
        volleyRequest = new VolleyRequest();
        mainActivity = (MainActivity) getActivity();

        fastItemAdapter = new FastItemAdapter<>();
        recyclerView.setLayoutManager(new GridLayoutManager(mainActivity, 2));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(fastItemAdapter);

        getData();

        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (requestQueue != null)
            requestQueue.cancelAll(TAG);
    }

    private void getData() {
        JsonArrayRequest jsonArrayRequest = volleyRequest.getAuthJsonArray(mainActivity.getApplicationContext(), TAG, volleyRequest.getBulletin(), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                final List<RealmBulletinInfo> realmBulletinInfos = JSONParser.parseBulletinInfo(response);

                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.delete(RealmBulletinInfo.class);

                        for(RealmBulletinInfo realmBulletinInfo : realmBulletinInfos)
                            realm.copyToRealm(realmBulletinInfo);
                    }
                });

                RealmResults<RealmBulletinInfo> query1 = mRealm.where(RealmBulletinInfo.class).findAll();

                List<RealmBulletinInfo> newList = new ArrayList<>();
                final ArrayList<String> paths = new ArrayList<>();

                for(RealmBulletinInfo realmBulletinInfo : query1) {
                    newList.add(realmBulletinInfo);
                    paths.add(realmBulletinInfo.getFilePath());
                }

                fastItemAdapter.withItemEvent(new ClickEventHook<RealmBulletinInfo>() {
                    @Nullable
                    @Override
                    public View onBind(@NonNull RecyclerView.ViewHolder viewHolder) {
                        if (viewHolder instanceof RealmBulletinInfo.ViewHolder) {
                            return ((RealmBulletinInfo.ViewHolder) viewHolder).itemView;
                        }
                        return null;
                    }

                    @Override
                    public void onClick(View v, int position, FastAdapter<RealmBulletinInfo> fastAdapter, RealmBulletinInfo item) {
                        Bundle bundle = new Bundle();
                        bundle.putStringArrayList("paths", paths);
                        bundle.putInt("position", position);
                        SlideshowDialogFragment fragment = new SlideshowDialogFragment();
                        fragment.setArguments(bundle);
                        fragment.show(mainActivity.getSupportFragmentManager(), "Slideshow");
                    }
                });

                fastItemAdapter.setNewList(newList);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(jsonArrayRequest);
    }
}
