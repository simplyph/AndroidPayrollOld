package ph.net.swak.attendanceinsights.models;


public class Itinerary {

    private int autoid;
    private String reason;
    private String datefiled;
    private String dateitinerary;
    private String status;
    private String statusby;
    private String statusreason;
    private String statusdate;

    public int getAutoid() {
        return autoid;
    }

    public void setAutoid(int autoid) {
        this.autoid = autoid;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDatefiled() {
        return datefiled;
    }

    public void setDatefiled(String datefiled) {
        this.datefiled = datefiled;
    }

    public String getDateitinerary() {
        return dateitinerary;
    }

    public void setDateitinerary(String dateitinerary) {
        this.dateitinerary = dateitinerary;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusby() {
        return statusby;
    }

    public void setStatusby(String statusby) {
        this.statusby = statusby;
    }

    public String getStatusreason() {
        return statusreason;
    }

    public void setStatusreason(String statusreason) {
        this.statusreason = statusreason;
    }

    public String getStatusdate() {
        return statusdate;
    }

    public void setStatusdate(String statusdate) {
        this.statusdate = statusdate;
    }
}
