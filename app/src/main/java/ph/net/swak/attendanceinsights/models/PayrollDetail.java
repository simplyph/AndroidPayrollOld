package ph.net.swak.attendanceinsights.models;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;
import java.util.TreeMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.ui.fragments.CustomBottomFragment;

public class PayrollDetail extends AbstractItem<PayrollDetail, PayrollDetail.ViewHolder> {

    private int paycode;
    private String paytype;
    private String fromdate;
    private String todate;
    private String week;
    private String compName;
    private String firstName;
    private String lastName;
    private String compId;
    private String empId;
    private boolean closed;
    private double gross;
    private double deduction;
    private double net;
    private int totalCompany;
    private int totalEmployee;
    private int totalManager;
    private int totalSupervisor;
    private int totalStaff;

    private double totalGrossManager;
    private double totalGrossSupervisor;
    private double totalGrossStaff;

    private String title;
    private String info;
    private String subinfo;
    private TreeMap<String, String> mapList;

    private int bundleYear;
    private int bundleMonth;

    private String payrollType;
    private int quarter;

    public int getPaycode() {
        return paycode;
    }

    public void setPaycode(int paycode) {
        this.paycode = paycode;
    }

    public String getPaytype() {
        return paytype;
    }

    public void setPaytype(String paytype) {
        this.paytype = paytype;
    }

    public String getFromdate() {
        return fromdate;
    }

    public void setFromdate(String fromdate) {
        this.fromdate = fromdate;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public boolean isClose() {
        return closed;
    }

    public void setClosed(String status) {
        this.closed = status.equalsIgnoreCase("closed");
    }

    public double getGross() {
        return gross;
    }

    public void setGross(double gross) {
        this.gross = gross;
    }

    public double getDeduction() {
        return deduction;
    }

    public void setDeduction(double deduction) {
        this.deduction = deduction;
    }

    public double getNet() {
        return net;
    }

    public void setNet(double net) {
        this.net = net;
    }

    public int getTotalCompany() {
        return totalCompany;
    }

    public void setTotalCompany(int totalCompany) {
        this.totalCompany = totalCompany;
    }

    public int getTotalEmployee() {
        return totalEmployee;
    }

    public void setTotalEmployee(int totalEmployee) {
        this.totalEmployee = totalEmployee;
    }

    public int getTotalManager() {
        return totalManager;
    }

    public void setTotalManager(int totalManager) {
        this.totalManager = totalManager;
    }

    public int getTotalSupervisor() {
        return totalSupervisor;
    }

    public void setTotalSupervisor(int totalSupervisor) {
        this.totalSupervisor = totalSupervisor;
    }

    public int getTotalStaff() {
        return totalStaff;
    }

    public void setTotalStaff(int totalStaff) {
        this.totalStaff = totalStaff;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getSubinfo() {
        return subinfo;
    }

    public void setSubinfo(String subinfo) {
        this.subinfo = subinfo;
    }

    public TreeMap<String, String> getMapList() {
        return mapList;
    }

    public void setMapList(TreeMap<String, String> mapList) {
        this.mapList = mapList;
    }

    public int getBundleYear() {
        return bundleYear;
    }

    public void setBundleYear(int bundleYear) {
        this.bundleYear = bundleYear;
    }

    public int getBundleMonth() {
        return bundleMonth;
    }

    public void setBundleMonth(int bundleMonth) {
        this.bundleMonth = bundleMonth;
    }

    public double getTotalGrossManager() {
        return totalGrossManager;
    }

    public void setTotalGrossManager(double totalGrossManager) {
        this.totalGrossManager = totalGrossManager;
    }

    public double getTotalGrossSupervisor() {
        return totalGrossSupervisor;
    }

    public void setTotalGrossSupervisor(double totalGrossSupervisor) {
        this.totalGrossSupervisor = totalGrossSupervisor;
    }

    public double getTotalGrossStaff() {
        return totalGrossStaff;
    }

    public void setTotalGrossStaff(double totalGrossStaff) {
        this.totalGrossStaff = totalGrossStaff;
    }

    public String getPayrollType() {
        return payrollType;
    }

    public PayrollDetail withPayrollType(String payrollType) {
        this.payrollType = payrollType;
        return this;
    }

    public int getQuarter() {
        return quarter;
    }

    public PayrollDetail withQuarter(int quarter) {
        this.quarter = quarter;
        return this;
    }

    @Override
    public int getType() {
        return R.id.fastadapter_payroll_detail_list_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.viewgroup_payroll_monthly;
    }

    @Override
    public void bindView(final PayrollDetail.ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);
        holder.title.setText(getTitle());
        holder.info.setText(getInfo());
        holder.subinfo.setText(getSubinfo());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("year", getBundleYear());

                bundle.putString("payrolltype", getPayrollType());

                if(getPayrollType().equalsIgnoreCase("monthly"))
                    bundle.putInt("month", getBundleMonth());
                else
                    bundle.putInt("quarter", getQuarter());

                CustomBottomFragment customBottomFragment = new CustomBottomFragment();
                customBottomFragment.setArguments(bundle);
                customBottomFragment.show(((MainActivity) holder.itemView.getContext()).getSupportFragmentManager(), null);
            }
        });
    }

    @Override
    public void unbindView(PayrollDetail.ViewHolder holder) {
        super.unbindView(holder);
        holder.title.setText(null);
        holder.info.setText(null);
        holder.subinfo.setText(null);
        holder.itemView.setOnClickListener(null);
    }

    @Override
    public ViewHolder getViewHolder(View view) {
        return new ViewHolder(view);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textview_viewgroup_payroll_monthly_title)
        TextView title;

        @BindView(R.id.textview_viewgroup_payroll_monthly_info)
        TextView info;

        @BindView(R.id.textview_viewgroup_payroll_monthly_subinfo)
        TextView subinfo;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
