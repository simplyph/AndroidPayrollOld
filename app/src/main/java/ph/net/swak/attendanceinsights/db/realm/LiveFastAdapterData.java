package ph.net.swak.attendanceinsights.db.realm;

import android.arch.lifecycle.LiveData;

import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

public class LiveFastAdapterData<T extends AbstractItem> extends LiveData<List<T>> {

    private List<T> mResults;

    public LiveFastAdapterData(List<T> results) {
        mResults = results;
    }

    @Override
    protected void onActive() {
        super.onActive();
        postValue(mResults);
    }

    @Override
    protected void onInactive() {
        super.onInactive();
    }
}
