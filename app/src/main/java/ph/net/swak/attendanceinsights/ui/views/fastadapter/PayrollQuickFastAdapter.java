package ph.net.swak.attendanceinsights.ui.views.fastadapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.models.PayrollQuickList;
import ph.net.swak.attendanceinsights.utils.Helper;

public class PayrollQuickFastAdapter extends AbstractItem<PayrollQuickFastAdapter, PayrollQuickFastAdapter.ViewHolder> {

    private int covMonth;
    private int covYear;
    private double totalPayroll;
    private List<PayrollQuickList> compListData;

    public int getCovMonth() {
        return covMonth;
    }

    public PayrollQuickFastAdapter withCovMonth(int covMonth) {
        this.covMonth = covMonth;
        return this;
    }

    public int getCovYear() {
        return covYear;
    }

    public PayrollQuickFastAdapter withCovYear(int covYear) {
        this.covYear = covYear;
        return this;
    }

    public double getTotalPayroll() {
        return totalPayroll;
    }

    public PayrollQuickFastAdapter withTotalPayroll(double totalPayroll) {
        this.totalPayroll = totalPayroll;
        return this;
    }

    public List<PayrollQuickList> getCompListData() {
        return compListData;
    }

    public PayrollQuickFastAdapter withCompListData(List<PayrollQuickList> compListData) {
        this.compListData = compListData;
        return this;
    }

    @Override
    public PayrollQuickFastAdapter.ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.fastadapter_home_simple_list_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.item_home_simple_list;
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);

        holder.tvName.setText(Helper.toStringMonth(getCovMonth() - 1));
        holder.tvValue.setText(String.format("Php %s", Helper.toDecimal(getTotalPayroll())));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuilder sb = new StringBuilder();

                for (PayrollQuickList payrollQuickList : getCompListData()) {
                    sb.append(payrollQuickList.getCompName());
                    sb.append("\n\t\t");
                    sb.append(String.format(Locale.ENGLISH, "%d Employees: \t\tPhp %s", payrollQuickList.getCountEmployee(), Helper.toDecimal(payrollQuickList.getTotalEmployee())));
                    sb.append("\n\t\t");
                    sb.append(String.format(Locale.ENGLISH, "%d Manager: \t\t\tPhp %s", payrollQuickList.getCountManager(), Helper.toDecimal(payrollQuickList.getTotalManager())));
                    sb.append("\n\t\t");
                    sb.append(String.format(Locale.ENGLISH, "%d Supervisor: \t\tPhp %s", payrollQuickList.getCountSupervisor(), Helper.toDecimal(payrollQuickList.getTotalSupervisor())));
                    sb.append("\n\t\t");
                    sb.append(String.format(Locale.ENGLISH, "%d Staff: \t\t\t\t\t\t\tPhp %s", payrollQuickList.getCountStaff(), Helper.toDecimal(payrollQuickList.getTotalStaff())));
                    sb.append("\n\n");
                }

                new MaterialDialog.Builder(holder.itemView.getContext())
                        .title(Helper.toStringMonth(getCovMonth() - 1))
                        .content(sb.toString())
                        .positiveText(R.string.dialog_prompt_ok)
                        .build().show();
            }
        });
    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);

        holder.tvName.setText(null);
        holder.tvValue.setText(null);
        holder.itemView.setOnClickListener(null);
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textview_home_simple_list_name)
        protected TextView tvName;

        @BindView(R.id.textview_home_simple_list_value)
        protected TextView tvValue;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
