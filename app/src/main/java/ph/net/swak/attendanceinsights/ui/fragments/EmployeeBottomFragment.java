package ph.net.swak.attendanceinsights.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IItemAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.listeners.ClickEventHook;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.events.UserDetailEvent;
import ph.net.swak.attendanceinsights.models.RealmPayroll;
import ph.net.swak.attendanceinsights.models.SimpleList;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;

public class EmployeeBottomFragment extends BottomSheetDialogFragment {

    @BindView(R.id.recyclerview_bottom_sheet_employee)
    protected RecyclerView recyclerView;

    @BindView(R.id.textview_bottom_sheet_employee_nodata)
    protected TextView textViewNoData;

    private Unbinder unbinder;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private Realm mRealm;
    private final String TAG = "EmployeeBottomFragment";
    private DecimalFormat dF = new DecimalFormat("#,###.00");

    private FastItemAdapter<SimpleList> mFastItemAdapter;

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View view = View.inflate(getContext(), R.layout.fragment_bottom_sheet_employee, null);
        unbinder = ButterKnife.bind(this, view);
        mRealm = Realm.getDefaultInstance();
        requestQueue = VolleyAPI.getInstance(getContext()).getRequestQueue();
        volleyRequest = new VolleyRequest();

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);

        mFastItemAdapter = new FastItemAdapter<>();
        mFastItemAdapter.withSelectable(true);
        mFastItemAdapter.withItemEvent(new ClickEventHook<SimpleList>() {
            @Nullable
            @Override
            public View onBind(@NonNull RecyclerView.ViewHolder viewHolder) {
                if (viewHolder instanceof SimpleList.ViewHolder) {
                    return ((SimpleList.ViewHolder) viewHolder).itemView;
                }
                return null;
            }

            @Override
            public void onClick(View v, int position, FastAdapter<SimpleList> fastAdapter, SimpleList item) {
                dismiss();
                EventBus.getDefault().post(new UserDetailEvent(item.getEmpid()));
            }
        });
        mFastItemAdapter.getItemFilter().withFilterPredicate(new IItemAdapter.Predicate<SimpleList>() {
            @Override
            public boolean filter(SimpleList item, CharSequence constraint) {
                return !item.getLabel().toLowerCase().contains(constraint.toString().toLowerCase());
            }
        });
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mFastItemAdapter);

        textViewNoData.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);

        Bundle bundle = getArguments();
        if(bundle != null && !bundle.isEmpty()) {
            textViewNoData.setText("Loading Employee Data");
            GetEmployeeData(Integer.parseInt(bundle.getString("month")), bundle.getInt("compid"), bundle.getString("divname"), bundle.getString("position"), bundle.getString("payrolltype"));
        } else {
            textViewNoData.setText("No Employee Found");
        }

        dialog.setContentView(view);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(requestQueue != null)
            requestQueue.cancelAll(TAG);
    }

    @OnTextChanged(R.id.textlayout_search_query)
    protected void onQuerySearch(CharSequence charSequence) {
        mFastItemAdapter.filter(charSequence);
    }

    @OnClick(R.id.textview_bottom_sheet_employee_close)
    protected void onCloseClick() {
        dismiss();
    }

    private void GetEmployeeData(int month, int compid, String divName, String position, String payrollType) {

        RealmResults<RealmPayroll> results = null;
        List<SimpleList> simpleLists = new ArrayList<>();

        if(divName.equalsIgnoreCase("all")) {

            if (payrollType.equalsIgnoreCase("monthly")) {

                if (position.equalsIgnoreCase("manager"))
                    results = mRealm.where(RealmPayroll.class).equalTo("covMonth", month).equalTo("compId", compid).equalTo("manager", true).distinct("empId").sort("totalGross", Sort.DESCENDING);
                else if (position.equalsIgnoreCase("supervisor"))
                    results = mRealm.where(RealmPayroll.class).equalTo("covMonth", month).equalTo("compId", compid).equalTo("supervisor", true).distinct("empId").sort("totalGross", Sort.DESCENDING);
                else
                    results = mRealm.where(RealmPayroll.class).equalTo("covMonth", month).equalTo("compId", compid).equalTo("staff", true).distinct("empId").sort("totalGross", Sort.DESCENDING);

            } else if (payrollType.equalsIgnoreCase("quarterly")) {

                Integer[] monthArray = new Integer[]{1,2,3};

                if(month == 1)
                    monthArray = new Integer[]{1,2,3};
                else if(month == 2)
                    monthArray = new Integer[]{4,5,6};
                else if(month == 3)
                    monthArray = new Integer[]{7,8,9};
                else if(month == 4)
                    monthArray = new Integer[]{10,11,12};

                if (position.equalsIgnoreCase("manager"))
                    results = mRealm.where(RealmPayroll.class).in("covMonth", monthArray).equalTo("compId", compid).equalTo("manager", true).distinct("empId").sort("totalGross", Sort.DESCENDING);
                else if (position.equalsIgnoreCase("supervisor"))
                    results = mRealm.where(RealmPayroll.class).in("covMonth", monthArray).equalTo("compId", compid).equalTo("supervisor", true).distinct("empId").sort("totalGross", Sort.DESCENDING);
                else
                    results = mRealm.where(RealmPayroll.class).in("covMonth", monthArray).equalTo("compId", compid).equalTo("staff", true).distinct("empId").sort("totalGross", Sort.DESCENDING);

            }

        } else {
            if (payrollType.equalsIgnoreCase("monthly")) {

                if(position.equalsIgnoreCase("manager"))
                    results = mRealm.where(RealmPayroll.class).equalTo("divName", divName, Case.INSENSITIVE).equalTo("covMonth", month).equalTo("compId", compid).equalTo("manager", true).distinct("empId").sort("totalGross", Sort.DESCENDING);
                else if(position.equalsIgnoreCase("supervisor"))
                    results = mRealm.where(RealmPayroll.class).equalTo("divName", divName, Case.INSENSITIVE).equalTo("covMonth", month).equalTo("compId", compid).equalTo("supervisor", true).distinct("empId").sort("totalGross", Sort.DESCENDING);
                else
                    results = mRealm.where(RealmPayroll.class).equalTo("divName", divName, Case.INSENSITIVE).equalTo("covMonth", month).equalTo("compId", compid).equalTo("staff", true).distinct("empId").sort("totalGross", Sort.DESCENDING);

            } else if (payrollType.equalsIgnoreCase("quarterly")) {

                Integer[] monthArray = new Integer[]{1,2,3};

                if(month == 1)
                    monthArray = new Integer[]{1,2,3};
                else if(month == 2)
                    monthArray = new Integer[]{4,5,6};
                else if(month == 3)
                    monthArray = new Integer[]{7,8,9};
                else if(month == 4)
                    monthArray = new Integer[]{10,11,12};

                if(position.equalsIgnoreCase("manager"))
                    results = mRealm.where(RealmPayroll.class).equalTo("divName", divName, Case.INSENSITIVE).in("covMonth", monthArray).equalTo("compId", compid).equalTo("manager", true).distinct("empId").sort("totalGross", Sort.DESCENDING);
                else if(position.equalsIgnoreCase("supervisor"))
                    results = mRealm.where(RealmPayroll.class).equalTo("divName", divName, Case.INSENSITIVE).in("covMonth", monthArray).equalTo("compId", compid).equalTo("supervisor", true).distinct("empId").sort("totalGross", Sort.DESCENDING);
                else
                    results = mRealm.where(RealmPayroll.class).equalTo("divName", divName, Case.INSENSITIVE).in("covMonth", monthArray).equalTo("compId", compid).equalTo("staff", true).distinct("empId").sort("totalGross", Sort.DESCENDING);

            }
        }

        if(results != null) {
            for(RealmPayroll realmPayroll : results) {

                double sumOfGross = 0;

                if (payrollType.equalsIgnoreCase("monthly")) {
                    sumOfGross = mRealm.where(RealmPayroll.class).equalTo("covMonth", month).equalTo("empId", realmPayroll.getEmpId()).sum("totalGross").doubleValue();
                } else if (payrollType.equalsIgnoreCase("quarterly")) {

                    Integer[] monthArray = new Integer[]{1,2,3};

                    if(month == 1)
                        monthArray = new Integer[]{1,2,3};
                    else if(month == 2)
                        monthArray = new Integer[]{4,5,6};
                    else if(month == 3)
                        monthArray = new Integer[]{7,8,9};
                    else if(month == 4)
                        monthArray = new Integer[]{10,11,12};

                    sumOfGross = mRealm.where(RealmPayroll.class).in("covMonth", monthArray).equalTo("empId", realmPayroll.getEmpId()).sum("totalGross").doubleValue();
                }
                SimpleList simpleList = new SimpleList();
                simpleList.setLabel(realmPayroll.getlName() + ", " + realmPayroll.getfName());
                simpleList.setInfo("Gross: Php " + dF.format(sumOfGross));
                simpleList.setEmpid(String.valueOf(realmPayroll.getEmpId()));
                simpleList.setDialog(getDialog());
                simpleList.setFrom("payrolldetail_employee");
                simpleList.withGross(sumOfGross);
                simpleLists.add(simpleList);

            }

            mFastItemAdapter.setNewList(simpleLists);
            mFastItemAdapter.getItemAdapter().withComparator(new NumberSortingDescending());

            textViewNoData.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            textViewNoData.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            textViewNoData.setText("No Data Found");
        }
    }

    private class NumberSortingDescending implements Comparator<SimpleList>, Serializable {

        @Override
        public int compare(SimpleList simpleList, SimpleList t1) {
            return Double.valueOf(t1.getGross()).compareTo(simpleList.getGross());
        }

    }
}
