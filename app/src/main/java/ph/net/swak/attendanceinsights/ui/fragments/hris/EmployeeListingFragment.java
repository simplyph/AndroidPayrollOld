package ph.net.swak.attendanceinsights.ui.fragments.hris;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.mikepenz.fastadapter.IItemAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.models.RealmEmployeeList;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.ui.views.adapters.EmployeeFastAdapter;
import ph.net.swak.attendanceinsights.utils.JSONParser;

public class EmployeeListingFragment extends Fragment {

    @BindView(R.id.progressbar_employee_list)
    protected MaterialProgressBar progressBar;

    @BindView(R.id.progressbar_employee_list_text)
    protected TextView textView;

    @BindView(R.id.recyclerview_employee_list)
    protected RecyclerView recyclerView;

    private Unbinder unbinder;
    private MainActivity mainActivity;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private Realm mRealm;

    private static final String TAG = "EmployeeList";

    private FastItemAdapter<EmployeeFastAdapter> fastItemAdapter;

    public EmployeeListingFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_employee_listing, container, false);
        unbinder = ButterKnife.bind(this, view);
        requestQueue = VolleyAPI.getInstance(getActivity()).getRequestQueue();
        volleyRequest = new VolleyRequest();
        mainActivity = (MainActivity) getActivity();
        setHasOptionsMenu(true);
        mRealm = Realm.getDefaultInstance();
        mainActivity.setupUI("Employee List");

        fastItemAdapter = new FastItemAdapter<>();
        fastItemAdapter.getItemFilter().withFilterPredicate(new IItemAdapter.Predicate<EmployeeFastAdapter>() {
            @Override
            public boolean filter(EmployeeFastAdapter item, CharSequence constraint) {
                return !item.getFname().toLowerCase().contains(constraint.toString().toLowerCase())
                        && !item.getLname().toLowerCase().contains(constraint.toString().toLowerCase());
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(mainActivity));
        recyclerView.setAdapter(fastItemAdapter);

        setupData();

        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (requestQueue != null)
            requestQueue.cancelAll(TAG);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem searchItem = menu.findItem(R.id.app_bar_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        SearchView.SearchAutoComplete searchText = (SearchView.SearchAutoComplete) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchText.setHint("Search Employee");
        searchText.setHintTextColor(getResources().getColor(R.color.md_grey_300));
        searchText.setTextColor(getResources().getColor(R.color.md_white_1000));
        //searchView.setBackgroundColor(getResources().getColor(R.color.md_white_1000));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                fastItemAdapter.filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                fastItemAdapter.filter(newText);
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.app_bar_search:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupData() {
        progressBar.setVisibility(View.VISIBLE);
        textView.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        textView.setText("Loading Data");

        JsonArrayRequest jsonArrayRequest = volleyRequest.getAuthJsonArray(mainActivity.getApplicationContext(), TAG, volleyRequest.getEmployeeList(), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                final List<RealmEmployeeList> realmEmployeeListList = JSONParser.parseEmployeeList(response);

                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.delete(RealmEmployeeList.class);

                        for (RealmEmployeeList realmEmployeeList : realmEmployeeListList) {
                            realm.copyToRealm(realmEmployeeList);
                        }
                    }
                });

                RealmResults<RealmEmployeeList> query1 = mRealm.where(RealmEmployeeList.class).equalTo("status_type", "ACTIVE").findAllSorted("lname", Sort.ASCENDING);

                List<EmployeeFastAdapter> newList = new ArrayList<>();

                for (RealmEmployeeList realmList : query1) {
                    newList.add(new EmployeeFastAdapter()
                            .withFname(realmList.getFname())
                            .withLname(realmList.getLname())
                            .withImgPath(realmList.getImgPath())
                            .withEmpId(realmList.getEmpId())
                    );
                }

                if (newList.size() > 0) {
                    progressBar.setVisibility(View.GONE);
                    textView.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);

                    fastItemAdapter.setNewList(newList);
                } else {
                    progressBar.setVisibility(View.GONE);
                    textView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    textView.setText("No Employee Found");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                textView.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                textView.setText("No Employee Found");
            }
        });


        requestQueue.add(jsonArrayRequest);
    }

}
