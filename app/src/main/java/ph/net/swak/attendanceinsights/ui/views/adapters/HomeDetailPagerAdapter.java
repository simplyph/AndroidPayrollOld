package ph.net.swak.attendanceinsights.ui.views.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ph.net.swak.attendanceinsights.ui.fragments.AnnouncementFragment;
import ph.net.swak.attendanceinsights.ui.fragments.BulletinFragment;

public class HomeDetailPagerAdapter extends FragmentStatePagerAdapter {

    private int PAGE_COUNT = 2;
    private String tabTitles[] = new String[]{"Home", "Announcement"};

    public HomeDetailPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 1)
            return new BulletinFragment();
        else
            return new AnnouncementFragment();
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
