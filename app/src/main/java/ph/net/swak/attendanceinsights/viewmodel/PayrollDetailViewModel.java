package ph.net.swak.attendanceinsights.viewmodel;

import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import ph.net.swak.attendanceinsights.repository.PayrollRepository;
import ph.net.swak.attendanceinsights.repository.Resource;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.PayrollDetailFastAdapter;

public class PayrollDetailViewModel extends ViewModel {

    private PayrollRepository payrollRepo;
    private MutableLiveData<Resource<List<PayrollDetailFastAdapter>>> payrollDetail = new MutableLiveData<>();

    @Inject
    public PayrollDetailViewModel(PayrollRepository payrollRepository) {
        payrollRepo = payrollRepository;
    }

    public void fetch(int covYear, int covMonth, int compId, int posId, int payrollPeriod) {
        payrollDetail.setValue(null);
        payrollRepo.getPayrollByCompAndType(covYear, covMonth, compId, posId, payrollPeriod).observeForever(listResource -> payrollDetail.setValue(listResource));
    }

    public LiveData<Resource<List<PayrollDetailFastAdapter>>> getPayrollDetail() {
        return payrollDetail;
    }
}
