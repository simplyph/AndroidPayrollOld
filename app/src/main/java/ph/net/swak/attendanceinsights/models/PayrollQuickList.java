package ph.net.swak.attendanceinsights.models;

public class PayrollQuickList {

    private String compName;
    private double totalEmployee;
    private int countEmployee;
    private double totalManager;
    private int countManager;
    private double totalSupervisor;
    private int countSupervisor;
    private double totalStaff;
    private int countStaff;

    public String getCompName() {
        return compName;
    }

    public PayrollQuickList withCompName(String compName) {
        this.compName = compName;
        return this;
    }

    public double getTotalEmployee() {
        return totalEmployee;
    }

    public PayrollQuickList withTotalEmployee(double totalEmployee) {
        this.totalEmployee = totalEmployee;
        return this;
    }

    public int getCountEmployee() {
        return countEmployee;
    }

    public PayrollQuickList withCountEmployee(int countEmployee) {
        this.countEmployee = countEmployee;
        return this;
    }

    public double getTotalManager() {
        return totalManager;
    }

    public PayrollQuickList withTotalManager(double totalManager) {
        this.totalManager = totalManager;
        return this;
    }

    public int getCountManager() {
        return countManager;
    }

    public PayrollQuickList withCountManager(int countManager) {
        this.countManager = countManager;
        return this;
    }

    public double getTotalSupervisor() {
        return totalSupervisor;
    }

    public PayrollQuickList withTotalSupervisor(double totalSupervisor) {
        this.totalSupervisor = totalSupervisor;
        return this;
    }

    public int getCountSupervisor() {
        return countSupervisor;
    }

    public PayrollQuickList withCountSupervisor(int countSupervisor) {
        this.countSupervisor = countSupervisor;
        return this;
    }

    public double getTotalStaff() {
        return totalStaff;
    }

    public PayrollQuickList withTotalStaff(double totalStaff) {
        this.totalStaff = totalStaff;
        return this;
    }

    public int getCountStaff() {
        return countStaff;
    }

    public PayrollQuickList withCountStaff(int countStaff) {
        this.countStaff = countStaff;
        return this;
    }
}
