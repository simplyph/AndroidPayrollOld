package ph.net.swak.attendanceinsights.ui.fragments.lifecycle;


import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.transition.TransitionManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.di.Injectable;
import ph.net.swak.attendanceinsights.events.FilterClickEvent;
import ph.net.swak.attendanceinsights.models.RealmFilterComp;
import ph.net.swak.attendanceinsights.models.RealmFilterYear;
import ph.net.swak.attendanceinsights.repository.Status;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.PayrollDashboardFastAdapter;
import ph.net.swak.attendanceinsights.utils.Helper;
import ph.net.swak.attendanceinsights.viewmodel.PayrollDashboardViewModel;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class PayrollDashboardFragment extends LifecycleFragment implements Injectable {

    @BindView(R.id.swipelayout_payroll_dashboard)
    protected SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recyclerview_payroll_monthly)
    protected RecyclerView recyclerView;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private MainActivity mainActivity;
    private PayrollDashboardViewModel viewModel;
    private Unbinder unbinder;
    private FastItemAdapter<PayrollDashboardFastAdapter> mFastItemAdapter;
    private Realm mRealm;


    public PayrollDashboardFragment() {}

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(PayrollDashboardViewModel.class);
        updateData(Helper.getCurrentYear());
        viewModel.getPayrollMonthly().observe(this, listResource -> {
            if (listResource != null && listResource.data != null) {
                if(listResource.status == Status.SUCCESS) {
                    Toast.makeText(PayrollDashboardFragment.this.getContext(), "CALLED: " + listResource.data.size(), Toast.LENGTH_SHORT).show();
                    mFastItemAdapter.setNewList(listResource.data);
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payroll_dashboard_2, container, false);
        unbinder = ButterKnife.bind(this, view);
        mainActivity = (MainActivity) getActivity();
        mRealm = Realm.getDefaultInstance();

        List<Integer> filterOption = new ArrayList<>();
        filterOption.add(MainActivity.FILTER_YEAR);
        filterOption.add(MainActivity.FILTER_COMP);
        mainActivity.setupUI("Payroll", false, true, filterOption, false, null);


        mFastItemAdapter = new FastItemAdapter<>();
        recyclerView.setAdapter(mFastItemAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        RealmResults<RealmFilterYear> yearResult = mRealm.where(RealmFilterYear.class).findAll();
        RealmResults<RealmFilterComp> compAllResult = mRealm.where(RealmFilterComp.class).findAll();
        RealmResults<RealmFilterComp> compSelectedResult = mRealm.where(RealmFilterComp.class).equalTo("mSelected", true).findAll();

        String compString = "All Companies";

        if (compSelectedResult.where().count() == 0) {
            compString = "No Company is Selected";
        } else if (compSelectedResult.where().count() == 1) {
            if (compAllResult.where().count() > 1)
                compString = compSelectedResult.where().findFirst().getTitle();
        } else if (compSelectedResult.where().count() > 1) {
            if (compAllResult.where().count() == compSelectedResult.where().count())
                compString = "All Companies";
            else if (compAllResult.where().count() > compSelectedResult.where().count())
                compString = String.valueOf(compSelectedResult.where().count()) + " Companies";
        }

        mainActivity.mFilterHeadYear.setText(String.valueOf(yearResult.max("mIdentifier").intValue()));
        mainActivity.mFilterYearContent.setText(String.valueOf(yearResult.max("mIdentifier").intValue()));
        mainActivity.mFilterHeadComp.setText(compString);
        mainActivity.mFilterCompContent.setText(compString);

        // OnClickListeners
        mainActivity.mFilterHead.setOnClickListener(view12 -> onFilterClick());
        mainActivity.mFilterDisplayButton.setOnClickListener(view1 -> {
            onFilterClick();
            RealmResults<RealmFilterComp> compList = mRealm.where(RealmFilterComp.class).equalTo("mSelected", true).findAll();
            Long[] selectedComp = new Long[compList.size()];
            for (int i = 0; i < compList.size(); i++) {
                selectedComp[i] = compList.get(i).getIdentifier();
            }
            updateData(Integer.parseInt(mainActivity.mFilterYearContent.getText().toString()));
        });
        mainActivity.mFilterYear.setOnClickListener(view13 -> EventBus.getDefault().post(new FilterClickEvent("year")));
        mainActivity.mFilterComp.setOnClickListener(view14 -> EventBus.getDefault().post(new FilterClickEvent("comp")));

        swipeRefreshLayout.setOnRefreshListener(() -> updateData(Integer.parseInt(mainActivity.mFilterYearContent.getText().toString())));

        return view;
    }

    private void updateData(int year) {
        RealmResults<RealmFilterComp> compList = mRealm.where(RealmFilterComp.class).equalTo("mSelected", true).findAll();
        Long[] selectedComp = new Long[compList.size()];
        for (int i = 0; i < compList.size(); i++) {
            selectedComp[i] = compList.get(i).getIdentifier();
        }
        swipeRefreshLayout.setRefreshing(true);
        viewModel.updateLiveData(year, selectedComp);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        mRealm.close();
        super.onDestroyView();
    }
    protected void onFilterClick() {
        TransitionManager.beginDelayedTransition(mainActivity.appBarLayout);
        ViewGroup.LayoutParams params = mainActivity.mFilter.getLayoutParams();
        if (params.height == convertPx(48)) {
            mainActivity.mFilterHeadYear.setVisibility(GONE);
            mainActivity.mFilterHeadComp.setVisibility(GONE);
            mainActivity.mFilterHeadArrow.setImageDrawable(ContextCompat.getDrawable(mainActivity, R.drawable.ic_close_black_48dp));
            mainActivity.mFilterYear.setVisibility(VISIBLE);
            mainActivity.mFilterRange.setVisibility(GONE);
            mainActivity.mFilterComp.setVisibility(VISIBLE);
            mainActivity.mFilterDisplay.setVisibility(VISIBLE);
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        } else {
            mainActivity.mFilterHeadYear.setVisibility(VISIBLE);
            mainActivity.mFilterHeadComp.setVisibility(VISIBLE);
            mainActivity.mFilterHeadArrow.setImageDrawable(ContextCompat.getDrawable(mainActivity, R.drawable.ic_keyboard_arrow_down_black_48dp));
            mainActivity.mFilterYear.setVisibility(GONE);
            mainActivity.mFilterRange.setVisibility(GONE);
            mainActivity.mFilterComp.setVisibility(GONE);
            mainActivity.mFilterDisplay.setVisibility(GONE);
            params.height = convertPx(48);
        }

        mainActivity.mFilter.setLayoutParams(params);
    }

    private int convertPx(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }
}
