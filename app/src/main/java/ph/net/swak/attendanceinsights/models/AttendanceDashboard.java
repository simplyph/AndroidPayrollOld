package ph.net.swak.attendanceinsights.models;

/**
 * Created by Knowell on 1/17/2017.
 */

public class AttendanceDashboard {

    private String empId;
    private String fullname;
    private double late;
    private double early;
    private double absent;
    private String coveringDay;
    private double leave;
    private double tardiness;
    private double onTime;

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public double getLate() {
        return late;
    }

    public void setLate(double late) {
        this.late = late;
    }

    public double getEarly() {
        return early;
    }

    public void setEarly(double early) {
        this.early = early;
    }

    public double getAbsent() {
        return absent;
    }

    public void setAbsent(double absent) {
        this.absent = absent;
    }

    public String getCoveringDay() {
        return coveringDay;
    }

    public void setCoveringDay(String coveringDay) {
        this.coveringDay = coveringDay;
    }

    public double getLeave() {
        return leave;
    }

    public void setLeave(double leave) {
        this.leave = leave;
    }

    public double getTardiness() {
        return tardiness;
    }

    public void setTardiness(double tardiness) {
        this.tardiness = tardiness;
    }

    public double getOnTime() {
        return onTime;
    }

    public void setOnTime(double onTime) {
        this.onTime = onTime;
    }
}
