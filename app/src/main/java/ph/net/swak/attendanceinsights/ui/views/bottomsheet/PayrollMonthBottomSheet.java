package ph.net.swak.attendanceinsights.ui.views.bottomsheet;

import android.app.Dialog;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.di.Injectable;
import ph.net.swak.attendanceinsights.repository.Status;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.PayrollMonthFastAdapter;
import ph.net.swak.attendanceinsights.utils.PayrollPeriodType;
import ph.net.swak.attendanceinsights.viewmodel.PayrollMonthBottomSheetViewModel;

public class PayrollMonthBottomSheet extends BottomSheetDialogFragment implements LifecycleRegistryOwner, Injectable {

    @BindView(R.id.textview_bottomsheet_payroll_month_title)
    protected TextView tvTitle;

    @BindView(R.id.textview_bottomsheet_payroll_month_none)
    protected TextView tvNoData;

    @BindView(R.id.recyclerview_bottom_sheet_payroll_month)
    protected RecyclerView recyclerView;

    private PayrollMonthBottomSheetViewModel viewModel;
    private LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    private Unbinder unbinder;
    private FastItemAdapter<PayrollMonthFastAdapter> mFastItemAdapter;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(PayrollMonthBottomSheetViewModel.class);

        Bundle bundle = getArguments();
        if (bundle != null && !bundle.isEmpty()) {
            viewModel.fetchData(bundle.getInt("year"), bundle.getInt("compId", 0), bundle.getInt("from", 0), bundle.getInt("fromId", 0));

            if(bundle.getInt("periodType") == PayrollPeriodType.MONTHLY.getOrder()) {
                viewModel.getMonthList().observe(this, listResource -> {
                    tvTitle.setText(bundle.getString("compName", "None"));

                    if (listResource != null && listResource.data != null && listResource.status == Status.SUCCESS) {
                        List<PayrollMonthFastAdapter> list = new ArrayList<>();

                        for (PayrollMonthFastAdapter payrollMonthFastAdapter : listResource.data) {
                            list.add(new PayrollMonthFastAdapter()
                                    .withCount(payrollMonthFastAdapter.getCount())
                                    .withMonth(payrollMonthFastAdapter.getMonth())
                                    .withValue(payrollMonthFastAdapter.getValue())
                                    .withCompId(payrollMonthFastAdapter.getCompId())
                                    .withCompName(payrollMonthFastAdapter.getCompName())
                                    .withYear(bundle.getInt("year"))
                                    .withPayrollPeriod(PayrollPeriodType.MONTHLY.getOrder())
                                    .withDialog(getDialog())
                            );
                        }

                        if (list.isEmpty())
                            tvNoData.setVisibility(View.VISIBLE);
                        else
                            tvNoData.setVisibility(View.GONE);

                        mFastItemAdapter.setNewList(list);
                    }
                });
            } else if(bundle.getInt("periodType") == PayrollPeriodType.QUARTERLY.getOrder()) {
                viewModel.getQuarterList().observe(this, listResource -> {
                    tvTitle.setText(bundle.getString("compName", "None"));

                    if (listResource != null && listResource.data != null && listResource.status == Status.SUCCESS) {
                        List<PayrollMonthFastAdapter> list = new ArrayList<>();

                        for (PayrollMonthFastAdapter payrollMonthFastAdapter : listResource.data) {
                            list.add(new PayrollMonthFastAdapter()
                                    .withCount(payrollMonthFastAdapter.getCount())
                                    .withMonth(payrollMonthFastAdapter.getMonth())
                                    .withValue(payrollMonthFastAdapter.getValue())
                                    .withCompId(payrollMonthFastAdapter.getCompId())
                                    .withCompName(payrollMonthFastAdapter.getCompName())
                                    .withYear(2017)
                                    .withPayrollPeriod(PayrollPeriodType.QUARTERLY.getOrder())
                                    .withDialog(getDialog())
                            );
                        }

                        if (list.isEmpty())
                            tvNoData.setVisibility(View.VISIBLE);
                        else
                            tvNoData.setVisibility(View.GONE);

                        mFastItemAdapter.setNewList(list);
                    }
                });
            }
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this.getContext(), this.getTheme());
        View view = View.inflate(getContext(), R.layout.bottomsheet_payroll_month, null);
        unbinder = ButterKnife.bind(this, view);
        bottomSheetDialog.setContentView(view);

        mFastItemAdapter = new FastItemAdapter<>();
        mFastItemAdapter.withSelectable(true);
        recyclerView.setAdapter(mFastItemAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return bottomSheetDialog;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
