package ph.net.swak.attendanceinsights.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.firebase.messaging.FirebaseMessaging;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import io.realm.Realm;
import io.realm.RealmAsyncTask;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.events.FilterClickEvent;
import ph.net.swak.attendanceinsights.events.PayrollDetailEvent;
import ph.net.swak.attendanceinsights.events.ShowDialogEvent;
import ph.net.swak.attendanceinsights.events.UserDetailEvent;
import ph.net.swak.attendanceinsights.models.RealmFilterComp;
import ph.net.swak.attendanceinsights.models.RealmFilterYear;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.ui.fragments.CalendarBottomFragment;
import ph.net.swak.attendanceinsights.ui.fragments.FilterCompBottomFragment;
import ph.net.swak.attendanceinsights.ui.fragments.FilterTypeBottomFragment;
import ph.net.swak.attendanceinsights.ui.fragments.FilterYearBottomFragment;
import ph.net.swak.attendanceinsights.ui.fragments.SettingsFragment;
import ph.net.swak.attendanceinsights.ui.fragments.appraisal.AppraisalDashboardFragment;
import ph.net.swak.attendanceinsights.ui.fragments.attendance.AttendanceDashboardFragment;
import ph.net.swak.attendanceinsights.ui.fragments.attendance.AttendanceInsightFragment;
import ph.net.swak.attendanceinsights.ui.fragments.employee.FileItineraryMainFragment;
import ph.net.swak.attendanceinsights.ui.fragments.hris.EmployeeListingFragment;
import ph.net.swak.attendanceinsights.ui.fragments.hris.UserDetailsFragment;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.HomeFragment;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.HrisDashboardFragment;
import ph.net.swak.attendanceinsights.ui.fragments.payroll.PayrollDashboardFragment;
import ph.net.swak.attendanceinsights.ui.fragments.payroll.PayrollMainFragment;
import ph.net.swak.attendanceinsights.utils.JSONParser;
import ph.net.swak.attendanceinsights.utils.LocaleHelper;

public class MainActivity extends AppCompatActivity implements HasSupportFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    //region Setup UI
    public static final int
            FILTER_YEAR = 1,
            FILTER_RANGE = 2,
            FILTER_TYPE = 3,
            FILTER_COMP = 4;
    private static final int
            DRAWER_MAIN_HOME = 1,
            DRAWER_MAIN_EMPLOYEE = 2,
            DRAWER_SUB_EMP_PROFILE = 201,
            DRAWER_SUB_EMP_LEAVE = 202,
            DRAWER_SUB_EMP_ITINERARY = 203,
            DRAWER_SUB_EMP_OTND = 204,
            DRAWER_SUB_EMP_PAYSLIP = 205,
            DRAWER_MAIN_HRIS = 4,
            DRAWER_SUB_HRIS_DASHBOARD = 401,
            DRAWER_SUB_HRIS_EMPLOYEE_LIST = 402,
            DRAWER_MAIN_ATTENDANCE = 5,
            DRAWER_SUB_ATTENDANCE_DASHBOARD = 501,
            DRAWER_SUB_ATTENDANCE_PARTICULAR = 502,
            DRAWER_MAIN_PAYROLL = 6,
            DRAWER_SUB_PAYROLL_DASHBOARD_OLD = 601,
            DRAWER_SUB_PAYROLL_DASHBOARD_NEW = 602,
            DRAWER_MAIN_APPRAISAL = 7,
            DRAWER_SUB_APPRAISAL_DASHBOARD = 701,
            DRAWER_MAIN_SETTINGS = 8,
            DRAWER_MAIN_SIGNOUT = 9;
    private final String TAG = "MainActivity";
    // Variables
    public FragmentManager fragmentManager = null;

    // MaterialDrawer
    public Drawer result;
    public AccountHeader accountHeader;
    // RealmTransactions
    public List<RealmAsyncTask> transactions = new ArrayList<>();
    // ButterKnife
    @BindView(R.id.coordinatorLayout)
    public CoordinatorLayout coordinatorLayout;
    @BindView(R.id.main_appbar)
    public AppBarLayout appBarLayout;
    @BindView(R.id.main_toolbar)
    public Toolbar toolbar;
    @BindView(R.id.main_frame)
    public FrameLayout mainFrameLayout;
    // Filter
    @BindView(R.id.cardview_main_filter)
    public FrameLayout mFilter;
    @BindView(R.id.relativelayout_main_filter_head)
    public RelativeLayout mFilterHead;
    @BindView(R.id.textview_main_filter_head_title)
    public TextView mFilterHeadTitle;
    @BindView(R.id.chip_main_filter_head_year)
    public TextView mFilterHeadYear;
    @BindView(R.id.chip_main_filter_head_comp)
    public TextView mFilterHeadComp;
    @BindView(R.id.button_main_filter_head_dropdown)
    public ImageView mFilterHeadArrow;
    @BindView(R.id.relativelayout_main_filter_year)
    public RelativeLayout mFilterYear;
    @BindView(R.id.textview_main_filter_year_title)
    public TextView mFilterYearTitle;
    @BindView(R.id.textview_main_filter_year_content)
    public TextView mFilterYearContent;
    @BindView(R.id.button_main_filter_year_dropdown)
    public ImageView mFilterYearDrop;
    @BindView(R.id.relativelayout_main_filter_range)
    public RelativeLayout mFilterRange;
    @BindView(R.id.textview_main_filter_range_title)
    public TextView mFilterRangeTitle;
    @BindView(R.id.textview_main_filter_range_content)
    public TextView mFilterRangeContent;
    @BindView(R.id.button_main_filter_range_dropdown)
    public ImageView mFilterRangeDrop;
    @BindView(R.id.relativelayout_main_filter_type)
    public RelativeLayout mFilterType;
    @BindView(R.id.textview_main_filter_type_title)
    public TextView mFilterTypeTitle;
    @BindView(R.id.textview_main_filter_type_content)
    public TextView mFilterTypeContent;
    @BindView(R.id.button_main_filter_type_dropdown)
    public ImageView mFilterTypeDrop;
    @BindView(R.id.relativelayout_main_filter_comp)
    public RelativeLayout mFilterComp;
    @BindView(R.id.textview_main_filter_comp_title)
    public TextView mFilterCompTitle;
    @BindView(R.id.textview_main_filter_comp_content)
    public TextView mFilterCompContent;
    @BindView(R.id.button_main_filter_comp_dropdown)
    public ImageView mFilterCompDrop;
    @BindView(R.id.relativelayout_main_filter_display)
    public RelativeLayout mFilterDisplay;
    @BindView(R.id.button_main_filter_display)
    public Button mFilterDisplayButton;
    @BindView(R.id.fab)
    public FloatingActionButton fab;
    @BindView(R.id.main_tablayout)
    public TabLayout mTabLayout;
    private boolean isBackPress;
    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            isBackPress = false;
        }
    };
    private Handler mHandler = new Handler();
    private Toast mBackToast;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private SharedPreferences sharedPreferences;
    private Realm mRealm;

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        mBackToast = Toast.makeText(MainActivity.this, "Press back again to exit", Toast.LENGTH_SHORT);
        mRealm = Realm.getDefaultInstance();
        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);
        requestQueue = VolleyAPI.getInstance(this).getRequestQueue();
        volleyRequest = new VolleyRequest();
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ViewCompat.postOnAnimation(coordinatorLayout, new Runnable() {
            @Override
            public void run() {
                ViewCompat.postInvalidateOnAnimation(coordinatorLayout);
            }
        });

        // Firebase Topics & Initialization
        FirebaseMessaging.getInstance().subscribeToTopic("mdruat");                     // Site
        FirebaseMessaging.getInstance().subscribeToTopic("manager");                    // Role
        FirebaseMessaging.getInstance().subscribeToTopic("promotion");                  // SubModule
        FirebaseMessaging.getInstance().subscribeToTopic("payrollprocess");             // SubModule
        FirebaseMessaging.getInstance().subscribeToTopic("announcement");               // SubModule

        // Toolbar
        setSupportActionBar(toolbar);
        toolbar.setTitle("Payroll Analytics");

        // Arrange Data
        setupFilters();
        requestQueue.add(volleyRequest.getOAuthToken(getApplicationContext()));

        // Layout Manipulation
        fab.setVisibility(View.GONE);
        setupDrawer(savedInstanceState);

        if(sharedPreferences.getBoolean("welcome", false))
            showWelcome();

        // Fragments
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_frame, new HomeFragment()).commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState = result.saveInstanceState(outState);
        outState = accountHeader.saveInstanceState(outState);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (result != null && result.isDrawerOpen()) {
            result.closeDrawer();
        } else {
            if (count == 0) {
                if (isBackPress) {
                    if (mBackToast != null)
                        mBackToast.cancel();
                    super.onBackPressed();
                    return;
                }

                isBackPress = true;
                mBackToast.show();

                mHandler.postDelayed(mRunnable, 2000);
            } else {
                getSupportFragmentManager().popBackStack();
            }
        }
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        if (transactions != null) {
            for (RealmAsyncTask transaction : transactions) {
                if (transaction != null && !transaction.isCancelled())
                    transaction.cancel();
            }
        }

        if (requestQueue != null)
            requestQueue.cancelAll(TAG);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHandler != null) mHandler.removeCallbacks(mRunnable);
        closeRealm();
    }

    public void closeRealm() {
        if (!mRealm.isClosed())
            mRealm.close();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase, "en"));
        LocaleHelper.setLocale(newBase, "en");
    }

    //region EventBus Events
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFilterClick(FilterClickEvent event) {
        if (event.type.equalsIgnoreCase("comp")) {
            FilterCompBottomFragment filterCompBottomFragment = new FilterCompBottomFragment();
            filterCompBottomFragment.show(this.getSupportFragmentManager(), null);
        } else if (event.type.equalsIgnoreCase("year")) {
            FilterYearBottomFragment filterYearBottomFragment = new FilterYearBottomFragment();
            filterYearBottomFragment.show(this.getSupportFragmentManager(), null);
        } else if (event.type.equalsIgnoreCase("range")) {
            CalendarBottomFragment calendarBottomFragment = new CalendarBottomFragment();
            calendarBottomFragment.show(this.getSupportFragmentManager(), null);
        } else if (event.type.equalsIgnoreCase("type")) {
            FilterTypeBottomFragment filterTypeBottomFragment = new FilterTypeBottomFragment();
            filterTypeBottomFragment.show(this.getSupportFragmentManager(), null);
        }
    }
    //endregion

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShowDialog(ShowDialogEvent event) {
        event.bottomSheet.show(this.getSupportFragmentManager(), null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPayrollDetailEvent(PayrollDetailEvent event) {
        Bundle bundle = new Bundle();
        bundle.putString("year", event.getYear());
        bundle.putString("month", event.getMonth());
        bundle.putInt("compid", event.getCompid());
        bundle.putString("compName", event.getCompName());
        bundle.putInt("totalManager", event.getManagerList());
        bundle.putInt("totalSupervisor", event.getSupervisorList());
        bundle.putInt("totalStaff", event.getStaffList());
        bundle.putDouble("grossManager", event.getManagerGross());
        bundle.putDouble("grossSupervisor", event.getSupervisorGross());
        bundle.putDouble("grossStaff", event.getStaffGross());
        bundle.putStringArrayList("divs", event.getDivList());
        bundle.putString("payrolltype", event.getPayrollType());
        PayrollMainFragment fragment = new PayrollMainFragment();
        fragment.setArguments(bundle);
        fragmentManager.beginTransaction().addToBackStack("payroll_detail").replace(R.id.main_frame, fragment).commit();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserDetail(UserDetailEvent event) {
        Bundle bundle = new Bundle();
        bundle.putString("empid", event.empId);
        UserDetailsFragment fragment = new UserDetailsFragment();
        fragment.setArguments(bundle);
        toolbar.setTitle("Employee Details");
        fragmentManager.beginTransaction().addToBackStack("employee_detail").replace(R.id.main_frame, fragment).commit();
    }

    public void setupUI(String title) {
        setupUI(title, false, false, null, false, null);
    }

    public void setupUI(String title, boolean fabEnabled, boolean filterEnabled, boolean pagerEnabled) {
        setupUI(title, fabEnabled, filterEnabled, null, pagerEnabled, null);
    }
    //endregion

    public void setupUI(String title, boolean fabEnabled, boolean filterEnabled, @Nullable List<Integer> filterOptions, boolean pagerEnabled, @Nullable List<Integer> pagerOptions) {
        toolbar.setTitle(title);

        // Views
        // Fab
        fab.setVisibility(fabEnabled ? View.VISIBLE : View.GONE);

        // Filter

        mTabLayout.setVisibility(pagerEnabled ? View.VISIBLE : View.GONE);

        if (filterEnabled) {
            if (filterOptions != null && filterOptions.size() > 0) {
                mFilter.setVisibility(View.VISIBLE);

                if (filterOptions.contains(FILTER_YEAR))
                    mFilterYear.setVisibility(View.VISIBLE);
                else
                    mFilterYear.setVisibility(View.GONE);

                if (filterOptions.contains(FILTER_RANGE))
                    mFilterRange.setVisibility(View.VISIBLE);
                else
                    mFilterRange.setVisibility(View.GONE);

                if (filterOptions.contains(FILTER_TYPE))
                    mFilterType.setVisibility(View.VISIBLE);
                else
                    mFilterType.setVisibility(View.GONE);

                if (filterOptions.contains(FILTER_COMP))
                    mFilterComp.setVisibility(View.VISIBLE);
                else
                    mFilterComp.setVisibility(View.GONE);

            } else {
                mFilter.setVisibility(View.GONE);
            }
        } else {
            mFilter.setVisibility(View.GONE);
        }
    }

    private void setupFilters() {
        JsonArrayRequest jsonFilterYear = volleyRequest.getAuthJsonArray(getApplicationContext(), TAG, volleyRequest.getPayrollFilterYear(), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                final List<RealmFilterYear> realmFilterYears = JSONParser.parsePayrollFilterYear(response);

                RealmAsyncTask transaction = mRealm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.delete(RealmFilterYear.class);

                        for (RealmFilterYear realmFromResponse : realmFilterYears) {
                            realm.copyToRealm(realmFromResponse);
                        }
                    }
                });

                if (transactions != null)
                    transactions.add(transaction);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        JsonArrayRequest jsonFilterComp = volleyRequest.getAuthJsonArray(getApplicationContext(), TAG, volleyRequest.getPayrollFilterCompany(), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                final List<RealmFilterComp> realmFilterComps = JSONParser.parsePayrollFilterComp(response);

                RealmAsyncTask transaction = mRealm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.delete(RealmFilterComp.class);

                        for (RealmFilterComp realmFromResponse : realmFilterComps) {
                            RealmFilterComp realmFromCopy = realm.copyToRealm(realmFromResponse);
                            realmFromCopy.withSetSelected(true);
                        }
                    }
                });

                if (transactions != null)
                    transactions.add(transaction);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(jsonFilterYear);
        requestQueue.add(jsonFilterComp);
    }

    private void setupDrawer(Bundle savedInstanceState) {

        //region Variables
        String uri = sharedPreferences.getString("img", "/Images/default.jpg");
        String name = sharedPreferences.getString("fullname", "Name");
        String role = sharedPreferences.getString("role", "Role");
        int colorSelected = R.color.md_orange_700;
        int colorMain = R.color.md_blue_800;
        //endregion

        IProfile profile = new ProfileDrawerItem()
                .withName(name)
                .withIcon(Uri.parse(VolleyRequest.URL_BASE + uri))
                .withEmail(role);

        //region DrawerItems
        // Home
        PrimaryDrawerItem drawerHome = new PrimaryDrawerItem().withName("Home")
                .withIdentifier(DRAWER_MAIN_HOME)
                .withIcon(FontAwesome.Icon.faw_home)
                .withIconColor(ContextCompat.getColor(this, colorMain))
                .withTextColor(ContextCompat.getColor(this, colorMain))
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected));

        // Employee
        ExpandableDrawerItem drawerEmployee = new ExpandableDrawerItem().withName("Employee")
                .withIdentifier(DRAWER_MAIN_EMPLOYEE)
                .withIcon(FontAwesome.Icon.faw_user)
                .withIconColor(ContextCompat.getColor(this, colorMain))
                .withTextColor(ContextCompat.getColor(this, colorMain))
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected))
                .withSelectable(false);
        SecondaryDrawerItem drawerEmployeeProfile = new SecondaryDrawerItem().withName("Profile")
                .withIdentifier(DRAWER_SUB_EMP_PROFILE)
                .withIcon(FontAwesome.Icon.faw_user_circle_o)
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected))
                .withLevel(2);
        SecondaryDrawerItem drawerEmployeeLeave = new SecondaryDrawerItem().withName("File Leave")
                .withIdentifier(DRAWER_SUB_EMP_LEAVE)
                .withIcon(FontAwesome.Icon.faw_file_text_o)
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected))
                .withLevel(2);
        SecondaryDrawerItem drawerEmployeeItinerary = new SecondaryDrawerItem().withName("File Itinerary")
                .withIdentifier(DRAWER_SUB_EMP_ITINERARY)
                .withIcon(FontAwesome.Icon.faw_file_text_o)
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected))
                .withLevel(2);
        SecondaryDrawerItem drawerEmployeeOTND = new SecondaryDrawerItem().withName("File OT/ND")
                .withIdentifier(DRAWER_SUB_EMP_OTND)
                .withIcon(FontAwesome.Icon.faw_file_text_o)
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected))
                .withLevel(2);
        SecondaryDrawerItem drawerEmployeePayslip = new SecondaryDrawerItem().withName("Payslip")
                .withIdentifier(DRAWER_SUB_EMP_PAYSLIP)
                .withIcon(FontAwesome.Icon.faw_money)
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected))
                .withLevel(2);


        // HRIS
        ExpandableDrawerItem drawerHris = new ExpandableDrawerItem().withName("HRIS")
                .withIdentifier(DRAWER_MAIN_HRIS)
                .withIcon(FontAwesome.Icon.faw_address_book_o)
                .withIconColor(ContextCompat.getColor(this, colorMain))
                .withTextColor(ContextCompat.getColor(this, colorMain))
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected))
                .withSelectable(false);
        SecondaryDrawerItem drawerHrisDashboard = new SecondaryDrawerItem().withName("Dashboard")
                .withIdentifier(DRAWER_SUB_HRIS_DASHBOARD)
                .withIcon(FontAwesome.Icon.faw_tachometer)
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected))
                .withLevel(2);
        SecondaryDrawerItem drawerHrisEmployeeList = new SecondaryDrawerItem().withName("Employee List")
                .withIdentifier(DRAWER_SUB_HRIS_EMPLOYEE_LIST)
                .withIcon(FontAwesome.Icon.faw_users)
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected))
                .withLevel(2);

        // ATTENDANCE
        ExpandableDrawerItem drawerAttendance = new ExpandableDrawerItem().withName("Attendance")
                .withIdentifier(DRAWER_MAIN_ATTENDANCE)
                .withIcon(FontAwesome.Icon.faw_clock_o)
                .withIconColor(ContextCompat.getColor(this, colorMain))
                .withTextColor(ContextCompat.getColor(this, colorMain))
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected))
                .withSelectable(false);
        SecondaryDrawerItem drawerAttendanceDashboard = new SecondaryDrawerItem().withName("Dashboard")
                .withIdentifier(DRAWER_SUB_ATTENDANCE_DASHBOARD)
                .withIcon(FontAwesome.Icon.faw_tachometer)
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected))
                .withLevel(2);
        SecondaryDrawerItem drawerAttendanceInsights = new SecondaryDrawerItem().withName("Particular")
                .withIdentifier(DRAWER_SUB_ATTENDANCE_PARTICULAR)
                .withIcon(FontAwesome.Icon.faw_id_card_o)
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected))
                .withLevel(2);

        // PAYROLL
        ExpandableDrawerItem drawerPayroll = new ExpandableDrawerItem().withName("Payroll")
                .withIdentifier(DRAWER_MAIN_PAYROLL)
                .withIcon(FontAwesome.Icon.faw_line_chart)
                .withIconColor(ContextCompat.getColor(this, colorMain))
                .withTextColor(ContextCompat.getColor(this, colorMain))
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected))
                .withSelectable(false);
        SecondaryDrawerItem drawerPayrollDashboardNew = new SecondaryDrawerItem().withName("New")
                .withIdentifier(DRAWER_SUB_PAYROLL_DASHBOARD_NEW)
                .withIcon(FontAwesome.Icon.faw_tachometer)
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected))
                .withLevel(2);
        SecondaryDrawerItem drawerPayrollDashboardOld = new SecondaryDrawerItem().withName("OLD")
                .withIdentifier(DRAWER_SUB_PAYROLL_DASHBOARD_OLD)
                .withIcon(FontAwesome.Icon.faw_tachometer)
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected))
                .withLevel(2);

        // APPRAISAL
        PrimaryDrawerItem drawerAppraisal = new PrimaryDrawerItem().withName("Appraisal")
                .withIdentifier(DRAWER_MAIN_APPRAISAL)
                .withIcon(FontAwesome.Icon.faw_star_half_o)
                .withIconColor(ContextCompat.getColor(this, colorMain))
                .withTextColor(ContextCompat.getColor(this, colorMain))
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected));
        SecondaryDrawerItem drawerAppraisalDashboard = new SecondaryDrawerItem().withName("Dashboard")
                .withIdentifier(DRAWER_SUB_APPRAISAL_DASHBOARD)
                .withIcon(FontAwesome.Icon.faw_tachometer)
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected))
                .withLevel(2);

        // OTHER
        PrimaryDrawerItem drawerSettings = new PrimaryDrawerItem().withName("Settings")
                .withIdentifier(DRAWER_MAIN_SETTINGS)
                .withIcon(FontAwesome.Icon.faw_cogs)
                .withIconColor(ContextCompat.getColor(this, colorMain))
                .withTextColor(ContextCompat.getColor(this, colorMain))
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected));
        PrimaryDrawerItem drawerSignout = new PrimaryDrawerItem().withName("Logout")
                .withIdentifier(DRAWER_MAIN_SIGNOUT)
                .withIcon(FontAwesome.Icon.faw_sign_out)
                .withIconColor(ContextCompat.getColor(this, colorMain))
                .withTextColor(ContextCompat.getColor(this, colorMain))
                .withSelectedIconColor(ContextCompat.getColor(this, colorSelected))
                .withSelectedTextColor(ContextCompat.getColor(this, colorSelected));
        //endregion

        accountHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withTranslucentStatusBar(true)
                .withHeaderBackground(R.drawable.drawer_background1)
                .withCurrentProfileHiddenInList(true)
                .addProfiles(profile)
                .withSavedInstance(savedInstanceState)
                .build();

        if (savedInstanceState == null) {
            accountHeader.setActiveProfile(profile);
        }

        //region DrawerBuilder
        result = new DrawerBuilder()
                .withActivity(this)
                .withTranslucentStatusBar(true)
                .withToolbar(toolbar)
                .withHasStableIds(true)
                .withAccountHeader(accountHeader)
                .addDrawerItems(
                        drawerHome,
                        /*
                        drawerEmployee.withSubItems(
                                drawerEmployeeProfile,
                                drawerEmployeeLeave,
                                drawerEmployeeItinerary,
                                drawerEmployeeOTND,
                                drawerEmployeePayslip
                        ),
                        */
                        drawerHris.withSubItems(
                                drawerHrisDashboard,
                                drawerHrisEmployeeList
                        ),
                        drawerAttendance.withSubItems(
                                drawerAttendanceDashboard,
                                drawerAttendanceInsights
                        ),
                        drawerPayroll.withSubItems(
                                drawerPayrollDashboardNew,
                                drawerPayrollDashboardOld
                        ),
                        drawerAppraisal
                        //drawerSettings
                )
                .addStickyDrawerItems(drawerSignout)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Fragment fragment = null;

                        switch ((int) drawerItem.getIdentifier()) {
                            case DRAWER_MAIN_HOME:
                                fragment = new HomeFragment();
                                break;

                            case DRAWER_SUB_HRIS_DASHBOARD:
                                fragment = new HrisDashboardFragment();
                                break;

                            case DRAWER_SUB_HRIS_EMPLOYEE_LIST:
                                fragment = new EmployeeListingFragment();
                                break;

                            case DRAWER_SUB_ATTENDANCE_DASHBOARD:
                                fragment = new AttendanceDashboardFragment();
                                break;

                            case DRAWER_SUB_PAYROLL_DASHBOARD_NEW:
                                fragment = new ph.net.swak.attendanceinsights.ui.fragments.lifecycle.PayrollDashboardFragment();
                                break;

                            case DRAWER_SUB_PAYROLL_DASHBOARD_OLD:
                                fragment = new PayrollDashboardFragment();
                                break;

                            case DRAWER_SUB_ATTENDANCE_PARTICULAR:
                                fragment = new AttendanceInsightFragment();
                                break;

                            case DRAWER_MAIN_APPRAISAL:
                                fragment = new AppraisalDashboardFragment();
                                break;

                            case DRAWER_MAIN_SIGNOUT:
                                FirebaseMessaging.getInstance().unsubscribeFromTopic("mdruat");                     // Site
                                FirebaseMessaging.getInstance().unsubscribeFromTopic("manager");                    // Role
                                FirebaseMessaging.getInstance().unsubscribeFromTopic("promotion");                  // SubModule
                                FirebaseMessaging.getInstance().unsubscribeFromTopic("payrollprocess");             // SubModule
                                FirebaseMessaging.getInstance().unsubscribeFromTopic("announcement");               // SubModule
                                sharedPreferences.edit().putBoolean("login", false).putString("fullname", null).putString("role", null).putString("img", "/Images/default.jpg").putString("username", null).putString("password", null).commit();
                                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case DRAWER_MAIN_SETTINGS:
                                toolbar.setTitle("Settings");
                                fragment = new SettingsFragment();
                                break;

                            case DRAWER_SUB_EMP_ITINERARY:
                                toolbar.setTitle("Itinerary");
                                fragment = new FileItineraryMainFragment();
                                break;
                        }

                        if (fragment != null) {
                            if (fragmentManager.getBackStackEntryCount() > 0) {
                                fragmentManager.popBackStack();
                            }
                            fragmentManager.beginTransaction().replace(R.id.main_frame, fragment).commit();
                        }

                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .withShowDrawerOnFirstLaunch(true)
                .build();
        //endregion
    }

    private void showWelcome() {
        new MaterialDialog.Builder(this)
                .iconRes(R.mipmap.ic_launcher)
                .limitIconToDefaultSize()
                .title(R.string.app_name_display)
                .content(R.string.app_desc_long, true)
                .positiveText(R.string.dialog_prompt_ok)
                .positiveColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .show();

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("welcome", false);
        editor.apply();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }
}
