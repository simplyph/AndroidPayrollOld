package ph.net.swak.attendanceinsights.ui.views.fastadapter;

import android.app.Dialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.events.UserDetailEvent;
import ph.net.swak.attendanceinsights.utils.Helper;

public class PayrollEmployeeFastAdapter extends AbstractItem<PayrollEmployeeFastAdapter, PayrollEmployeeFastAdapter.ViewHolder> {

    private long empId;
    private String fName;
    private String lName;
    private String designation;
    private double value;
    private Dialog dialog;

    public long getEmpId() {
        return empId;
    }

    public PayrollEmployeeFastAdapter withEmpId(long empId) {
        this.empId = empId;
        return this;
    }

    public String getfName() {
        return fName;
    }

    public PayrollEmployeeFastAdapter withfName(String fName) {
        this.fName = fName;
        return this;
    }

    public String getlName() {
        return lName;
    }

    public PayrollEmployeeFastAdapter withlName(String lName) {
        this.lName = lName;
        return this;
    }

    public String getDesignation() {
        return designation;
    }

    public PayrollEmployeeFastAdapter withDesignation(String designation) {
        this.designation = designation;
        return this;
    }

    public double getValue() {
        return value;
    }

    public PayrollEmployeeFastAdapter withValue(double value) {
        this.value = value;
        return this;
    }

    public Dialog getDialog() {
        return dialog;
    }

    public PayrollEmployeeFastAdapter withDialog(Dialog dialog) {
        this.dialog = dialog;
        return this;
    }

    @Override
    public ViewHolder getViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public int getType() {
        return R.id.fastadapter_payroll_employee_bottom_sheet_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.item_payroll_employee_bottom_sheet;
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);

        holder.tvName.setText(String.format(Locale.ENGLISH,"%s, %s", Helper.toTitleCase(getlName()), Helper.toTitleCase(getfName())));
        holder.tvValue.setText(String.format(Locale.ENGLISH,"Php %s", Helper.toDecimal(getValue())));
        holder.tvDesignation.setText(Helper.toTitleCase(getDesignation()));
        holder.itemView.setOnClickListener(v -> {
            getDialog().dismiss();
            EventBus.getDefault().post(new UserDetailEvent(String.valueOf(getEmpId())));
        });
    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);

        holder.tvName.setText(null);
        holder.tvValue.setText(null);
        holder.tvDesignation.setText(null);
        holder.itemView.setOnClickListener(null);
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageview_payroll_employee_bottom_sheet)
        protected ImageView imageView;

        @BindView(R.id.textview_payroll_employee_bottom_sheet_name)
        protected TextView tvName;

        @BindView(R.id.textview_payroll_employee_bottom_sheet_value)
        protected TextView tvValue;

        @BindView(R.id.textview_payroll_employee_bottom_sheet_designation)
        protected TextView tvDesignation;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
