package ph.net.swak.attendanceinsights.ui.activities;

import android.arch.lifecycle.LifecycleActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.models.User;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.utils.JSONParser;

public class LoginActivity extends LifecycleActivity {

    @BindView(R.id.login_user)
    protected TextInputLayout loginUser;

    @BindView(R.id.login_pass)
    protected TextInputLayout loginPass;

    @BindView(R.id.login_loading)
    protected FrameLayout loginLoading;

    private Unbinder unbinder;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private SharedPreferences sharedPreferences;
    private final String TAG = "LoginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        unbinder = ButterKnife.bind(this);
        // Logic for Login/Logout
        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);
        if (sharedPreferences.getBoolean("login", false)) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        requestQueue = VolleyAPI.getInstance(this).getRequestQueue();
        volleyRequest = new VolleyRequest();

        loginPass.setPasswordVisibilityToggleEnabled(true);
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (requestQueue != null)
            requestQueue.cancelAll(TAG);
    }

    @OnClick(R.id.login_login)
    protected void onLogin() {
        if (isFieldEmpty()) {
            Toast.makeText(this, "Username/Password must not be empty", Toast.LENGTH_SHORT).show();
        } else {
            loginLoading.setVisibility(View.VISIBLE);

            JsonArrayRequest loginRequest = volleyRequest.getJsonArray(TAG, volleyRequest.getLogin(loginUser.getEditText().getText().toString(), loginPass.getEditText().getText().toString()), new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    User user = JSONParser.parseLogin(response);
                    loginLoading.setVisibility(View.GONE);

                    if (!user.isActive()) {
                        Toast.makeText(LoginActivity.this, "Your account is currently inactive", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();

                    editor.putBoolean("login", true);
                    editor.putBoolean("welcome", true);
                    editor.putString("fullname", user.getFullname());
                    editor.putString("role", user.getRole());
                    editor.putString("img", user.getImg());
                    editor.putString("username", loginUser.getEditText().getText().toString());
                    editor.putString("password", loginPass.getEditText().getText().toString());
                    editor.commit();

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loginLoading.setVisibility(View.GONE);
                    Toast.makeText(LoginActivity.this, "Incorrect Username/Password", Toast.LENGTH_SHORT).show();
                }
            });

            requestQueue.add(loginRequest);
        }

    }

    private boolean isFieldEmpty() {
        if (loginUser.getEditText().getText().length() > 0 && loginPass.getEditText().getText().length() > 0)
            return false;
        else
            return true;
    }
}

