package ph.net.swak.attendanceinsights.ui.fragments;

import android.app.Dialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.AppCompatCheckedTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.models.RealmFilterComp;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;

/**
 * Created by Knowell on 2/6/2017.
 */

public class FilterCompBottomFragment extends BottomSheetDialogFragment {

    @BindView(R.id.recyclerview_bottom_sheet_filter_comp)
    protected RecyclerView recyclerView;

    @BindView(R.id.progress_bar_bottom_sheet_filter_comp)
    protected MaterialProgressBar progressBar;

    @BindView(R.id.textview_bottom_sheet_filter_comp_info)
    protected TextView textViewInfo;

    @BindView(R.id.button_bottom_sheet_filter_comp_select)
    protected TextView textViewSelect;

    private FastItemAdapter<RealmFilterComp> mFastItemAdapter;

    private Realm mRealm;
    private Unbinder unbinder;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private MainActivity mainActivity;

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        mainActivity = (MainActivity) getContext();
        View view = View.inflate(mainActivity, R.layout.fragment_bottom_sheet_filter_comp, null);
        unbinder = ButterKnife.bind(this, view);
        requestQueue = VolleyAPI.getInstance(mainActivity).getRequestQueue();
        volleyRequest = new VolleyRequest();
        mRealm = Realm.getDefaultInstance();

        mFastItemAdapter = new FastItemAdapter<>();
        mFastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<RealmFilterComp>() {
            @Override
            public boolean onClick(View v, IAdapter<RealmFilterComp> adapter, final RealmFilterComp item, int position) {
                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        RealmFilterComp realmFromObject = realm.copyToRealm(item);
                        realmFromObject.withSetSelected(!realmFromObject.isSelected());
                    }
                });
                ((AppCompatCheckedTextView) v).toggle();
                return false;
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mFastItemAdapter);

        textViewInfo.setText("Loading data");

        mRealm.where(RealmFilterComp.class).findAllAsync().addChangeListener(new RealmChangeListener<RealmResults<RealmFilterComp>>() {
            @Override
            public void onChange(RealmResults<RealmFilterComp> element) {
                if (element.size() > 0) {
                    if(progressBar != null)
                        progressBar.setVisibility(View.GONE);
                    if(textViewInfo != null)
                        textViewInfo.setVisibility(View.GONE);
                    if(recyclerView != null)
                        recyclerView.setVisibility(View.VISIBLE);
                } else {
                    if(progressBar != null)
                        textViewInfo.setText("No data found.");
                    if(progressBar != null)
                        progressBar.setVisibility(View.VISIBLE);
                    if(textViewInfo != null)
                        textViewInfo.setVisibility(View.VISIBLE);
                    if(recyclerView != null)
                        recyclerView.setVisibility(View.GONE);
                }
                mFastItemAdapter.setNewList(element);
            }
        });

        dialog.setContentView(view);
    }

    @OnClick(R.id.button_bottom_sheet_filter_comp_select)
    protected void onSelectClick() {
        RealmResults<RealmFilterComp> query1 = mRealm.where(RealmFilterComp.class).findAll();
        RealmResults<RealmFilterComp> query2 = mRealm.where(RealmFilterComp.class).equalTo("mSelected", true).findAll();

        String compString = "All Companies";

        if(query2.where().count() == 0) {
            dismiss();
        } else if(query2.where().count() == 1) {
            if(query1.where().count() > 1)
                    compString = query2.where().findFirst().getTitle();
        } else if(query2.where().count() > 1) {
            if(query1.where().count() == query2.where().count())
                compString = "All Companies";
            else if(query1.where().count() > query2.where().count())
                compString = String.valueOf(query2.where().count()) + " Companies";
        }

        mainActivity.mFilterHeadComp.setText(compString);
        mainActivity.mFilterCompContent.setText(compString);

        dismiss();
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        closeRealm();
    }

    private void closeRealm() {
        if (!mRealm.isClosed())
            mRealm.close();
    }

    @OnClick(R.id.button_bottom_sheet_filter_comp_close)
    protected void onClose() {
        dismiss();
    }
}
