package ph.net.swak.attendanceinsights.ui.fragments.appraisal;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import org.json.JSONArray;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.models.AppraisalDetail;
import ph.net.swak.attendanceinsights.models.RealmIndividualAppraisal;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.utils.JSONParser;

/**
 * A simple {@link Fragment} subclass.
 */
public class IndividualAppraisalFragment extends Fragment {

    @BindView(R.id.progressbar_individual_appraisal)
    protected MaterialProgressBar progressBar;

    @BindView(R.id.progressbar_individual_appraisal_text)
    protected TextView progressBarText;

    @BindView(R.id.recyclerview_individual_appraisal)
    protected RecyclerView recyclerView;

    private Unbinder unbinder;
    private MainActivity mainActivity;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private Realm mRealm;
    private FastItemAdapter<RealmIndividualAppraisal> fastItemAdapter;

    private final String TAG = "IndividualAppraisalFragment";

    public IndividualAppraisalFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_individual_appraisal, container, false);
        unbinder = ButterKnife.bind(this, view);
        requestQueue = VolleyAPI.getInstance(getActivity()).getRequestQueue();
        volleyRequest = new VolleyRequest();
        mainActivity = (MainActivity) getActivity();
        mRealm = Realm.getDefaultInstance();

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mainActivity, DividerItemDecoration.VERTICAL);

        fastItemAdapter = new FastItemAdapter<>();
        fastItemAdapter.withSelectable(true);
        fastItemAdapter.withMultiSelect(true);
        fastItemAdapter.withSelectOnLongClick(true);
        fastItemAdapter.withPositionBasedStateManagement(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(mainActivity));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(fastItemAdapter);

        Bundle bundle = getArguments();

        if (bundle != null) {
            if (bundle.containsKey("empid")) {
                setupData(bundle.getString("empid"));
            } else {
                progressBar.setVisibility(View.GONE);
                progressBarText.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                progressBarText.setText("No data Found");
            }
        } else {
            progressBar.setVisibility(View.GONE);
            progressBarText.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            progressBarText.setText("No data Found");
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
        closeRealm();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(requestQueue != null)
            requestQueue.cancelAll(TAG);
    }

    public void closeRealm() {
        if (!mRealm.isClosed())
            mRealm.close();
    }

    private void setupData(String empid) {
        progressBar.setVisibility(View.VISIBLE);
        progressBarText.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        progressBarText.setText("Loading data");

        JsonArrayRequest jsonArrayRequest = volleyRequest.getAuthJsonArray(mainActivity.getApplicationContext(), TAG, volleyRequest.getIndividualAppraisal(empid), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                final List<RealmIndividualAppraisal> realmIndividualAppraisals = JSONParser.parseIndividualAppraisal(response);

                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.delete(RealmIndividualAppraisal.class);

                        for (RealmIndividualAppraisal realmIndividualAppraisal : realmIndividualAppraisals) {
                            realm.copyToRealm(realmIndividualAppraisal);
                        }
                    }
                });

                RealmResults<RealmIndividualAppraisal> results = mRealm.where(RealmIndividualAppraisal.class).distinct("appId");

                List<RealmIndividualAppraisal> appraiseList = new ArrayList<>();

                for (RealmIndividualAppraisal realmIndividualAppraisal : results) {

                    List<AppraisalDetail> iItems = new LinkedList<>();

                    RealmResults<RealmIndividualAppraisal> appraisal_details =
                            mRealm.where(RealmIndividualAppraisal.class)
                                    .equalTo("appId", realmIndividualAppraisal.getAppId()).findAllSorted("catMainId", Sort.ASCENDING);

                    if (appraisal_details.size() > 0) {

                        boolean headerSet = false;
                        boolean bodySet = false;
                        boolean footerSet = false;

                        for (int i = 0; appraisal_details.size() > i; i++) {
                            RealmIndividualAppraisal details = appraisal_details.get(i);
                            DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
                            StringBuilder builder;

                            if(details.getCatMainId() == 1) {
                                if(!headerSet) {
                                    builder = new StringBuilder();
                                    builder.append(decimalFormat.format(details.getCatMainRate()));
                                    builder.append("% of ");
                                    builder.append(decimalFormat.format(details.getCatMainWeight()));
                                    builder.append("% = ");
                                    builder.append(decimalFormat.format(details.getCatMainScore()));
                                    builder.append("%");

                                    iItems.add(new AppraisalDetail().withHeader(true)
                                            .withTitle("Business Level")
                                            .withInfo(builder.toString())
                                            .withIdentifier(2000 + i));
                                    headerSet = true;
                                }

                                builder = new StringBuilder();
                                builder.append(decimalFormat.format(details.getCatSubRate()));
                                builder.append("% of ");
                                builder.append(decimalFormat.format(details.getCatSubWeight()));
                                builder.append("% = ");
                                builder.append(decimalFormat.format(details.getCatSubScore()));
                                builder.append("%");

                                iItems.add(new AppraisalDetail().withHeader(false)
                                        .withTitle(details.getCatSubDesc())
                                        .withSubtitle(builder.toString())
                                        .withInfo(details.getSubRatingName())
                                        .withIdentifier(1000 + i));

                            } else if(details.getCatMainId() == 2) {
                                if(!bodySet) {
                                    builder = new StringBuilder();
                                    builder.append(decimalFormat.format(details.getCatMainRate()));
                                    builder.append("% of ");
                                    builder.append(decimalFormat.format(details.getCatMainWeight()));
                                    builder.append("% = ");
                                    builder.append(decimalFormat.format(details.getCatMainScore()));
                                    builder.append("%");

                                    iItems.add(new AppraisalDetail().withHeader(true)
                                            .withTitle("Department Level")
                                            .withInfo(builder.toString())
                                            .withIdentifier(3000 + i));
                                    bodySet = true;
                                }

                                builder = new StringBuilder();
                                builder.append(decimalFormat.format(details.getCatSubRate()));
                                builder.append("% of ");
                                builder.append(decimalFormat.format(details.getCatSubWeight()));
                                builder.append("% = ");
                                builder.append(decimalFormat.format(details.getCatSubScore()));
                                builder.append("%");

                                iItems.add(new AppraisalDetail().withHeader(false)
                                        .withTitle(details.getCatSubDesc())
                                        .withSubtitle(builder.toString())
                                        .withInfo(details.getSubRatingName())
                                        .withIdentifier(2000 + i));

                            } else if(details.getCatMainId() == 3) {
                                if(!footerSet) {
                                    builder = new StringBuilder();
                                    builder.append(decimalFormat.format(details.getCatMainRate()));
                                    builder.append("% of ");
                                    builder.append(decimalFormat.format(details.getCatMainWeight()));
                                    builder.append("% = ");
                                    builder.append(decimalFormat.format(details.getCatMainScore()));
                                    builder.append("%");

                                    iItems.add(new AppraisalDetail().withHeader(true)
                                            .withTitle("KPI / Goal")
                                            .withInfo(builder.toString())
                                            .withIdentifier(4000 + i));
                                    footerSet = true;
                                }

                                builder = new StringBuilder();
                                builder.append(decimalFormat.format(details.getCatSubRate()));
                                builder.append("% of ");
                                builder.append(decimalFormat.format(details.getCatSubWeight()));
                                builder.append("% = ");
                                builder.append(decimalFormat.format(details.getCatSubScore()));
                                builder.append("%");

                                iItems.add(new AppraisalDetail().withHeader(false)
                                        .withTitle(details.getCatSubDesc())
                                        .withSubtitle(builder.toString())
                                        .withInfo(details.getSubRatingName())
                                        .withIdentifier(2000 + i));
                            }
                        }

                        realmIndividualAppraisal.withSubItems(iItems);
                    }

                    appraiseList.add(realmIndividualAppraisal);
                }

                if (appraiseList.size() < 1) {
                    progressBar.setVisibility(View.GONE);
                    progressBarText.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    progressBarText.setText("No data Found");
                    return;
                }

                progressBar.setVisibility(View.GONE);
                progressBarText.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

                fastItemAdapter.setNewList(appraiseList);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                progressBarText.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                progressBarText.setText("No data Found");

            }
        });

        requestQueue.add(jsonArrayRequest);
    }

}
