package ph.net.swak.attendanceinsights.ui.fragments.lifecycle;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.ui.views.pageradapter.HrisDashboardPagerAdapter;

public class HrisDashboardMainFragment extends Fragment {

    @BindView(R.id.viewpager_hris_dashboard_main)
    protected ViewPager viewPager;

    private Unbinder unbinder;
    private MainActivity mainActivity;

    public HrisDashboardMainFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hris_dashboard_main, container, false);
        unbinder = ButterKnife.bind(this, view);
        mainActivity = (MainActivity) getActivity();
        mainActivity.setupUI("Company Details", false, false, true);

        Bundle bundle = getArguments();
        if(bundle != null) {
            viewPager.setAdapter(new HrisDashboardPagerAdapter(getChildFragmentManager(), bundle));
            mainActivity.mTabLayout.setupWithViewPager(viewPager);
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
