package ph.net.swak.attendanceinsights.network;

import android.content.Context;

import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;

public class TokenRetryPolicy implements RetryPolicy {
    /** The current timeout in milliseconds. */
    private int mCurrentTimeoutMs;

    /** The current retry count. */
    private int mCurrentRetryCount;

    /** The maximum number of attempts. */
    private final int mMaxNumRetries;

    /** The backoff multiplier for the policy. */
    private final float mBackoffMultiplier;

    private Context mContext;

    /** The default socket timeout in milliseconds */
    public static final int DEFAULT_TIMEOUT_MS = 20000;

    /** The default number of retries */
    public static final int DEFAULT_MAX_RETRIES = 6;

    /** The default backoff multiplier */
    public static final float DEFAULT_BACKOFF_MULT = 1f;

    public TokenRetryPolicy(Context appContext, int initialTimeoutMs, int maxNumRetries, float backoffMultiplier) {
        mContext = appContext;
        mCurrentTimeoutMs = initialTimeoutMs;
        mMaxNumRetries = maxNumRetries;
        mBackoffMultiplier = backoffMultiplier;
    }

    @Override
    public int getCurrentTimeout() {
        return mCurrentTimeoutMs;
    }

    @Override
    public int getCurrentRetryCount() {
        return mCurrentRetryCount;
    }

    public float getBackoffMultiplier() {
        return mBackoffMultiplier;
    }

    @Override
    public void retry(VolleyError error) throws VolleyError {
        mCurrentRetryCount++;
        mCurrentTimeoutMs += (mCurrentTimeoutMs * mBackoffMultiplier);

        if (!hasAttemptRemaining()) {
            throw error;
        }

        if(error.networkResponse != null) {
            switch (error.networkResponse.statusCode) {
                case VolleyRequest.UNAUTHORIZED:
                    VolleyRequest volleyRequest = new VolleyRequest();
                    VolleyAPI.getInstance(mContext).addToRequestQueue(volleyRequest.getOAuthToken(mContext));
                    break;
            }
        }
    }

    protected boolean hasAttemptRemaining() {
        return mCurrentRetryCount <= mMaxNumRetries;
    }
}
