package ph.net.swak.attendanceinsights.ui.fragments.payroll;

import android.os.Bundle;
import android.support.transition.TransitionManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.events.FilterClickEvent;
import ph.net.swak.attendanceinsights.models.PayrollDetail;
import ph.net.swak.attendanceinsights.models.RealmFilterComp;
import ph.net.swak.attendanceinsights.models.RealmFilterYear;
import ph.net.swak.attendanceinsights.models.RealmPayroll;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.utils.DateUtil;
import ph.net.swak.attendanceinsights.utils.JSONParser;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class PayrollDashboardFragment extends Fragment {

    @BindView(R.id.progressbar_payroll_textual)
    protected MaterialProgressBar spinner;

    @BindView(R.id.progressbar_payroll_textual_text)
    protected TextView spinnerText;

    @BindView(R.id.container_payroll_textual)
    protected NestedScrollView container;

    @BindView(R.id.recyclerview_payroll_quarterly)
    protected RecyclerView recyclerQuarterly;

    @BindView(R.id.recyclerview_payroll_monthly)
    protected RecyclerView recyclerMonthly;

    @BindView(R.id.relativeLayout_payroll_dashboard_container)
    protected RelativeLayout mainContainer;

    @BindView(R.id.relativelayout_payroll_monthly)
    protected RelativeLayout monthlyContainer;

    @BindView(R.id.relativelayout_payroll_quarterly)
    protected RelativeLayout quarterlyContainer;

    private FastItemAdapter<PayrollDetail> fastAdapterGross;
    private FastItemAdapter<PayrollDetail> fastAdapterQuarterly;

    private Unbinder unbinder;
    private MainActivity mainActivity;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;

    private final String TAG = "PayrollDashboardFragment";
    private Realm mRealm;
    private DecimalFormat dF = new DecimalFormat("###,##0.00");

    public PayrollDashboardFragment() {}

    private static final int
            PAYROLL_MONTHLY_ITEM = 1,
            PAYROLL_QUARTERLY_ITEM = 2;

    private int selectedItem = PAYROLL_MONTHLY_ITEM;

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_payroll_textual, container, false);

        unbinder = ButterKnife.bind(this, rootView);
        requestQueue = VolleyAPI.getInstance(getActivity()).getRequestQueue();
        volleyRequest = new VolleyRequest();
        mainActivity = (MainActivity) getActivity();

        List<Integer> filterOption = new ArrayList<>();
        filterOption.add(MainActivity.FILTER_YEAR);
        filterOption.add(MainActivity.FILTER_COMP);
        mainActivity.setupUI("Payroll", false, true, filterOption, false, null);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mainActivity, DividerItemDecoration.VERTICAL);

        fastAdapterGross = new FastItemAdapter<>();
        fastAdapterGross.withSelectable(true);
        recyclerMonthly.addItemDecoration(dividerItemDecoration);
        recyclerMonthly.setLayoutManager(new LinearLayoutManager(mainActivity));
        recyclerMonthly.setItemAnimator(new DefaultItemAnimator());
        recyclerMonthly.setAdapter(fastAdapterGross);

        fastAdapterQuarterly = new FastItemAdapter<>();
        fastAdapterQuarterly.withSelectable(true);
        recyclerQuarterly.addItemDecoration(dividerItemDecoration);
        recyclerQuarterly.setLayoutManager(new LinearLayoutManager(mainActivity));
        recyclerQuarterly.setItemAnimator(new DefaultItemAnimator());
        recyclerQuarterly.setAdapter(fastAdapterQuarterly);

        mRealm = Realm.getDefaultInstance();

        RealmResults<RealmFilterYear> yearResult = mRealm.where(RealmFilterYear.class).findAll();
        RealmResults<RealmFilterComp> compAllResult = mRealm.where(RealmFilterComp.class).findAll();
        RealmResults<RealmFilterComp> compSelectedResult = mRealm.where(RealmFilterComp.class).equalTo("mSelected", true).findAll();

        String compString = "All Companies";

        if (compSelectedResult.where().count() == 0) {
            compString = "No Company is Selected";
        } else if (compSelectedResult.where().count() == 1) {
            if (compAllResult.where().count() > 1)
                compString = compSelectedResult.where().findFirst().getTitle();
        } else if (compSelectedResult.where().count() > 1) {
            if (compAllResult.where().count() == compSelectedResult.where().count())
                compString = "All Companies";
            else if (compAllResult.where().count() > compSelectedResult.where().count())
                compString = String.valueOf(compSelectedResult.where().count()) + " Companies";
        }

        mainActivity.mFilterHeadYear.setText(String.valueOf(yearResult.max("mIdentifier").intValue()));
        mainActivity.mFilterYearContent.setText(String.valueOf(yearResult.max("mIdentifier").intValue()));
        mainActivity.mFilterHeadComp.setText(compString);
        mainActivity.mFilterCompContent.setText(compString);

        // OnClickListeners
        mainActivity.mFilterHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onFilterClick();
            }
        });
        mainActivity.mFilterDisplayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onFilterClick();
                GetChartData();
            }
        });
        mainActivity.mFilterYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new FilterClickEvent("year"));
            }
        });
        mainActivity.mFilterComp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new FilterClickEvent("comp"));
            }
        });

        GetChartData();
        return rootView;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (requestQueue != null)
            requestQueue.cancelAll(TAG);
    }

    protected void onFilterClick() {
        TransitionManager.beginDelayedTransition(mainActivity.appBarLayout);
        ViewGroup.LayoutParams params = mainActivity.mFilter.getLayoutParams();
        if (params.height == convertPx(48)) {
            mainActivity.mFilterHeadYear.setVisibility(GONE);
            mainActivity.mFilterHeadComp.setVisibility(GONE);
            mainActivity.mFilterHeadArrow.setImageDrawable(ContextCompat.getDrawable(mainActivity, R.drawable.ic_close_black_48dp));
            mainActivity.mFilterYear.setVisibility(VISIBLE);
            mainActivity.mFilterRange.setVisibility(GONE);
            mainActivity.mFilterComp.setVisibility(VISIBLE);
            mainActivity.mFilterDisplay.setVisibility(VISIBLE);
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        } else {
            mainActivity.mFilterHeadYear.setVisibility(VISIBLE);
            mainActivity.mFilterHeadComp.setVisibility(VISIBLE);
            mainActivity.mFilterHeadArrow.setImageDrawable(ContextCompat.getDrawable(mainActivity, R.drawable.ic_keyboard_arrow_down_black_48dp));
            mainActivity.mFilterYear.setVisibility(GONE);
            mainActivity.mFilterRange.setVisibility(GONE);
            mainActivity.mFilterComp.setVisibility(GONE);
            mainActivity.mFilterDisplay.setVisibility(GONE);
            params.height = convertPx(48);
        }

        mainActivity.mFilter.setLayoutParams(params);
    }

    private int convertPx(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    @OnClick(R.id.relativelayout_payroll_monthly)
    protected void onMonthlyPayrollClick() {
        TransitionManager.beginDelayedTransition(container);

        if (selectedItem == PAYROLL_MONTHLY_ITEM) {
            monthlyContainer.setBackgroundColor(ContextCompat.getColor(mainActivity, android.R.color.transparent));
            recyclerMonthly.setVisibility(GONE);
            selectedItem = 0;
        } else {
            monthlyContainer.setBackgroundColor(ContextCompat.getColor(mainActivity, R.color.md_white_1000));
            quarterlyContainer.setBackgroundColor(ContextCompat.getColor(mainActivity, android.R.color.transparent));
            recyclerMonthly.setVisibility(VISIBLE);
            recyclerQuarterly.setVisibility(GONE);
            selectedItem = PAYROLL_MONTHLY_ITEM;
        }
    }

    @OnClick(R.id.relativelayout_payroll_quarterly)
    protected void onQuaterlyPayrollClick() {
        TransitionManager.beginDelayedTransition(container);

        if (selectedItem == PAYROLL_QUARTERLY_ITEM) {
            quarterlyContainer.setBackgroundColor(ContextCompat.getColor(mainActivity, android.R.color.transparent));
            recyclerQuarterly.setVisibility(GONE);
            selectedItem = 0;
        } else {
            quarterlyContainer.setBackgroundColor(ContextCompat.getColor(mainActivity, R.color.md_white_1000));
            monthlyContainer.setBackgroundColor(ContextCompat.getColor(mainActivity, android.R.color.transparent));
            recyclerQuarterly.setVisibility(VISIBLE);
            recyclerMonthly.setVisibility(GONE);
            selectedItem = PAYROLL_QUARTERLY_ITEM;
        }
    }

    private void GetChartData() {
        final String selectedYear = mainActivity.mFilterYearContent.getText().toString();

        spinner.setVisibility(VISIBLE);
        spinnerText.setVisibility(VISIBLE);
        container.setVisibility(GONE);
        spinnerText.setText("Loading Data");

        JsonArrayRequest jsonArrayRequest = volleyRequest.getAuthJsonArray(mainActivity.getApplicationContext(),TAG, volleyRequest.getPayrollOne(selectedYear), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                final List<RealmPayroll> realmPayrolls = JSONParser.parsePayrollOne(response);

                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.delete(RealmPayroll.class);

                        for (RealmPayroll realmPayroll : realmPayrolls) {
                            realm.copyToRealmOrUpdate(realmPayroll);
                        }
                    }
                });

                RealmResults<RealmFilterComp> compList = mRealm.where(RealmFilterComp.class).equalTo("mSelected", true).findAll();
                Long[] selectedComp = new Long[compList.size()];
                for (int i = 0; i < compList.size(); i++) {
                    selectedComp[i] = compList.get(i).getIdentifier();
                }

                RealmResults<RealmPayroll> monthResults = mRealm.where(RealmPayroll.class).in("compId", selectedComp).distinct("covMonth").sort("covMonth");
                List<PayrollDetail> payrollDetailList = new ArrayList<>();

                double tempQuarter1 = 0, tempQuarter2 = 0, tempQuarter3 = 0, tempQuarter4 = 0;
                String[] monthList = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

                DateUtil dateUtil = new DateUtil();

                for (RealmPayroll realmPayroll : monthResults) {
                    double totalGross = mRealm.where(RealmPayroll.class).equalTo("covMonth", realmPayroll.getCovMonth()).in("compId", selectedComp).sum("totalGross").doubleValue();
                    int totalEmployee = mRealm.where(RealmPayroll.class).equalTo("covMonth", realmPayroll.getCovMonth()).in("compId", selectedComp).distinct("empId").size();

                    PayrollDetail payrollDetail = new PayrollDetail();
                    payrollDetail.setTitle(monthList[realmPayroll.getCovMonth() - 1]);
                    payrollDetail.setInfo("Gross: Php " + dF.format(totalGross));
                    payrollDetail.setSubinfo("Total: " + totalEmployee + " Employees");
                    payrollDetail.setBundleYear(Integer.parseInt(selectedYear));
                    payrollDetail.setBundleMonth(realmPayroll.getCovMonth());
                    payrollDetail.withPayrollType("monthly");
                    payrollDetailList.add(payrollDetail);

                    switch (realmPayroll.getCovMonth()) {
                        case 1:
                        case 2:
                        case 3:
                            tempQuarter1 += mRealm.where(RealmPayroll.class).equalTo("covMonth", realmPayroll.getCovMonth()).in("compId", selectedComp).sum("totalGross").doubleValue();
                            break;

                        case 4:
                        case 5:
                        case 6:
                            tempQuarter2 += mRealm.where(RealmPayroll.class).equalTo("covMonth", realmPayroll.getCovMonth()).in("compId", selectedComp).sum("totalGross").doubleValue();
                            break;

                        case 7:
                        case 8:
                        case 9:
                            tempQuarter3 += mRealm.where(RealmPayroll.class).equalTo("covMonth", realmPayroll.getCovMonth()).in("compId", selectedComp).sum("totalGross").doubleValue();
                            break;

                        case 10:
                        case 11:
                        case 12:
                            tempQuarter4 += mRealm.where(RealmPayroll.class).equalTo("covMonth", realmPayroll.getCovMonth()).in("compId", selectedComp).sum("totalGross").doubleValue();
                            break;
                    }
                }

                List<PayrollDetail> quarterlyList = new ArrayList<>();

                PayrollDetail payrollDetail1 = new PayrollDetail();
                PayrollDetail payrollDetail2 = new PayrollDetail();
                PayrollDetail payrollDetail3 = new PayrollDetail();
                PayrollDetail payrollDetail4 = new PayrollDetail();

                payrollDetail1.setTitle("Quarter 1");
                payrollDetail2.setTitle("Quarter 2");
                payrollDetail3.setTitle("Quarter 3");
                payrollDetail4.setTitle("Quarter 4");

                payrollDetail1.withQuarter(1);
                payrollDetail2.withQuarter(2);
                payrollDetail3.withQuarter(3);
                payrollDetail4.withQuarter(4);

                payrollDetail1.setInfo("Gross: Php " + dF.format(tempQuarter1));
                payrollDetail2.setInfo("Gross: Php " + dF.format(tempQuarter2));
                payrollDetail3.setInfo("Gross: Php " + dF.format(tempQuarter3));
                payrollDetail4.setInfo("Gross: Php " + dF.format(tempQuarter4));

                payrollDetail1.setSubinfo("Total: " + mRealm.where(RealmPayroll.class).in("covMonth", new Integer[]{1,2,3}).in("compId", selectedComp).distinct("empId").size() + " Employees");
                payrollDetail2.setSubinfo("Total: " + mRealm.where(RealmPayroll.class).in("covMonth", new Integer[]{4,5,6}).in("compId", selectedComp).distinct("empId").size() + " Employees");
                payrollDetail3.setSubinfo("Total: " + mRealm.where(RealmPayroll.class).in("covMonth", new Integer[]{7,8,9}).in("compId", selectedComp).distinct("empId").size() + " Employees");
                payrollDetail4.setSubinfo("Total: " + mRealm.where(RealmPayroll.class).in("covMonth", new Integer[]{10,11,12}).in("compId", selectedComp).distinct("empId").size() + " Employees");

                payrollDetail1.setBundleYear(Integer.parseInt(selectedYear));
                payrollDetail2.setBundleYear(Integer.parseInt(selectedYear));
                payrollDetail3.setBundleYear(Integer.parseInt(selectedYear));
                payrollDetail4.setBundleYear(Integer.parseInt(selectedYear));

                payrollDetail1.withQuarter(1);
                payrollDetail2.withQuarter(2);
                payrollDetail3.withQuarter(3);
                payrollDetail4.withQuarter(4);

                payrollDetail1.withPayrollType("quarterly");
                payrollDetail2.withPayrollType("quarterly");
                payrollDetail3.withPayrollType("quarterly");
                payrollDetail4.withPayrollType("quarterly");

                quarterlyList.add(payrollDetail1);
                quarterlyList.add(payrollDetail2);
                quarterlyList.add(payrollDetail3);
                quarterlyList.add(payrollDetail4);

                fastAdapterGross.setNewList(payrollDetailList);
                fastAdapterQuarterly.setNewList(quarterlyList);

                spinner.setVisibility(GONE);
                spinnerText.setVisibility(GONE);
                container.setVisibility(VISIBLE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                spinner.setVisibility(GONE);
                spinnerText.setVisibility(VISIBLE);
                container.setVisibility(GONE);
                spinnerText.setText("No Data Found");
            }
        });

        requestQueue.add(jsonArrayRequest);
    }
}
