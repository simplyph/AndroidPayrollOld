package ph.net.swak.attendanceinsights.utils;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Locale;

public class Helper {

    public static String toTitleCase(String entry) {
        final String DELIMITERS = " ";

        StringBuilder sb = new StringBuilder();
        boolean capNext = true;

        for(char c : entry.toCharArray()) {
            c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);

            sb.append(c);
            capNext = (DELIMITERS.indexOf((int) c) >= 0);
        }

        return sb.toString();
    }

    public static String toOrdinal(int toBeConverted) {

        if((toBeConverted % 10) == 1) {
            return toBeConverted + "st";
        } else if((toBeConverted % 10) == 2) {
            return toBeConverted + "nd";
        } else if((toBeConverted % 10) == 3) {
            return toBeConverted + "rd";
        } else {
            return toBeConverted + "th";
        }
    }

    public static int getCurrentYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }
    public static String toStringMonth(int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, month);
        return calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH);
    }

    public static String toDecimal(double value) {
        DecimalFormat decimalFormat = new DecimalFormat("###,##0.00");
        return decimalFormat.format(value);
    }

}
