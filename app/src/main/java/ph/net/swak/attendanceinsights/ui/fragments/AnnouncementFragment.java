package ph.net.swak.attendanceinsights.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.ui.fragments.appraisal.AppraisalDashboardFragment;
import ph.net.swak.attendanceinsights.ui.fragments.attendance.AttendanceDashboardFragment;
import ph.net.swak.attendanceinsights.ui.fragments.hris.HrisDashboardFragment;
import ph.net.swak.attendanceinsights.ui.fragments.payroll.PayrollDashboardFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class AnnouncementFragment extends Fragment {

    private Unbinder unbinder;
    private MainActivity mainActivity;

    public AnnouncementFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_announcement, container, false);
        unbinder = ButterKnife.bind(this, view);
        mainActivity = (MainActivity) getActivity();

        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @OnClick({R.id.imageview_hris, R.id.textview_hris})
    protected void onHrisClick() {
        HrisDashboardFragment fragment = new HrisDashboardFragment();
        mainActivity.getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, fragment).commit();
    }

    @OnClick({R.id.imageview_attendance, R.id.textview_attendance})
    protected void onAttendanceClick() {
        AttendanceDashboardFragment fragment = new AttendanceDashboardFragment();
        mainActivity.getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, fragment).commit();
    }

    @OnClick({R.id.imageview_payroll, R.id.textview_payroll})
    protected void onPayrollClick() {
        PayrollDashboardFragment fragment = new PayrollDashboardFragment();
        mainActivity.getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, fragment).commit();
    }

    @OnClick({R.id.imageview_appraisal, R.id.textview_appraisal})
    protected void onAppraisalClick() {
        AppraisalDashboardFragment fragment = new AppraisalDashboardFragment();
        mainActivity.getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, fragment).commit();
    }

}
