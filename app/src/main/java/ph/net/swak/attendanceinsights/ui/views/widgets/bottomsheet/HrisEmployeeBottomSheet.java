package ph.net.swak.attendanceinsights.ui.views.widgets.bottomsheet;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IItemAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.listeners.ClickEventHook;
import com.mikepenz.fastadapter.listeners.ItemFilterListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.events.UserDetailEvent;
import ph.net.swak.attendanceinsights.models.BottomSheetEmp;
import ph.net.swak.attendanceinsights.models.RealmHrisDashboard;
import ph.net.swak.attendanceinsights.utils.DateUtil;

public class HrisEmployeeBottomSheet extends BottomSheetDialogFragment {

    @BindView(R.id.recyclerview)
    protected RecyclerView recyclerView;

    @BindView(R.id.spinner_bottom_sheet_sort)
    protected Spinner spinner;

    @BindView(R.id.text_search_result)
    protected TextView textResult;

    @BindView(R.id.textview_bottom_sheet_title)
    protected TextView textTitle;

    private RealmResults<RealmHrisDashboard> realmResults = null;
    private FastItemAdapter<BottomSheetEmp> fastItemAdapter;
    private Unbinder unbinder;
    private Realm mRealm;
    private String category = "";
    private String type = "";
    private String year = "";
    private String sortedBy = "Alphabetically";

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View view = View.inflate(getContext(), R.layout.bottom_sheet_hris_employee, null);
        unbinder = ButterKnife.bind(this, view);
        mRealm = Realm.getDefaultInstance();

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);

        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(getContext(), R.array.hris_sort_selection, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);

        Bundle bundle = getArguments();

        Calendar calendar = Calendar.getInstance();

        if (bundle != null) {
            category = bundle.getString("category", "");
            type = bundle.getString("type", "");
            year = bundle.getString("year", String.valueOf(calendar.get(Calendar.YEAR)));
        } else {
            category = "";
            type = "";
            year = String.valueOf(calendar.get(Calendar.YEAR));
        }

        if (category.equals("") || type.equals("")) {
            textResult.setText("0 result. Sorted by " + sortedBy);
            textTitle.setText("Statistics");
            dialog.setContentView(view);
            return;
        } else {
            if (category.equalsIgnoreCase("gender"))
                textTitle.setText("Population by Gender");
            else if (category.equalsIgnoreCase("age"))
                textTitle.setText("Population by Age");
            else if (category.equalsIgnoreCase("dept"))
                textTitle.setText("Population by Department");
            else if (category.equalsIgnoreCase("stat"))
                textTitle.setText("Population by Status");
            else if (category.equalsIgnoreCase("hiring"))
                textTitle.setText("Hiring Statistics");
            else if (category.equalsIgnoreCase("renounce"))
                textTitle.setText("Renounciation Statistics");
        }

        fastItemAdapter = new FastItemAdapter<>();
        fastItemAdapter.getItemFilter().withFilterPredicate(new IItemAdapter.Predicate<BottomSheetEmp>() {
            @Override
            public boolean filter(BottomSheetEmp item, CharSequence constraint) {
                return !item.getName().toLowerCase().contains(String.valueOf(constraint).toLowerCase())
                        && !item.getCompany().toLowerCase().contains(String.valueOf(constraint).toLowerCase())
                        && !item.getDesignation().toLowerCase().contains(String.valueOf(constraint).toLowerCase());
            }
        });

        fastItemAdapter.getItemFilter().withItemFilterListener(new ItemFilterListener<BottomSheetEmp>() {
            @Override
            public void itemsFiltered(@Nullable CharSequence charSequence, @Nullable List<BottomSheetEmp> list) {
                textResult.setText(fastItemAdapter.getItemCount() + (fastItemAdapter.getItemCount() < 2 ? " Result" : " Results") + ". Sorted " + sortedBy);
            }

            @Override
            public void onReset() {

            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(fastItemAdapter);

        getData();

        dialog.setContentView(view);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @OnClick(R.id.button_bottom_sheet_close)
    protected void onCloseClick() {
        dismiss();
    }

    @OnTextChanged(R.id.textlayout_search_query)
    protected void onQuerySearch(CharSequence charSequence) {
        fastItemAdapter.filter(charSequence);
    }

    @OnItemSelected(R.id.spinner_bottom_sheet_sort)
    protected void onItemSpinnerSelected(Spinner spinner, int position) {
        if (spinner.getSelectedItem().toString().equalsIgnoreCase("a-z")) {
            sortedBy = "Alphabetically";
            getData();
        } else if (spinner.getSelectedItem().toString().equalsIgnoreCase("type")) {
            sortedBy = "Type";
            getData();
        } else if (spinner.getSelectedItem().toString().equalsIgnoreCase("company")) {
            sortedBy = "Company";
            getData();
        }
    }

    private void getData() {
        switch (category) {
            case "gender":
                if (type.equalsIgnoreCase("male"))
                    if (sortedBy.equalsIgnoreCase("alphabetically"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("gender", "male", Case.INSENSITIVE).findAllSorted("nameLast", Sort.ASCENDING);
                    else if (sortedBy.equalsIgnoreCase("type"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("gender", "male", Case.INSENSITIVE).findAllSorted("gender", Sort.ASCENDING);
                    else if (sortedBy.equalsIgnoreCase("company"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("gender", "male", Case.INSENSITIVE).findAllSorted("comp", Sort.ASCENDING);
                    else
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("gender", "male", Case.INSENSITIVE).findAll();
                else if (type.equalsIgnoreCase("female"))
                    if (sortedBy.equalsIgnoreCase("alphabetically"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("gender", "female", Case.INSENSITIVE).findAllSorted("nameLast", Sort.ASCENDING);
                    else if (sortedBy.equalsIgnoreCase("type"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("gender", "female", Case.INSENSITIVE).findAllSorted("gender", Sort.ASCENDING);
                    else if (sortedBy.equalsIgnoreCase("company"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("gender", "female", Case.INSENSITIVE).findAllSorted("comp", Sort.ASCENDING);
                    else
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("gender", "female", Case.INSENSITIVE).findAll();
                break;
            case "age":
                if (type.equalsIgnoreCase(("Age below 17").toLowerCase()))
                    if (sortedBy.equalsIgnoreCase("alphabetically"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).lessThanOrEqualTo("age", 17).findAllSorted("nameLast", Sort.ASCENDING);
                    else if (sortedBy.equalsIgnoreCase("type"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).lessThanOrEqualTo("age", 17).findAllSorted("age", Sort.ASCENDING);
                    else if (sortedBy.equalsIgnoreCase("company"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).lessThanOrEqualTo("age", 17).findAllSorted("comp", Sort.ASCENDING);
                    else
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).lessThanOrEqualTo("age", 17).findAll();
                else if (type.equalsIgnoreCase(("Age of 18 to 25").toLowerCase()))
                    if (sortedBy.equalsIgnoreCase("alphabetically"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).between("age", 18, 25).findAllSorted("nameLast", Sort.ASCENDING);
                    else if (sortedBy.equalsIgnoreCase("type"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).between("age", 18, 25).findAllSorted("age", Sort.ASCENDING);
                    else if (sortedBy.equalsIgnoreCase("company"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).between("age", 18, 25).findAllSorted("comp", Sort.ASCENDING);
                    else
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).between("age", 18, 25).findAll();
                else if (type.equalsIgnoreCase(("Age of 26 to 39").toLowerCase()))
                    if (sortedBy.equalsIgnoreCase("alphabetically"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).between("age", 26, 39).findAllSorted("nameLast", Sort.ASCENDING);
                    else if (sortedBy.equalsIgnoreCase("type"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).between("age", 26, 39).findAllSorted("age", Sort.ASCENDING);
                    else if (sortedBy.equalsIgnoreCase("company"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).between("age", 26, 39).findAllSorted("comp", Sort.ASCENDING);
                    else
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).between("age", 26, 39).findAll();
                else if (type.equalsIgnoreCase(("Age of 40 to 59").toLowerCase()))
                    if (sortedBy.equalsIgnoreCase("alphabetically"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).between("age", 40, 59).findAllSorted("nameLast", Sort.ASCENDING);
                    else if (sortedBy.equalsIgnoreCase("type"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).between("age", 40, 59).findAllSorted("age", Sort.ASCENDING);
                    else if (sortedBy.equalsIgnoreCase("company"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).between("age", 40, 59).findAllSorted("comp", Sort.ASCENDING);
                    else
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).between("age", 40, 59).findAll();
                else if (type.equalsIgnoreCase(("Age above 60").toLowerCase()))
                    if (sortedBy.equalsIgnoreCase("alphabetically"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).greaterThanOrEqualTo("age", 60).findAllSorted("nameLast", Sort.ASCENDING);
                    else if (sortedBy.equalsIgnoreCase("type"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).greaterThanOrEqualTo("age", 60).findAllSorted("age", Sort.ASCENDING);
                    else if (sortedBy.equalsIgnoreCase("company"))
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).greaterThanOrEqualTo("age", 60).findAllSorted("comp", Sort.ASCENDING);
                    else
                        realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).greaterThanOrEqualTo("age", 60).findAll();
                break;
            case "dept":
                if (sortedBy.equalsIgnoreCase("alphabetically"))
                    realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("dept", type.toLowerCase(), Case.INSENSITIVE).findAllSorted("nameLast", Sort.ASCENDING);
                else if (sortedBy.equalsIgnoreCase("type"))
                    realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("dept", type.toLowerCase(), Case.INSENSITIVE).findAllSorted("dept", Sort.ASCENDING);
                else if (sortedBy.equalsIgnoreCase("company"))
                    realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("dept", type.toLowerCase(), Case.INSENSITIVE).findAllSorted("comp", Sort.ASCENDING);
                else
                    realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("dept", type.toLowerCase(), Case.INSENSITIVE).findAll();
                break;
            case "stat":
                if (sortedBy.equalsIgnoreCase("alphabetically"))
                    realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("statusDesc", type.toLowerCase(), Case.INSENSITIVE).findAllSorted("nameLast", Sort.ASCENDING);
                else if (sortedBy.equalsIgnoreCase("type"))
                    realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("statusDesc", type.toLowerCase(), Case.INSENSITIVE).findAllSorted("statusDesc", Sort.ASCENDING);
                else if (sortedBy.equalsIgnoreCase("company"))
                    realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("statusDesc", type.toLowerCase(), Case.INSENSITIVE).findAllSorted("comp", Sort.ASCENDING);
                else
                    realmResults = mRealm.where(RealmHrisDashboard.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("statusDesc", type.toLowerCase(), Case.INSENSITIVE).findAll();
                break;
            case "hiring":
                String filter = "1/*/" + year + "*";

                if(type.equalsIgnoreCase("january"))
                    filter = "1/*/" + year + "*";
                else if(type.equalsIgnoreCase("february"))
                    filter = "2/*/" + year + "*";
                else if(type.equalsIgnoreCase("march"))
                    filter = "3/*/" + year + "*";
                else if(type.equalsIgnoreCase("april"))
                    filter = "4/*/" + year + "*";
                else if(type.equalsIgnoreCase("may"))
                    filter = "5/*/" + year + "*";
                else if(type.equalsIgnoreCase("june"))
                    filter = "6/*/" + year + "*";
                else if(type.equalsIgnoreCase("july"))
                    filter = "7/*/" + year + "*";
                else if(type.equalsIgnoreCase("august"))
                    filter = "8/*/" + year + "*";
                else if(type.equalsIgnoreCase("september"))
                    filter = "9/*/" + year + "*";
                else if(type.equalsIgnoreCase("october"))
                    filter = "10/*/" + year + "*";
                else if(type.equalsIgnoreCase("november"))
                    filter = "11/*/" + year + "*";
                else if(type.equalsIgnoreCase("december"))
                    filter = "12/*/" + year + "*";

                if (sortedBy.equalsIgnoreCase("alphabetically"))
                    realmResults = mRealm.where(RealmHrisDashboard.class).like("dateHired", filter).findAllSorted("nameLast", Sort.ASCENDING);
                else if (sortedBy.equalsIgnoreCase("type"))
                    realmResults = mRealm.where(RealmHrisDashboard.class).like("dateHired", filter).findAllSorted("hiredDate", Sort.ASCENDING);
                else if (sortedBy.equalsIgnoreCase("company"))
                    realmResults = mRealm.where(RealmHrisDashboard.class).like("dateHired", filter).findAllSorted("comp", Sort.ASCENDING);
                else
                    realmResults = mRealm.where(RealmHrisDashboard.class).like("dateHired", filter).findAll();
                break;
            case "renounce":
                String filter1 = year + "-01-??*";

                if(type.equalsIgnoreCase("january"))
                    filter1 = year + "-01-??*";
                else if(type.equalsIgnoreCase("february"))
                    filter1 = year + "-02-??*";
                else if(type.equalsIgnoreCase("march"))
                    filter1 = year + "-03-??*";
                else if(type.equalsIgnoreCase("april"))
                    filter1 = year + "-04-??*";
                else if(type.equalsIgnoreCase("may"))
                    filter1 = year + "-05-??*";
                else if(type.equalsIgnoreCase("june"))
                    filter1 = year + "-06-??*";
                else if(type.equalsIgnoreCase("july"))
                    filter1 = year + "-07-??*";
                else if(type.equalsIgnoreCase("august"))
                    filter1 = year + "-08-??*";
                else if(type.equalsIgnoreCase("september"))
                    filter1 = year + "-09-??*";
                else if(type.equalsIgnoreCase("october"))
                    filter1 = year + "-10-??*";
                else if(type.equalsIgnoreCase("november"))
                    filter1 = year + "-11-??*";
                else if(type.equalsIgnoreCase("december"))
                    filter1 = year + "-12-??*";

                if (sortedBy.equalsIgnoreCase("alphabetically"))
                    realmResults = mRealm.where(RealmHrisDashboard.class).notEqualTo("statusType", "active", Case.INSENSITIVE).like("dateTerminated", filter1).findAllSorted("nameLast", Sort.ASCENDING);
                else if (sortedBy.equalsIgnoreCase("type"))
                    realmResults = mRealm.where(RealmHrisDashboard.class).notEqualTo("statusType", "active", Case.INSENSITIVE).like("dateTerminated", filter1).findAllSorted("renounceDate", Sort.ASCENDING);
                else if (sortedBy.equalsIgnoreCase("company"))
                    realmResults = mRealm.where(RealmHrisDashboard.class).notEqualTo("statusType", "active", Case.INSENSITIVE).like("dateTerminated", filter1).findAllSorted("comp", Sort.ASCENDING);
                else
                    realmResults = mRealm.where(RealmHrisDashboard.class).notEqualTo("statusType", "active", Case.INSENSITIVE).like("dateTerminated", filter1).findAll();
                break;
        }

        if (realmResults == null) {
            textResult.setText("0 result. Sorted by " + sortedBy);
            return;
        } else if (realmResults.size() < 1) {
            textResult.setText("0 result. Sorted by " + sortedBy);
            return;
        }

        List<BottomSheetEmp> bottomSheetEmps = new ArrayList<>();

        for (RealmHrisDashboard realmHrisDashboard : realmResults) {
            BottomSheetEmp bottomSheetEmp = new BottomSheetEmp();
            bottomSheetEmp
                    .withName(realmHrisDashboard.getNameLast() + ", " + realmHrisDashboard.getNameFirst())
                    .withEmpId(realmHrisDashboard.getEmpId())
                    .withCompany(realmHrisDashboard.getComp())
                    .withDesignation(realmHrisDashboard.getDesig());

            DateUtil dateUtil = new DateUtil();

            if (category.equalsIgnoreCase("age"))
                bottomSheetEmp.withHasAge(true).withAge(realmHrisDashboard.getAge());

            if (category.equalsIgnoreCase("hiring"))
                bottomSheetEmp.withHasDate(true)
                        .withDateLabel("Hired Date:")
                        .withDate(dateUtil.formatDate(realmHrisDashboard.getDateHired(), "M/d/y h:mm:ss a", "MMM d, yyyy"));

            if (category.equalsIgnoreCase("renounce"))
                bottomSheetEmp.withHasDate(true)
                        .withDateLabel("Renounce Date:")
                        .withDate(dateUtil.formatDate(realmHrisDashboard.getDateTerminated(), "yyyy-MM-dd H:mm:ss", "MMM d, yyyy"));

            bottomSheetEmps.add(bottomSheetEmp);
        }


        fastItemAdapter.withItemEvent(new ClickEventHook<BottomSheetEmp>() {
            @Nullable
            @Override
            public View onBind(@NonNull RecyclerView.ViewHolder viewHolder) {
                if (viewHolder instanceof BottomSheetEmp.ViewHolder) {
                    return ((BottomSheetEmp.ViewHolder) viewHolder).itemView;
                }
                return null;
            }

            @Override
            public void onClick(View v, int position, FastAdapter<BottomSheetEmp> fastAdapter, BottomSheetEmp item) {
                dismiss();
                EventBus.getDefault().post(new UserDetailEvent(String.valueOf(item.getEmpId())));
            }
        });

        textResult.setText(bottomSheetEmps.size() + (bottomSheetEmps.size() < 2 ? " Result" : " Results") + ". Sorted by " + sortedBy);
        fastItemAdapter.setNewList(bottomSheetEmps);
    }
}
