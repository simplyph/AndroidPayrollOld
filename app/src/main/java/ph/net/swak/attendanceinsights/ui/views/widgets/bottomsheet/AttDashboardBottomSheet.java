package ph.net.swak.attendanceinsights.ui.views.widgets.bottomsheet;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IItemAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.listeners.ClickEventHook;
import com.mikepenz.fastadapter.listeners.ItemFilterListener;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.events.UserDetailEvent;
import ph.net.swak.attendanceinsights.models.RealmAttendanceNow;
import ph.net.swak.attendanceinsights.ui.views.adapters.AttStatsFastAdapter;
import ph.net.swak.attendanceinsights.ui.views.adapters.AttendanceDashboardFastAdapter;

public class AttDashboardBottomSheet extends BottomSheetDialogFragment {

    @BindView(R.id.recyclerview)
    protected RecyclerView recyclerView;

    @BindView(R.id.spinner_bottom_sheet_sort)
    protected Spinner spinner;

    @BindView(R.id.text_search_result)
    protected TextView textResult;

    @BindView(R.id.textview_bottom_sheet_title)
    protected TextView textTitle;

    private Unbinder unbinder;
    private Realm mRealm;
    private FastItemAdapter<AttendanceDashboardFastAdapter> fastItemAdapter1;
    private FastItemAdapter<AttStatsFastAdapter> fastItemAdapter2;
    private RealmResults<RealmAttendanceNow> realmResults = null;
    private String category = "";
    private String type = "";
    private String year = "";
    private String sortedBy = "Alphabetically";


    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View view = View.inflate(getContext(), R.layout.bottom_sheet_att_dashboard, null);
        unbinder = ButterKnife.bind(this, view);
        mRealm = Realm.getDefaultInstance();

        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(getContext(), R.array.hris_sort_selection, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);

        fastItemAdapter1 = new FastItemAdapter<>();
        fastItemAdapter2 = new FastItemAdapter<>();

        Bundle bundle = getArguments();

        if (bundle != null) {
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.addItemDecoration(dividerItemDecoration);
            recyclerView.setItemAnimator(new DefaultItemAnimator());

            if (bundle.getString("category", "").equalsIgnoreCase("today")) {
                spinner.setVisibility(View.VISIBLE);
                category = "today";
                if (bundle.getString("type", "").equalsIgnoreCase("today_total")) {
                    textTitle.setText("Total Employment");
                    type = "total";
                } else if (bundle.getString("type", "").equalsIgnoreCase("today_late")) {
                    textTitle.setText("Late Employees");
                    type = "late";
                } else if (bundle.getString("type", "").equalsIgnoreCase("today_absent")) {
                    textTitle.setText("No Time-in Yet Employees");
                    type = "absent";
                }

                fastItemAdapter1.getItemFilter().withFilterPredicate(new IItemAdapter.Predicate<AttendanceDashboardFastAdapter>() {
                    @Override
                    public boolean filter(AttendanceDashboardFastAdapter item, CharSequence constraint) {
                        return !(item.getLastName() + ", " + item.getFirstName()).toLowerCase().contains(String.valueOf(constraint).toLowerCase())
                                && !item.getCompName().toLowerCase().contains(String.valueOf(constraint).toLowerCase())
                                && !item.getDesigName().toLowerCase().contains(String.valueOf(constraint).toLowerCase());
                    }
                });
                fastItemAdapter1.withEventHook(new ClickEventHook<AttendanceDashboardFastAdapter>() {
                    @Nullable
                    @Override
                    public View onBind(@NonNull RecyclerView.ViewHolder viewHolder) {
                        if (viewHolder instanceof AttendanceDashboardFastAdapter.ViewHolder) {
                            return ((AttendanceDashboardFastAdapter.ViewHolder) viewHolder).itemView;
                        }
                        return null;
                    }

                    @Override
                    public void onClick(View v, int position, FastAdapter<AttendanceDashboardFastAdapter> fastAdapter, AttendanceDashboardFastAdapter item) {
                        dismiss();
                        EventBus.getDefault().post(new UserDetailEvent(String.valueOf(item.getEmpId())));
                    }
                });

                fastItemAdapter1.getItemFilter().withItemFilterListener(new ItemFilterListener<AttendanceDashboardFastAdapter>() {
                    @Override
                    public void itemsFiltered(@Nullable CharSequence charSequence, @Nullable List<AttendanceDashboardFastAdapter> list) {
                        textResult.setText(fastItemAdapter1.getItemCount() + (fastItemAdapter1.getItemCount() < 2 ? " Result" : " Results") + ". Sorted by " + sortedBy);
                    }
                    @Override
                    public void onReset() {

                    }
                });

                recyclerView.setAdapter(fastItemAdapter1);
                Log.e("ASASASA", type);
                getToday();
            } else if (bundle.getString("category", "").equalsIgnoreCase("monthly")) {
                spinner.setVisibility(View.GONE);
                category = "monthly";
                if (bundle.getString("type", "").equalsIgnoreCase("monthly_late")) {
                    textTitle.setText("Top Lates");
                    type = "late";
                } else if (bundle.getString("type", "").equalsIgnoreCase("monthly_absent")) {
                    textTitle.setText("Top Absent");
                    type = "absent";
                } else if (bundle.getString("type", "").equalsIgnoreCase("monthly_perfect")) {
                    textTitle.setText("Perfect Attendance");
                    type = "perfect";
                }

                fastItemAdapter2.getItemFilter().withFilterPredicate(new IItemAdapter.Predicate<AttStatsFastAdapter>() {
                    @Override
                    public boolean filter(AttStatsFastAdapter item, CharSequence constraint) {
                        return !item.getName().toLowerCase().contains(String.valueOf(constraint).toLowerCase())
                                && !item.getCompName().toLowerCase().contains(String.valueOf(constraint).toLowerCase())
                                && !item.getDesigName().toLowerCase().contains(String.valueOf(constraint).toLowerCase());
                    }
                });
                fastItemAdapter2.withEventHook(new ClickEventHook<AttStatsFastAdapter>() {
                    @Nullable
                    @Override
                    public View onBind(@NonNull RecyclerView.ViewHolder viewHolder) {
                        if (viewHolder instanceof AttStatsFastAdapter.ViewHolder) {
                            return ((AttStatsFastAdapter.ViewHolder) viewHolder).itemView;
                        }
                        return null;
                    }

                    @Override
                    public void onClick(View v, int position, FastAdapter<AttStatsFastAdapter> fastAdapter, AttStatsFastAdapter item) {
                        dismiss();
                        EventBus.getDefault().post(new UserDetailEvent(String.valueOf(item.getEmpId())));
                    }
                });

                fastItemAdapter2.getItemFilter().withItemFilterListener(new ItemFilterListener<AttStatsFastAdapter>() {
                    @Override
                    public void itemsFiltered(@Nullable CharSequence charSequence, @Nullable List<AttStatsFastAdapter> list) {
                        textResult.setText(fastItemAdapter2.getItemCount() + (fastItemAdapter2.getItemCount() < 2 ? " Result" : " Results"));
                    }

                    @Override
                    public void onReset() {

                    }
                });

                recyclerView.setAdapter(fastItemAdapter2);
                Log.e("ASASASA", type);
                getMonthly();

            }
        }


        dialog.setContentView(view);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @OnTextChanged(R.id.textlayout_search_query)
    protected void onQuerySearch(CharSequence charSequence) {
        if (category != null)
            if (category.equalsIgnoreCase("today"))
                fastItemAdapter1.filter(charSequence);
            else if (category.equalsIgnoreCase("monthly"))
                fastItemAdapter2.filter(charSequence);
    }

    @OnClick(R.id.button_bottom_sheet_close)
    protected void onCloseClick() {
        dismiss();
    }

    @OnItemSelected(R.id.spinner_bottom_sheet_sort)
    protected void onItemSpinnerSelected(Spinner spinner, int position) {
        if (spinner.getSelectedItem().toString().equalsIgnoreCase("a-z")) {
            sortedBy = "Alphabetically";
            getToday();
        } else if (spinner.getSelectedItem().toString().equalsIgnoreCase("type")) {
            sortedBy = "Type";
            getToday();
        } else if (spinner.getSelectedItem().toString().equalsIgnoreCase("company")) {
            sortedBy = "Company";
            getToday();
        }
    }

    private void getToday() {

        switch (type) {
            case "total":
                if (sortedBy.equalsIgnoreCase("alphabetically"))
                    realmResults = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).findAllSorted("lastName", Sort.ASCENDING);
                else if (sortedBy.equalsIgnoreCase("type"))
                    realmResults = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).findAllSorted("isPresent", Sort.ASCENDING, "lateToday", Sort.DESCENDING);
                else if (sortedBy.equalsIgnoreCase("company"))
                    realmResults = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).findAllSorted("compName", Sort.ASCENDING, "lastName", Sort.ASCENDING);
                else
                    realmResults = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).findAllSorted("lastName", Sort.ASCENDING);
                break;
            case "late":
                if (sortedBy.equalsIgnoreCase("alphabetically"))
                    realmResults = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).greaterThan("lateToday", 0d).findAllSorted("lastName", Sort.ASCENDING);
                else if (sortedBy.equalsIgnoreCase("type"))
                    realmResults = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).greaterThan("lateToday", 0d).findAllSorted("lateToday", Sort.DESCENDING);
                else if (sortedBy.equalsIgnoreCase("company"))
                    realmResults = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).greaterThan("lateToday", 0d).findAllSorted("compName", Sort.ASCENDING, "lastName", Sort.ASCENDING);
                else
                    realmResults = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).greaterThan("lateToday", 0d).findAllSorted("lastName", Sort.ASCENDING);
                break;
            case "absent":
                if (sortedBy.equalsIgnoreCase("alphabetically"))
                    realmResults = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("isPresent", false).findAllSorted("lastName", Sort.ASCENDING);
                else if (sortedBy.equalsIgnoreCase("type"))
                    realmResults = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("isPresent", false).findAllSorted("lastName", Sort.ASCENDING);
                else if (sortedBy.equalsIgnoreCase("company"))
                    realmResults = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("isPresent", false).findAllSorted("compName", Sort.ASCENDING, "lastName", Sort.ASCENDING);
                else
                    realmResults = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("isPresent", false).findAllSorted("lastName", Sort.ASCENDING);
                break;
        }

        List<AttendanceDashboardFastAdapter> realmAttendanceNowList = new ArrayList<>();

        for (RealmAttendanceNow realmAttendanceNow : realmResults) {
            AttendanceDashboardFastAdapter data = new AttendanceDashboardFastAdapter();
            data.withEmpId(realmAttendanceNow.getEmpId())
                    .withFirstName(realmAttendanceNow.getFirstName())
                    .withLastName(realmAttendanceNow.getLastName())
                    .withCompName(realmAttendanceNow.getCompName())
                    .withDesigName(realmAttendanceNow.getDesigName())
                    .withHasSched(true)
                    .withSchedName(realmAttendanceNow.getSchedDesc());


            if (type.equalsIgnoreCase("total")) {
                if (!realmAttendanceNow.isPresent()) {
                    data.withHasAbsent(true).withHasLateToday(false);
                } else {
                    data.withHasAbsent(false).withHasLateToday(true)
                            .withLateToday(realmAttendanceNow.getLateToday());
                }

                data.withHasAbsent(!realmAttendanceNow.isPresent())
                        .withHasLateMonthly(true)
                        .withLateMonthName(Calendar.getInstance().getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()))
                        .withLateMonthValue(mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("empId", realmAttendanceNow.getEmpId()).greaterThan("lateToday", 0d).sum("lateToday").doubleValue());
            } else {
                if (type.equalsIgnoreCase("late"))
                    data.withHasLateMonthly(true)
                            .withLateMonthName(Calendar.getInstance().getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()))
                            .withLateMonthValue(mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("empId", realmAttendanceNow.getEmpId()).greaterThan("lateToday", 0d).sum("lateToday").doubleValue())
                            .withHasLateToday(true)
                            .withLateToday(realmAttendanceNow.getLateToday());
                else
                    data.withHasLateMonthly(false).withHasLateToday(false);

                if (type.equalsIgnoreCase("absent"))
                    data.withHasAbsent(true);
                else
                    data.withHasAbsent(false);
            }


            realmAttendanceNowList.add(data);
        }

        fastItemAdapter1.setNewList(realmAttendanceNowList);
        textResult.setText(fastItemAdapter1.getItemCount() + (fastItemAdapter1.getItemCount() < 2 ? " Result" : " Results") + ". Sorted by " + sortedBy);
    }

    private void getMonthly() {
        switch (type) {
            case "late":
                realmResults = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).findAllSorted("totalLate", Sort.DESCENDING, "lastName", Sort.DESCENDING);
                break;
            case "absent":
                realmResults = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).findAllSorted("totalAbsent", Sort.DESCENDING, "lastName", Sort.DESCENDING);
                break;
            case "perfect":
                realmResults = mRealm.where(RealmAttendanceNow.class).equalTo("statusType", "active", Case.INSENSITIVE).equalTo("totalLate", 0d).equalTo("totalAbsent", 0d).findAllSorted("lastName", Sort.ASCENDING);
                break;
        }

        List<AttStatsFastAdapter> realmAttendanceNowList = new ArrayList<>();

        int rank = 0;

        for (RealmAttendanceNow realmAttendanceNow : realmResults) {
            if (rank >= 20)
                if(!type.equalsIgnoreCase("perfect"))
                    break;

            if(type.equalsIgnoreCase("late"))
                if(realmAttendanceNow.getTotalLate() == 0)
                    break;

            if(type.equalsIgnoreCase("absent"))
                if(realmAttendanceNow.getTotalAbsent() == 0)
                    break;

            AttStatsFastAdapter data = new AttStatsFastAdapter();
            data.withEmpId(realmAttendanceNow.getEmpId())
                    .withName(realmAttendanceNow.getLastName() + ", " + realmAttendanceNow.getFirstName())
                    .withCompName(realmAttendanceNow.getCompName())
                    .withDesigName(realmAttendanceNow.getDesigName())
                    .withRank(++rank);

            DecimalFormat dF = new DecimalFormat("###,##0.00");

            if (type.equalsIgnoreCase("late"))
                data.withDetails(dF.format(realmAttendanceNow.getTotalLate()) + (realmAttendanceNow.getTotalLate() < 2 ? " Min" : " Mins"));

            if (type.equalsIgnoreCase("absent"))
                data.withDetails(dF.format(realmAttendanceNow.getTotalAbsent()) + (realmAttendanceNow.getTotalAbsent() < 2 ? " Day" : " Days"));


            realmAttendanceNowList.add(data);
        }

        fastItemAdapter2.setNewList(realmAttendanceNowList);
        textResult.setText(fastItemAdapter2.getItemCount() + (fastItemAdapter2.getItemCount() < 2 ? " Result" : " Results"));
    }
}
