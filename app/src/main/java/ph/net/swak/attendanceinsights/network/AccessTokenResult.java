package ph.net.swak.attendanceinsights.network;

import com.google.gson.annotations.SerializedName;

public class AccessTokenResult {

    @SerializedName("access_token")
    public String accessToken;
    @SerializedName("token_type")
    public String tokeType;
    @SerializedName("expires_in")
    public String expiresIn;
    @SerializedName("as:client_id")
    public String clientId;
    @SerializedName("userName")
    public String userName;
    @SerializedName(".issued")
    public String issued;
    @SerializedName(".expires")
    public String expires;

    public AccessTokenResult() {

    }

}
