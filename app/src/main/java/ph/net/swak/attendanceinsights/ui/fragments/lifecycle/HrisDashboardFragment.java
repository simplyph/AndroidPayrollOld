package ph.net.swak.attendanceinsights.ui.fragments.lifecycle;


import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.di.Injectable;
import ph.net.swak.attendanceinsights.repository.Status;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.HrisCompanyFastAdapter;
import ph.net.swak.attendanceinsights.viewmodel.HrisDashboardViewModel;

public class HrisDashboardFragment extends LifecycleFragment implements Injectable {

    @BindView(R.id.recyclerview_companies)
    protected RecyclerView recyclerView;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private MainActivity mainActivity;
    private HrisDashboardViewModel viewModel;
    private Unbinder unbinder;
    private FastItemAdapter<HrisCompanyFastAdapter> mFastItemAdapter;

    public HrisDashboardFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hris_dashboard2, container, false);
        unbinder = ButterKnife.bind(this, view);
        mainActivity = (MainActivity) getActivity();
        mainActivity.setupUI("Hris Dashboard", false, false, false);

        // Setup Adapter
        mFastItemAdapter = new FastItemAdapter<>();
        recyclerView.setAdapter(mFastItemAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HrisDashboardViewModel.class);

        viewModel.getEmployees().observe(this, listResource -> {
            if (listResource != null && listResource.status == Status.SUCCESS) {
                viewModel.getAllData();
            }
        });

        viewModel.getEmployeeByCompany().observe(this, listResource -> {
            if(listResource != null && listResource.data != null) {
                if(listResource.status == Status.SUCCESS) {
                    mFastItemAdapter.setNewList(listResource.data);
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
