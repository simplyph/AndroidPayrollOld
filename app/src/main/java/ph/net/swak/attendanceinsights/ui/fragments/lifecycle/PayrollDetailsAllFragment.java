package ph.net.swak.attendanceinsights.ui.fragments.lifecycle;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ph.net.swak.attendanceinsights.R;

public class PayrollDetailsAllFragment extends Fragment {


    public PayrollDetailsAllFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payroll_details_all, container, false);
    }

}
