package ph.net.swak.attendanceinsights.repository;

public enum  Status {
    SUCCESS,
    ERROR,
    LOADING
}
