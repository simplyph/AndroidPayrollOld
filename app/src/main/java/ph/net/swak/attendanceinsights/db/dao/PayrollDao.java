package ph.net.swak.attendanceinsights.db.dao;

import android.arch.lifecycle.LiveData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import ph.net.swak.attendanceinsights.db.realm.LiveRealmData;
import ph.net.swak.attendanceinsights.db.realm.RealmEmployee;
import ph.net.swak.attendanceinsights.db.realm.RealmPayrollHead;
import ph.net.swak.attendanceinsights.db.realm.RealmPayrollQuick;
import ph.net.swak.attendanceinsights.di.scope.ApplicationScope;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.PayrollDetailFastAdapter;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.PayrollDashboardFastAdapter;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.PayrollEmployeeFastAdapter;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.PayrollMonthFastAdapter;
import ph.net.swak.attendanceinsights.utils.PayrollPeriodType;

import static ph.net.swak.attendanceinsights.utils.RealmUtils.asLiveData;


public class PayrollDao {

    private Realm mRealm;

    public PayrollDao() {
        mRealm = Realm.getDefaultInstance();
    }

    public LiveRealmData<RealmPayrollHead> getPayrollHead(int year) {
        return asLiveData(mRealm.where(RealmPayrollHead.class)
                .equalTo("covYear", year)
                .findAllAsync());
    }

    public LiveRealmData<RealmPayrollQuick> getPayrollQuick() {
        return asLiveData(mRealm.where(RealmPayrollQuick.class)
                .findAllAsync());
    }

    public LiveData<List<PayrollDashboardFastAdapter>> getPayrollMonthly(int year, Long[] companyId) {
        RealmResults<RealmPayrollHead> results = mRealm.where(RealmPayrollHead.class)
                .equalTo("covYear", year)
                .in("compId", companyId)
                .distinct("compId").sort("compName", Sort.ASCENDING);

        List<PayrollDashboardFastAdapter> payrollCompanies = new ArrayList<>();

        payrollCompanies.add(new PayrollDashboardFastAdapter()
                .withCompName("Total Payroll")
                .withCompId(0)
                .withCountEmployee(mRealm.where(RealmPayrollHead.class)
                        .equalTo("covYear", year)
                        .distinct("employeeId").size())
                .withCountManager(mRealm.where(RealmPayrollHead.class)
                        .equalTo("covYear", year)
                        .equalTo("isManager", true)
                        .distinct("employeeId").size())
                .withCountSupervisor(mRealm.where(RealmPayrollHead.class)
                        .equalTo("covYear", year)
                        .equalTo("isSupervisor", true)
                        .distinct("employeeId").size())
                .withCountStaff(mRealm.where(RealmPayrollHead.class)
                        .equalTo("covYear", year)
                        .equalTo("isStaff", true)
                        .distinct("employeeId").size())
                .withValueEmployee(mRealm.where(RealmPayrollHead.class)
                        .equalTo("covYear", year)
                        .sum("totalGross").doubleValue())
                .withValueManager(mRealm.where(RealmPayrollHead.class)
                        .equalTo("covYear", year)
                        .equalTo("isManager", true)
                        .sum("totalGross").doubleValue())
                .withValueSupervisor(mRealm.where(RealmPayrollHead.class)
                        .equalTo("covYear", year)
                        .equalTo("isSupervisor", true)
                        .sum("totalGross").doubleValue())
                .withValueStaff(mRealm.where(RealmPayrollHead.class)
                        .equalTo("covYear", year)
                        .equalTo("isStaff", true)
                        .sum("totalGross").doubleValue())
                .withFrom(0)
                .withFromId(0)
                .withYear(year)
        );

        for (RealmPayrollHead realmPayrollHead : results) {
            payrollCompanies.add(new PayrollDashboardFastAdapter()
                    .withCompName(realmPayrollHead.getCompName())
                    .withCompId(realmPayrollHead.getCompId())
                    .withCountEmployee(mRealm.where(RealmPayrollHead.class)
                            .equalTo("covYear", year)
                            .equalTo("compId", realmPayrollHead.getCompId())
                            .distinct("employeeId").size())
                    .withCountManager(mRealm.where(RealmPayrollHead.class)
                            .equalTo("covYear", year)
                            .equalTo("compId", realmPayrollHead.getCompId())
                            .equalTo("isManager", true)
                            .distinct("employeeId").size())
                    .withCountSupervisor(mRealm.where(RealmPayrollHead.class)
                            .equalTo("covYear", year)
                            .equalTo("compId", realmPayrollHead.getCompId())
                            .equalTo("isSupervisor", true)
                            .distinct("employeeId").size())
                    .withCountStaff(mRealm.where(RealmPayrollHead.class)
                            .equalTo("covYear", year)
                            .equalTo("compId", realmPayrollHead.getCompId())
                            .equalTo("isStaff", true)
                            .distinct("employeeId").size())
                    .withValueEmployee(mRealm.where(RealmPayrollHead.class)
                            .equalTo("covYear", year)
                            .equalTo("compId", realmPayrollHead.getCompId())
                            .sum("totalGross").doubleValue())
                    .withValueManager(mRealm.where(RealmPayrollHead.class)
                            .equalTo("covYear", year)
                            .equalTo("compId", realmPayrollHead.getCompId())
                            .equalTo("isManager", true)
                            .sum("totalGross").doubleValue())
                    .withValueSupervisor(mRealm.where(RealmPayrollHead.class)
                            .equalTo("covYear", year)
                            .equalTo("compId", realmPayrollHead.getCompId())
                            .equalTo("isSupervisor", true)
                            .sum("totalGross").doubleValue())
                    .withValueStaff(mRealm.where(RealmPayrollHead.class)
                            .equalTo("covYear", year)
                            .equalTo("compId", realmPayrollHead.getCompId())
                            .equalTo("isStaff", true)
                            .sum("totalGross").doubleValue())
                    .withFrom(0)
                    .withFromId(0)
                    .withYear(year)
            );
        }

        return new LiveData<List<PayrollDashboardFastAdapter>>() {
            @Override
            protected void onActive() {
                super.onActive();
                postValue(payrollCompanies);
            }
        };

    }

    public LiveData<List<PayrollEmployeeFastAdapter>> getPayrollByCompAndPos(int year, int compId, int posId, int from, int fromId) {
        RealmQuery<RealmPayrollHead> query = mRealm.where(RealmPayrollHead.class)
                .equalTo("covYear", year);

        switch (posId) {
            case 3:
                query.equalTo("isManager", true);
                break;
            case 2:
                query.equalTo("isSupervisor", true);
                break;
            case 1:
            default:
                query.equalTo("isStaff", true);
                break;

        }


        if (compId != 0)
            query.equalTo("compId", compId);

        if (from == 2) {
            query.equalTo("deptId", fromId);
        } else if (from == 1) {
            query.equalTo("divId", fromId);
        }

        RealmResults<RealmPayrollHead> realmPayrollHeads = query.distinct("employeeId").sort("employeeId", Sort.ASCENDING);

        List<PayrollEmployeeFastAdapter> list = new ArrayList<>();

        for(RealmPayrollHead realmPayrollHead : realmPayrollHeads) {
            list.add(new PayrollEmployeeFastAdapter()
                    .withEmpId(realmPayrollHead.getEmployeeId())
                    .withValue(mRealm.where(RealmPayrollHead.class)
                            .equalTo("covYear", year)
                            .equalTo("employeeId", realmPayrollHead.getEmployeeId())
                            .sum("totalGross").doubleValue()
                    )
                    .withDesignation(realmPayrollHead.getDesigName())
                    .withfName(realmPayrollHead.getfName())
                    .withlName(realmPayrollHead.getlName())
            );
        }

        return new LiveData<List<PayrollEmployeeFastAdapter>>() {
            @Override
            protected void onActive() {
                super.onActive();
                postValue(list);
            }
        };
    }

    public LiveData<List<PayrollDetailFastAdapter>> getPayrollByCompAndType(int covYear, int covMonth, int compId, int posId, int payrollPeriod) {
        RealmQuery<RealmPayrollHead> query = mRealm.where(RealmPayrollHead.class)
                .equalTo("covYear", covYear);

        if(payrollPeriod == PayrollPeriodType.MONTHLY.getOrder()) {
            query.equalTo("covMonth", covMonth);
        } else {
            query.in("covMonth", new Integer[]{1+((covMonth-1)*3), 2+((covMonth-1)*3), 3+((covMonth-1)*3)});
        }

        if(compId != 0)
            query.equalTo("compId", compId);

        switch (posId) {
            case 3:
                query.equalTo("isManager", true);
                break;
            case 2:
                query.equalTo("isSupervisor", true);
                break;
            case 1:
                query.equalTo("isStaff", true);
                break;
            case 0:
            default:
                break;
        }



        RealmResults<RealmPayrollHead> results = query.distinct("employeeId")
                .sort("lName", Sort.ASCENDING, "fName", Sort.ASCENDING);

        List<PayrollDetailFastAdapter> payrollDetailFastAdapters = new ArrayList<>();

        for (RealmPayrollHead realmPayrollHead : results) {
            RealmQuery<RealmPayrollHead> query2 = mRealm.where(RealmPayrollHead.class)
                    .equalTo("employeeId", realmPayrollHead.getEmployeeId())
                    .equalTo("covYear", covYear);

            if(compId != 0)
                query2.equalTo("compId", compId);

            if(payrollPeriod == PayrollPeriodType.MONTHLY.getOrder()) {
                query2.equalTo("covMonth", covMonth);
            } else {
                query2.in("covMonth", new Integer[]{1+((covMonth-1)*3), 2+((covMonth-1)*3), 3+((covMonth-1)*3)});
            }

            payrollDetailFastAdapters.add(new PayrollDetailFastAdapter()
                    .withEmpId(realmPayrollHead.getEmployeeId())
                    .withfName(realmPayrollHead.getfName())
                    .withlName(realmPayrollHead.getlName())
                    .withDesig(realmPayrollHead.getDesigName())
                    .withValue(query2.sum("totalGross").doubleValue())
            );
        }

        return new LiveData<List<PayrollDetailFastAdapter>>() {
            @Override
            protected void onActive() {
                super.onActive();
                postValue(payrollDetailFastAdapters);
            }
        };
    }

    public LiveData<List<PayrollMonthFastAdapter>> getPayrollByMonth(int year, int compId, int from, int fromId) {
        RealmQuery<RealmPayrollHead> query = mRealm.where(RealmPayrollHead.class)
                .equalTo("covYear", year);

        if(compId != 0)
            query.equalTo("compId", compId);

        RealmResults<RealmPayrollHead> results = query.distinct("covMonth").sort("covMonth", Sort.DESCENDING);

        List<PayrollMonthFastAdapter> list = new ArrayList<>();

        if(compId != 0) {
            for(RealmPayrollHead realmPayrollHead : results) {
                list.add(new PayrollMonthFastAdapter()
                        .withCompId(realmPayrollHead.getCompId())
                        .withCompName(realmPayrollHead.getCompName())
                        .withValue(mRealm.where(RealmPayrollHead.class)
                                .equalTo("covYear", year)
                                .equalTo("compId", compId)
                                .equalTo("covMonth", realmPayrollHead.getCovMonth())
                                .sum("totalGross").doubleValue()
                        )
                        .withCount(mRealm.where(RealmPayrollHead.class)
                                .equalTo("covYear", year)
                                .equalTo("compId", compId)
                                .equalTo("covMonth", realmPayrollHead.getCovMonth())
                                .distinct("employeeId").size()
                        )
                        .withMonth(realmPayrollHead.getCovMonth())
                );
            }
        } else {
            for(RealmPayrollHead realmPayrollHead : results) {
                list.add(new PayrollMonthFastAdapter()
                        .withCompId(0)
                        .withCompName("All Companies")
                        .withValue(mRealm.where(RealmPayrollHead.class)
                                .equalTo("covYear", year)
                                .equalTo("covMonth", realmPayrollHead.getCovMonth())
                                .sum("totalGross").doubleValue()
                        )
                        .withCount(mRealm.where(RealmPayrollHead.class)
                                .equalTo("covYear", year)
                                .equalTo("covMonth", realmPayrollHead.getCovMonth())
                                .distinct("employeeId").size()
                        )
                        .withMonth(realmPayrollHead.getCovMonth())
                );
            }
        }

        return new LiveData<List<PayrollMonthFastAdapter>>() {
            @Override
            protected void onActive() {
                super.onActive();
                postValue(list);
            }
        };
    }

    public LiveData<List<PayrollMonthFastAdapter>> getPayrollByQuarter(int year, int compId, int from, int fromId) {
        RealmQuery<RealmPayrollHead> query = mRealm.where(RealmPayrollHead.class)
                .equalTo("covYear", year);

        if(compId != 0)
            query.equalTo("compId", compId);

        RealmResults<RealmPayrollHead> results = query.distinct("covMonth").sort("covMonth", Sort.DESCENDING);

        List<PayrollMonthFastAdapter> list = new ArrayList<>();

        if(compId != 0) {
            for(int i = 0; i < 4; i++) {
                list.add(new PayrollMonthFastAdapter()
                        .withMonth(1+i)
                        .withCompId(compId)
                        .withCompName(results.get(0).getCompName())
                        .withValue(mRealm.where(RealmPayrollHead.class)
                                .equalTo("covYear", year)
                                .in("covMonth", new Integer[]{(1+(i*3)),(2+(i*3)),(3+(i*3))})
                                .equalTo("compId", compId)
                                .sum("totalGross").doubleValue()
                        )
                        .withCount(mRealm.where(RealmPayrollHead.class)
                                .equalTo("covYear", year)
                                .in("covMonth", new Integer[]{(1+(i*3)),(2+(i*3)),(3+(i*3))})
                                .equalTo("compId", compId)
                                .distinct("employeeId").size()
                        )
                );
            }
        } else {
            for(int i = 0; i < 4; i++) {
                list.add(new PayrollMonthFastAdapter()
                        .withMonth(1+i)
                        .withCompId(0)
                        .withCompName("All Companies")
                        .withValue(mRealm.where(RealmPayrollHead.class)
                                .equalTo("covYear", year)
                                .in("covMonth", new Integer[]{(1+(i*3)),(2+(i*3)),(3+(i*3))})
                                .sum("totalGross").doubleValue()
                        )
                        .withCount(mRealm.where(RealmPayrollHead.class)
                                .equalTo("covYear", year)
                                .in("covMonth", new Integer[]{(1+(i*3)),(2+(i*3)),(3+(i*3))})
                                .distinct("employeeId").size()
                        )
                );
            }
        }

        return new LiveData<List<PayrollMonthFastAdapter>>() {
            @Override
            protected void onActive() {
                super.onActive();
                postValue(list);
            }
        };
    }

    public void savePayrollHead(List<RealmPayrollHead> realmPayrollHeads) {
        mRealm.executeTransaction(realm -> {
            for (RealmPayrollHead realmPayrollHead : realmPayrollHeads) {
                if (realmPayrollHead.getAutoid() != -1) {
                    realm.copyToRealmOrUpdate(realmPayrollHead);
                }
            }
        });
    }

    public void savePayrollQuick(List<RealmPayrollQuick> quickList) {
        List<RealmPayrollQuick> newList = quickList;
        mRealm.executeTransaction(realm -> {
            realm.delete(RealmPayrollQuick.class);
            int autoid = 0;
            for (RealmPayrollQuick realmPayrollHead : newList) {

                if (realmPayrollHead.getCompId() != -1) {
                    realmPayrollHead.withAutoid(autoid++);
                    realm.copyToRealm(realmPayrollHead);
                }
            }
        });
    }
}
