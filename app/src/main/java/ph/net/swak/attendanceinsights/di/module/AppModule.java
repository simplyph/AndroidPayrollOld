package ph.net.swak.attendanceinsights.di.module;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;
import ph.net.swak.attendanceinsights.di.scope.ApplicationScope;
import ph.net.swak.attendanceinsights.network.PAService;
import ph.net.swak.attendanceinsights.network.TokenAuthenticator;
import ph.net.swak.attendanceinsights.utils.LiveDataCallAdapterFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = ViewModelModule.class)
public class AppModule {

    @Provides
    @ApplicationScope
    Context provideContext(Application application) {
        return application.getApplicationContext();
    }

    @Provides
    @ApplicationScope
    Realm provideRealm() {
        return Realm.getDefaultInstance();
    }

    @Provides
    @ApplicationScope
    TokenAuthenticator provideTokenAuthenticator(Application application) {
        return new TokenAuthenticator(application.getApplicationContext());
    }

    @Provides
    @ApplicationScope
    OkHttpClient provideOKHttpClient(TokenAuthenticator tokenAuthenticator) {
        Dispatcher dispatcher = new Dispatcher();
        dispatcher.setMaxRequests(1);

        return new OkHttpClient.Builder()
                .authenticator(tokenAuthenticator)
                .dispatcher(dispatcher)
                .build();
    }

    @Provides
    @ApplicationScope
    Gson provideGson() {
        return new GsonBuilder()
                .setDateFormat("MMM dd, YYYY")
                .registerTypeAdapter(Boolean.class, booleanAsIntAdapter)
                .registerTypeAdapter(boolean.class, booleanAsIntAdapter)
                .create();
    }

    @Provides
    @ApplicationScope
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl("http://mdrpayroll.swak.net.ph//")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @ApplicationScope
    PAService providePAService(Retrofit retrofit){
        return retrofit.create(PAService.class);

    }

    private static final TypeAdapter<Boolean> booleanAsIntAdapter = new TypeAdapter<Boolean>() {
        @Override
        public void write(JsonWriter out, Boolean value) throws IOException {
            if(value == null) {
                out.nullValue();
            } else {
                out.value(value);
            }
        }

        @Override
        public Boolean read(JsonReader in) throws IOException {
            JsonToken peek = in.peek();

            switch (peek) {
                case BOOLEAN:
                    return in.nextBoolean();

                case NULL:
                    in.nextNull();
                    return null;

                case NUMBER:
                    return in.nextInt() != 0;

                case STRING:
                    return in.nextString().equalsIgnoreCase("1");

                default:
                    throw new IllegalStateException("Expected BOOLEAN or NUMBER but was " + peek);
            }
        }
    };
}
