package ph.net.swak.attendanceinsights.events;

import java.util.ArrayList;

public class PayrollDetailEvent {

    private String year;
    private String month;
    private int compid;
    private String compName;

    private int managerList;
    private int supervisorList;
    private int staffList;

    private double managerGross;
    private double supervisorGross;
    private double staffGross;

    private String payrollType;

    private ArrayList<String> divList;

    public String getYear() {
        return year;
    }

    public String getMonth() {
        return month;
    }

    public int getCompid() {
        return compid;
    }

    public String getCompName() {
        return compName;
    }

    public int getManagerList() {
        return managerList;
    }

    public int getSupervisorList() {
        return supervisorList;
    }

    public int getStaffList() {
        return staffList;
    }

    public double getManagerGross() {
        return managerGross;
    }

    public double getSupervisorGross() {
        return supervisorGross;
    }

    public double getStaffGross() {
        return staffGross;
    }

    public ArrayList<String> getDivList() {
        return divList;
    }

    public String getPayrollType() {
        return payrollType;
    }

    public PayrollDetailEvent(String year, String month, int compid, String compName, int managerList, int supervisorList, int staffList, double managerGross, double supervisorGross, double staffGross, ArrayList<String> divList, String payrollType) {
        this.year = year;
        this.month = month;
        this.compid = compid;
        this.compName = compName;

        this.managerList = managerList;
        this.supervisorList = supervisorList;
        this.staffList = staffList;

        this.managerGross = managerGross;
        this.supervisorGross = supervisorGross;
        this.staffGross = staffGross;

        this.divList = divList;

        this.payrollType = payrollType;
    }
}
