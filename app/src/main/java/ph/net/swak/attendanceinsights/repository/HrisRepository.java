package ph.net.swak.attendanceinsights.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import ph.net.swak.attendanceinsights.db.realm.LiveRealmData;
import ph.net.swak.attendanceinsights.db.realm.RealmEmployee;
import ph.net.swak.attendanceinsights.db.realm.RealmHrisQuick;
import ph.net.swak.attendanceinsights.di.scope.ApplicationScope;
import ph.net.swak.attendanceinsights.network.ApiResponse;
import ph.net.swak.attendanceinsights.network.PAService;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.HrisCompanyDetailFastAdapter;
import ph.net.swak.attendanceinsights.ui.views.fastadapter.HrisCompanyFastAdapter;
import ph.net.swak.attendanceinsights.utils.AppExecutors;

import static ph.net.swak.attendanceinsights.utils.RealmUtils.employeeModel;

@ApplicationScope
public class HrisRepository {

    private final AppExecutors appExecutors;
    private final Realm mRealm;
    private final PAService paService;

    @Inject
    public HrisRepository(AppExecutors appExecutors, Realm realm, PAService paService) {
        this.appExecutors = appExecutors;
        this.mRealm = realm;
        this.paService = paService;

    }

    public LiveData<Resource<List<RealmEmployee>>> getEmployees() {
        return new NetworkBoundResource<List<RealmEmployee>, List<RealmEmployee>>(appExecutors) {
            @Override
            protected boolean shouldFetch(@Nullable List<RealmEmployee> data) {
                return data == null || data.size() == 0 || true;
            }

            @Override
            protected void saveCallResult(@NonNull List<RealmEmployee> item) {
                employeeModel(mRealm).saveEmployees(item);
            }

            @NonNull
            @Override
            protected LiveData<List<RealmEmployee>> loadFromDb() {
                LiveRealmData<RealmEmployee> employeeLiveRealmData = employeeModel(mRealm).getAllEmployees();

                return Transformations.map(employeeLiveRealmData, realmEmployees -> {
                    List<RealmEmployee> employeeList = new ArrayList<>();
                    employeeList.addAll(realmEmployees);
                    return employeeList;
                });
            }

            @Override
            protected LiveData<ApiResponse<List<RealmEmployee>>> createCall() {
                return paService.loadEmployees();
            }

            @Override
            protected void cancelTransaction() {

            }
        }.asLiveData();
    }

    public LiveData<Resource<List<RealmHrisQuick>>> getHrisQuick() {
        return new NetworkBoundResource<List<RealmHrisQuick>, List<RealmHrisQuick>>(appExecutors) {

            @Override
            protected boolean shouldFetch(@Nullable List<RealmHrisQuick> data) {
                return true;
            }

            @Override
            protected void saveCallResult(@NonNull List<RealmHrisQuick> item) {
                employeeModel(mRealm).saveHrisQuick(item);
            }

            @NonNull
            @Override
            protected LiveData<List<RealmHrisQuick>> loadFromDb() {
                LiveRealmData<RealmHrisQuick> hrisQuickLiveRealmData = employeeModel(mRealm).getHrisQuick();

                return Transformations.map(hrisQuickLiveRealmData, realmHrisQuicks -> {
                    List<RealmHrisQuick> hrisQuicks = new ArrayList<>();
                    hrisQuicks.addAll(realmHrisQuicks);
                    return hrisQuicks;
                });
            }

            @Override
            protected LiveData<ApiResponse<List<RealmHrisQuick>>> createCall() {
                return paService.loadHrisQuick();
            }

            @Override
            protected void cancelTransaction() {

            }
        }.asLiveData();
    }

    public LiveData<Resource<List<HrisCompanyFastAdapter>>> getEmployeesByCompany() {
        return new NetworkBoundResource<List<HrisCompanyFastAdapter>, List<HrisCompanyFastAdapter>>(appExecutors) {

            @Override
            protected boolean shouldFetch(@Nullable List<HrisCompanyFastAdapter> data) {
                return false;
            }

            @Override
            protected void saveCallResult(@NonNull List<HrisCompanyFastAdapter> item) {

            }

            @NonNull
            @Override
            protected LiveData<List<HrisCompanyFastAdapter>> loadFromDb() {
                return employeeModel(mRealm).getEmployeesByCompany();
            }

            @Override
            protected LiveData<ApiResponse<List<HrisCompanyFastAdapter>>> createCall() {
                return null;
            }

            @Override
            protected void cancelTransaction() {

            }
        }.asLiveData();
    }

    public LiveData<Resource<List<RealmEmployee>>> getEmployeesByCompAndPos(int compId, int posId, int from, int fromId) {
        return new NetworkBoundResource<List<RealmEmployee>, List<RealmEmployee>>(appExecutors) {

            @Override
            protected boolean shouldFetch(@Nullable List<RealmEmployee> data) {
                return false;
            }

            @Override
            protected void saveCallResult(@NonNull List<RealmEmployee> item) {

            }

            @NonNull
            @Override
            protected LiveData<List<RealmEmployee>> loadFromDb() {
                LiveRealmData<RealmEmployee> employeeLiveRealmData = employeeModel(mRealm).getEmployeesByCompAndPos(compId, posId, from, fromId);

                return Transformations.map(employeeLiveRealmData, realmEmployees -> {
                    List<RealmEmployee> employeeList = new ArrayList<>();
                    employeeList.addAll(realmEmployees);
                    return realmEmployees;
                });
            }

            @Override
            protected LiveData<ApiResponse<List<RealmEmployee>>> createCall() {
                return null;
            }

            @Override
            protected void cancelTransaction() {

            }
        }.asLiveData();
    }

    public LiveData<Resource<List<HrisCompanyDetailFastAdapter>>> getEmployeesByCompAndType(int compId, int type) {
        return new NetworkBoundResource<List<HrisCompanyDetailFastAdapter>, List<HrisCompanyDetailFastAdapter>>(appExecutors) {
            @Override
            protected boolean shouldFetch(@Nullable List<HrisCompanyDetailFastAdapter> data) {
                return false;
            }

            @Override
            protected void saveCallResult(@NonNull List<HrisCompanyDetailFastAdapter> item) {

            }

            @NonNull
            @Override
            protected LiveData<List<HrisCompanyDetailFastAdapter>> loadFromDb() {
                return employeeModel(mRealm).getEmployeesByCompAndType(compId, type);
            }

            @Override
            protected LiveData<ApiResponse<List<HrisCompanyDetailFastAdapter>>> createCall() {
                return null;
            }

            @Override
            protected void cancelTransaction() {

            }
        }.asLiveData();
    }
}
