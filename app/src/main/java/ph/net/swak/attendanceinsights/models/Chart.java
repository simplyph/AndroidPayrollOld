package ph.net.swak.attendanceinsights.models;

/**
 * Created by Knowell on 1/11/2017.
 */

public class Chart {
    private String fName;
    private String lName;
    private boolean active;
    private String empid;
    private String company;
    private String department;
    private String designation;
    private String employmentStatus;
    private double salaryRate;
    private int age;
    private String gender;

    private String dateTerminated;
    private String dateHired;

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(String active) {
        if(active.equalsIgnoreCase("inactive")) {
            this.active = false;
        } else if (active.equalsIgnoreCase("active")) {
            this.active = true;
        }
    }

    public String getEmpid() {
        return empid;
    }

    public void setEmpid(String empid) {
        this.empid = empid;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(String employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    public double getSalaryRate() {
        return salaryRate;
    }

    public void setSalaryRate(double salaryRate) {
        this.salaryRate = salaryRate;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateTerminated() {
        return dateTerminated;
    }

    public void setDateTerminated(String dateTerminated) {
        this.dateTerminated = dateTerminated;
    }

    public String getDateHired() {
        return dateHired;
    }

    public void setDateHired(String dateHired) {
        this.dateHired = dateHired;
    }
}
