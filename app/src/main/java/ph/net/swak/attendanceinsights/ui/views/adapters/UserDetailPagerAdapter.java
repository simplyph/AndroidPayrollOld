package ph.net.swak.attendanceinsights.ui.views.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ph.net.swak.attendanceinsights.ui.fragments.SamplePageFragment;
import ph.net.swak.attendanceinsights.ui.fragments.appraisal.IndividualAppraisalFragment;
import ph.net.swak.attendanceinsights.ui.fragments.hris.ProfileFragment;
import ph.net.swak.attendanceinsights.ui.fragments.payroll.IndividualPayrollFragment;

public class UserDetailPagerAdapter extends FragmentStatePagerAdapter {

    private int PAGE_COUNT = 3;
    private String tabTitles[] = new String[]{"Basic Info", "Payroll", "Appraisal"};
    private String empid = "";

    public UserDetailPagerAdapter(FragmentManager fm, String empid) {
        super(fm);
        this.empid = empid;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new SamplePageFragment();

        if(position == 0)
            fragment = new ProfileFragment();
        else if(position == 1)
            fragment = new IndividualPayrollFragment();
        else if(position == 2)
            fragment = new IndividualAppraisalFragment();

        Bundle bundle = new Bundle();
        bundle.putString("empid", empid);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
