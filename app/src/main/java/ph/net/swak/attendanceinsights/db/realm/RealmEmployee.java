package ph.net.swak.attendanceinsights.db.realm;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class RealmEmployee implements RealmModel {

    // Variables
    @SerializedName("emp_id") @PrimaryKey
    private long employeeId = -1;
    @SerializedName("fname")
    private String fName;
    @SerializedName("mname")
    private String mName;
    @SerializedName("lname")
    private String lName;
    @SerializedName("position_id")
    private int positionId;
    @SerializedName("position")
    private String positionName;
    @SerializedName("status_id")
    private int statusId;
    @SerializedName("status")
    private String statusName;
    @SerializedName("status_type")
    private String statusType;
    @SerializedName("hired")
    private String dateHired;
    @SerializedName("terminated")
    private String dateTerminated;
    @SerializedName("regularization")
    private String dateRegularization;
    @SerializedName("comp_id")
    private int compId;
    @SerializedName("comp_name")
    private String compName;
    @SerializedName("comp_status")
    private int compStatus;
    @SerializedName("div_id")
    private int divId;
    @SerializedName("div_name")
    private String divName;
    @SerializedName("dept_id")
    private int deptId;
    @SerializedName("dept_name")
    private String deptName;
    @SerializedName("desig_id")
    private int desigId;
    @SerializedName("desig_name")
    private String desigName;
    @SerializedName("func_id")
    private int funcId;
    @SerializedName("func_name")
    private String funcName;
    @SerializedName("wbase_id")
    private int wbaseId;
    @SerializedName("wbase_name")
    private String wbaseName;
    @SerializedName("religion_id")
    private int religionId;
    @SerializedName("religion_name")
    private String religionName;
    @SerializedName("birthdate")
    private String dateBirth;
    @SerializedName("civil_status")
    private String civilStatus;
    @SerializedName("gender")
    private String gender;
    @SerializedName("img_path")
    private String imgPath;


    // Getters/Setters
    public long getEmployeeId() {
        return employeeId;
    }

    public RealmEmployee withEmployeeId(long employeeId) {
        this.employeeId = employeeId;
        return this;
    }

    public String getfName() {
        return fName;
    }

    public RealmEmployee withfName(String fName) {
        this.fName = fName;
        return this;
    }

    public String getmName() {
        return mName;
    }

    public RealmEmployee withmName(String mName) {
        this.mName = mName;
        return this;
    }

    public String getlName() {
        return lName;
    }

    public RealmEmployee withlName(String lName) {
        this.lName = lName;
        return this;
    }

    public int getPositionId() {
        return positionId;
    }

    public RealmEmployee withPositionId(int positionId) {
        this.positionId = positionId;
        return this;
    }

    public String getPositionName() {
        return positionName;
    }

    public RealmEmployee withPositionName(String positionName) {
        this.positionName = positionName;
        return this;
    }

    public int getStatusId() {
        return statusId;
    }

    public RealmEmployee withStatusId(int statusId) {
        this.statusId = statusId;
        return this;
    }

    public String getStatusName() {
        return statusName;
    }

    public RealmEmployee withStatusName(String statusName) {
        this.statusName = statusName;
        return this;
    }

    public String getStatusType() {
        return statusType;
    }

    public RealmEmployee withStatusType(String statusType) {
        this.statusType = statusType;
        return this;
    }

    public String getDateHired() {
        return dateHired;
    }

    public RealmEmployee withDateHired(String dateHired) {
        this.dateHired = dateHired;
        return this;
    }

    public String getDateTerminated() {
        return dateTerminated;
    }

    public RealmEmployee withDateTerminated(String dateTerminated) {
        this.dateTerminated = dateTerminated;
        return this;
    }

    public String getDateRegularization() {
        return dateRegularization;
    }

    public RealmEmployee withDateRegularization(String dateRegularization) {
        this.dateRegularization = dateRegularization;
        return this;
    }

    public int getCompId() {
        return compId;
    }

    public RealmEmployee withCompId(int compId) {
        this.compId = compId;
        return this;
    }

    public String getCompName() {
        return compName;
    }

    public RealmEmployee withCompName(String compName) {
        this.compName = compName;
        return this;
    }

    public int getCompStatus() {
        return compStatus;
    }

    public RealmEmployee withCompStatus(int compStatus) {
        this.compStatus = compStatus;
        return this;
    }

    public int getDivId() {
        return divId;
    }

    public RealmEmployee withDivId(int divId) {
        this.divId = divId;
        return this;
    }

    public String getDivName() {
        return divName;
    }

    public RealmEmployee withDivName(String divName) {
        this.divName = divName;
        return this;
    }

    public int getDeptId() {
        return deptId;
    }

    public RealmEmployee withDeptId(int deptId) {
        this.deptId = deptId;
        return this;
    }

    public String getDeptName() {
        return deptName;
    }

    public RealmEmployee withDeptName(String deptName) {
        this.deptName = deptName;
        return this;
    }

    public int getDesigId() {
        return desigId;
    }

    public RealmEmployee withDesigId(int desigId) {
        this.desigId = desigId;
        return this;
    }

    public String getDesigName() {
        return desigName;
    }

    public RealmEmployee withDesigName(String desigName) {
        this.desigName = desigName;
        return this;
    }

    public int getFuncId() {
        return funcId;
    }

    public RealmEmployee withFuncId(int funcId) {
        this.funcId = funcId;
        return this;
    }

    public String getFuncName() {
        return funcName;
    }

    public RealmEmployee withFuncName(String funcName) {
        this.funcName = funcName;
        return this;
    }

    public int getWbaseId() {
        return wbaseId;
    }

    public RealmEmployee withWbaseId(int wbaseId) {
        this.wbaseId = wbaseId;
        return this;
    }

    public String getWbaseName() {
        return wbaseName;
    }

    public RealmEmployee withWbaseName(String wbaseName) {
        this.wbaseName = wbaseName;
        return this;
    }

    public int getReligionId() {
        return religionId;
    }

    public RealmEmployee withReligionId(int religionId) {
        this.religionId = religionId;
        return this;
    }

    public String getReligionName() {
        return religionName;
    }

    public RealmEmployee withReligionName(String religionName) {
        this.religionName = religionName;
        return this;
    }

    public String getDateBirth() {
        return dateBirth;
    }

    public RealmEmployee withDateBirth(String dateBirth) {
        this.dateBirth = dateBirth;
        return this;
    }

    public String getCivilStatus() {
        return civilStatus;
    }

    public RealmEmployee withCivilStatus(String civilStatus) {
        this.civilStatus = civilStatus;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public RealmEmployee withGender(String gender) {
        this.gender = gender;
        return this;
    }

    public String getImgPath() {
        return imgPath;
    }

    public RealmEmployee withImgPath(String imgPath) {
        this.imgPath = imgPath;
        return this;
    }

    public RealmEmployee() {}

}
