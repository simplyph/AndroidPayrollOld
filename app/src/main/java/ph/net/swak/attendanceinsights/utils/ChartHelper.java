package ph.net.swak.attendanceinsights.utils;

import android.graphics.Color;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import ph.net.swak.attendanceinsights.R;

public class ChartHelper {

    private static final ChartHelper INSTANCE = new ChartHelper();

    public static ChartHelper get() {
        return INSTANCE;
    }

    public PieChart stylePie(ArrayList<PieEntry> entries, PieChart pieChart) {
        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        dataSet.setSliceSpace(1f);
        dataSet.setValueTextSize(12f);
        dataSet.setValueTextColor(R.color.md_grey_800);
        dataSet.setValueFormatter(new PieValueFormatter(true));
        dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setValueLineColor(R.color.md_grey_800);

        PieData data = new PieData(dataSet);

        pieChart.getDescription().setEnabled(false);
        pieChart.setDrawEntryLabels(false);
        pieChart.setRotationEnabled(false);
        pieChart.getLegend().setEnabled(false);
        pieChart.setHardwareAccelerationEnabled(true);
        pieChart.setData(data);

        return pieChart;
    }

    public BarChart styleBar(ArrayList<BarEntry> entries, BarChart barChart) {
        return styleBar(entries, barChart, null, null);
    }

    public BarChart styleBar(ArrayList<BarEntry> entries, BarChart barChart, IAxisValueFormatter formatter, IAxisValueFormatter leftFormatter) {
        BarDataSet dataSet = new BarDataSet(entries, "");
        dataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        dataSet.setValueTextSize(12f);
        dataSet.setValueTextColor(R.color.md_grey_800);

        BarData data = new BarData(dataSet);

        barChart.getDescription().setEnabled(false);
        barChart.getLegend().setEnabled(false);
        barChart.setHardwareAccelerationEnabled(true);

        if(formatter != null) {
            XAxis xAxis = barChart.getXAxis();
            xAxis.setValueFormatter(formatter);
        }

        if(leftFormatter != null) {
            barChart.getAxisLeft().setValueFormatter(leftFormatter);
        }

        barChart.getAxisRight().setEnabled(false);
        barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        barChart.getXAxis().setGranularity(1f);
        barChart.setData(data);

        return barChart;
    }

    public LineChart styleLine(ArrayList<Entry> entries, LineChart lineChart) {
        return styleLine(entries, lineChart, null);
    }

    public LineChart styleLine(ArrayList<Entry> entries, LineChart lineChart, IAxisValueFormatter formatter) {
        return styleLine(entries, lineChart, formatter, null);
    }

    public LineChart styleLine(ArrayList<Entry> entries, LineChart lineChart, IAxisValueFormatter formatter, IAxisValueFormatter leftFormatter) {
        LineDataSet dataSet = new LineDataSet(entries, "");
        dataSet.setColor(R.color.md_blue_grey_800);
        dataSet.setValueTextColor(R.color.md_grey_800);
        dataSet.setCircleColor(Color.BLACK);
        dataSet.setDrawCircleHole(false);

        LineData data = new LineData(dataSet);

        lineChart.getDescription().setEnabled(false);
        lineChart.getLegend().setEnabled(false);
        lineChart.setHardwareAccelerationEnabled(true);

        if(formatter != null) {
            XAxis xAxis = lineChart.getXAxis();
            xAxis.setValueFormatter(formatter);
        }

        if(leftFormatter != null) {
            lineChart.getAxisLeft().setValueFormatter(leftFormatter);
        }

        lineChart.getAxisRight().setEnabled(false);
        lineChart.getXAxis().setGranularity(1f);
        lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        lineChart.setData(data);

        return lineChart;
    }

    public LineChart styleLine(TreeMap<String, ArrayList<Entry>> entryList, LineChart lineChart, IAxisValueFormatter formatter) {

        List<ILineDataSet> lineDataSetList = new ArrayList<>();

        int colorCounter = 0;

        for(Map.Entry<String, ArrayList<Entry>> entries : entryList.entrySet()) {
            LineDataSet lineDataSet = new LineDataSet(entries.getValue(), entries.getKey());
            lineDataSet.setColor(ColorTemplate.MATERIAL_COLORS[colorCounter++]);
            lineDataSet.setValueTextColor(R.color.md_grey_800);
            lineDataSet.setCircleColor(Color.BLACK);
            lineDataSet.setDrawCircleHole(false);
            lineDataSetList.add(lineDataSet);
        }

        LineData data = new LineData(lineDataSetList);

        lineChart.getDescription().setEnabled(false);
        lineChart.getLegend().setEnabled(false);
        lineChart.setHardwareAccelerationEnabled(true);

        if(formatter != null) {
            XAxis xAxis = lineChart.getXAxis();
            xAxis.setValueFormatter(formatter);
        }

        lineChart.getAxisRight().setEnabled(false);
        lineChart.getXAxis().setGranularity(1f);
        lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        lineChart.setData(data);

        return lineChart;
    }
}
