package ph.net.swak.attendanceinsights.ui.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.net.swak.attendanceinsights.R;

public class AttStatsFastAdapter extends AbstractItem<AttStatsFastAdapter, AttStatsFastAdapter.ViewHolder> {

    private int rank;
    private int empId;
    private String name;
    private String compName;
    private String desigName;
    private String details;

    public int getRank() {
        return rank;
    }

    public AttStatsFastAdapter withRank(int rank) {
        this.rank = rank;
        return this;
    }

    public int getEmpId() {
        return empId;
    }

    public AttStatsFastAdapter withEmpId(int empId) {
        this.empId = empId;
        return this;
    }

    public String getName() {
        return name;
    }

    public AttStatsFastAdapter withName(String name) {
        this.name = name;
        return this;
    }

    public String getCompName() {
        return compName;
    }

    public AttStatsFastAdapter withCompName(String compName) {
        this.compName = compName;
        return this;
    }

    public String getDesigName() {
        return desigName;
    }

    public AttStatsFastAdapter withDesigName(String desigName) {
        this.desigName = desigName;
        return this;
    }

    public String getDetails() {
        return details;
    }

    public AttStatsFastAdapter withDetails(String details) {
        this.details = details;
        return this;
    }

    @Override
    public int getType() {
        return R.id.fastadapter_bottom_sheet_att_stats_item_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.list_attendance_dashboard;
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);

        holder.textRank.setText(String.valueOf(getRank()));
        holder.textName.setText(getName());
        holder.textComp.setText(getCompName());
        holder.textDesig.setText(getDesigName());
        holder.textDetails.setText(getDetails());

    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);

        holder.textRank.setText(null);
        holder.textName.setText(null);
        holder.textComp.setText(null);
        holder.textDesig.setText(null);
        holder.textDetails.setText(null);


    }

    @Override
    public ViewHolder getViewHolder(View view) {
        return new ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_rank)
        protected TextView textRank;

        @BindView(R.id.text_name)
        protected TextView textName;

        @BindView(R.id.text_comp)
        protected TextView textComp;

        @BindView(R.id.text_desig)
        protected TextView textDesig;

        @BindView(R.id.text_details)
        protected TextView textDetails;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
