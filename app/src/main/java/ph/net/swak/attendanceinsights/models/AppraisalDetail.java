package ph.net.swak.attendanceinsights.models;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mikepenz.fastadapter.IItem;
import com.mikepenz.fastadapter.ISubItem;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.net.swak.attendanceinsights.R;

public class AppraisalDetail implements IItem<AppraisalDetail, AppraisalDetail.ViewHolder>, ISubItem<AppraisalDetail, RealmIndividualAppraisal> {

    private long mIdentifier = -1;
    private String title;
    private String subtitle;
    private String info;
    private boolean header = false;

    public String getTitle() {
        return title;
    }

    public AppraisalDetail withTitle(String title) {
        this.title = title;
        return this;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public AppraisalDetail withSubtitle(String subtitle) {
        this.subtitle = subtitle;
        return this;
    }

    public String getInfo() {
        return info;
    }

    public AppraisalDetail withInfo(String info) {
        this.info = info;
        return this;
    }

    public boolean isHeader() {
        return header;
    }

    public AppraisalDetail withHeader(boolean header) {
        this.header = header;
        return this;
    }

    private RealmIndividualAppraisal mParent;

    @Override
    public RealmIndividualAppraisal getParent() {
        return mParent;
    }

    @Override
    public AppraisalDetail withParent(RealmIndividualAppraisal parent) {
        this.mParent = parent;
        return this;
    }

    @Override
    public Object getTag() {
        return null;
    }

    @Override
    public AppraisalDetail withTag(Object tag) {
        return null;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    @Override
    public AppraisalDetail withEnabled(boolean enabled) {
        return null;
    }

    @Override
    public boolean isSelected() {
        return false;
    }

    @Override
    public AppraisalDetail withSetSelected(boolean selected) {
        return null;
    }

    @Override
    public boolean isSelectable() {
        return false;
    }

    @Override
    public AppraisalDetail withSelectable(boolean selectable) {
        return null;
    }

    @Override
    public int getType() {
        return R.id.fastadapter_individual_appraisal_sub_item_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.viewgroup_appraisal_detail;
    }

    @Override
    public View generateView(Context ctx) {
        AppraisalDetail.ViewHolder viewHolder = getViewHolder(LayoutInflater.from(ctx).inflate(getLayoutRes(), null, false));
        bindView(viewHolder, Collections.EMPTY_LIST);
        return viewHolder.itemView;
    }

    @Override
    public View generateView(Context ctx, ViewGroup parent) {
        AppraisalDetail.ViewHolder viewHolder = getViewHolder(LayoutInflater.from(ctx).inflate(getLayoutRes(), parent, false));
        bindView(viewHolder, Collections.EMPTY_LIST);
        return viewHolder.itemView;
    }

    @Override
    public ViewHolder getViewHolder(ViewGroup parent) {
        return getViewHolder(LayoutInflater.from(parent.getContext()).inflate(getLayoutRes(), parent, false));
    }

    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        if (isHeader()) {
            holder.headerTitle.setVisibility(View.VISIBLE);
            holder.headerInfo.setVisibility(View.VISIBLE);
            holder.bodyTitle.setVisibility(View.GONE);
            holder.bodySubtitle.setVisibility(View.GONE);
            holder.bodyInfo.setVisibility(View.GONE);
            holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.md_blue_grey_200));

            holder.headerTitle.setText(getTitle());
            holder.headerInfo.setText(getInfo());
        } else {
            holder.headerTitle.setVisibility(View.GONE);
            holder.headerInfo.setVisibility(View.GONE);
            holder.bodyTitle.setVisibility(View.VISIBLE);
            holder.bodySubtitle.setVisibility(View.VISIBLE);
            holder.bodyInfo.setVisibility(View.VISIBLE);
            holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), android.R.color.transparent));

            holder.bodyTitle.setText(getTitle());
            holder.bodySubtitle.setText(getSubtitle());
            holder.bodyInfo.setText(getInfo());
        }
    }

    @Override
    public void unbindView(ViewHolder holder) {
        holder.headerTitle.setText(null);
        holder.headerInfo.setText(null);
        holder.bodyTitle.setText(null);
        holder.bodySubtitle.setText(null);
        holder.bodyInfo.setText(null);
    }

    @Override
    public void attachToWindow(ViewHolder viewHolder) {

    }

    @Override
    public void detachFromWindow(ViewHolder viewHolder) {

    }

    @Override
    public boolean failedToRecycle(ViewHolder viewHolder) {
        return false;
    }

    @Override
    public boolean equals(int id) {
        return false;
    }

    @Override
    public AppraisalDetail withIdentifier(long identifier) {
        this.mIdentifier = identifier;
        return this;
    }

    @Override
    public long getIdentifier() {
        return mIdentifier;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textview_viewgroup_appraisal_detail_header_title)
        protected TextView headerTitle;

        @BindView(R.id.textview_viewgroup_appraisal_detail_header_info)
        protected TextView headerInfo;

        @BindView(R.id.textview_viewgroup_appraisal_detail_body_title)
        protected TextView bodyTitle;

        @BindView(R.id.textview_viewgroup_appraisal_detail_body_subtitle)
        protected TextView bodySubtitle;

        @BindView(R.id.textview_viewgroup_appraisal_detail_body_info)
        protected TextView bodyInfo;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
