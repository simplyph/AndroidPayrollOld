package ph.net.swak.attendanceinsights.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.adapters.ItemAdapter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.models.BottomSheetCompany;
import ph.net.swak.attendanceinsights.models.RealmFilterComp;
import ph.net.swak.attendanceinsights.models.RealmPayroll;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;

public class CustomBottomFragment extends BottomSheetDialogFragment {

    @BindView(R.id.textview_bottom_sheet_title)
    TextView textView;

    @BindView(R.id.recyclerview_bottom_sheet)
    RecyclerView recyclerView;

    private Unbinder unbinder;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private Realm mRealm;
    private DecimalFormat dF = new DecimalFormat("###,##0.00");
    private final String TAG = "CustomBottomFragment";

    private FastAdapter<BottomSheetCompany> fastAdapter;
    private ItemAdapter<BottomSheetCompany> itemAdapter;

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View view = View.inflate(getContext(), R.layout.fragment_bottom_sheet, null);
        unbinder = ButterKnife.bind(this, view);
        mRealm = Realm.getDefaultInstance();
        requestQueue = VolleyAPI.getInstance(getContext()).getRequestQueue();
        volleyRequest = new VolleyRequest();

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);

        fastAdapter = new FastAdapter<>();
        fastAdapter.withSelectable(true);
        itemAdapter = new ItemAdapter<>();
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(itemAdapter.wrap(fastAdapter));

        Bundle bundle = this.getArguments();
        if (bundle != null && !bundle.isEmpty()) {
            if(bundle.getString("payrolltype").equalsIgnoreCase("monthly")) {
                getSubData1(bundle.getInt("year"), bundle.getInt("month"));
            } else {
                getSubData2(bundle.getInt("year"), bundle.getInt("quarter"));
            }

        }

        dialog.setContentView(view);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (requestQueue != null)
            requestQueue.cancelAll(TAG);
    }

    @OnClick(R.id.textview_bottom_sheet_close)
    protected void onCloseClick() {
        dismiss();
    }

    private void getSubData1(int year, int month) {

        RealmResults<RealmFilterComp> compList = mRealm.where(RealmFilterComp.class).equalTo("mSelected", true).findAll();
        Long[] selectedComp = new Long[compList.size()];
        for (int i = 0; i < compList.size(); i++) {
            selectedComp[i] = compList.get(i).getIdentifier();
        }

        RealmResults<RealmPayroll> realmPayrolls = mRealm.where(RealmPayroll.class).equalTo("covMonth", month).in("compId", selectedComp).distinct("compId").sort("compName");
        List<BottomSheetCompany> bottomSheetCompanies = new ArrayList<>();

        for (RealmPayroll realmPayroll : realmPayrolls) {
            double totalGross = mRealm.where(RealmPayroll.class).equalTo("covMonth", month).equalTo("compId", realmPayroll.getCompId()).sum("totalGross").doubleValue();
            int totalEmployee = mRealm.where(RealmPayroll.class).equalTo("covMonth", month).equalTo("compId", realmPayroll.getCompId()).distinct("empId").size();
            int totalManager = mRealm.where(RealmPayroll.class).equalTo("covMonth", month).equalTo("compId", realmPayroll.getCompId()).equalTo("manager", true).distinct("empId").size();
            int totalSupervisor = mRealm.where(RealmPayroll.class).equalTo("covMonth", month).equalTo("compId", realmPayroll.getCompId()).equalTo("supervisor", true).distinct("empId").size();
            int totalStaff = mRealm.where(RealmPayroll.class).equalTo("covMonth", month).equalTo("compId", realmPayroll.getCompId()).equalTo("staff", true).distinct("empId").size();

            double grossManager = mRealm.where(RealmPayroll.class).equalTo("covMonth", month).equalTo("compId", realmPayroll.getCompId()).equalTo("manager", true).sum("totalGross").doubleValue();
            double grossSupervisor = mRealm.where(RealmPayroll.class).equalTo("covMonth", month).equalTo("compId", realmPayroll.getCompId()).equalTo("supervisor", true).sum("totalGross").doubleValue();
            double grossStaff = mRealm.where(RealmPayroll.class).equalTo("covMonth", month).equalTo("compId", realmPayroll.getCompId()).equalTo("staff", true).sum("totalGross").doubleValue();

            RealmResults<RealmPayroll> divResults = mRealm.where(RealmPayroll.class).equalTo("covMonth", month).equalTo("compId", realmPayroll.getCompId()).distinct("divId").sort("divName", Sort.ASCENDING);

            ArrayList<String> divList = new ArrayList<>();
            divList.add("All");

            for (RealmPayroll divResult : divResults) {
                divList.add(divResult.getDivName());
            }

            Log.e("COMPANY DIV: ", realmPayroll.getCompName() + ": " + divList.size());

            BottomSheetCompany bottomSheetCompany = new BottomSheetCompany();
            bottomSheetCompany.setTitle(realmPayroll.getCompName());
            bottomSheetCompany.setSubtitle("Gross: Php " + String.valueOf(dF.format(totalGross)));
            bottomSheetCompany.setInfo1(String.valueOf(totalEmployee) + " Employees");
            bottomSheetCompany.setInfo2(String.valueOf(totalManager) + " Manager");
            bottomSheetCompany.setInfo3(String.valueOf(totalSupervisor) + " Supervisor");
            bottomSheetCompany.setInfo4(String.valueOf(totalStaff) + " Regular Staff");
            bottomSheetCompany.setDialog(getDialog());
            bottomSheetCompany.setYear(String.valueOf(year));
            bottomSheetCompany.setMonth(String.valueOf(month));
            bottomSheetCompany.setCompid(realmPayroll.getCompId());
            bottomSheetCompany.setCompName(realmPayroll.getCompName());
            bottomSheetCompany.setTotalManager(totalManager);
            bottomSheetCompany.setTotalSupervisor(totalSupervisor);
            bottomSheetCompany.setTotalStaff(totalStaff);
            bottomSheetCompany.setGrossManager(grossManager);
            bottomSheetCompany.setGrossSupervisor(grossSupervisor);
            bottomSheetCompany.setGrossStaff(grossStaff);
            bottomSheetCompany.withDivList(divList);
            bottomSheetCompany.withPayrollType("monthly");
            bottomSheetCompanies.add(bottomSheetCompany);
        }

        itemAdapter.add(bottomSheetCompanies);
    }

    private void getSubData2(int year, int quarter) {

        RealmResults<RealmFilterComp> compList = mRealm.where(RealmFilterComp.class).equalTo("mSelected", true).findAll();
        Long[] selectedComp = new Long[compList.size()];
        for (int i = 0; i < compList.size(); i++) {
            selectedComp[i] = compList.get(i).getIdentifier();
        }

        RealmResults<RealmPayroll> realmPayrolls = mRealm.where(RealmPayroll.class)
                .in("covMonth", quarter == 1 ? new Integer[]{1,2,3} : quarter == 2 ? new Integer[]{4,5,6} : quarter == 3 ? new Integer[]{7,8,9} : new Integer[]{10,11,12})
                .in("compId", selectedComp).distinct("compId").sort("compName");
        List<BottomSheetCompany> bottomSheetCompanies = new ArrayList<>();

        for (RealmPayroll realmPayroll : realmPayrolls) {
            double totalGross = mRealm.where(RealmPayroll.class)
                    .in("covMonth", quarter == 1 ? new Integer[]{1,2,3} : quarter == 2 ? new Integer[]{4,5,6} : quarter == 3 ? new Integer[]{7,8,9} : new Integer[]{10,11,12})
                    .equalTo("compId", realmPayroll.getCompId()).sum("totalGross").doubleValue();
            int totalEmployee = mRealm.where(RealmPayroll.class)
                    .in("covMonth", quarter == 1 ? new Integer[]{1,2,3} : quarter == 2 ? new Integer[]{4,5,6} : quarter == 3 ? new Integer[]{7,8,9} : new Integer[]{10,11,12})
                    .equalTo("compId", realmPayroll.getCompId()).distinct("empId").size();
            int totalManager = mRealm.where(RealmPayroll.class)
                    .in("covMonth", quarter == 1 ? new Integer[]{1,2,3} : quarter == 2 ? new Integer[]{4,5,6} : quarter == 3 ? new Integer[]{7,8,9} : new Integer[]{10,11,12})
                    .equalTo("compId", realmPayroll.getCompId()).equalTo("manager", true).distinct("empId").size();
            int totalSupervisor = mRealm.where(RealmPayroll.class)
                    .in("covMonth", quarter == 1 ? new Integer[]{1,2,3} : quarter == 2 ? new Integer[]{4,5,6} : quarter == 3 ? new Integer[]{7,8,9} : new Integer[]{10,11,12})
                    .equalTo("compId", realmPayroll.getCompId()).equalTo("supervisor", true).distinct("empId").size();
            int totalStaff = mRealm.where(RealmPayroll.class)
                    .in("covMonth", quarter == 1 ? new Integer[]{1,2,3} : quarter == 2 ? new Integer[]{4,5,6} : quarter == 3 ? new Integer[]{7,8,9} : new Integer[]{10,11,12})
                    .equalTo("compId", realmPayroll.getCompId()).equalTo("staff", true).distinct("empId").size();

            double grossManager = mRealm.where(RealmPayroll.class)
                    .in("covMonth", quarter == 1 ? new Integer[]{1,2,3} : quarter == 2 ? new Integer[]{4,5,6} : quarter == 3 ? new Integer[]{7,8,9} : new Integer[]{10,11,12})
                    .equalTo("compId", realmPayroll.getCompId()).equalTo("manager", true).sum("totalGross").doubleValue();
            double grossSupervisor = mRealm.where(RealmPayroll.class)
                    .in("covMonth", quarter == 1 ? new Integer[]{1,2,3} : quarter == 2 ? new Integer[]{4,5,6} : quarter == 3 ? new Integer[]{7,8,9} : new Integer[]{10,11,12})
                    .equalTo("compId", realmPayroll.getCompId()).equalTo("supervisor", true).sum("totalGross").doubleValue();
            double grossStaff = mRealm.where(RealmPayroll.class)
                    .in("covMonth", quarter == 1 ? new Integer[]{1,2,3} : quarter == 2 ? new Integer[]{4,5,6} : quarter == 3 ? new Integer[]{7,8,9} : new Integer[]{10,11,12})
                    .equalTo("compId", realmPayroll.getCompId()).equalTo("staff", true).sum("totalGross").doubleValue();

            RealmResults<RealmPayroll> divResults = mRealm.where(RealmPayroll.class).equalTo("covMonth", 1).equalTo("compId", realmPayroll.getCompId()).distinct("divId").sort("divName", Sort.ASCENDING);

            ArrayList<String> divList = new ArrayList<>();
            divList.add("All");

            for (RealmPayroll divResult : divResults) {
                divList.add(divResult.getDivName());
            }

            Log.e("COMPANY DIV: ", realmPayroll.getCompName() + ": " + divList.size());

            BottomSheetCompany bottomSheetCompany = new BottomSheetCompany();
            bottomSheetCompany.setTitle(realmPayroll.getCompName());
            bottomSheetCompany.setSubtitle("Gross: Php " + String.valueOf(dF.format(totalGross)));
            bottomSheetCompany.setInfo1(String.valueOf(totalEmployee) + " Employees");
            bottomSheetCompany.setInfo2(String.valueOf(totalManager) + " Manager");
            bottomSheetCompany.setInfo3(String.valueOf(totalSupervisor) + " Supervisor");
            bottomSheetCompany.setInfo4(String.valueOf(totalStaff) + " Regular Staff");
            bottomSheetCompany.setDialog(getDialog());
            bottomSheetCompany.setYear(String.valueOf(year));
            bottomSheetCompany.setMonth(String.valueOf(1));
            bottomSheetCompany.setCompid(realmPayroll.getCompId());
            bottomSheetCompany.setCompName(realmPayroll.getCompName());
            bottomSheetCompany.setTotalManager(totalManager);
            bottomSheetCompany.setTotalSupervisor(totalSupervisor);
            bottomSheetCompany.setTotalStaff(totalStaff);
            bottomSheetCompany.setGrossManager(grossManager);
            bottomSheetCompany.setGrossSupervisor(grossSupervisor);
            bottomSheetCompany.setGrossStaff(grossStaff);
            bottomSheetCompany.withDivList(divList);
            bottomSheetCompany.withPayrollType("quarterly");
            bottomSheetCompanies.add(bottomSheetCompany);
        }

        itemAdapter.add(bottomSheetCompanies);
    }
}
