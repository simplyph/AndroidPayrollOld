package ph.net.swak.attendanceinsights.ui.views.fastadapter;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.ui.fragments.lifecycle.HrisDashboardMainFragment;
import ph.net.swak.attendanceinsights.ui.views.bottomsheet.HrisEmployeeBottomSheet;
import ph.net.swak.attendanceinsights.utils.Helper;

public class HrisCompanyFastAdapter extends AbstractItem<HrisCompanyFastAdapter, HrisCompanyFastAdapter.ViewHolder> {

    private String compName;
    private int compId;
    private int totalEmployee;
    private int totalManager;
    private int totalSupervisor;
    private int totalStaff;
    private int from;
    private int fromId;

    public String getCompName() {
        return compName;
    }

    public HrisCompanyFastAdapter withCompName(String compName) {
        this.compName = compName;
        return this;
    }

    public int getCompId() {
        return compId;
    }

    public HrisCompanyFastAdapter withCompId(int compId) {
        this.compId = compId;
        return this;
    }

    public int getTotalEmployee() {
        return totalEmployee;
    }

    public HrisCompanyFastAdapter withTotalEmployee(int totalEmployee) {
        this.totalEmployee = totalEmployee;
        return this;
    }

    public int getTotalManager() {
        return totalManager;
    }

    public HrisCompanyFastAdapter withTotalManager(int totalManager) {
        this.totalManager = totalManager;
        return this;
    }

    public int getTotalSupervisor() {
        return totalSupervisor;
    }

    public HrisCompanyFastAdapter withTotalSupervisor(int totalSupervisor) {
        this.totalSupervisor = totalSupervisor;
        return this;
    }

    public int getTotalStaff() {
        return totalStaff;
    }

    public HrisCompanyFastAdapter withTotalStaff(int totalStaff) {
        this.totalStaff = totalStaff;
        return this;
    }

    public int getFrom() {
        return from;
    }

    public HrisCompanyFastAdapter withFrom(int from) {
        this.from = from;
        return this;
    }

    public int getFromId() {
        return fromId;
    }

    public HrisCompanyFastAdapter withFromId(int fromId) {
        this.fromId = fromId;
        return this;
    }

    @Override
    public ViewHolder getViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public int getType() {
        return R.id.fastadapter_hris_company_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.item_hris_company;
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);

        holder.tvTitle.setText(Helper.toTitleCase(getCompName()));
        holder.tvSubtitle.setText(String.format(Locale.ENGLISH, "%d Employees", getTotalEmployee()));
        holder.tvManager.setText(String.valueOf(getTotalManager()));
        holder.tvSupervisor.setText(String.valueOf(getTotalSupervisor()));
        holder.tvStaff.setText(String.valueOf(getTotalStaff()));

        // OnClick Events
        holder.linearLayoutManager.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putInt("type", 3);
            bundle.putString("typeName", "Manager");
            bundle.putInt("compId", getCompId());
            bundle.putString("compName", getCompName());
            bundle.putInt("from", getFrom());
            bundle.putInt("fromId", getFromId());

            HrisEmployeeBottomSheet hrisEmployeeBottomSheet = new HrisEmployeeBottomSheet();
            hrisEmployeeBottomSheet.setArguments(bundle);
            hrisEmployeeBottomSheet.show(((MainActivity) holder.itemView.getContext()).getSupportFragmentManager(), null);
        });

        holder.linearLayoutSupervisor.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putInt("type", 2);
            bundle.putString("typeName", "Supervisor");
            bundle.putInt("compId", getCompId());
            bundle.putString("compName", getCompName());
            bundle.putInt("from", getFrom());
            bundle.putInt("fromId", getFromId());

            HrisEmployeeBottomSheet hrisEmployeeBottomSheet = new HrisEmployeeBottomSheet();
            hrisEmployeeBottomSheet.setArguments(bundle);
            hrisEmployeeBottomSheet.show(((MainActivity) holder.itemView.getContext()).getSupportFragmentManager(), null);
        });

        holder.linearLayoutStaff.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putInt("type", 1);
            bundle.putString("typeName", "Staff");
            bundle.putInt("compId", getCompId());
            bundle.putString("compName", getCompName());
            bundle.putInt("from", getFrom());
            bundle.putInt("fromId", getFromId());

            HrisEmployeeBottomSheet hrisEmployeeBottomSheet = new HrisEmployeeBottomSheet();
            hrisEmployeeBottomSheet.setArguments(bundle);
            hrisEmployeeBottomSheet.show(((MainActivity) holder.itemView.getContext()).getSupportFragmentManager(), null);
        });

        holder.bDetails.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putInt("compId", getCompId());
            bundle.putString("compName", getCompName());

            HrisDashboardMainFragment fragment = new HrisDashboardMainFragment();
            fragment.setArguments(bundle);
            ((MainActivity) v.getContext()).getSupportFragmentManager().beginTransaction().addToBackStack("hris_dashboard_main").replace(R.id.main_frame, fragment).commit();
        });
    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);

        holder.tvTitle.setText(null);
        holder.tvSubtitle.setText(null);
        holder.tvManager.setText(null);
        holder.tvSupervisor.setText(null);
        holder.tvStaff.setText(null);

        holder.linearLayoutManager.setOnClickListener(null);
        holder.linearLayoutSupervisor.setOnClickListener(null);
        holder.linearLayoutStaff.setOnClickListener(null);
        holder.bDetails.setOnClickListener(null);

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textview_hris_company_title)
        public TextView tvTitle;

        @BindView(R.id.textview_hris_company_subtitle)
        protected TextView tvSubtitle;

        @BindView(R.id.linearlayout_hris_company_manager)
        protected LinearLayout linearLayoutManager;

        @BindView(R.id.linearlayout_hris_company_supervisor)
        protected LinearLayout linearLayoutSupervisor;

        @BindView(R.id.linearlayout_hris_company_staff)
        protected LinearLayout linearLayoutStaff;

        @BindView(R.id.textview_hris_company_manager_value)
        protected TextView tvManager;

        @BindView(R.id.textview_hris_company_supervisor_value)
        protected TextView tvSupervisor;

        @BindView(R.id.textview_hris_company_staff_value)
        protected TextView tvStaff;

        @BindView(R.id.button_hris_company_details)
        protected Button bDetails;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
