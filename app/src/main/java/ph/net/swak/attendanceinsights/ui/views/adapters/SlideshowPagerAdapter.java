package ph.net.swak.attendanceinsights.ui.views.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import ph.net.swak.attendanceinsights.R;

public class SlideshowPagerAdapter extends PagerAdapter {

    private ArrayList<String> imagePath = new ArrayList<>();

    public SlideshowPagerAdapter(ArrayList<String> imagePath) {
        this.imagePath = imagePath;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        LayoutInflater inflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.viewgroup_image_fullscreen, container, false);

        ImageView imageView = (ImageView) view.findViewById(R.id.image_preview);

        String image = imagePath.get(position);

        RequestOptions options = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL);

        Glide.with(container.getContext()).load(image)
                .apply(options)
                .into(imageView);

        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return imagePath.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (View) object;
    }
}
