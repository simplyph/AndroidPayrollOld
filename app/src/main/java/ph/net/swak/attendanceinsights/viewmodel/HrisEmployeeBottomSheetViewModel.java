package ph.net.swak.attendanceinsights.viewmodel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import ph.net.swak.attendanceinsights.db.realm.RealmEmployee;
import ph.net.swak.attendanceinsights.repository.HrisRepository;
import ph.net.swak.attendanceinsights.repository.Resource;

public class HrisEmployeeBottomSheetViewModel extends ViewModel {

    private HrisRepository hrisRepo;
    private LiveData<Resource<List<RealmEmployee>>> employeeList;

    @Inject
    public HrisEmployeeBottomSheetViewModel(HrisRepository hrisRepository) {
        hrisRepo = hrisRepository;
    }

    public LiveData<Resource<List<RealmEmployee>>> getEmployeeList(int compId, int posId, int from, int fromId) {
        employeeList = hrisRepo.getEmployeesByCompAndPos(compId, posId, from, fromId);

        return employeeList;
    }

}
