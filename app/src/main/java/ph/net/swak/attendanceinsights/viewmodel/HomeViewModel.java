package ph.net.swak.attendanceinsights.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import ph.net.swak.attendanceinsights.db.realm.RealmHrisQuick;
import ph.net.swak.attendanceinsights.db.realm.RealmPayrollQuick;
import ph.net.swak.attendanceinsights.repository.HrisRepository;
import ph.net.swak.attendanceinsights.repository.PayrollRepository;
import ph.net.swak.attendanceinsights.repository.Resource;

public class HomeViewModel extends ViewModel {

    private PayrollRepository payrollRepo;
    private HrisRepository hrisRepo;
    private LiveData<Resource<List<RealmPayrollQuick>>> payrollQuick;
    private LiveData<Resource<List<RealmHrisQuick>>> hrisQuick;

    @Inject
    public HomeViewModel(HrisRepository hrisRepository, PayrollRepository payrollRepository) {
        payrollRepo = payrollRepository;
        hrisRepo = hrisRepository;
    }

    public void updateLiveData(int prevMonth, int prevYear, int presentMonth, int presentYear) {
        payrollQuick = payrollRepo.getPayrollQuick(prevMonth, prevYear, presentMonth, presentYear);
        hrisQuick = hrisRepo.getHrisQuick();
    }

    public LiveData<Resource<List<RealmPayrollQuick>>> getPayrollQuick() {
        return payrollQuick;
    }

    public LiveData<Resource<List<RealmHrisQuick>>> getHrisQuick() {
        return hrisQuick;
    }
}
