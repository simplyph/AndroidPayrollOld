package ph.net.swak.attendanceinsights.models;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mikepenz.fastadapter.IItem;
import com.mikepenz.fastadapter.ISubItem;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.net.swak.attendanceinsights.R;

public class SampleSubItem implements IItem<SampleSubItem, SampleSubItem.ViewHolder>, ISubItem<SampleSubItem, RealmIndividualPayroll> {

    private String title;
    private String content;
    private boolean mIsHeading;

    public String getTitle() {
        return title;
    }

    public SampleSubItem withTitle(String title) {
        this.title = title;
        return this;
    }

    public String getContent() {
        return content;
    }

    public SampleSubItem withContent(String content) {
        this.content = content;
        return this;
    }

    private RealmIndividualPayroll mParent;

    @Override
    public RealmIndividualPayroll getParent() {
        return mParent;
    }

    @Override
    public SampleSubItem withParent(RealmIndividualPayroll parent) {
        this.mParent = parent;
        return this;
    }

    @Override
    public Object getTag() {
        return null;
    }

    @Override
    public SampleSubItem withTag(Object tag) {
        return null;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    @Override
    public SampleSubItem withEnabled(boolean enabled) {
        return null;
    }

    @Override
    public boolean isSelected() {
        return false;
    }

    @Override
    public SampleSubItem withSetSelected(boolean selected) {
        return null;
    }

    @Override
    public boolean isSelectable() {
        return false;
    }

    @Override
    public SampleSubItem withSelectable(boolean selectable) {
        return null;
    }

    public boolean getIsHeading() {
        return mIsHeading;
    }

    public SampleSubItem withIsHeading(boolean isHeading) {
        this.mIsHeading = isHeading;
        return this;
    }

    @Override
    public int getType() {
        return R.id.fastadapter_individual_payroll_sub_item_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.viewgroup_sample_sub_item;
    }

    @Override
    public View generateView(Context ctx) {
        SampleSubItem.ViewHolder viewHolder = getViewHolder(LayoutInflater.from(ctx).inflate(getLayoutRes(), null, false));
        bindView(viewHolder, Collections.EMPTY_LIST);
        return viewHolder.itemView;
    }

    @Override
    public View generateView(Context ctx, ViewGroup parent) {
        SampleSubItem.ViewHolder viewHolder = getViewHolder(LayoutInflater.from(ctx).inflate(getLayoutRes(), parent, false));
        bindView(viewHolder, Collections.EMPTY_LIST);
        return viewHolder.itemView;
    }

    @Override
    public ViewHolder getViewHolder(ViewGroup parent) {
        return getViewHolder(LayoutInflater.from(parent.getContext()).inflate(getLayoutRes(), parent, false));
    }

    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        if (getIsHeading()) {
            holder.body.setVisibility(View.GONE);
            holder.header.setVisibility(View.VISIBLE);
            holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.md_blue_grey_200));
            holder.headerName.setText(getTitle());
            holder.headerContent.setText(getContent());
        } else {
            holder.header.setVisibility(View.GONE);
            holder.body.setVisibility(View.VISIBLE);
            holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), android.R.color.transparent));
            holder.bodyName.setText(getTitle());
            holder.bodyContent.setText(getContent());
        }
    }

    @Override
    public void unbindView(ViewHolder holder) {
        holder.headerName.setText(null);
        holder.headerContent.setText(null);
        holder.bodyName.setText(null);
        holder.bodyContent.setText(null);
    }

    @Override
    public void attachToWindow(ViewHolder viewHolder) {

    }

    @Override
    public void detachFromWindow(ViewHolder viewHolder) {

    }

    @Override
    public boolean failedToRecycle(ViewHolder viewHolder) {
        return false;
    }

    @Override
    public boolean equals(int id) {
        return false;
    }

    @Override
    public SampleSubItem withIdentifier(long identifier) {
        return null;
    }

    @Override
    public long getIdentifier() {
        return 0;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.relativelayout_viewgroup_sample_sub_header)
        protected RelativeLayout header;

        @BindView(R.id.textview_viewgroup_sample_sub_header_title)
        protected TextView headerName;

        @BindView(R.id.textview_viewgroup_sample_sub_header_content)
        protected TextView headerContent;

        @BindView(R.id.relativelayout_viewgroup_sample_sub_body)
        protected RelativeLayout body;

        @BindView(R.id.textview_viewgroup_sample_sub_body_title)
        protected TextView bodyName;

        @BindView(R.id.textview_viewgroup_sample_sub_body_content)
        protected TextView bodyContent;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
