package ph.net.swak.attendanceinsights.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.mikepenz.fastadapter.IItemAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.models.RealmAppSubDetail;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.utils.JSONParser;

public class AppraisalDetailBottomFragment extends BottomSheetDialogFragment {

    @BindView(R.id.progressbar_bottom_sheet_app)
    protected ProgressBar progressBar;

    @BindView(R.id.textview_bottom_sheet_app_nodata)
    protected TextView progressBarText;

    @BindView(R.id.recyclerview_bottom_sheet_app)
    protected RecyclerView recyclerView;

    @BindView(R.id.textview_bottom_sheet_app_title)
    protected TextView title;

    private Unbinder unbinder;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private Realm mRealm;
    private final String TAG = "AppraisalDetailBottomFragment";
    private FastItemAdapter<RealmAppSubDetail> fastItemAdapter;

    @Override
    public void setupDialog(Dialog dialog, int style) {
        View view = View.inflate(getContext(), R.layout.fragment_bottom_sheet_app_detail, null);
        mRealm = Realm.getDefaultInstance();
        unbinder = ButterKnife.bind(this, view);
        requestQueue = VolleyAPI.getInstance(getContext()).getRequestQueue();
        volleyRequest = new VolleyRequest();

        fastItemAdapter = new FastItemAdapter<>();
        fastItemAdapter.withSelectable(true);
        fastItemAdapter.getItemFilter().withFilterPredicate(new IItemAdapter.Predicate<RealmAppSubDetail>() {
            @Override
            public boolean filter(RealmAppSubDetail item, CharSequence constraint) {
                return !item.getLname().toLowerCase().contains(String.valueOf(constraint).toLowerCase())
                        && !item.getFname().toLowerCase().contains(String.valueOf(constraint).toLowerCase());
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(fastItemAdapter);

        Bundle bundle = getArguments();

        if (bundle != null && !bundle.isEmpty()) {
            if (!bundle.getString("title").equalsIgnoreCase("")) {
                title.setText(bundle.getString("title"));
            } else {
                title.setText("Employee");
            }

            if (!bundle.getString("empid").equalsIgnoreCase("")) {
                setupData(bundle.getString("empid"));
            } else {
                progressBar.setVisibility(View.GONE);
                progressBarText.setVisibility(View.VISIBLE);
                progressBarText.setText("No Employee Found");
                recyclerView.setVisibility(View.GONE);
            }
        } else {
            progressBar.setVisibility(View.GONE);
            progressBarText.setVisibility(View.VISIBLE);
            progressBarText.setText("No Employee Found");
            recyclerView.setVisibility(View.GONE);
        }

        dialog.setContentView(view);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @OnTextChanged(R.id.edittext_search)
    protected void onSearch(CharSequence charSequence) {
        fastItemAdapter.filter(charSequence);
    }

    @OnClick(R.id.textview_bottom_sheet_app_close)
    protected void onClose() {
        dismiss();
    }

    private void setupData(String empid) {
        progressBar.setVisibility(View.VISIBLE);
        progressBarText.setVisibility(View.VISIBLE);
        progressBarText.setText("Loading Data");
        recyclerView.setVisibility(View.GONE);

        JsonArrayRequest request = volleyRequest.getAuthJsonArray(getContext().getApplicationContext(), TAG, volleyRequest.getEmployeeByList(empid), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                final List<RealmAppSubDetail> realmAppSubDetails = JSONParser.parseAppSubDetail(response);

                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.delete(RealmAppSubDetail.class);

                        for (RealmAppSubDetail realmAppSubDetail : realmAppSubDetails) {
                            realm.copyToRealm(realmAppSubDetail);
                        }
                    }
                });

                RealmResults<RealmAppSubDetail> realmAppSubDetails1 = mRealm.where(RealmAppSubDetail.class).findAllSorted("lastScore", Sort.DESCENDING);

                List<RealmAppSubDetail> realmAppSubDetails2 = new ArrayList<>();

                for(RealmAppSubDetail realmAppSubDetail : realmAppSubDetails1) {
                    realmAppSubDetail.withDialog(getDialog());
                    realmAppSubDetails2.add(realmAppSubDetail);
                }

                if (realmAppSubDetails2.size() > 0) {
                    fastItemAdapter.setNewList(realmAppSubDetails2);
                    progressBar.setVisibility(View.GONE);
                    progressBarText.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.GONE);
                    progressBarText.setVisibility(View.VISIBLE);
                    progressBarText.setText("No Employee Found");
                    recyclerView.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressBar.setVisibility(View.GONE);
                progressBarText.setVisibility(View.VISIBLE);
                progressBarText.setText("No Employee Found");
                recyclerView.setVisibility(View.GONE);

            }
        });

        requestQueue.add(request);
    }


}
