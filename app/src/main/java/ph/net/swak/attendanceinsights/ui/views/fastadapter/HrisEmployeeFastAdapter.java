package ph.net.swak.attendanceinsights.ui.views.fastadapter;

import android.app.Dialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.events.UserDetailEvent;
import ph.net.swak.attendanceinsights.utils.Helper;

public class HrisEmployeeFastAdapter extends AbstractItem<HrisEmployeeFastAdapter, HrisEmployeeFastAdapter.ViewHolder> {

    private int empId;
    private String fName;
    private String mName;
    private String lName;
    private String designation;
    private Dialog dialog;

    public int getEmpId() {
        return empId;
    }

    public HrisEmployeeFastAdapter withEmpId(int empId) {
        this.empId = empId;
        return this;
    }

    public String getfName() {
        return fName;
    }

    public HrisEmployeeFastAdapter withfName(String fName) {
        this.fName = fName;
        return this;
    }

    public String getmName() {
        return mName;
    }

    public HrisEmployeeFastAdapter withmName(String mName) {
        this.mName = mName;
        return this;
    }

    public String getlName() {
        return lName;
    }

    public HrisEmployeeFastAdapter withlName(String lName) {
        this.lName = lName;
        return this;
    }

    public String getDesignation() {
        return designation;
    }

    public HrisEmployeeFastAdapter withDesignation(String designation) {
        this.designation = designation;
        return this;
    }

    public Dialog getDialog() {
        return dialog;
    }

    public HrisEmployeeFastAdapter withDialog(Dialog dialog) {
        this.dialog = dialog;
        return this;
    }

    @Override
    public HrisEmployeeFastAdapter.ViewHolder getViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public int getType() {
        return R.id.fastadapter_hris_employee_bottom_sheet_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.item_hris_employee_bottom_sheet;
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);

        holder.tvName.setText(String.format(Locale.ENGLISH, "%s, %s %s", Helper.toTitleCase(getlName()), Helper.toTitleCase(getfName()), getmName().substring(0, 1)));
        holder.tvDesignation.setText(Helper.toTitleCase(getDesignation()));
        holder.itemView.setOnClickListener(v -> {
            getDialog().dismiss();
            EventBus.getDefault().post(new UserDetailEvent(String.valueOf(getEmpId())));
        });
    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);

        holder.tvName.setText(null);
        holder.tvDesignation.setText(null);
        holder.itemView.setOnClickListener(null);
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageview_hris_employee_bottom_sheet)
        protected ImageView imageView;

        @BindView(R.id.textview_hris_employee_bottom_sheet_name)
        protected TextView tvName;

        @BindView(R.id.textview_hris_employee_bottom_sheet_designation)
        protected TextView tvDesignation;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
