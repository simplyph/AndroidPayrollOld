package ph.net.swak.attendanceinsights.ui.fragments.hris;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import ph.net.swak.attendanceinsights.R;
import ph.net.swak.attendanceinsights.models.RealmEmployeeProfile;
import ph.net.swak.attendanceinsights.network.VolleyAPI;
import ph.net.swak.attendanceinsights.network.VolleyRequest;
import ph.net.swak.attendanceinsights.ui.activities.MainActivity;
import ph.net.swak.attendanceinsights.utils.JSONParser;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    @BindView(R.id.scrollview_profile)
    protected NestedScrollView scrollView;

    @BindView(R.id.progressbar_profile)
    protected MaterialProgressBar progressBar;

    @BindView(R.id.progressbar_profile_text)
    protected TextView progressBarText;

    private Unbinder unbinder;
    private MainActivity mainActivity;
    private RequestQueue requestQueue;
    private VolleyRequest volleyRequest;
    private final String TAG = "ProfileFragment";

    public ProfileFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        requestQueue = VolleyAPI.getInstance(getActivity()).getRequestQueue();
        volleyRequest = new VolleyRequest();
        mainActivity = (MainActivity) getActivity();

        Bundle bundle = getArguments();

        if (bundle != null) {
            if (bundle.containsKey("empid")) {
                setupData(bundle.getString("empid"));
            }
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(requestQueue != null)
            requestQueue.cancelAll(TAG);
    }

    @BindView(R.id.imageview_profile_name)
    protected ImageView imageView;

    @BindView(R.id.textview_profile_empid_content)
    protected TextView textViewEmpId;

    @BindView(R.id.textview_profile_name_content)
    protected TextView textViewName;

    @BindView(R.id.textview_profile_name_label)
    protected TextView textViewPosition;

    @BindView(R.id.textview_profile_status_content)
    protected TextView textViewStatus;

    @BindView(R.id.textview_profile_hired_content)
    protected TextView textViewHired;

    @BindView(R.id.textview_profile_terminated_content)
    protected TextView textViewTerminated;

    @BindView(R.id.relativelayout_profile_terminated_content)
    protected RelativeLayout relativeLayoutTerminated;

    @BindView(R.id.textview_profile_regularization_content)
    protected TextView textViewRegularization;

    @BindView(R.id.relativelayout_profile_regularization_content)
    protected RelativeLayout relativeLayoutRegularization;

    @BindView(R.id.textview_profile_company_content)
    protected TextView textViewCompany;

    @BindView(R.id.textview_profile_division_content)
    protected TextView textViewDivision;

    @BindView(R.id.textview_profile_dept_content)
    protected TextView textViewDepartment;

    @BindView(R.id.textview_profile_function_content)
    protected TextView textViewFunction;

    @BindView(R.id.textview_profile_workbase_content)
    protected TextView textViewWorkbase;

    @BindView(R.id.textview_profile_desig_content)
    protected TextView textViewDesig;

    @BindView(R.id.textview_profile_religion_content)
    protected TextView textViewReligion;

    @BindView(R.id.textview_profile_birthdate_content)
    protected TextView textViewBirthdate;

    @BindView(R.id.textview_profile_civil_content)
    protected TextView textViewCivil;

    @BindView(R.id.textview_profile_gender_content)
    protected TextView textViewGender;

    private void setupData(String empid) {
        progressBar.setVisibility(View.VISIBLE);
        progressBarText.setVisibility(View.VISIBLE);
        scrollView.setVisibility(View.GONE);
        progressBarText.setText("Loading Data");

        JsonArrayRequest jsonArrayRequest = volleyRequest.getAuthJsonArray(mainActivity.getApplicationContext(), TAG, volleyRequest.getEmployeeProfile(empid), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                List<RealmEmployeeProfile> realmEmployeeProfiles = JSONParser.parseEmployeeProfile(response);

                RealmEmployeeProfile profile = realmEmployeeProfiles.get(0);

                textViewEmpId.setText(String.valueOf(profile.getEmpId()));
                textViewName.setText(profile.getFname() + " " + profile.getLname());
                textViewPosition.setText(profile.getPosition());
                textViewStatus.setText(profile.getStatus());
                textViewHired.setText(profile.getHired());

                if(profile.getStatusType().equalsIgnoreCase("active") && profile.getStatus().equalsIgnoreCase("regular")) {
                    relativeLayoutTerminated.setVisibility(View.GONE);
                    relativeLayoutRegularization.setVisibility(View.VISIBLE);
                    textViewRegularization.setText(profile.getRegularization());
                } else if(profile.getStatusType().equalsIgnoreCase("inactive") &&
                    (profile.getStatus().equalsIgnoreCase("resigned") || profile.getStatus().equalsIgnoreCase("awol") ||
                            profile.getStatus().equalsIgnoreCase("terminated") || profile.getStatus().equalsIgnoreCase("end of contract"))) {
                        relativeLayoutRegularization.setVisibility(View.GONE);
                        relativeLayoutTerminated.setVisibility(View.VISIBLE);
                        textViewTerminated.setText(profile.getTerminated());
                } else {
                    relativeLayoutTerminated.setVisibility(View.GONE);
                    relativeLayoutRegularization.setVisibility(View.GONE);
                }

                textViewCompany.setText(profile.getCompName());
                textViewDivision.setText(profile.getDivName());
                textViewDepartment.setText(profile.getDeptName());
                textViewFunction.setText(profile.getFuncName());
                textViewWorkbase.setText(profile.getWbaseName());
                textViewDesig.setText(profile.getDesigName());
                textViewReligion.setText(profile.getReligionName());
                textViewBirthdate.setText(profile.getBirthdate());
                textViewCivil.setText(profile.getCivilStatus());
                textViewGender.setText(profile.getGender());

                RequestOptions options = new RequestOptions().centerCrop();

                Glide.with(mainActivity).load(VolleyRequest.URL_BASE + profile.getImgPath()).apply(options).into(imageView);

                progressBar.setVisibility(View.GONE);
                progressBarText.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                progressBarText.setVisibility(View.VISIBLE);
                scrollView.setVisibility(View.GONE);
                progressBarText.setText("No data Found");
            }
        });

        requestQueue.add(jsonArrayRequest);

    }
}
