package ph.net.swak.attendanceinsights.utils;

import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;

import java.text.DecimalFormat;

import ph.net.swak.attendanceinsights.R;

/**
 * Created by Knowell on 1/16/2017.
 */

public class BarMarkerView extends MarkerView {

    private TextView marker_view_tv;

    /**
     * Constructor. Sets up the MarkerView with a custom layout resource.
     *
     * @param context
     * @param layoutResource the layout resource to use for the MarkerView
     */
    public BarMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);

        marker_view_tv = (TextView) findViewById(R.id.marker_view_tv);
    }

    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        DecimalFormat mFormat = new DecimalFormat("###E00");
        String[] SUFFIX = new String[]{
                "", "K", " M", " B", " T"
        };
        int MAX_LENGTH = 5;

        String r = mFormat.format(e.getY());

        int numericValue1 = Character.getNumericValue(r.charAt(r.length() - 1));
        int numericValue2 = Character.getNumericValue(r.charAt(r.length() - 2));
        int combined = Integer.valueOf(numericValue2 + "" + numericValue1);

        r = r.replaceAll("E[0-9][0-9]", SUFFIX[combined / 3]);

        while (r.length() > MAX_LENGTH || r.matches("[0-9]+\\.[a-z]")) {
            r = r.substring(0, r.length() - 2) + r.substring(r.length() - 1);
        }

        marker_view_tv.setText(r);

        super.refreshContent(e, highlight);
    }

    private MPPointF mOffset;

    @Override
    public MPPointF getOffset() {

        if (mOffset == null) {
            mOffset = new MPPointF(-(getWidth() / 2), -getHeight());
        }

        return mOffset;
    }
}
