package ph.net.swak.attendanceinsights.models;

import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;


public class RealmAttParticular extends RealmObject {

    private double result;

    public double getResult() {
        return result;
    }

    public RealmAttParticular withResult(double result) {
        this.result = result;
        return this;
    }

    private String fname;

    public String getFname() {
        return fname;
    }

    public RealmAttParticular withFname(String fname) {
        this.fname = fname;
        return this;
    }

    private String lname;

    public String getLname() {
        return lname;
    }

    public RealmAttParticular withLname(String lname) {
        this.lname = lname;
        return this;
    }

    private String empId;

    public String getEmpId() {
        return empId;
    }

    public RealmAttParticular withEmpId(String empId) {
        this.empId = empId;
        return this;
    }

    private String detail;

    public String getDetail() {
        return detail;
    }

    public RealmAttParticular withDetail(String detail) {
        this.detail = detail;
        return this;
    }

    private String imgPath;

    public String getImgPath() {
        return imgPath;
    }

    public RealmAttParticular withImgPath(String imgPath) {
        this.imgPath = imgPath;
        return this;
    }

    @Ignore
    private List<Long> selectedComp;

    public RealmAttParticular withSelectedComp(long compid) {
        this.selectedComp.add(compid);
        return this;
    }

    public List<Long> getSelectedComp() {
        return selectedComp;
    }

    @PrimaryKey
    protected long mIdentifier = -1;

    public RealmAttParticular withIdentifier(long identifier) {
        this.mIdentifier = identifier;
        return this;
    }

    public long getIdentifier() {
        return mIdentifier;
    }


}
